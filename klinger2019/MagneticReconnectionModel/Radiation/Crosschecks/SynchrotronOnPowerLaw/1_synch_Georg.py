import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb

from Radiation_simps import Radiation
import setFigureConfig as sfc
import coloring as cl

'''





VERY IMPORTANT:

SET Ly_esc = 1/s






'''

# properties of system
L_system = 5*10**16  # cm
B = 1  # G
# power law injection for source term
inj_N0 = 1e3  # norm
inj_si = -2  # spectral index
inj_min = 1e1
inj_max = 1e4
# Black Body
l_BB = 1  # norm
Theta_BB = 1e-5  # temperature (kT/mc^2)

r = Radiation(
    L=L_system, B=B, UinjUB=1,
    inj_N0=inj_N0, inj_min=inj_min, inj_max=inj_max, inj_si=inj_si,
    Theta_BB=Theta_BB, L_BLR=1, R_BLR=1, z_diss=2,
    yEscFactor=1
)

# set grid
r.setLinTime(dt=20./299/5, saveEvery=1)
r.setLogElectronEnergy(eexpmin=0, eexpmax=6, NeE=200)
r.setLogPhotonEnergy(yexpmin=-15, yexpmax=8, NyE=401)

# set initial definitions
# initial electron term
pivot = 1
qe = inj_N0 * (r.eE/pivot)**inj_si
eInitial = qe * ((r.eE > inj_min) & (r.eE < inj_max))
r.setInitalElectronDistribution(Ninitial=eInitial)
# initial photon term
yInitial = 0 * r.yE
r.setInitalPhotonDistribution(Ninitial=yInitial)

'''
commented = turned on
uncommented = turned off
'''
# escaqpe term has to be set to 1/s not 2/s
r.turnOff(
    [
        "Qe_injection",
        "Qe_yy",
        "Le_sync",
        "Le_icT",
        "Le_icKN",
        # "Ly_esc",
        "Ly_yy",
        # "Ly_ssa",
        # "Qy_sync",
        "Qy_icT",
        "Qy_icKN",
        "BB",
        "SSC"
    ]
)

# Size of the system: for all times L_system
T = 20
one = np.ones(int(T / r.dt) * 2)
zero = np.zeros(int(T / r.dt) * 2)
r.solveUntil(T=T, sArray=one, betaArray=zero, dopplerBBArray=one,
             printEvery=10)

# r.saveToh5("efixed_esc+ssa+synch.h5")

# plt.show(r.plotEvolution())

sfc.fullWidth(aspectRatio=0.5)
fig_dif, (ax, axcb) = plt.subplots(
    ncols=2, gridspec_kw={"width_ratios": [40, 1]}, constrained_layout=True
)
cmap = plt.get_cmap("viridis_r")
cmap = cl.c_time(reverse=True)
cy = cmap(np.linspace(0, 1, 401))

# reference line
ax.axhline(y=1, color="grey", linestyle=":")

# electron plot
ax.set_prop_cycle('color', cy)

# Georg Schwefer
pathToGeorg = "../GeorgSchwefer/comparison/"
x_G = np.load(pathToGeorg + "1_Synch/1_Synch_x.npy")
y_G = np.load(pathToGeorg + "1_Synch/1_Synch_y.npy")


dy = r.Ny_all[::5]/y_G

for dyi in dy:
    ax.plot(x_G, dyi)   

ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$N_{\gamma} / N_{\gamma, GS}$")
ax.set_xscale("log")
ax.set_xlim(3.6*10**-12, 2*10**-6)
ax.set_ylim(0.8, 1.2)
# ax.set_yscale("log")

# colorbar
t = r.t
cb2 = cb.ColorbarBase(
    axcb, cmap=cmap,
    boundaries=t,
    ticks=np.linspace(min(t), max(t), 5),
    orientation='vertical',
    spacing="proportional"
)
cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=10)
fig_dif.savefig("1_synch_Georg.pdf")
# plt.show(fig_dif)
