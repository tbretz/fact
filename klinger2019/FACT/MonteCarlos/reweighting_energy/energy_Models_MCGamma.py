from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
import setFigureConfig as sfc

MCGammaFile = "../data/MC_Gamma.root"
dp = DataProcessor(data=dFrameFromRoot(MCGammaFile, keys=["Result"]))


# log energy
dp.calcLogs(["Energy"])

# in model
EnergyModelIn = Model.fromFile("EnergyModelIn_OriginalMC.p")
# create histogram of lgE called lgE_hist
EHist = dp.getHistogram("lgE_Hist", "logEnergy")
EnergyModelIn.setNormFromData(EHist)


# out model
si = -2.5  # spectral index to reweight to
EnergyModelOut = Model("expo10")
EnergyModelOut.getParametersFromArray(
    [1e8, si + 1], EnergyModelIn.getNorm(),
    min(dp.getData("logEnergy")), max(dp.getData("logEnergy"))
)

EnergyModelIn.save("EnergyModelIn_MCGamma.p")
EnergyModelOut.save("EnergyModelOut_MCGamma_%g.p" % (si))
print("finished in Model")


# calc histograms for control plot
dp.updateModels(EnergyModelIn, EnergyModelOut)
dp.addWeights("logEnergy", name="wE")

hm = HistogramManager()
EHist = dp.getHistogram(
    "lgE_Hist", "logEnergy", useWeights=None, nBins=64
)
hm.addHistogram("before_MCGamma", EHist)
EHist_weighted = dp.getHistogram(
    "lgE_Hist", "logEnergy", useWeights="all", nBins=64
)
hm.addHistogram("after_MCGamma", EHist_weighted)
hm.saveHistogramDict("MC_Gamma_reweightingEnergy_%g.p" % (si))

# create control plot
sfc.halfWidth(aspectRatio=0.9)
fig = hm.plotHistograms(
    ["before_MCGamma", "after_MCGamma"], ["initial", "reweighted"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"]
)
ax = fig.gca()
ax.set_ylabel(
    r"$\frac{\mathrm{d}N}{\mathrm{d} \log_{10} E/GeV}$",
    # rotation=90, labelpad=10
)
fig.tight_layout()
fig.savefig("MC_Gamma_reweightingEnergy_%g.pdf" % (si))
