import numpy as np
import pandas as pd


def calcExcessEvents(NoSig, NoBg, NoBgWobble=5):
    return NoSig - NoBg/NoBgWobble


calcExcessEvents = np.vectorize(calcExcessEvents)


def calcExcessError(NoSig, NoBg, NoBgWobble=5):
    alpha = 1./NoBgWobble
    return np.sqrt(NoSig + alpha**2*NoBg)


calcExcessError = np.vectorize(calcExcessError)


def timeWeightedMean(x, sigma, effTime):
    x, sigma, effTime = np.array(x), np.array(sigma), np.array(effTime)
    mean = sum(x*effTime)/sum(effTime)
    std = sum((sigma*effTime)**2)**(1/2)/sum(effTime)
    return mean, std


def excessPerBinUncorrected(binnedDf, grpRuns):

    binnedDf["N_S"] = grpRuns["fNumSigEvts"].apply(lambda group: np.sum(group))

    binnedDf["N_B"] = grpRuns["fNumBgEvts"].apply(lambda group: np.sum(group))
    binnedDf["t_eff"] = grpRuns["fEffectiveTime_h"].apply(
        lambda group: np.sum(group)
    )

    binnedDf["N_E"] = calcExcessEvents(binnedDf["N_S"], binnedDf["N_B"])
    binnedDf["sigma_E"] = calcExcessError(binnedDf["N_S"], binnedDf["N_B"])
    binnedDf["RExc"] = binnedDf["N_E"]/binnedDf["t_eff"]
    binnedDf["RExcErr"] = binnedDf["sigma_E"]/binnedDf["t_eff"]

    return binnedDf


def excessPerBinCorrected(binnedDf, grpRuns, binningColumn="fNight"):

    twMean = grpRuns.apply(
        lambda group: timeWeightedMean(
            group["rExcCorr"], group["rExcErrCorr"], group["fEffectiveTime_h"]
        )
    ).apply(pd.Series, index=["RExcCorr", "RExcErrCorr"])

    binnedDf = binnedDf.merge(
        twMean, how="outer", left_on=binningColumn, right_on=binningColumn
    )

    return binnedDf
