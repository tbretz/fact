import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns

import setFigureConfig as sfc
from HistogramManager import HistogramManager

EBins = 64
IBins = 54
ZdBins = 60
si = -2.5

hm = HistogramManager()
hm.loadHistogramDict("../data/EventsMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)

cp = sns.color_palette()

# efficiencies (as in thesis)
sfc.fullWidth(aspectRatio=0.5, alpha=0.8)
fig, axe = plt.subplots()
axe.set_prop_cycle("color", [cp[3], cp[0]])

ylim = [0, 1]
# E
axe = hm.plotEfficiencies(
    ["lgE_Q_tsq", "lgE_S", "lgE_L"],
    cuts=[r"$\Theta^2$-cut", r"spectrum", "light curve"],
    mode="lgEnergy", legloc=[None, "center right"], style="stairs",
    h0Name=r"$\Theta^2$-cut", ax=axe, yscale="linear", ylim=ylim
)
axe.set_xticks(np.linspace(2.5, 4.5, 5))
# Zd
divider = make_axes_locatable(axe)
axz = divider.append_axes("right", size="100%", pad=0.0)
axz.set_prop_cycle("color", [cp[3], cp[0]])
axe.get_legend().remove()
axe.grid()
axz.grid()
axz = hm.plotEfficiencies(
    ["Zd_Q_tsq", "Zd_S", "Zd_L"],
    cuts=[r"$\Theta^2$-cut", r"spectrum", "light curve"],
    mode="Zd", legloc=[None, "lower center"], style="stairs",
    h0Name=r"$\Theta^2$-cut", ax=axz, yscale="linear", ylim=ylim
)
axz.set_xticks(np.linspace(0, 60, 7))
axz.set_xticklabels([str(int(xt)) + r"°" for xt in axz.get_xticks()])
axz.set_xlabel(r"$Zd$")
axz.set_ylabel("")
axz.set_yticklabels("")
for tic in axz.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False
axi = divider.append_axes("right", size="100%", pad=0.0)
axi.set_prop_cycle("color", [cp[3], cp[0]])
# impact has to be trewated extra, because last bin is empty (0/0)
# axi = hm.plotEfficiencies(
#     ["Impact_Q_tsq", "Impact_S", "Impact_L"],
#     cuts=[r"$\Theta^2$-cut", r"spectrum", "light curve"],
#     mode="Impact", legloc=[None, "center left"], style="stairs",
#     h0Name=r"$\Theta^2$-cut", ax=axi, yscale="linear", ylim=ylim
# )
h0 = hm.get("Impact_Q_tsq")
hS = hm.get("Impact_S")
(effs, sigLows, sigUps), effCenters = hm.calcEfficiency(h0, hS)
x = np.reshape(
    h0.bins[:, None] * np.ones(2), (2*len(h0.bins))
)[1:-3]
y = np.reshape(
    effs[:, None] * np.ones(2), (2*len(effs))
)

axi.plot(
    x, y, label=r"spectrum", color=cp[3]
)
axi.errorbar(effCenters, effs, yerr=[sigLows, sigUps],
             linestyle="", ecolor=cp[3], label=None)

hL = hm.get("Impact_L")
(effs, sigLows, sigUps), effCenters = hm.calcEfficiency(h0, hL)
x = np.reshape(
    h0.bins[:, None] * np.ones(2), (2*len(h0.bins))
)[1:-3]
y = np.reshape(
    effs[:, None] * np.ones(2), (2*len(effs))
)

axi.plot(
    x, y, label=r"light curve", color=cp[0]
)
axi.errorbar(effCenters, effs, yerr=[sigLows, sigUps],
             linestyle="", ecolor=cp[0], label=None)

axi.set_ylim(ylim[0], ylim[1])
axi.grid()
axi.set_ylabel("")
axi.set_yticklabels("")
for tic in axi.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False
# axi.get_legend().remove()
axi.set_xlabel("Impact $I$ [m]")
axi.set_xticks(np.linspace(0, 54000, 5))
axi.set_xticklabels([str(int(1e-2*xt)) for xt in axi.get_xticks()])
axe.set_ylabel("cut efficiency")
fig.tight_layout()
fig.savefig("SizeAreaCut_efficiencies.pdf")
