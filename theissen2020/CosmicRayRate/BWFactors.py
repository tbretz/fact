import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import setFigureConfig as figConfig


runsCR = pd.read_hdf(
    "./Performance/AllData/CRRateWithBW.h5", key="CRRateWithBW"
)
runsCR["OldBWF"] = runsCR["fR750Cor"]/runsCR["fR750Ref"]

goodRunsCR = runsCR[runsCR["BWF"] >= 0.9]
goodRunsCR = goodRunsCR[goodRunsCR["BWF"] <= 1.1]
goodRunsCR.to_hdf("./CRSelectedRuns.h5", "CRSelectedRuns")

runsCR = runsCR[runsCR["fNight"] > 20140101]
goodRunsCR = goodRunsCR[goodRunsCR["fNight"] > 20140101]

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["BWF"], bins=np.linspace(0.4, 1.6, num=200), alpha=0.9,
    color="tab:blue", zorder=1.2, label=None
)
ax.axvline(x=0.9, color="tab:red", zorder=1.3, lw=1.5)
ax.axvline(
    x=1.1, color="tab:red", zorder=1.3, lw=1.5, label=r"$0.9\,<\,BWF\,<\,1.1$"
)
ax.axvspan(xmin=0, xmax=0.9, color="tab:purple", zorder=1.1, lw=2, alpha=0.3)
ax.axvspan(
    xmin=1.1, xmax=1.6, color="tab:purple", zorder=1.1, lw=2, alpha=0.3,
    label="rejected "+str(round(100-len(goodRunsCR)*100/len(runsCR), 2))+"%"
)
ax.axvspan(
    xmin=0.9, xmax=1.1, color="tab:green", zorder=1.1, lw=2, alpha=0.3,
    label="selected "+str(round(len(goodRunsCR)*100/len(runsCR), 2))+"%"
)
ax.set_xlim(0.4, 1.4)
ax.set_xlabel("bad-weather-factor "+r"$BWF=R_{\mathrm{CR}}/R_{\mathrm{Ref}}$")
ax.set_ylabel("number of runs per bin")
ax.legend()
ax.grid(axis="y", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./BWFDistribution.png", dpi=300)
fig.savefig("./BWFDistribution.pdf")

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["RCR"], bins=np.linspace(0, 400, num=401),
    alpha=1, color="tab:purple",
    label="before correction", zorder=1.1
)

ax.hist(
    goodRunsCR["RCR_corr"], bins=np.linspace(0, 400, num=401), alpha=0.7,
    color="tab:blue", label="after correction\nand BWF cut", zorder=1.2
)
ax.set_ylabel("number of runs per bin")
ax.set_xlabel(r"$R_{CR}$ in events/min")
ax.legend()
ax.set_ylim(0, 2200)
ax.set_xlim(50, 350)
ax.grid(axis="both", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./BWFCRDistribution.png", dpi=300)
fig.savefig("./BWFCRDistribution.pdf")
