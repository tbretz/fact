#!/usr/bin/env python3

#import numpy as np
import autograd.numpy as np
import matplotlib.pyplot as plt
#from iminuit import Minuit
from autograd import elementwise_grad as grad
from scipy.optimize import leastsq 

import seaborn as sns
sns.set_style('ticks')

true_vals = [3, 2, 1, 3]


def wave(x, p):
    return p[0]*np.sin(p[1]*x + p[2]) + p[3]

x = np.linspace(0, 10, 30)
y = wave(x, true_vals)

x_err = 0.2*abs(np.random.randn(len(x)))+0.1
y_err = 0.2*abs(np.random.randn(len(x)))+0.1

x_fit = np.array([x[i]+x_err[i]*np.random.randn() for i in range(len(x))])
y_fit = np.array([y[i]+y_err[i]*np.random.randn() for i in range(len(x))])

x_plot = np.linspace(0, 10, 300)
y_plot = wave(x_plot, true_vals)


def pull(p):
    return (y_fit - wave(x_fit, p))/np.sqrt(y_err**2 + grad(lambda x: wave(x, p))(x_fit)**2*x_err**2)

lsq = leastsq(pull, [2, 2, 2, 2], full_output=1)
print('chi^2 / ndf = %f / %i' % ((pull(lsq[0])**2).sum(), len(x)-4))
#plt.plot(x, y, 'o')
#plt.plot(x_plot, y_plot)
plt.errorbar(x_fit, y_fit, yerr=y_err, xerr=x_err, fmt='.')
plt.plot(x_plot, wave(x_plot, lsq[0]))
plt.show()