import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from rootRoutines import dFrameFromRoot
from DataProcessor import DataProcessor
import setFigureConfig as sfc

# load root file
OriginalMCFile = "../data/Original_Zd_FileId.root"
dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))

dp.data["Zd"] = np.degrees(dp.data["Theta"])
Zd_mins = dp.data.groupby("FileId")["Zd"].min()
Zd_maxs = dp.data.groupby("FileId")["Zd"].max()
Ids = np.unique(dp.data["FileId"])

# create figure
sfc.fullWidth()
fig, ax = plt.subplots()
ms = 2  # markersize

# mark simulated sets in left plot
ax.axvspan(10910, 12909, color="grey", alpha=0.5)
ax.text(11900, 50, "Gustav", rotation=90, horizontalalignment='center',
        verticalalignment='center')
ax.axvspan(12910, 14909, color="grey", alpha=0.5)
ax.text(13900, 50, "Werner", rotation=90, horizontalalignment='center',
        verticalalignment='center')

ax.plot(Ids, Zd_maxs, linestyle="", marker="v", label="max Zd", ms=ms)
ax.plot(Ids, Zd_mins, linestyle="", marker="^", label="min Zd", ms=ms)
ax.set_xlim(1e4, 1.59e4)

# add second plot on the right
divider = make_axes_locatable(ax)
ax2 = divider.append_axes("right", size="150%", pad=0.1)
ax2.set_yticklabels("")
for tic in ax2.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False

# mark simulated sets in left plot
ax2.axvspan(79734, 80733, color="grey", alpha=0.5)
ax2.text(80300, 17, "Bernd", rotation=90, horizontalalignment='center',
         verticalalignment='bottom')
ax2.axvspan(80765, 81764, color="grey", alpha=0.5)
ax2.text(81300, 17, "Kai", rotation=90, horizontalalignment='center',
         verticalalignment='bottom')
ax2.axvspan(81765, 83764, color="grey", alpha=0.5)
ax2.text(82765, 17, "Martin", rotation=90, horizontalalignment='center',
         verticalalignment='bottom')

ax2.axvspan(83765, 89764, color="grey", alpha=0.5)
ax2.text(88265, 7, "Schantalle", rotation=90, horizontalalignment='center',
         verticalalignment='bottom')

ax2.axvspan(89765, 95764, color="grey", alpha=0.5)
ax2.text(92765, 7, "Jackeline", rotation=90, horizontalalignment='center',
         verticalalignment='bottom')


ax2.plot(Ids, Zd_maxs, linestyle="", marker="v", label="max Zd", ms=ms)
ax2.plot(Ids, Zd_mins, linestyle="", marker="^", label="min Zd", ms=ms)
ax2.set_xlim(7.8e4, 9.7e4)
ax2.set_xticks([8e4, 8.5e4, 9e4, 9.5e4])
ax2.legend(loc="lower left")

''' Lines to show where runs at each Zd bin end and begin '''
# for i in np.arange(61):
#     ax.axhline(y=i, color="grey", zorder=0)
# ax.axhline(y=30, color="k")
# ax.axhline(y=60, color="k")

# for i in np.arange(10910, 12910, 67):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(12910, 14910, 67):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(79734, 80733, 34):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(80765, 81764, 34):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(81765, 83764, 67):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(83765, 87764, 134):
#     ax.axvline(x=i, color="grey")
# for i in np.arange(87765, 89764, 67):
#     ax.axvline(x=i, color="grey")

# for i in np.arange(89765, 95764, 201):
#     ax.axvline(x=i, color="grey")


ax2.set_xlabel("FileId")
ax.set_ylabel("Zd")
ax.set_yticklabels([str(int(yt)) + r"°" for yt in ax.get_yticks()])
fig.savefig("MCOirignal_FileIds.pdf")
plt.show()
