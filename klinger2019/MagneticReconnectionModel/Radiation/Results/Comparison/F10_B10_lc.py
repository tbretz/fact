import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u

import setFigureConfig as sfc

pid = 105
tf, lcf = np.load("../F10/F10_4/RadData/F10_4_sigma10_" + str(pid) + "_lc.npy")
tb, lcb = np.load("../B10/B10_4/RadData/B10_4_sigma10_" + str(pid) + "_lc.npy")


loweV = 1e11
upeV = 1e14
unitt = "h"
unitE = "TeV"
# convert units
tf_unit = tf * (u.s).to(unitt)
tb_unit = tb * (u.s).to(unitt)
lo = loweV * (u.eV).to(unitE)
up = upeV * (u.eV).to(unitE)

# plot
sfc.fullWidth(aspectRatio=0.5)
fig, ax = plt.subplots()
ax.set_title(r"$%g - %g$" % (lo, up) + unitE + r" (VHE $\gamma$)")
ax.plot(tf_unit - tf_unit[0], lcf, color="tab:blue", label="FSRQ")
ax.plot(tb_unit - tb_unit[0], lcb, color="tab:red", label="BL Lac")
ax.set_xlabel("t [" + unitt + "]")
ax.set_ylabel("L [erg/s]")
ax.legend()
ax.grid()
# ax.set_yscale("log")
fig.tight_layout()
fig.savefig("F10_B10_lc_" + str(pid) + ".pdf")