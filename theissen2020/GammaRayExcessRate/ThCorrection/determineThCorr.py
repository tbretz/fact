import emcee
import corner
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import pandas as pd
from scipy.optimize import minimize

import ExcessCalculation as ec
import setFigureConfig as figConfig
import correctionModels as cm
from DataFrameBinning import addLogBinning, addUniformBinning, addLogUniBinning
from LibFunctions import roundAndFollowDIN

# set random seed
np.random.seed(123)

# set flags to skip parts of the program
resample = False
loguni = False
recreateObsTimePlot = True
recreateCorrectionComparison = True
recreateCornerPlot = True
recreateDataSpacePlot = True
recreateCorrFactorsPlot = True
recreateSigmaCorrFactorsPlot = True

# load calibration set
calibrationCrabSample = ["../MergedExcessInfos/CrabExcess.h5", "CrabExcess"]
runsCR = pd.read_hdf(calibrationCrabSample[0], calibrationCrabSample[1])
print("\nCalibration Crab sample loaded from: "+calibrationCrabSample[0]+"\n")

# conservative cuts to avoid influence by Calima and theshold setting
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 30]
runsCR = runsCR[runsCR["fNight"] > 20140723]
runsCR = runsCR[runsCR["fThresholdMinSet"] > 290]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 880]

# total effective observation time
t_tot = np.sum(runsCR["fEffectiveTime_h"])
print(
    "Total effective observation time of the sample: " +
    str(round(t_tot, 2)) + "h\n"
)

# runwise performance correction

# - load results
perfCorrLocation = [
    "../CRCorrection/perfCorrectionResults.h5", "perfCorrectionResults"
]
perfCorrRes = pd.read_hdf(perfCorrLocation[0], perfCorrLocation[1])
print(
    "Performance correction results loaded from: " + perfCorrLocation[0]+"\n"
)
bestFit = perfCorrRes["mean"].values
p1 = bestFit[0]
p0 = bestFit[1]
rCrRef = 250.
print("\tModel: p0+p1*R_CR")
print("\tBest fit was: p0="+str(p0)+" p1="+str(p1))
print("\tCR ref rate: "+str(rCrRef)+" events/min\n")

# - apply correction
runsCR["C_perf"] = cm.perfCorrectionFactor(
    runsCR["RCR_corr"], p0, p1, rCrRef=rCrRef
)
runsCR["rExcCorr"] = runsCR["C_perf"]*runsCR["rExc"]
runsCR["rExcErrCorr"] = runsCR["C_perf"]*runsCR["rExcErr"]

# choose binning and group
if loguni:
    nLogBins = 24
    nUniBins = 24
    runsCR, logEdges, logDropped = addLogBinning(
        runsCR, "fThresholdMinSet", "ThLog", nLogBins, minVal=290
    )
    # print(logEdges)
    runsCR, uniEdges, uniDropped = addUniformBinning(
        runsCR, "fThresholdMinSet", "ThUni", nUniBins,
    )
    # print(uniEdges)
    runsCR, binEdges, dropped = addLogUniBinning(
        runsCR, "fThresholdMinSet", "ThBin", logEdges, uniEdges
    )
else:
    runsCR, binEdges, dropped = addLogBinning(
        runsCR, "fThresholdMinSet", "ThBin", 15
    )

grpRuns = runsCR.groupby("ThBin")

# rebinning
binnedDf = pd.DataFrame()
binnedDf["N"] = grpRuns.apply(lambda group: len(group))

# - find mean within Zd bins
binnedDf["ThMean"] = grpRuns["fThresholdMinSet"].apply(
    lambda group: np.mean(group)
)

# - calculate the excess for every bin with two methods

# -- method (i): ignoring the performance differences and calculate binwise
binnedDf = ec.excessPerBinUncorrected(binnedDf, grpRuns)

# -- method (ii): including the performance correction and calculate runwise
binnedDf = ec.excessPerBinCorrected(binnedDf, grpRuns, binningColumn="ThBin")

# - reset index to let CRBin become a column
binnedDf = binnedDf.reset_index()

# - reject outliers that are not contained in the binned parameter space
binnedDf = binnedDf[binnedDf["ThBin"] != -1]

# - add bin edges to data frame
binnedDf["BinStart"] = binEdges[:-1]
binnedDf["BinStop"] = binEdges[1:]
binnedDf["BinCenter"] = (binEdges[:-1]+binEdges[1:])/2
binnedDf["BinWidth"] = binnedDf["BinStop"]-binnedDf["BinStart"]

print(binnedDf)
# plot observation time distribution
if recreateObsTimePlot:

    t_tot = np.sum(binnedDf["t_eff"])
    print(
        "Total effective observation time after binning: " +
        str(round(t_tot, 2)) + "h\n"
    )
    highTh = runsCR[runsCR["fThresholdMinSet"] > 350]
    t_high = np.sum(highTh["fEffectiveTime_h"])

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.bar(
        binnedDf["BinCenter"], binnedDf["t_eff"],
        width=binnedDf["BinWidth"]*0.95,
        color="tab:blue", zorder=1.1,
        label="effective observation time:\n" +
        "   total:                 "+str(round(t_tot, 2))+"h\n" +
        "   "+r"$T>400$"+" DAC:     " +
        str(round(t_high, 2))+"h"
    )
    ax.set_xlabel("threshold "+r"$T$"+" in DAC counts")
    ax.set_ylabel("effective observation\ntime in h")
    ax.set_xlim(285, 900)
    ax.legend(handlelength=0, handletextpad=0)
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Th_ObsTime.pdf")
    fig.savefig("Th_ObsTime.png", dpi=300)

if recreateCorrectionComparison:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        np.log(binnedDf["ThMean"]), binnedDf["RExc"],
        yerr=binnedDf["RExcErr"],
        fmt="s", color="tab:orange", zorder=1.3,
        label="evaluated from event counts\ninside every bin"
    )
    ax.errorbar(
        np.log(binnedDf["ThMean"]), binnedDf["RExcCorr"],
        yerr=binnedDf["RExcErrCorr"],
        fmt="o", color="k", zorder=1.3,
        label="runwise evaluation corrected\nfor detector performance"
    )
    ax.legend(loc="lower left")
    ax.set_xticks(np.log([300, 400, 500, 600, 700, 800, 900]))
    ax.set_xticklabels(["300", "400", "500", "600", "700", "800", "900"])
    ax.set_xlabel("threshold "+r"$T$"+" in DAC counts")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_xlim(np.log(290), np.log(900))
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Th_CRCorrComp.pdf")
    fig.savefig("Th_CRCorrComp.png", dpi=300)

# Fitting the model to data


def logPrior(pars):
    r_th_min, th_mu, sigma = pars
    if (
        50 < r_th_min < 60 and
        300 < th_mu < 600 and
        1e-5 < sigma < 1e-2 and
        (1-cm.thresholdCorrectionFactor(290, th_mu, sigma))**2 < 0.1
    ):
        return 0.0
    return -np.inf


def nll(pars, x, y, yerr):
    r_th_min, th_mu, sigma = pars
    model = cm.thresholdModel(x, r_th_min, th_mu, sigma)
    yerr2 = yerr**2
    ll = -0.5*np.sum((y-model)**2/yerr2 + np.log(yerr2))
    return -ll


def logProbability(pars, x, y, yerr):
    lp = logPrior(pars)
    if not np.isfinite(lp):
        return -np.inf
    ll = - nll(pars, x, y, yerr)
    return lp + ll


labels = [r"$R(T^{\mathrm{min}})$", r"$T_{\mu}$", r"$\sigma$"]
labelsNoFormat = ["r_th_min", "th_mu", "sigma"]
initial = [55, 400, 1e-4]
soln = minimize(
    nll, initial,
    args=(
        binnedDf["ThMean"], binnedDf["RExcCorr"],
        binnedDf["RExcErrCorr"]
    )
)
r_th_min_ml, th_mu, sigma = soln.x

print("ML Results:")
print(r_th_min_ml, th_mu, sigma)
print()

pos = initial + 1e-4*np.random.randn(32, 3)
nwalkers, ndim = pos.shape

if resample and logPrior(initial) > -1:
    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, logProbability,
        args=(binnedDf["ThMean"], binnedDf["RExcCorr"],
              binnedDf["RExcErrCorr"])
    )
    sampler.run_mcmc(pos, 10000, progress=True)

    # check boost in phase and reduce samples accordingly
    tau = sampler.get_autocorr_time()
    print("boost-in phase information:")
    print(tau)
    print("\n")
    thinBy = int(np.mean(tau)/2.)
    discardIndex = int(4*np.mean(tau))
    flat_samples = sampler.get_chain(
        discard=discardIndex, thin=thinBy, flat=True
    )

    np.save("./flat_samples.npy", flat_samples)

    # Monitoring plot

    fig, axes = plt.subplots(len(initial), figsize=(10, 7), sharex=True)
    samples = sampler.get_chain()
    for i in range(ndim):
        ax = axes[i]
        ax.axvline(x=tau[i], color="tab:blue")
        ax.axvline(x=discardIndex, color="tab:purple")
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(labels[i])
        ax.yaxis.set_label_coords(-0.1, 0.5)

    axes[-1].set_xlabel("step number")
    fig.savefig("./samplerMonitor.png", dpi=300)
    fig.savefig("./samplerMonitor.pdf")

else:
    flat_samples = np.load("./flat_samples.npy")

# evaluation of the results:
resultDicts = []
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)
    resultDicts.append(
        {
            "key": labelsNoFormat[i],
            "mean": mcmc[1],
            "q16": mcmc[0],
            "q84": mcmc[2],
            "+": q[1], "-": q[0]
        }
    )
pRes = pd.DataFrame(resultDicts)
pRes.to_hdf("./ThresholdCorrectionResults.h5", "ThresholdCorrectionResults")
optvalues = pRes["mean"].values
inds = np.random.randint(len(flat_samples), size=100)
xvals = np.linspace(280, 900, num=200)

# print posterior results
print("posterior results dataframe:")
print(pRes)
print("\n")

# calculate cFactors
bestFitCorrFactor = cm.thresholdCorrectionFactor(
    xvals, optvalues[1], optvalues[2]
)
cFactorsOfSamples = []
for i in range(len(inds)):
    sample = flat_samples[inds[i]]
    for ind in inds:
        sample = flat_samples[ind]
        cFactorsSample = cm.thresholdCorrectionFactor(
            xvals, sample[1], sample[2]
        )
        cFactorsOfSamples.append(cFactorsSample)
cFactorsOfSamples = np.array(cFactorsOfSamples)

cFactorMeans = np.mean(cFactorsOfSamples, axis=0)
cFactorStds = np.std(cFactorsOfSamples, axis=0, ddof=1)
relCFactorStds = cFactorStds/cFactorMeans

# plot corner posterior probs
if recreateCornerPlot:
    figConfig.fullWidth()
    fig = corner.corner(
        flat_samples, labels=labels
    )
    ndim = np.shape(flat_samples)[1]
    axes = np.array(fig.axes).reshape((ndim, ndim))
    for j in range(ndim):
        ax = axes[j, j]
        parDf = pRes[pRes["key"] == labelsNoFormat[j]]
        mean = parDf["mean"].values[0]
        left = parDf["q16"].values[0]
        right = parDf["q84"].values[0]
        span = ax.axvspan(
            xmin=left, xmax=right, facecolor="tab:orange", alpha=0.5
        )
        ax.axvline(x=mean, color="tab:blue", lw=2)
        ax2 = ax.twiny()
        ax2.set_xlim(ax.get_xlim())
        ax2.set_xticks(ax.get_xticks())
        ax2.set_xticklabels([])
        rounded = roundAndFollowDIN(
            mean, [parDf["-"].values[0], parDf["+"].values[0]]
        )
        resultString = "{3}"+r"$= {0}_{{-{1}}}^{{+{2}}}$"
        resultString = resultString.format(
            str(rounded[0]), str(rounded[1][0]), str(rounded[1][1]), labels[j]
        )
        ax2.set_xlabel(resultString)

        for i in range(j):
            ax = axes[j, i]
            parDf2 = pRes[pRes["key"] == labelsNoFormat[i]]
            mean2 = parDf2["mean"].values[0]
            ax.axvline(x=mean2, color="tab:blue")
            ax.axhline(y=mean, color="tab:blue")
            ax.plot(mean2, mean, "s", color="tab:blue")

    legend = fig.legend(
        (
            mlines.Line2D(
                [0], [0], marker='s', color='tab:blue', linewidth=2,
                markerfacecolor="tab:blue", markersize=10
            ),
            span
        ),
        (
            "best fit as median of\nthe MC ensemble",
            "uncertainty estimation as\nquantile 16 % to 84 %"
        ),
        loc="upper right",
        framealpha=0.9, bbox_to_anchor=(0.98, 0.98),
        handlelength=3, fontsize=15
    )
    fig.savefig("Th_CornerPlot.png", dpi=300)
    fig.savefig("Th_CornerPlot.pdf")


# plot observation
if recreateDataSpacePlot:
    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        np.log(binnedDf["ThMean"]), binnedDf["RExcCorr"],
        yerr=binnedDf["RExcErrCorr"],
        fmt="o", color="k", zorder=1.3, label=None
    )
    ax.plot(
        np.log(xvals),
        cm.thresholdModel(
            xvals, optvalues[0], optvalues[1], optvalues[2]
        ),
        color="tab:blue",
        lw=3, zorder=1.2,
        label="\nbest fit values:\n" +
        " "+r"$R(T^{\mathrm{min}}) = (55.0 \pm 0.7)$"+"events/h\n" +
        " "+r"$T_{\mu}=(461_{-12}^{+18})$"+" DAC counts\n" +
        " "+r"$\sigma=0.005_{-0.004}^{+0.004}$"
    )
    firstflag = True
    for ind in inds:
        sample = flat_samples[ind]
        if firstflag:
            label = "100 random samples from\nthe MC ensemble"
            firstflag = False
        else:
            label = None
        ax.plot(
            np.log(xvals),
            cm.thresholdModel(xvals, sample[0], sample[1], sample[2]),
            color="tab:orange", alpha=0.1, zorder=1.1, label=label
        )
    ax.legend()
    legendHandels, legendLabels = ax.get_legend_handles_labels()
    legendHandels = [legendHandels[1], legendHandels[0]]
    legendLabels = [legendLabels[1], legendLabels[0]]
    leg = ax.legend(legendHandels, legendLabels)
    for lh in leg.legendHandles:
        lh.set_alpha(1)

    ax.set_xticks(np.log([300, 400, 500, 600, 700, 800, 900]))
    ax.set_xticklabels(["300", "400", "500", "600", "700", "800", "900"])
    ax.set_xlabel("threshold "+r"$T$"+" in DAC counts")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_xlim(np.log(280), np.log(900))
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Th_Dependence.pdf")
    fig.savefig("Th_Dependence.png", dpi=300)


if recreateCorrFactorsPlot:
    figConfig.halfWidth(aspectRatio=0.8)
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(
        np.log(xvals), bestFitCorrFactor, color="k",
        lw=2, zorder=4/100+1,
        label="best fit"
    )
    firstflag = True
    for i in range(len(inds)):
        if firstflag:
            label = "100 random samples from\nthe MC ensemble"
            firstflag = False
        else:
            label = None

        cFactors = cFactorsOfSamples[i]
        ax.plot(
            np.log(xvals),
            cFactors,
            lw=0.5,
            color="tab:orange", alpha=0.3, zorder=2/100.+1, label=label
        )
    ax.set_xticks(np.log([300, 400, 500, 600, 700, 800, 900]))
    ax.set_xticklabels(["300", "400", "500", "600", "700", "800", "900"])
    ax.set_xlabel("threshold "+r"$T$"+" in DAC counts")
    ax.set_ylim(0.9, 3.5)
    ax.set_xlim(np.log(280), np.log(900))
    ax.set_ylabel("correction factor"+r"$c_{T}$")
    ax.grid(alpha=0.5, axis="both")
    ax.set_axisbelow(True)
    leg = ax.legend()
    for lh in leg.legendHandles:
        lh.set_alpha(1)
    fig.tight_layout()
    fig.savefig("./Th_CorrFactors.png", dpi=300)
    fig.savefig("./Th_CorrFactors.pdf")

    if recreateSigmaCorrFactorsPlot:
        fig = plt.figure()
        ax = fig.gca()
        ax2 = ax.twinx()
        ax.plot(
            np.log(xvals), cFactorStds,
            color="tab:blue", lw=2,
            label="absolute uncertainty"
        )
        ax2.plot(
            np.log(xvals), relCFactorStds,
            color="tab:purple", lw=2, ls="-.",
            label="relative uncertainty"
        )

        ax.set_ylim(0, 0.15)
        ax2.set_ylim(0, 0.07)
        ax2.set_ylabel("relative uncertainty " +
                       r"$\sigma_{c_{T}}/c_{T}$")
        ax.set_ylabel("absolute uncertainty "+r"$\sigma_{c_{T}}$")

        ax.set_xticks(np.log([300, 400, 500, 600, 700, 800, 900]))
        ax.set_xticklabels(["300", "400", "500", "600", "700", "800", "900"])
        ax.set_xlabel("threshold "+r"$T$"+" in DAC counts")
        ax.set_xlim(np.log(280), np.log(900))
        ax.grid(alpha=0.5, axis="both", zorder=10)

        fig.legend(loc='upper left', bbox_to_anchor=(
            0.2, 0.94), handlelength=3)
        fig.tight_layout()
        fig.savefig("./Th_CorrUncertainty.png", dpi=300)
        fig.savefig("./Th_CorrUncertainty.pdf")
