import numpy as np
from scipy.integrate import simps, dblquad
from scipy.special import gamma, zeta
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import astropy.constants as c
import astropy.units as u

from Radiation_simps import Radiation, fitPowerLaw
import setFigureConfig as sfc
import coloring as cl

# properties of system
L_system = 5 * 1e16  # cm
B = 1  # G
# power law injection for source term
inj_N0 = 1e3  # norm
inj_si = -2  # spectral index
inj_min = 1
inj_max = 1e12
# Black Body
a = 4*c.sigma_sb/c.c
f = c.sigma_T * L_system * u.cm / c.m_e / c.c**2
tc = c.m_e * c.c**2 / c.k_B
Theta_BB = 1e-5  # temperature (kT/mc^2)
l_BB = (a * f * (tc*Theta_BB)**4).to("")
print(l_BB)
r = Radiation(
    L=L_system, B=B, UinjUB=1,
    inj_N0=inj_N0, inj_min=inj_min, inj_max=inj_max, inj_si=inj_si,
    Theta_BB=Theta_BB, L_BLR=1, R_BLR=1, z_diss=2,
    lBBto1=True, nIntegration=1000
)
r.l_BB = l_BB

# set grid
r.setLinTime(dt=1e-3, saveEvery=1)
r.setLogElectronEnergy(eexpmin=0, eexpmax=15, NeE=500)
r.setLogPhotonEnergy(yexpmin=-15, yexpmax=15, NyE=500)

# set initial definitions
# initial electron term
pivot = 1
# norm = (1 + inj_si) / (inj_max**(1+inj_si) - inj_min**(1+inj_si))
qe = inj_N0 * (r.eE/pivot)**inj_si
eInitial = qe * ((r.eE > inj_min) & (r.eE < inj_max))
r.setInitalElectronDistribution(Ninitial=eInitial)
# initial photon term
yInitial = 0 * r.yE
r.setInitalPhotonDistribution(Ninitial=yInitial)






def GB_Th(E, inj0=1e3, Theta=1e-5, p=2):
    factor = 3*np.pi
    consts = ((c.m_e * c.c / c.h)**3 * c.sigma_T.cgs * 5e16 * u.cm).to("")
    F = (
        2**(p+3) *
        (p**2 + 4*p + 11)/((p+3)**2*(p+1)*(p+5)) *
        gamma((p+5)/2) * zeta((p+5)/2)
    )
    print("F=", F)
    Th = Theta**((p+5)/2)
    return factor * consts * inj0 * F * Th * E**(-(p+1)/2)


def GB_KN(E, inj0=1e3, Theta=1e-5, p=2, withLog=1):
    factor = np.pi**3/2
    consts = ((c.m_e * c.c / c.h)**3 * c.sigma_T.cgs * 5e16 * u.cm).to("")
    if withLog:
        F = np.log(E*Theta) - 1.1472
    else:
        F = 1
    Th = Theta**2
    return factor * consts * inj0 * Th * E**(-(p+1)) * F



# figure to compare distributions
sfc.fullWidth(aspectRatio=1)
fig, axTop = plt.subplots()
divider = make_axes_locatable(axTop)
axd = divider.append_axes("bottom", size="35%", pad=0.7)

axTop.set_xscale("log")
axTop.set_yscale("log")
axTop.set_xlim(1e-5, 1e11)
axTop.set_ylim(1e5, 1e12)
axTop.grid()

ySlope = 2
if ySlope == 0:
    axTop.set_ylabel(r"$Q_{IC}$")
else:
    axTop.set_ylabel(r"$x^{%g} Q_{IC}$" % (ySlope))
axTop.set_xlabel(r"$x$")

# T
T = r.Qy_icT(r.yE, SSC=False, BB=True) * r.yE**ySlope
axTop.plot(
    r.yE,
    T,
    c="tab:green", label="T", linestyle=":"
)

# KN
KN = r.Qy_icKN(r.yE, SSC=False, BB=True) * r.yE**ySlope
axTop.plot(
    r.yE,
    KN,
    c="tab:blue", label="KN", linestyle=":"
)
# T + KN
TKN = (
    r.Qy_icKN(r.yE, SSC=False, BB=True) +
    r.Qy_icT(r.yE, SSC=False, BB=True)
) * r.yE**ySlope
axTop.plot(
    r.yE,
    TKN,
    c="tab:red", label="T + KN", linestyle="-"
)

# GB T
TGB = GB_Th(r.yE, inj0=inj_N0, p=-inj_si, Theta=Theta_BB) * r.yE**ySlope
axTop.plot(
    r.yE,
    TGB,
    c="black", label="T from GB", linestyle="--"
)

# GB KN
KNGB = GB_KN(
    r.yE, inj0=inj_N0, p=-inj_si, withLog=1, Theta=Theta_BB
) * r.yE**ySlope
axTop.plot(
    r.yE,
    KNGB,
    c="black", label="KN from GB", linestyle="-."
)

# GB full
xGB_full, GB_full = np.load("GB_fullInt.npy")
conversion = (c.sigma_T.cgs * 5e16 / u.cm**2).to("") * 1000
GB_full = GB_full*conversion
axTop.plot(
    xGB_full, xGB_full**2 * GB_full,
    c="black", label="full from GB", linestyle="-"
)


axTop.legend()





axd.set_xscale("log")
axd.set_yscale("log")
axd.set_xlim(1e-5, 1e11)
axd.set_ylim(5e-1, 1e1)

# full integral = 1
dtotGB = (
    r.Qy_icKN(xGB_full, SSC=False, BB=True) +
    r.Qy_icT(xGB_full, SSC=False, BB=True)
) / GB_full
dTGB = GB_Th(xGB_full, p=-inj_si) / GB_full
dKNGB = GB_KN(xGB_full, p=-inj_si) / GB_full

# dTGB = TKN / TGB
# dKNGB = TKN / KNGB
# dfull = TKN / GBfs

axd.axhline(y=1, c="black")
axd.plot(xGB_full, dtotGB, label="T + KN", c="tab:red", linestyle="-")
axd.plot(xGB_full, dTGB, label="T from GB", c="black", linestyle="--")
axd.plot(xGB_full, dKNGB, label="KN from GB", c="black", linestyle="-.")


axd.set_xlabel("x")
axd.set_ylabel(r"$Q_{IC} / Q_{GB}$")
axd.grid()




fig.tight_layout()
fig.savefig("3_final_plot.pdf")
