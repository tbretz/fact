import numpy as np


def Purity(s, b, alpha=0.2):
    return (s - alpha*b) / (s + alpha*b)
