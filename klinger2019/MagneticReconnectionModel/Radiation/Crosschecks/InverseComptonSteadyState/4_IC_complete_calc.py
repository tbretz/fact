import numpy as np
import matplotlib.pyplot as plt

from Radiation_simps import Radiation


'''





VERY IMPORTANT:


SET l_BB manually to 1





'''

# properties of system
L_system = 5 * 1e16  # cm
B = 1  # G
# power law injection for source term
inj_N0 = 1e3  # norm
inj_si = -3  # spectral index
inj_min = 10
inj_max = 1e7
# Black Body

Theta_BB = 1e-5  # temperature (kT/mc^2)

r = Radiation(
    L=L_system, B=B, UinjUB=1,
    inj_N0=inj_N0, inj_min=inj_min, inj_max=inj_max, inj_si=inj_si,
    Theta_BB=Theta_BB, L_BLR=1, R_BLR=1, z_diss=2,
    lBBto1=True, yEscFactor=2, nIntegration=100
)

# set grid
# Tmax= 15
r.setLinTime(dt=1e-3, saveEvery=1)
r.setLogElectronEnergy(eexpmin=0, eexpmax=8, NeE=100)
r.setLogPhotonEnergy(yexpmin=-15, yexpmax=8, NyE=100)

# set initial definitions
# initial electron term
pivot = 1
# norm = (1 + inj_si) / (inj_max**(1+inj_si) - inj_min**(1+inj_si))
qe = inj_N0 * (r.eE/pivot)**inj_si
eInitial = qe * ((r.eE > inj_min) & (r.eE < inj_max))
r.setInitalElectronDistribution(Ninitial=eInitial*0)
# initial photon term
yInitial = 0 * r.yE
r.setInitalPhotonDistribution(Ninitial=yInitial)


'''
commented = turned on
uncommented = turned off
'''
r.turnOff(
    [
        "Qe_injection",
        "Qe_yy",
        "Le_sync",
        # "Le_icT",
        # "Le_icKN",
        # "Ly_esc",
        "Ly_yy",
        "Ly_ssa",
        "Qy_sync",
        # "Qy_icT",
        # "Qy_icKN",
        # "BB",
        "SSC"
    ]
)
r.turnOn("Qe_powerlaw")

# fig, ax = plt.subplots()

# ax.plot(
#     r.eE,
#     r.U_T(r.n_BB(r.yE), r.eE)
# )
# ax.set_xscale("log")
# ax.set_yscale("log")
# plt.show(fig)
# Size of the system: for all times L_system
T = 5
one = np.ones(int(T / r.dt) * 2)
zero = np.zeros(int(T / r.dt) * 2)
r.solveUntil(T=T, sArray=one, betaArray=zero, dopplerBBArray=one,
             printEvery=100)

r.saveToh5("4_IC_complete_t5.h5")

plt.show(r.plotEvolution(BB=False))
