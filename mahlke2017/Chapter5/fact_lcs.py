import numpy as np
from peripylib import figures, query_fact


# ------
# Plot FACT data of Mrk501, Mrk421  and the RMS-Flux relation


if __name__ == '__main__':

    for source in ['Mrk501', 'Mrk421']:

        # Read in data
        _, nightly = query_fact.query_fact(source)

        # -------------
        # Set up plot
        fig, ax = figures.newfig(rows=2, ratio=1.2)

        # ---
        # Top panel: Excevts Rate vs Epoch
        ax[0].errorbar(nightly.mjd, nightly.evts, yerr=nightly.evts_err, linestyle='', marker='.')

        # Add labels only on first plot since all axes are shared
        ax[0].set(ylabel=r'$R(t)~/~\mathrm{h}^{-1}$', ylim=(-25, 225), xlim=(56000, 58000), xlabel='Epoch / MJD')

        # Include ticks for years
        year_mjds = [56293, 56658, 57023, 57388, 57754]  # these are Jan 1st for 2012-2017
        ax2 = ax[0].twiny()
        ax2.set_xlim((56000, 58000))

        # Set ticks and ticks locations
        # Tick locations are on linear scale between 56000 and 58000
        ax2.set_xticks([mjd for mjd in year_mjds])
        ax2.set_xticklabels(['2013', '2014', '2015', '2016', '2017'])
        ax2.set_axisbelow(True)
        ax2.grid(color='black', linestyle='--', linewidth=0.25)


        # ---
        # Second panel: Divide light curve into months and plot the rms-flux relation for these bins

        bin_width = 10 # in days
        bins = np.arange(min(nightly.mjd) - (max(nightly.mjd) - min(nightly.mjd)) % 30, max(nightly.mjd), bin_width)
        inds = np.digitize(nightly.mjd, bins)

        # Now compute mean and variance flux for each bin and plot those
        means_flux, vars_flux = [], []

        for j in range(1, len(bins)):

            # If too little data was recorded that month, continue
            if nightly.mjd[inds == j].size < 7:
                continue

            # Plot mean against mean MJD of that week
            mean_flux = np.mean(nightly.evts[inds == j])

            # Plot excess variance against mean MJD of that week
            # Excess variance = sqrt(variance^2 - mean_error^2)
            excvar_flux = (np.var(nightly.evts[inds == j]) - np.mean(nightly.evts_err[inds == j]**2))

            # We do not include the outlier of Mrk421
            #if excvar_flux < 1500:
            means_flux.append(mean_flux)
            vars_flux.append(excvar_flux)

        ax[1].plot(means_flux, vars_flux, linestyle='', marker='+', color=figures.colors(0))
        ax[1].set(ylabel=r'$\overline{\sigma_{XS}}^2~/~\mathrm{h}^{-2}$', xlabel=r'$\bar{R}~/~\mathrm{h}^{-1}$')
        figures.savefig('%s_statistics' % source, pgf=True)
