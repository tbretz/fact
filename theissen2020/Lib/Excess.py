import numpy as np


def Excess(s, b, alpha=0.2):
    return s - alpha*b
