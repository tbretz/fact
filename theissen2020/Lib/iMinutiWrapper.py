import numpy as np
from iminuit import Minuit
from autograd import jacobian


def iMinuitFit(func, guess, fix=None, limit=None):
    '''
    func:   function to be MINIMIZED (e.g. square sum of pull)
    guess:  tuple guess for initial params
    fix:    tuple of length guess with True for a param which is
            fixed and false otherwise (optional)
    limit:  tuple of length guess containing 2-tuple with
            (lower limit, upper limit) for each value
            in case of no limit value is None (e.g. (2, None)) (optional)

    returns array of best params and cov matrix
    '''
    if fix is None:
        fix = tuple(np.ones(len(guess)) < 0)
    if limit is None:
        limit = tuple(len(guess)*[(None, None)])
    m = Minuit.from_array_func(
        func, guess, error=tuple(0.1*np.ones(len(guess))),
        fix=fix, limit=limit, errordef=1, print_level=0
    )
    _, param = m.migrad()
    best_param = np.array([i.value for i in param])
    m.minos()
    cov = np.array(m.matrix())
    return best_param, cov
