#!rootifysql
SELECT
    -- Identification
    EventsMC.FileID,
    EventsMC.EvtNumber,
    -- for quality cuts
    EventsMC.NumIslands,
    EventsMC.NumUsedPixels,
    EventsMC.Leakage1,
    -- for BG cut
    EventsMC.Size,
    PI()*Width*Length AS Area,
    -- for thetasq cut
    EventsMC.MeanX,
    EventsMC.MeanY,
    EventsMC.Width,
    EventsMC.Length,
    EventsMC.CosDelta,
    EventsMC.SinDelta,
    EventsMC.M3Long,
    EventsMC.SlopeLong,
    PositionMC.X,
    PositionMC.Y,
    -- for Energy Regressor
    EventsMC.Conc,
    -- Zd
    EventsMC.Zd,
    EventsMC.Az,
    -- known parameters
    EventsMC.Disp,
    EventsMC.Energy,
    EventsMC.Impact
FROM
    EventsMC
LEFT JOIN PositionMC USING (FileId, EvtNumber)
LEFT JOIN RunInfoMC  USING (FileId)
WHERE
    RunInfoMC.PartID=1 -- only photons
 