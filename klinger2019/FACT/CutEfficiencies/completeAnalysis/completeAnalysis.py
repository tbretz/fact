import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns

import setFigureConfig as sfc
from HistogramManager import HistogramManager
from Histogram import Histogram


EBins = 64
IBins = 54
ZdBins = 60
si = -2.5

hm = HistogramManager()
hm.loadHistogramDict("../data/EventsMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)
hm.loadHistogramDict("../data/OriginalMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)

cp = sns.color_palette()
basic = sns.color_palette()
raw = basic[9]
orig = basic[7]
trig = basic[4]
qc = basic[1]
tsq = basic[2]
spec = basic[3]
lc = basic[0]
# palettes
spec_palette = [orig, trig, qc, tsq, spec]
lc_palette = [orig, trig, qc, tsq, lc]

pdf = PdfPages("allCutHistograms.pdf")
sfc.halfWidth(aspectRatio=0.8, alpha=1.2)

# Histograms spectrum
fig, axe = plt.subplots()
axe.set_prop_cycle("color", spec_palette)

axe = hm.plotHistograms(
    ["lgE_original", "lgE_triggered", "lgE_Q", "lgE_Q_tsq", "lgE_S"],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "spectrum cut"
    ],
    mode="lgEnergy", legloc=None,
    ax=axe
)
axe.get_legend().remove()
axe.set_ylabel(r"d$N$/d$\log_{10}(E/GeV)$")
fig.tight_layout()
pdf.savefig(fig)


fig, axz = plt.subplots()
axz.set_prop_cycle("color", spec_palette)

axz = hm.plotHistograms(
    ["Zd_original_nostep", "Zd_triggered", "Zd_Q", "Zd_Q_tsq", "Zd_S"],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "spectrum cut"
    ],
    mode="Zd", legloc=[[0.99, 0.7], "center right"],
    ax=axz
)
# axz.get_legend().remove()
axz.set_xticks(np.linspace(0, 60, 7))
axz.set_ylabel("")
axz.set_xlabel(r"$Zd$")
axz.set_xticklabels([str(int(xt)) + r"°" for xt in axz.get_xticks()])
axz.set_ylabel(r"d$N$/d$Zd$")
fig.tight_layout()
pdf.savefig(fig)


fig, axi = plt.subplots()
axi.set_prop_cycle("color", spec_palette)
axi = hm.plotHistograms(
    [
        "Impact_original_nostep", "Impact_triggered", "Impact_Q", 
        "Impact_Q_tsq", "Impact_S"
    ],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "spectrum cut"
    ],
    mode="Impact", legloc=[[1, 0.5], "center left"],
    ax=axi
)
axi.set_xlabel(r"Impact $I$ [m]")
axi.set_xticks(np.linspace(0, 54000, 5))
axi.set_xticklabels([str(int(1e-2*xt)) for xt in axi.get_xticks()])
axi.get_legend().remove()
axi.set_ylabel(r"d$N$/d$I$")
fig.tight_layout()
pdf.savefig(fig)


# Histograms lightcurve
fig, axe = plt.subplots()
axe.set_prop_cycle("color", lc_palette)

axe = hm.plotHistograms(
    ["lgE_original", "lgE_triggered", "lgE_Q", "lgE_Q_tsq", "lgE_L"],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "light curve cut"
    ],
    mode="lgEnergy", legloc=None,
    ax=axe
)
axe.get_legend().remove()
fig.tight_layout()
pdf.savefig(fig)


fig, axz = plt.subplots()
axz.set_prop_cycle("color", spec_palette)
axz = hm.plotHistograms(
    ["Zd_original_nostep", "Zd_triggered", "Zd_Q", "Zd_Q_tsq", "Zd_L"],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "light curve cut"
    ],
    mode="Zd", legloc=[[0.99, 0.7], "center right"],
    ax=axz
)
# axz.get_legend().remove()
axz.set_xticks(np.linspace(0, 60, 7))
axz.set_ylabel("")
axz.set_xlabel(r"$Zd$")
axz.set_xticklabels([str(int(xt)) + r"°" for xt in axz.get_xticks()])
axz.set_ylabel("")
fig.tight_layout()
pdf.savefig(fig)


fig, axi = plt.subplots()
axi.set_prop_cycle("color", spec_palette)
axi = hm.plotHistograms(
    [
        "Impact_original_nostep", "Impact_triggered", "Impact_Q",
        "Impact_Q_tsq", "Impact_L"
    ],
    labels=[
        "produced", "triggered", "quality cuts", r"$\Theta^2$-cut",
        "light curve cut"
    ],
    mode="Impact", legloc=[[1, 0.5], "center left"],
    ax=axi
)
axi.set_xlabel(r"Impact $I$ [m]")
axi.set_xticks(np.linspace(0, 54000, 5))
axi.set_xticklabels([str(int(1e-2*xt)) for xt in axi.get_xticks()])
axi.get_legend().remove()
fig.tight_layout()
pdf.savefig(fig)

pdf.close()
