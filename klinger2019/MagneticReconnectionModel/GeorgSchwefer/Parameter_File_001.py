# -*- coding: utf-8 -*-
"""
Created on Wed May 29 18:41:02 2019

@author: Georg Schwefer
"""

'''
File für globale Konstanten für eine Simulation. Muss jeweils eingebunden werden
'''

import numpy as np

#Konstanten
Bc=4.4*10**13
c=3*10**10
me=9.11*10**(-28)
mp=1836*me
sigmaT=6.65*10**(-25)

#freie Parameter
#Ränder der Grids
ymin=1.001
ymax=10**12
xmin=10**(-15)
xmax=10**(8)
#Anzahlgridpunkte
jmax=200
tsteps=400


#physikalische Parameter
B=1#Magnetfeld in Gauß
ymin_inj=1.1#Untere Grenze Injektion
ymax_inj=3*10**4#Obere Grenze
p=1.5#Exponent der Injektion
Amp1=10**3#nicht nötig
Amp2=2*10**-10#Amplitude der Elektroneninjektion

L=5*10**16#Layerlänge
numLc=7#Anzahl simulierte Lightcrossing times 
sigma=10#Im Moment nicht benötigt
UinjUb=1#Im Moment nicht benötigt

T=10**(-5)#BlackBody Temperatur T=k*T/m*c^2
Amp_BB=10**-3#Amplitude BlackBody

add=2000#nach Ende des Plasmoids noch berechnete Zeitschritte
#zusammengesetzte Größen
b=B/Bc
tsim=numLc
deltat=tsim/tsteps
#npic=(2*UinjUb*sigma*mp*(ymax_inj**(1-p)-ymin_inj**(1-p))*(2-p))/((ymax_inj**(2-p)-ymin_inj**(2-p))*(1-p)*me)
#nco=(npic*B**2)/(16*np.pi*sigma*mp*c**2)
Ub=(B**2)/(8*np.pi)
e_loss_const=(4*sigmaT*L)/3
lb=e_loss_const*Ub/(me*c**2)
ssa_const=(np.pi*137)/(6)

'''
Parameter für Beaming
'''

t_layer=2#LC-times in MC simuliert
tsteps_layer=20000#Schritte im MC code
deltat_layer=t_layer/tsteps_layer


Theta_obs=0 #Winkel zwischen Observer und Jet
Theta_prim=0 #Winkel zwischen Plasmoid und Jet
b_j=0.996 #Jet Geschw. in Observerframe



