# -*- coding: utf-8 -*-
"""
Created on Wed May 29 17:23:32 2019

@author: Georg Schwefer
"""

import Parameter_File_001 as par
import Moduldatei as Mod
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

'''
Moduldatei und Parameterdatei müssen eingelesen werden
'''

"""
Plasmoid definieren(Größe)


w_t=np.zeros(par.tsteps)
for i in range(par.tsteps):
    if (i<par.tsteps/2):
        w_t[i]=0.005*par.L+(0.006*par.L*np.tanh(i/10))
    else:
        w_t[i]=0.011*par.L

w_t1=np.zeros(par.tsteps)
for i in range(par.tsteps):
        w_t1[i]=0.001*par.L+(0.01*par.L*i/par.tsteps)
    

#Initialisierung Geschwindigkeit Plasmoid

Pos0=0

b_co=np.zeros(par.tsteps)
for i in range (par.tsteps):
    if (i<par.tsteps/5):
        b_co[i]=-0.2-0.3*np.tanh(i/22)
    else:
        b_co[i]=-0.5

"""


#Einlesen
recon=pd.read_hdf('./3Layers.h5',key='2')
a=recon.values



pos=[]
beta=[]
w=[]
ts=[]

for i in range(len(a)):
    if(a[i][0]==2242):#Wähle Plasmoid Nummer
        
        pos.append(a[i][1])
        w.append(a[i][2])
        beta.append(a[i][3])
        ts.append(a[i][4])
        
        if(len(pos)==1):
            t0=ts[0]

tfin=t0+(len(pos))*par.deltat_layer

for k in range(par.add):
    pos.append(pos[-1]+beta[-1]*par.deltat_layer)
    beta.append(beta[-1])
    w.append(w[-1])

pos=np.array(pos)-1

Ye,n_e,Xe,n_y=Mod.SSC_Spectrum(w,beta,t0,tfin)

Xe_obs,Ts,I_obs,Total_Is=Mod.Obs_Data(Xe,n_y,beta,pos,w,t0,tfin)

'''
Plot Elektronenenergiedichte über Magnetfelddichte
'''

Ns=[]
for i in range(par.tsteps):
    Es=0
    for j in range(par.jmax+1):
        Es+=n_e[i][j]/(w[i]**3*par.L**3)*(Mod.y(j+1)-Mod.y(j))
    Ns.append(Es)

plt.figure()
plt.plot(np.array(Ns)*(619.7/par.B**2))


'''
Plots Elektronenzahlen im Eigensystem
'''   
    
'''
plt.figure()
plt.plot(Ye[1:par.jmax],np.abs(n_e[10][1:par.jmax]),label='t=0.18')
plt.plot(Ye[1:par.jmax],np.abs(n_e[20][1:par.jmax]),label='t=0.35')
plt.plot(Ye[1:par.jmax],np.abs(n_e[30][1:par.jmax]),label='t=0.53')
#plt.plot(Ye[1:par.jmax],np.abs(n_e[400][1:par.jmax]),label='t=0.93')
#plt.plot(Ye[1:jmax],np.abs(n_e[90][1:jmax]))

#plt.plot(Ye,(10**5*np.array(Ye))**(-(p-1)),label='$\gamma^{-(s-1)}$')
#plt.plot(Ye,(10**0*np.array(Ye))**(-(p+1)),label='$\gamma^{-(s+1)}$' )

#plt.plot(Xe,Xe*n_y[90])
plt.xscale('log')
plt.yscale('log')   
#plt.ylim(bottom=10**20) 
plt.title('Electron spectrum.\n Injection with s=3.')

plt.xlabel('$\gamma$')
plt.ylabel('$N_e$')
#plt.xlim(1,10**8)
plt.axvline(x=10**2,label='minimum of injection',linestyle='dashed',c='r') 
plt.axvline(x=5*10**3,label='maximum of injection',linestyle='dashed',c='r') 
plt.legend() 
''' 

'''
Plot Photonenspektrum im Eigensystem
'''

plt.figure()
plt.plot(Xe[1:par.jmax],np.array(Xe[1:par.jmax])**2*np.abs((n_y)[10][1:par.jmax]),label='t=0.18')
plt.plot(Xe[1:par.jmax],np.array(Xe[1:par.jmax])**2*np.abs((n_y)[20][1:par.jmax]),label='t=0.35')
plt.plot(Xe[1:par.jmax],np.array(Xe[1:par.jmax])**2*np.abs((n_y)[30][1:par.jmax]),label='t=0.53')
#plt.plot(Xe[1:par.jmax],np.array(Xe[1:par.jmax])**2*np.abs((n_y)[400][1:par.jmax]),label='t=0.93')
#plt.plot(Xe[1:jmax],Xe[1:jmax]*np.abs((n_y)[90][1:jmax]))
plt.xlabel('x')
plt.ylabel('$x^2 \cdot N_{\gamma}$')

#plt.figure()
#plt.plot(Xe[1:jmax],Xe[1:jmax]*np.abs(n_BB[90][1:jmax]))
#plt.plot(Xe,(10**-5*np.array(Xe))**(-(s-3)/2),label='$x^{-p}$,p=(s-1)/2,')
#plt.plot(Xe,(10**-5*np.array(Xe))**(7/2), label='$x^{5/2}$, Synchro-self absorption')
#plt.plot(Xe,(np.array(Xe))**(2),label='$x^2$, independent of s')
#plt.plot(Xe,Xe*n_y[90])
plt.xscale('log')
plt.yscale('log')   
#plt.ylim(bottom=10**20) 
plt.title('Plasmoid Spectrum. \n power law electron injection with s=3. Magnetic field B =5 G.\n Time in light crossing times ')
plt.xlabel('x')
plt.ylabel('$x^2\cdot N_{\gamma}$')
plt.legend()
print(n_y[133])
print(n_y[330])
#print(SSA[50])


'''
Plot Light Curve
'''

plt.figure()
plt.plot((np.array(Ts))*463,Total_Is)
plt.xlabel('t in h')
plt.ylabel('Luminosität in erg/s')
#plt.yscale('log')
#plt.plot(Pos)
#print(Ts)
#print(Total_Is)
#print(Thetas)
#print(Dops)


