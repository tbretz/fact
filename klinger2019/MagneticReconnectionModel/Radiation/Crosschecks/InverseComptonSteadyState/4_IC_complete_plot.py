import numpy as np
import matplotlib.pyplot as plt

from Radiation_simps import plotEvolutionFromFileV, Ne_fromFile, \
    Ny_fromFile, fitPowerLaw

import setFigureConfig as sfc
import coloring as cl

sfc.fullWidth(aspectRatio=1.3, alpha=0.8)

filename_only = "4_IC_complete_t5.h5"

fig = plotEvolutionFromFileV(
    filename_only, BB=False, cmap=cl.c_time(reverse=True), maxLines=100,
    ind0=1, nTicks=6
)


# electron fit
axe = fig.get_axes()[0]

eE, Ne = Ne_fromFile(filename_only)

# fit 1
fitRange1 = (eE > 2) & (eE < 10)
print("Fit e 1")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange1], Ne.iloc[-1].values[fitRange1],
    pivot=10, guess=[1e3, -2],
    ax=axe, slope=2, height=3000, textpos="center"
)

# fit 2
fitRange2 = (eE > 30) & (eE < 1e4)
print("Fit e 2")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange2], Ne.iloc[-1].values[fitRange2],
    pivot=1000, guess=[1, -3],
    ax=axe, slope=2, height=3000, textpos="logCenter"
)

# fit 3
fitRange3 = (eE > 3e5) & (eE < 9e6)
print("Fit e 3")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange3], Ne.iloc[-1].values[fitRange3],
    pivot=1e6, guess=[10, -1],
    ax=axe, slope=2, height=3000, textpos="logCenter"
)

# set limits
axe.set_ylim(1e-5, 1e4)
axe.set_xlim(1, 2e7)




# photon fit
axy = fig.get_axes()[1]

yE, Ny = Ny_fromFile(filename_only)

# fit 1
fitRange1 = (yE > 1e-4) & (yE < 3e-3)
print("\nFit y 1")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange1], Ny.iloc[-1].values[fitRange1],
    pivot=1e-3, guess=[1e3, 1.517],
    ax=axy, slope=2, height=3e4, textpos="logCenter"
)
# fit 2
fitRange2 = (yE > 1e-2) & (yE < 1e4)
print("\nFit y 2")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange2], Ny.iloc[-1].values[fitRange2],
    pivot=1, guess=[1e3, -2.1777],
    ax=axy, slope=2, height=3e4, textpos="logCenter"
)

# fit 3
fitRange3 = (yE > 3e5) & (yE < 1e7)
print("\nFit y 3")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange3], Ny.iloc[-1].values[fitRange3],
    pivot=1e3, guess=[1e3, -0.51777],
    ax=axy, slope=2, height=3e4, textpos="logCenter"
)

# fit 4
fitRange4 = (yE > 1e-8) & (yE < 2e-5)
print("\nFit y 1")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange4], Ny.iloc[-1].values[fitRange4],
    pivot=1e-7, guess=[1e-7, 1.517],
    ax=axy, slope=2, height=3e4, textpos="logCenter"
)

# set limits
axy.set_xlim(1e-8, 5e7)
axy.set_ylim(1e-9, 1e5)



'''
# electron fit
eE, Ne = Ne_fromFile(filename_only)
axe = fig.get_axes()[0]


# Georg
pathToGeorg = "../../GerogSchwefer/comparison/4_ICS_complete/higherResolution/"
x_G = np.load(pathToGeorg + "4_ICS_complete_electron_x.npy")
y_G = np.load(pathToGeorg + "4_ICS_complete_electron_y.npy")[-1]

axe.plot(x_G, x_G**2 * y_G, label="Georg Schwefer", c="tab:red",
         drawstyle="steps-mid")


inds = (eE > 12) & (eE < 10000)
axe, popt, pcov = fitPowerLaw(
    eE[inds], Ne.iloc[-1].values[inds], guess=[1e2, -3],
    pivot=1, threshold=0, ax=axe, slope=2, c="black"
)
inds = (eE > 71000) & (eE < 8e6)
axe, popt, pcov = fitPowerLaw(
    eE[inds], Ne.iloc[-1].values[inds], guess=[1e2, -1],
    pivot=1, threshold=0, ax=axe, slope=2, c="grey"
)

axe.set_ylim(1e-1, 1e3)


# photon fit
axy = fig.get_axes()[1]


x_G = np.load(pathToGeorg + "4_ICS_complete_photon_x.npy")
y_G = np.load(pathToGeorg + "4_ICS_complete_photon_y.npy")[-1]

axy.plot(x_G, x_G**2 * y_G, label="Georg Schwefer", c="tab:red",
         drawstyle="steps-mid")


# yE, Ny = Ny_fromFile(filename_only)
# inds = (yE > 10**-13) & (yE < 1e-8)

# axy, popty, pcovy = fitPowerLaw(
#     yE[inds], Ny.iloc[-1].values[inds], guess=[4e17, 1.5],
#     pivot=1e-6, threshold=1e-6, ax=axy, c="red", slope=2
# )
# axy.set_ylim(1e-12, 1e6)


# print(popty)
# print(pcovy)
'''
fig.tight_layout()
fig.savefig("4_IC_complete_t5.pdf")
# plt.show(fig)
