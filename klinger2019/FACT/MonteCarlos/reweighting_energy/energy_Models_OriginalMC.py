from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
import setFigureConfig as sfc

OriginalMCFile = "../data/OriginalMC_Energy_Impact_Zd.root"
dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))


# log energy
dp.calcLogs(["Energy"])

# in model
# create histogram of lgE called lgE_hist
EHist = dp.getHistogram("lgE_Hist", "logEnergy")
# create Input Model with 10^(x) function
EnergyModelIn = Model("expo10")
# reweight from -1.7 (since histogram of log(Energy))
EnergyModelIn.getParametersFromFit(EHist, [3e11, -1.7])
EnergyModelIn.save("EnergyModelIn_OriginalMC.p")
print("finished in Model")


# out model
si = -3  # spectral index to reweight to
EnergyModelOut = Model("expo10")
# reweight to si + 1 (since histogram of log(Energy))
EnergyModelOut.getParametersFromArray(
    [1e8, si+1],
    EnergyModelIn.getNorm(),
    min(EHist.bins), max(EHist.bins)
)
EnergyModelOut.save("EnergyModelOut_OriginalMC_%g.p" % (si))

# calc histograms for control plot
dp.updateModels(EnergyModelIn, EnergyModelOut)
dp.addWeights("logEnergy", name="wE")

hm = HistogramManager()
EHist = dp.getHistogram(
    "lgE_Hist", "logEnergy", useWeights=None, nBins=64
)
hm.addHistogram("before_original", EHist)
EHist_weighted = dp.getHistogram(
    "lgE_Hist", "logEnergy", useWeights="all", nBins=64
)
hm.addHistogram("after_original", EHist_weighted)
hm.saveHistogramDict("OriginalMC_reweightingEnergy_%g.p" % (si))

# create control plot
sfc.halfWidth(aspectRatio=0.9)
fig = hm.plotHistograms(
    ["before_original", "after_original"], ["initial", "reweighted"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"]
)
ax = fig.gca()
ax.set_ylabel(
    r"$\frac{\mathrm{d}N}{\mathrm{d} \log_{10} E/GeV}$",
    # rotation=90, labelpad=10
)
fig.tight_layout()
fig.savefig("OriginalMC_reweightingEnergy_%g.pdf" % (si))
