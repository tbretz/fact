import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import gaussian_kde as gkde

import DataFrameBinning as dfb
import setFigureConfig as figConfig
from LibFunctions import weightedMean
from rootRoutines import dFrameFromRoot

recalculateFromRoot = True
outputPDF = PdfPages("./ThBinnedCRRatesNoZdCorr.pdf")


def evaluateBinningForVirtualTh(newDf, binnedGroup, vThStr):
    binnedRates = binnedGroup["fR"+vThStr]
    newDf[vThStr+"meanCR"] = binnedRates.apply(lambda r: np.mean(r))
    newDf[vThStr+"stdCR"] = binnedRates.apply(lambda r: np.std(r, ddof=1))
    newDf[vThStr+"meanCRErr"] = binnedRates.apply(
        lambda r: np.std(r, ddof=1)/np.sqrt(len(r))
    )


runsCR = dFrameFromRoot(
    "../ArtificialTriggerRatesWithNULL.root",
    keys=["sql"], prints=False
)

runsCR = runsCR[runsCR["fNight"] > 20140101]
runsCR = runsCR[runsCR["fEffectiveOn"] > 0.9]
runsCR = runsCR[runsCR["fRunDuration"] > 297]
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 30]
runsCR = runsCR[runsCR["fR750Cor"] < 1000]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 950]
runsCR["RAWTime"] = (runsCR["fRunStart"] + runsCR["fRunStop"])/2
runsCR["Time"] = pd.to_datetime(runsCR["RAWTime"], unit="s")
runsCR.sort_values("Time")

runsCR["fR500"] = 60*runsCR["fNumThreshold500"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]
runsCR["fR750"] = 60*runsCR["fNumThreshold750"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]
runsCR["fR1000"] = 60*runsCR["fNumThreshold1000"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

R_mean = np.mean(runsCR["fR750"])

thBinEdges = dfb.addLogBinning(runsCR, "fThresholdMinSet", "ThBin", 20)[1]
thBinnedDf = pd.DataFrame()
thBinnedGroup = runsCR.groupby("ThBin")
thBinnedDf["Th"] = thBinnedGroup["fThresholdMinSet"].apply(
    lambda r: np.mean(r)
)
thBinnedDf["ThCount"] = thBinnedGroup["fThresholdMinSet"].apply(
    lambda r: len(r)
)

for vThStr in ["500", "750", "1000"]:
    evaluateBinningForVirtualTh(
        thBinnedDf, thBinnedGroup, vThStr
    )

thBinnedDf.to_hdf("./ThBinnedCRRatesNoZdCorr.h5", key="Th")
norm500 = weightedMean(thBinnedDf["500meanCR"], thBinnedDf["500meanCRErr"])[0]
norm750 = weightedMean(thBinnedDf["750meanCR"], thBinnedDf["750meanCRErr"])[0]
norm1000 = weightedMean(
    thBinnedDf["1000meanCR"], thBinnedDf["1000meanCRErr"]
)[0]


KDEs = []
KDEmax = []
KDEerr = []
bin0Mean = 0
bin0Std = 0

for i in range(len(thBinnedGroup)):
    currentBin = runsCR[runsCR["ThBin"] == i]
    binMean = np.mean(currentBin["fR750"])
    binStd = np.std(currentBin["fR750"], ddof=1)
    binErr = binStd/np.sqrt(len(currentBin))

    if i == 0:
        bin0Mean = binMean
        bin0Std = binStd

    binKDE = gkde(currentBin["fR750"])
    binxKDE = np.linspace(bin0Mean-4*bin0Std, bin0Mean+4*bin0Std, num=500)
    binyKDE = binKDE(binxKDE)
    binMaxKDE = binxKDE[np.argmax(binyKDE)]

    binKDEdict = {
        "id": i,
        "mean": binMean,
        "std": binStd,
        "err": binErr,
        "x": binxKDE,
        "y": binyKDE,
        "max": binMaxKDE
    }

    KDEs.append(binKDEdict)
    KDEmax.append(binMaxKDE)
    KDEerr.append(binErr)

KDEmax = np.array(KDEmax)
KDEerr = np.array(KDEerr)

"""
    Figure 1 - Trigger Rate binned in threshold for different virt. thresholds
"""
figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["500meanCR"], yerr=thBinnedDf["500meanCRErr"],
    fmt=".", label="virt. th.   500 ADC", color="tab:blue"
)
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["750meanCR"], yerr=thBinnedDf["750meanCRErr"],
    fmt=".", label="virt. th.   750 ADC", color="tab:purple"
)
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["1000meanCR"], yerr=thBinnedDf["1000meanCRErr"],
    fmt=".", label="virt. th. 1000 ADC", color="tab:green"
)
ax.grid(alpha=0.5)
ax.set_ylim(130, 520)
ax.set_xlim(280, 950)
ax.set_xlabel("threshold in DAC")
ax.set_ylabel("trigger rate in events/min")
ax.legend(framealpha=1)
fig.tight_layout()
fig.savefig("./SeperatePlots/ThBinnedCRRates.pdf")
outputPDF.savefig(fig)

"""
    Figure 2 - Normalised version of Figure 1
"""
figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["500meanCR"]/norm500, yerr=thBinnedDf["500meanCRErr"]/norm500,
    fmt=".", label="virt. th.   500 ADC", color="tab:blue"
)
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["750meanCR"]/norm750, yerr=thBinnedDf["750meanCRErr"]/norm750,
    fmt=".", label="virt. th.   750 ADC", color="tab:purple"
)
ax.errorbar(
    thBinnedDf["Th"],
    thBinnedDf["1000meanCR"]/norm1000,
    yerr=thBinnedDf["1000meanCRErr"]/norm1000,
    fmt=".", label="virt. th. 1000 ADC", color="tab:green"
)
ax.grid(alpha=0.5)
ax.set_yticks([0.85, 0.9, 0.95, 1.0, 1.05])
ax.set_xlim(280, 950)
ax.set_xlabel("threshold in DAC")
ax.set_ylabel("normalised trigger rate")
ax.legend()
fig.tight_layout()
fig.savefig("./SeperatePlots/ThBinnedNormalisedCRRates.pdf")
outputPDF.savefig(fig)

"""
    Figure 3 - Distribution of all data runs in threshold space
"""
figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.bar(
    [(thBinEdges[i+1]+thBinEdges[i])/2 for i in range(len(thBinEdges)-1)],
    thBinnedDf["ThCount"],
    width=np.array(
        [thBinEdges[i+1]-thBinEdges[i] for i in range(len(thBinEdges)-1)]
    ),
    align="center",
    edgecolor="white",
    linewidth=0.6,
    label=str(int(thBinnedDf["ThCount"].sum()))+" Runs", zorder=4
)
ax.errorbar(
    thBinnedDf["Th"], thBinnedDf["ThCount"],
    yerr=np.sqrt(thBinnedDf["ThCount"]),
    fmt=".", color="k",
    label="center of gravity", zorder=5
)
ax.set_yscale("log")
ax.set_xlim(280, 950)
ax.set_ylim(1e2, 5e4)
ax.grid(axis="y", alpha=0.5, zorder=0, which="both")
ax.set_xlabel("threshold in DAC")
ax.set_ylabel("number of runs per bin")
ax.legend()
fig.tight_layout()
fig.savefig("./SeperatePlots/ThDistribution.pdf")
outputPDF.savefig(fig)

"""
    Figure 4 - KDE of 1st bin with comparison (lower mean)
"""
figConfig.halfWidth(aspectRatio=1.)
fig = plt.figure()
ax = fig.gca()
ax.plot(
    KDEs[0]["x"], KDEs[0]["y"]*1e3, color="tab:blue",
    label="1st bin: KDE and mean\n"+r"$290\,\mathrm{DAC}<T<308\,\mathrm{DAC}$"
)
ax.plot(
    KDEs[13]["x"], KDEs[13]["y"]*1e3, color="tab:purple",
    label="14th bin: KDE and mean\n"+r"$624\,\mathrm{DAC}<T<662\,\mathrm{DAC}$"
)
ax.set_yscale("log")
ax.axvline(x=KDEs[0]["mean"], color="tab:blue")
ax.axvline(x=KDEs[13]["mean"], color="tab:purple")
ax.set_ylabel("KDE in a.u.")
ax.set_xlabel(r"$R_{CR}$ in events/min")
ax.set_ylim(3e-2, 3e2)
ax.set_xlim(40, 350)
ax.grid(alpha=0.5, axis="both")
ax.legend()
fig.tight_layout()
fig.savefig("./SeperatePlots/bin1KDE.pdf")
outputPDF.savefig(fig)

"""
    Figure 5 - KDE of last bins (lower mean)
"""
figConfig.halfWidth(aspectRatio=1.)
fig = plt.figure()
ax = fig.gca()
ax.plot(
    KDEs[17]["x"], KDEs[17]["y"]*1e3, color="tab:blue",
    label="18th bin: KDE and mean\n"+r"$790\,\mathrm{DAC}<T<838\,\mathrm{DAC}$"
)
ax.plot(
    KDEs[18]["x"], KDEs[18]["y"]*1e3, color="tab:purple",
    label="19th bin: KDE and mean\n"+r"$838\,\mathrm{DAC}<T<889\,\mathrm{DAC}$"
)
ax.plot(
    KDEs[19]["x"], KDEs[19]["y"]*1e3, color="tab:green",
    label="20th bin: KDE and mean\n"+r"$889\,\mathrm{DAC}<T<943\,\mathrm{DAC}$"
)
ax.set_yscale("log")
ax.axvline(x=KDEs[17]["mean"], color="tab:blue")
ax.axvline(x=KDEs[18]["mean"], color="tab:purple")
ax.axvline(x=KDEs[19]["mean"], color="tab:green")
ax.set_ylabel("KDE in A.U.")
ax.set_xlabel(r"$R_{CR}$ in events/min")
ax.set_ylim(3e-2, 3e2)
ax.set_xlim(40, 350)
ax.grid(alpha=0.5, axis="both")
ax.legend()
fig.tight_layout()
fig.savefig("./SeperatePlots/bin20KDE.pdf")
outputPDF.savefig(fig)


"""
    Figure 6 - All runs in scatter plot with density + KDE Maxima
"""
calculated = True
if calculated:
    density_kde = np.load("./densityInfo.npy")
else:
    density = np.vstack([
        runsCR["fThresholdMinSet"],
        runsCR["fR750"]
    ])
    density_kde = gkde(density)(density)
    np.save("./densityInfo.npy", density_kde)

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.scatter(
    runsCR["fThresholdMinSet"], runsCR["fR750"], s=2,
    c=density_kde, edgecolor='', zorder=-5, alpha=0.5,
)
ax.plot(
    thBinnedDf["Th"], KDEmax, ".", markeredgecolor="k",
    markeredgewidth=1., marker=".", markersize=11, color="tab:orange",
    zorder=5, label="KDE maxima in\n"+"corresponding\n"+"threshold bins"
)
ax.axhline(
    y=np.average(KDEmax, weights=thBinnedDf["ThCount"]), linewidth=1.5,
    color="k", label=r"$\bar{R}_{CR}$"+" as weighted\n" +
    "average of KDE\n"+"maxima", zorder=3
)
ax.set_ylim(0, 310)
ax.set_xlim(291, 940)
ax.set_xscale("log")
ax.minorticks_off()
ax.set_xticks([300, 400, 500, 600, 700, 800, 900])
ax.set_xticklabels(["300", "400", "500", "600", "700", "800", "900"])
ax.set_xlabel("threshold in DAC")
ax.set_ylabel(r"$R_{CR}$ in events/min")
ax.grid(alpha=0.5, axis="both", zorder=10)
handels, labels = ax.get_legend_handles_labels()
fig.legend([handels[0], handels[1]], [labels[0], labels[1]], framealpha=1,
           loc="upper left", bbox_to_anchor=(0.6, 0.54))
fig.tight_layout()
fig.savefig("./SeperatePlots/ThScatterKDE.png", dpi=300)
fig.savefig("./SeperatePlots/ThScatterKDE.pdf")
outputPDF.savefig(fig)

outputPDF.close()
