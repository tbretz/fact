from HistogramManager import HistogramManager
import setFigureConfig as sfc

'''
Plots reweighted energy distributions for OriginaMC in one figure (Fig. 2.9)
for different spectral indices
histograms computed with energy_Models_OriginalMC.py
'''

si = -2.5  # spectral index to reweight to
si2 = -3.

hm = HistogramManager()
hm.loadHistogramDict("OriginalMC_reweightingEnergy_%g.p" % (si))

# create combined plot
sfc.halfWidth(aspectRatio=0.9)
fig = hm.plotHistograms(
    ["after_original", "before_original"], ["-2.5", "-2.7"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"]
)
hm.loadHistogramDict("OriginalMC_reweightingEnergy_%g.p" % (si2))
ax = fig.gca()
ax = hm.plotHistograms(
    ["after_original"], ["-3.0"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"], ax=ax
)
ax.set_ylabel(
    r"$\frac{\mathrm{d}N}{\mathrm{d} \log_{10} E/GeV}$",
    # rotation=90, labelpad=10
)
fig.tight_layout()
fig.savefig("combined_si_reweightingEnergy_%g.pdf" % (si))
