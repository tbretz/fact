import os
import pandas as pd


class SQLToHDF5(object):

    def __init__(self, lib="sqlalchemy"):

        self.establishConnection(lib)

        # Default Paths and Filenames
        self.__queryPath = "./QueryFiles/"
        self.__queryFile = "TestQuery.txt"
        self.__hdf5Path = "./Data/"
        self.__hdf5File = ""

    def establishConnection(self, lib):
        self.__lib = lib
        if lib == "MySQLdb":
            import MySQLdb
            import MySQLdb.cursors
            self.__connector = MySQLdb.connect(
                user='fact', password='40drs4320Bias',
                host='ihp-pc45.ethz.ch', database='factdata', port=3306,
                cursorclass=MySQLdb.cursors.DictCursor)
            self.__cursor = self.__connector.cursor()

        elif lib == "mysql":
            import mysql.connector
            self.__connector = mysql.connector.connect(
                user='fact', password='40drs4320Bias',
                host='ihp-pc45.ethz.ch', database='factdata', port=3306)
            self.__cursor = self.__connector.cursor(dictionary=True)

        elif lib == "sqlalchemy":
            import sqlalchemy
            self.__connector = sqlalchemy.create_engine(
                'mysql://%s:%s@%s/%s?charset=utf8mb4' % (
                    'fact', '40drs4320Bias', 'ihp-pc45.ethz.ch', 'factdata'),
                encoding='utf8')

    def readQuery(self, path="", file=""):
        if not path == "":
            self.__queryPath = path
        if not file == "":
            self.__queryFile = file
        queryFile = open(self.__queryPath + self.__queryFile)
        self.__query = queryFile.read()
        queryFile.close()

    def executeQuery(self, path="", file="", printProgress=False, buffer=None):
        self.readQuery(path=path, file=file)
        # Load the data from the database into a DataFrame

        self.__sqlData = pd.DataFrame()
        if self.__lib != "sqlalchemy":
            self.__cursor.execute(self.__query)
            dfList = []

            if printProgress:
                c = 0
                for Event in self.__cursor:
                    c += 1
                    if c % 1000 == 0:
                        print(c)
                    dfList.append(pd.DataFrame([Event]))

            else:
                for Event in self.__cursor:
                    dfList.append(pd.DataFrame([Event]))

            self.__sqlData = pd.concat(dfList, ignore_index=True)
            self.__connector.close()
        else:
            # if buffer is given, load data in chunks using less memory
            if buffer is None:
                self.__sqlData = pd.read_sql(self.__query,
                                             con=self.__connector)
            else:
                list_of_dfs = []
                sql_iter = pd.read_sql(self.__query, con=self.__connector,
                                       chunksize=buffer)
                for chunk in sql_iter:
                    list_of_dfs.append(chunk)
                self.__sqlData = pd.concat(list_of_dfs)

        if self.__hdf5File == "":
            self.__hdf5File = self.__queryFile[:-3]+"h5"

        self.__sqlData.to_hdf(self.__hdf5Path+self.__hdf5File, "/sql")

        hdf5Size = os.path.getsize(self.__hdf5Path + self.__hdf5File)
        print("Database request has been written to " +
              self.__hdf5Path + self.__hdf5File +
              " with size " + str(round(hdf5Size*1e-6, 2)) + "MB")

    @property
    def queryPath(self):
        return self.__queryPath

    @queryPath.setter
    def queryPath(self, queryPath):
        self.__queryPath = queryPath

    @property
    def queryFile(self):
        return self.__queryFile

    @queryFile.setter
    def queryFile(self, queryFile):
        self.__queryFile = queryFile

    @property
    def hdf5Path(self):
        return self.__hdf5Path

    @hdf5Path.setter
    def hdf5Path(self, hdf5Path):
        self.__hdf5Path = hdf5Path

    @property
    def hdf5File(self):
        return self.__hdf5File

    @hdf5File.setter
    def hdf5File(self, hdf5File):
        self.__hdf5File = hdf5File

    @property
    def query(self):
        return self.__readQuery()
