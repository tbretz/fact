import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import setFigureConfig as figConfig


simrange = np.linspace(0.5, 2.4, num=50)

slopes501 = np.load("./fittedIndsMrk501_rms07.npy")
err501 = np.load("./unevenErrsMrk501_rms07.npy")

slopes421 = np.load("./fittedIndsMrk421_rms07.npy")
err421 = np.load("./unevenErrsMrk421_rms07.npy")

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.errorbar(
    simrange, slopes421, yerr=err421,
    label="Timmer-König with Mrk 421 window", fmt="s",
    color="tab:purple"
)
ax.errorbar(
    simrange, slopes501, yerr=err501,
    label="Timmer-König with Mrk 501 window", fmt="o",
    color="tab:blue"
)
ax.axhline(
    y=0.79, color="tab:purple", ls="--", label="best fit Mrk 421 LSP slope"
)
ax.axhspan(
    ymin=0.65, ymax=0.93, alpha=0.4, color="tab:purple"
)

ax.axhline(
    y=0.69, color="tab:blue", label="best fit Mrk 501 LSP slope"
)
ax.axhspan(
    ymin=0.60, ymax=0.78, alpha=0.4, color="tab:blue"
)


ax.set_xlim(0.45, 2.45)
ax.set_ylim(0.29, 0.95)
ax.set_xlabel("simulated spectral index")
ax.set_ylabel("fitted slope of the LSP")
ax.legend(loc="lower right")
fig.tight_layout()
fig.savefig("./SimulationIndexConversion.png", dpi=300)
fig.savefig("./SimulationIndexConversion.pdf")
