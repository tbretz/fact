from astropy.time import Time
import datetime as dt
from fact.factdb import RunInfo, AnalysisResultsRunISDC, AnalysisResultsRunLP
from fact.factdb.utils import read_into_dataframe
import numpy as np
import pandas as pd
import peewee
import os


def query_fact(source, database='ISDC', apply_correction_to=['excevts', 'rate', 'rate_err'], verbose=False,
               cut_threshold=True, cut_zenith=True, compute_nightly=True, exclude_bad_weather=True, force_new=False):
    '''
       Checks if query has already been executed by looking into cwd. If not:

       Query FACT data from different databases for different sources
       database: either 'ISDC' or 'LP'
       source: any source in FACT's source catalog, see below. Pass 'all' if you
       want all data

       Saves results to csv and returns pandas dataframe for Runs and for Nightly Data
        The Nightly Data only contains mean MJD, excess events and excess event errors.

       At the end, we add an MJD column to the data
    '''

    today = dt.datetime.now()
    if not force_new and os.path.isfile('%s_%i-%i-%i_%s.csv' % (source, today.year, today.month, today.day, database)):
        df = pd.read_csv('%s_%i-%i-%i_%s.csv' % (source, today.year, today.month, today.day, database))
        if verbose:
            print('\nRetrieving %s data from file..' % source)
        if compute_nightly:
          nightly_data = compute_nightly_data(df)
          return df, nightly_data
        else:
          return df, []

    else:
        if verbose:
            print('\nQuerying FACT DB..')

    # Connect to database. Not sure what the 'SECOND' does
    SECOND = peewee.SQL('SECOND')
    # Dictionary of FACT Source Keys, used for queries later on
    source_keys = {
       'Mrk421':          1,
       'Mrk501':          2,
       '1ES2344+51.4':    3,
       'PKS2155-304':     4,
       'Crab':             5,
       'H1426+428':       6,
       '1ES1959+650':     7,
       '1ES1218+304':     9,
       'IC310':           11,
       '1H0323+342':       12,
       'RGBJ0521.8+2112': 14,
       'PKS0736+01':      16,
       'V404Cyg':         17,
       'AMON20160218 26':  18,
       'HESE20160427 16':  19,
       'PKS1749+096':     20,
       'AMON20160731 14':  21
    }


    # Select database. Check that either ISDC or LP was passed to function
    assert database in ('ISDC', 'LP'), 'database argument has to be either "ISDC" or "LP"'
    db = AnalysisResultsRunISDC if database == 'ISDC' else AnalysisResultsRunLP

    # Query all Data Runs belonging to Source with measurement parameters from database
    duration = (peewee.fn.TIMESTAMPDIFF(SECOND, RunInfo.frunstart, RunInfo.frunstop))

    query = RunInfo.select(
       RunInfo.fnight.alias('night'),
       RunInfo.fsourcekey.alias('source_key'),
       RunInfo.frunid.alias('run_id'),
       RunInfo.frunstart.alias('start'),
       RunInfo.frunstop.alias('stop'),
       duration.alias('duration'),
       RunInfo.feffectiveon.alias('effective_on'),
       RunInfo.fzenithdistancemean.alias('zdmean'),
       RunInfo.fzenithdistancemax.alias('zdmax'),
       RunInfo.fzenithdistancemin.alias('zdmin'),
       RunInfo.fthresholdminset.alias('thmin'),
       RunInfo.fthresholdmax.alias('thmax'),
       RunInfo.fthresholdavgmean.alias('thmean'),
       db.fnumexcevts.alias('excevts'),
       db.fnumsigevts.alias('sigevts'),
       db.fnumbgevts.alias('bgevts'),
       RunInfo.fnumthreshold1000.alias('num1000')
       ).join(db, on=((db.frunid == RunInfo.frunid) & (db.fnight == RunInfo.fnight))
       )

    if source != 'all':
       query = query.where((RunInfo.fsourcekey == source_keys[source]))

    df = read_into_dataframe(query)


    if verbose:
        print('\nRead in %s data: ' % source, df.shape)

    # ------
    # Prepare raw data for analysis

    # Cuts
    # Clean data: drop rows where some cells are empty or infinite
    df = df.replace([np.inf, -np.inf], np.nan)
    df.dropna(subset=['excevts', 'duration', 'start', 'stop', 'zdmin', 'zdmax', 'thmean'], inplace=True)

    # Remove t_obs < 240s
    df = df[(df['duration']*df['effective_on']) > 240.]

    # Remove thmean > 700
    if cut_threshold:
      df = df[df['thmean'] < 650]

    # Apply zenith angle cut
    if cut_zenith:
      df = df[df['zdmean'] < 60]

    # Take out known problematic nights (eg due to LIDAR)
    if source == 'Mrk421':
        df = df[~df['night'].isin([20120724, 20120924, 20130621, 20160109])]


    # Add excess events per hour column
    df['rate'] = df.excevts*3600/(df.duration*df.effective_on)
    # Add error. There is no factor Alpha in front of the bgevts as it's already included in FACTS DB
    df['rate_err'] = np.sqrt( np.sqrt(df['bgevts'])**2 + np.sqrt(df.sigevts)**2) / (df.effective_on*df.duration) * 3600

    # Drop rows where evts is empty or infinite
    df = df.replace([np.inf, -np.inf], np.nan)
    df.dropna(subset=['rate'], inplace=True)

    # Add MJD column for Run. This is the middle point between START and STOP time. Using Astropy.time
    df['mjd'] = 0.
    # Using iterrows is bad
    for ind, row in df.iterrows():
         start_mjd = Time(row['start'].strftime('%Y-%m-%d %H:%M:%S'), format='iso').mjd
         stop_mjd = Time(row['stop'].strftime('%Y-%m-%d %H:%M:%S'), format='iso').mjd
         df.set_value(ind, 'mjd', (start_mjd + stop_mjd) / 2)

    today = dt.datetime.now()

    # Add zenith-angle corrected columns
    if apply_correction_to:

       if 'num1000' in apply_correction_to:
           # Here we need to apply the proton-correction function
           df = correct_dataframe(df, columns='num1000', particle_type='proton')
           apply_correction_to = [x for x in apply_correction_to if x != 'num1000']

       df = filter_and_correct.correct(df, columns=apply_correction_to, particle_type='gamma')

    df.to_csv('%s_%i-%i-%i_%s.csv' % (source, today.year, today.month, today.day, database))

    # Exclude bad weather runs
    bad_weather_limit = 2.281396855  # from th_corr.py
    if exclude_bad_weather:
      df['th1000'] = df.apply(_th1000_rate, axis=1)
      if verbose:
        print('Excluding bad weather runs', df.shape)
      df = df[df.th1000 > bad_weather_limit]
      if verbose:
        print('Excluded bad weather runs', df.shape)

    if compute_nightly:
      nightly_data = compute_nightly_data(df)

    if verbose:
        print('\nPassing %s Run data: ' % source, df.shape)
        if compute_nightly:
          print('\nPassing %s Nightly data: ' % source, nightly_data.shape)

    if compute_nightly:
      return df, nightly_data
    else:
      return df, []


def compute_nightly_data(data):
    # ------
    # Compute nightly rates and errors. These are returned in a separate pandas dataframe

    # Use the MJDs column in the dataframe. We want data from the same night
    # in one bin, so go from x.5 - y.5 (starting at 0 would go from midnight
    # to midnight)

    # Get list of observation nights. The timestamps are midnight, meaning that
    # the data was observed +- 0.5 days around the timestamp
    nights = sorted(set(data.mjd.map(lambda x: round(x))))

    # Nights with less than 1h observation time are excluded
    # Take the mean mjd date.
    mjds, nightly_rates, nightly_errors, nights_to_exclude = [], [], [], []

    for night in nights:

        # Get data of that night
        nightly_data = data[(data['mjd'] < night + 0.5) & (data['mjd'] > night - 0.5)]
        mean_mjd = nightly_data.mjd.mean()
        mjds.append(mean_mjd)

        # Nightly measurement duration
        duration = sum(nightly_data['duration'] * nightly_data['effective_on'])

        # Exclude nights when the source was only observed for an hour or less
        if duration / 3600 < 1:
            nights_to_exclude.append(mean_mjd)
            continue

        # Calculate nightly rate and error
        rate = sum(nightly_data['excevts_corr']) * 3600. / duration

        correction_factors = nightly_data.rate_corr/nightly_data.rate
        correction_factors.replace(np.nan, 1., inplace=True)

        error = np.sqrt(np.sqrt(sum(correction_factors*nightly_data['bgevts'])**2) + np.sqrt(sum(correction_factors*nightly_data['sigevts'])**2)) * 3600 /\
                duration

        if error == 0.:
            nights_to_exclude.append(mean_mjd)
            continue
        nightly_rates.append(rate)
        nightly_errors.append(error)
    mjds = [night for night in mjds[:] if night not in nights_to_exclude]

    return pd.DataFrame.from_dict({'mjd': mjds,'evts': nightly_rates,'evts_err': nightly_errors})

def _th1000_rate(row):
      # Returns TH1000 rate in Hz
      rate = row['num1000'] / (row['duration'] * row['effective_on'])
      return rate


def correct_dataframe(data, particle_type='gamma', columns=['excevts']):
  """
  Correct zenith dependence using Zenith-rate model. This method is called by other scripts.

  particle_type: either 'gamma' or 'proton', selects the model parameters
  column: the data column to correct for zenith angle
  """

  assert particle_type in ['gamma', 'proton'], 'Particle type has to be either "gamma" or "proton"'

  if particle_type == 'gamma':
    model_parameters = [ 0.61444529, 1.82638447, 1.14927505]  # results from gamma_rates.py
  elif particle_type == 'proton':
    model_parameters = [1.75267265, 0.33430478, 1.34746444]  # results from cr_rates.py

  # Calculate correction factors
  correction_factors = model.fit_model((data['zdmin'].as_matrix() + data['zdmax'].as_matrix()) / 2, *model_parameters)
  normierung = model.fit_model(0, *model_parameters)[0]

  # Apply correction
  for column in columns:
    data['%s_corr' % column] = data[column] * normierung / correction_factors

  return data
