from secondAxis import createYearAxisToMjdAxis
import matplotlib.pyplot as plt
from rootRoutines import dFrameFromRoot
import numpy as np
import pandas as pd
import ModifiedJulianDay as mjd
import setFigureConfig as figConfig
import DataFrameBinning as dfb
import emcee
from scipy.stats import gaussian_kde, norm
from scipy.optimize import minimize
from LibFunctions import weightedMean, findFWHM, roundAndFollowDIN

"""
###############################################################################
Switch on individual segments (sorry that was done quick and dirty)
###############################################################################
"""
distributionFigures = False
availablilityFigures = False
relVariationFigures = False
fitRelation = False
exportTNGAvNights = True
"""
###############################################################################
"""

cM = dFrameFromRoot(
    "./CalimaMeasures.root", keys=["sql"], prints=False
)
cM["fNight"] = np.array(cM.fNight.values, dtype=int)

gtcRaw = pd.read_csv("./gtc_dust_out.csv")
gtcRaw["fNight"] = np.array(gtcRaw.fNight.values, dtype=int)

firstNight = mjd.fNight_to_mjd(
    np.min([np.min(cM["fNight"]), np.min(gtcRaw["fNight"])])
)
lastNight = mjd.fNight_to_mjd(
    np.max([np.max(cM["fNight"]), np.max(gtcRaw["fNight"])])
)
dateRangeMJD = np.arange(firstNight, lastNight+1, dtype=int)


if distributionFigures:
    fTNGDust = cM["fTNGDust"].values
    logTNGDust = np.log10(fTNGDust[fTNGDust > 0])
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        logTNGDust, bins=np.linspace(-2.2, 2.4, num=27),
        rwidth=0.95, zorder=1/100+1, color="tab:blue"
    )
    ax.set_xlim(-2.25, 2.45)
    ax.set_ylim(0, 35000)
    ax.set_xlabel(r"$\mathrm{log}_{10}$"+"(concentration/(µg/m³)) TNG")
    ax.set_ylabel("number of measurements\n in 1000 per bin")
    ax.set_yticks([0, 5000, 10000, 15000, 20000, 25000, 30000, 35000])
    ax.set_yticklabels(["0", "5", "10", "15", "20", "25", "30", "35"])
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("TNGDistribution.pdf")
    fig.savefig("TNGDistribution.png", dpi=300)

    fGTCDust = gtcRaw["µg/m^3>25µm"].values
    logGTCDust = np.log10(fGTCDust[fGTCDust > 0])
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        logGTCDust, bins=np.linspace(-2.2, 2.4, num=27),
        rwidth=0.95, zorder=1/100+1, color="tab:purple"
    )
    ax.set_xlim(-2.25, 2.45)
    ax.set_ylim(0, 125000)
    ax.set_xlabel(r"$\mathrm{log}_{10}$"+"(concentration/(µg/m³)) GTC")
    ax.set_ylabel("number of measurements\n in 1000 per bin")
    ax.set_yticks([0, 25000, 50000, 75000, 100000, 125000])
    ax.set_yticklabels(["0", "25", "50", "75", "100", "125"])
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("GTCDistribution.pdf")
    fig.savefig("GTCDistribution.png", dpi=300)


cMnG = cM.groupby("fNight")
gtcnG = gtcRaw.groupby("fNight")

cMn = pd.DataFrame()
gtcn = pd.DataFrame()

cMn["TNG_mean"] = cMnG["fTNGDust"].apply(
    lambda group: np.mean(group)
)
cMn["TNG_std"] = cMnG["fTNGDust"].apply(
    lambda group: np.std(group, ddof=1) if len(group) > 1 else 0
)
cMn["TNG_N"] = cMnG["fTNGDust"].apply(
    lambda group: len(group)
)


cMn["LIDAR9_mean"] = cMnG["fLidarTransmission9"].apply(
    lambda group: np.mean(group)
)
cMn["LIDAR9_std"] = cMnG["fLidarTransmission9"].apply(
    lambda group: np.std(group, ddof=1) if len(group) > 1 else 0
)
cMn["LIDAR9_N"] = cMnG["fLidarTransmission9"].apply(
    lambda group: len(group)
)


gtcn["GTC_mean"] = gtcnG["µg/m^3>25µm"].apply(
    lambda group: np.mean(group)
)
gtcn["GTC_std"] = gtcnG["µg/m^3>25µm"].apply(
    lambda group: np.std(group, ddof=1) if len(group) > 1 else 0
)
gtcn["GTC_N"] = gtcnG["µg/m^3>25µm"].apply(
    lambda group: len(group)
)

# Combine to one dataframe containing all instruments
aI = pd.DataFrame()
aI["mjd"] = dateRangeMJD
aI["fNight"] = aI.apply(
    lambda row: mjd.mjd_to_fNight(row.mjd), axis=1
)
aI = aI.merge(
    cMn, how="left", left_on="fNight", right_on="fNight"
)
aI = aI.merge(
    gtcn, how="left", left_on="fNight", right_on="fNight"
)
aI = aI.fillna(-1)


def checkInstrumentAvailable(row, instrument):
    if row[instrument+"_mean"] <= 1e-6:
        return 0
    elif "LIDAR" not in instrument:
        if (row[instrument+"_std"] == 0
           and row[instrument+"_mean"] > 0
           and row[instrument+"_N"] > 5):
            return 0
    return 1


aI["TNG_av"] = aI.apply(
    lambda row: checkInstrumentAvailable(row, "TNG"), axis=1
)
aI["GTC_av"] = aI.apply(
    lambda row: checkInstrumentAvailable(row, "GTC"), axis=1
)
aI["BothCounters_av"] = aI.apply(
    lambda row: 1 if (row.TNG_av == 1 and row.GTC_av == 1) else 0, axis=1
)

if availablilityFigures:

    aI["LIDAR_av"] = aI.apply(
        lambda row: checkInstrumentAvailable(row, "LIDAR9"), axis=1
    )
    aI["Any_av"] = aI.apply(
        lambda row: 1 if (
            row.TNG_av == 1 or row.GTC_av == 1 or row.LIDAR_av == 1
            ) else 0, axis=1
    )
    aI["OnlyTNG_av"] = aI.apply(
        lambda row: 1 if (
            row.TNG_av == 1 and row.GTC_av == 0 and row.LIDAR_av == 0
            ) else 0, axis=1
    )
    aI["OnlyGTC_av"] = aI.apply(
        lambda row: 1 if (
            row.TNG_av == 0 and row.GTC_av == 1 and row.LIDAR_av == 0
        ) else 0, axis=1
    )
    aI["OnlyLIDAR_av"] = aI.apply(
        lambda row: 1 if (
            row.TNG_av == 0 and row.GTC_av == 0 and row.LIDAR_av == 1
        ) else 0, axis=1
    )
    aI["No_av"] = aI.apply(
        lambda row: 1 if (
            row.TNG_av == 0 and row.GTC_av == 0 and row.LIDAR_av == 0
            ) else 0, axis=1
    )

    print("At least one")
    print(aI["Any_av"].sum(), aI["Any_av"].sum()/len(aI))
    print("TNG")
    print(aI["TNG_av"].sum(), aI["TNG_av"].sum()/len(aI))
    print("GTC")
    print(aI["GTC_av"].sum(), aI["GTC_av"].sum()/len(aI))
    print("LIDAR")
    print(aI["LIDAR_av"].sum(), aI["LIDAR_av"].sum()/len(aI))
    print("OnlyTNG")
    print(aI["OnlyTNG_av"].sum(), aI["OnlyTNG_av"].sum()/len(aI))
    print("OnlyGTC")
    print(aI["OnlyGTC_av"].sum(), aI["OnlyGTC_av"].sum()/len(aI))
    print("OnlyLIDAR")
    print(aI["OnlyLIDAR_av"].sum(), aI["OnlyLIDAR_av"].sum()/len(aI))
    print("No")
    print(aI["No_av"].sum(), aI["No_av"].sum()/len(aI))

    def plotAvailability(aI, column, ax, y, color, available=True):
        if available:
            mod_aI = aI[aI[column] == 1]
        else:
            mod_aI = aI[aI[column] == 0]  
        mjd = mod_aI["mjd"].values
        val = mod_aI[column].values
        for i in range(len(mjd)):
            ax.plot(
                (mjd[i], mjd[i]), (y+0.125, y-0.125),
                lw=0.2, color=color, zorder=5
            )

    figConfig.fullWidth(aspectRatio=1./2)
    fig = plt.figure()
    ax = fig.gca()

    plotAvailability(aI, "TNG_av", ax, 3.5, "tab:blue")
    plotAvailability(aI, "GTC_av", ax, 3, "tab:purple")
    plotAvailability(aI, "LIDAR_av", ax, 2.5, "tab:green")

    plotAvailability(aI, "OnlyTNG_av", ax, 4, "tab:blue")
    plotAvailability(aI, "OnlyGTC_av", ax, 4, "tab:purple")
    plotAvailability(aI, "OnlyLIDAR_av", ax, 4, "tab:green")

    plotAvailability(aI, "No_av", ax, 2, "k")

    ax.set_xlabel("MJD")
    ax.set_ylim(1.5, 4.5)
    ax.set_xlim(mjd.fNight_to_mjd(20111231), mjd.fNight_to_mjd(20200103))
    ax2 = createYearAxisToMjdAxis(ax, offset=0)
    ax2.grid(axis="x", alpha=0.5, zorder=0)
    ax.set_yticks([2, 2.5, 3, 3.5, 4])
    ax.set_yticklabels(["none", "LIDAR", "GTC", "TNG", "only one"])
    fig.tight_layout()
    fig.savefig("InstrumentAvailability.png", dpi=300)
    fig.savefig("InstrumentAvailability.pdf")

    aI = aI.drop(["LIDAR_av", "Any_av", "OnlyLIDAR_av"], axis=1)

aI = aI.drop(["LIDAR9_N", "LIDAR9_mean", "LIDAR9_std"], axis=1)
aI["No_av"] = aI.apply(
    lambda row: 1 if (row.TNG_av==0 and row.GTC_av==0) else 0, axis=1
)
aI["Any_av"] = aI.apply(
   lambda row: 1 if (row.TNG_av==1 or row.GTC_av==1) else 0, axis=1
)
aI["OnlyTNG_av"] = aI.apply(
    lambda row: 1 if (row.TNG_av==1 and row.GTC_av==0) 
                else 0, axis=1
)
aI["OnlyGTC_av"] = aI.apply(
    lambda row: 1 if (row.TNG_av==0 and row.GTC_av==1) 
                else 0, axis=1
)

if relVariationFigures:

    tngCopy = aI.copy()
    tngCopy = tngCopy.drop(
        ["Any_av","OnlyGTC_av","OnlyTNG_av","GTC_av","No_av"], axis=1
    )
    tngCopy = tngCopy.drop(["GTC_N", "GTC_mean", "GTC_std"], axis=1)
    tngCopy = tngCopy[tngCopy["TNG_av"]==1]

    tngCopy["TNG_relStd"] = tngCopy["TNG_std"]/tngCopy["TNG_mean"]

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        tngCopy["TNG_relStd"], bins=np.linspace(0,2.5,num=25),
        color="tab:blue",rwidth=0.95, zorder=1/100+1,
        label="all available nights at TNG\nN="+str(len(tngCopy))
    )
    tngCopy = tngCopy[tngCopy["TNG_mean"]>5]
    ax.hist(
        tngCopy["TNG_relStd"], bins=np.linspace(0,2.5,num=25),
        color="tab:orange",rwidth=0.95, zorder=2/100+1,
        label="nights with mean >5(µg/m³)\nN="+str(len(tngCopy))
    )
    ax.set_xlim(-0.05,2.5)
    ax.set_xlabel("relative variance within one night")
    ax.set_ylabel("number of nights per bin")
    ax.grid(axis="y",alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    fig.tight_layout()
    fig.savefig("TNGrelVariation.pdf")
    fig.savefig("TNGrelVariation.png", dpi=300)

    del tngCopy

    gtcCopy = aI.copy()
    gtcCopy = gtcCopy.drop(
        ["Any_av","OnlyGTC_av","OnlyTNG_av","TNG_av","No_av"], axis=1
    )
    gtcCopy = gtcCopy.drop(["TNG_N", "TNG_mean", "TNG_std"], axis=1)
    gtcCopy = gtcCopy[gtcCopy["GTC_av"]==1]

    gtcCopy["GTC_relStd"] = gtcCopy["GTC_std"]/gtcCopy["GTC_mean"]

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        gtcCopy["GTC_relStd"], bins=np.linspace(0,2.5,num=25),
        color="tab:purple",rwidth=0.95, zorder=1/100+1,
        label="all available nights at GTC\nN="+str(len(gtcCopy))
    )
    gtcCopy = gtcCopy[gtcCopy["GTC_mean"]>5]
    ax.hist(
        gtcCopy["GTC_relStd"], bins=np.linspace(0,2.5,num=25),
        color="tab:orange",rwidth=0.95, zorder=2/100+1,
        label="nights with mean 5>(µg/m³)\nN="+str(len(gtcCopy))
    )
    ax.set_xlim(-0.05,2.5)
    ax.set_xlabel("relative variance within one night")
    ax.set_ylabel("number of nights per bin")
    ax.grid(axis="y",alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    fig.tight_layout()
    fig.savefig("GTCrelVariation.pdf")
    fig.savefig("GTCrelVariation.png", dpi=300)

    del gtcCopy

if fitRelation:
    bothAv = aI.copy()
    bothAv = bothAv[bothAv["BothCounters_av"]==1]
    bothAv["TNG_mean_log"] = np.log10(bothAv["TNG_mean"])
    bothAv["GTC_mean_log"] = np.log10(bothAv["GTC_mean"])
    bothAv = bothAv[bothAv["TNG_mean"]>1e-2]
    bothAv = bothAv[bothAv["TNG_mean"]<1e3]
    bothAv = bothAv[bothAv["GTC_mean"]>1e-2]
    bothAv = bothAv[bothAv["GTC_mean"]<1e3]
    bothAv = bothAv[bothAv["TNG_std"]>1e-4]
    bothAv = bothAv[bothAv["GTC_std"]>1e-4] 
    
    # Idea was to calculate the correlation by hand using the std
    # as a weight and the combined std as alpha value for the illustration. This did not work (probably stupid mistake).
    # The code for the weights is still here:

    # bothAv["TNG_std_log"] = bothAv["TNG_std"]/bothAv["TNG_mean"]
    # bothAv["GTC_std_log"] = bothAv["GTC_std"]/bothAv["GTC_mean"]
    #bothAv["combined_std_log"] = bothAv["TNG_std_log"]**2 + \
    #    bothAv["GTC_std_log"]**2
    #bothAv["alpha"] = 1-(bothAv["combined_std_log"] /
    #                    np.max(bothAv["combined_std_log"]))
    # bothAv["TNG_std_log"] = bothAv["TNG_std"]/bothAv["TNG_mean"]
    # bothAv["GTC_std_log"] = bothAv["GTC_std"]/bothAv["GTC_mean"]

    density = np.vstack([
        bothAv["TNG_mean_log"],
        bothAv["GTC_mean_log"]
    ])
    density_kde = gaussian_kde(density)(density)

    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.scatter(
        bothAv["TNG_mean_log"], bothAv["GTC_mean_log"],
        c=density_kde, edgecolor='', zorder=2/100+1
    )
    
    corrCoeff = np.corrcoef(bothAv["TNG_mean"], bothAv["GTC_mean"])[0,1]

    ax.plot(
       (-2.1,2.1),(-2.1,2.1),
       zorder=1/100+1, color="tab:grey", linestyle="--",label="corr. coef. = "+str(round(corrCoeff,2))
    )
    ax.set_xlim(-1.6, 2)
    ax.set_ylim(-1.8, 2)
    ax.set_xlabel(r"$\mathrm{log}_{10}$"+"(concentration/(µg/m³)) TNG")
    ax.set_ylabel(r"$\mathrm{log}_{10}$"+"(concentration/(µg/m³)) GTC")
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    handels, labels = ax.get_legend_handles_labels()
    ax.legend([handels[0]], [labels[0]], loc=4,handletextpad=0,
    handlelength=0)
    fig.tight_layout()
    
    fig.savefig("./SensorCorrelationLog.pdf")
    fig.savefig("./sensorCorrelationLog.png", dpi=300)

    # Bin data in TNG
    binEdges = dfb.addLinearBinning(
        bothAv, "TNG_mean_log", "TNGBin", 20, minVal=-1.4, maxVal=2.
    )[1]

    bothAv = bothAv[bothAv["TNGBin"] != -1]

    bothAvBinnedG = bothAv.groupby("TNGBin")
    bothAvBinned = pd.DataFrame()
    bothAvBinned["N"] = bothAvBinnedG["TNGBin"].apply(
        lambda group: len(group)
    )
    bothAvBinned["TNG_std"] = bothAvBinnedG["TNG_mean"].apply(
        lambda group: np.std(group, ddof=1)
    )
    bothAvBinned["TNG_std"] = bothAvBinned["TNG_std"]/bothAvBinned["N"]
    bothAvBinned["GTC_std"] = bothAvBinnedG["GTC_mean"].apply(
        lambda group: np.std(group, ddof=1)
    )
    bothAvBinned["GTC_std"] = bothAvBinned["GTC_std"]/bothAvBinned["N"]
    wMeanTNG = bothAvBinnedG.apply(
        lambda group: weightedMean(group.TNG_mean, group.TNG_std)
    ).apply(pd.Series, index=["TNG_wmean", "TNG_wstd"])
    
    bothAvBinned = bothAvBinned.merge(
        wMeanTNG, how="outer", left_on="TNGBin", right_on="TNGBin"
    )
    
    wMeanGTC = bothAvBinnedG.apply(
        lambda group: weightedMean(group.GTC_mean, group.GTC_std)
    ).apply(pd.Series, index=["GTC_wmean", "GTC_wstd"])

    bothAvBinned = bothAvBinned.merge(
        wMeanGTC, how="outer", left_on="TNGBin", right_on="TNGBin"
    )

    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()

    ax.errorbar(
        bothAvBinned["TNG_wmean"], bothAvBinned["GTC_wmean"],
        xerr=bothAvBinned["TNG_wstd"], yerr=bothAvBinned["GTC_wstd"],
        zorder=3/100+1, fmt="o", label="dust measurements binned in\nmeasured TNG concentration"
    )
    
    ax.plot(
        (10**(-2.1),10**(2.1)),(10**(-2.1),10**(2.1)),
        zorder=1/100+1, color="tab:grey", linestyle="--"
    )
    

    def log_likelihood(theta, x, y, xerr, yerr):
        m, b, log_f = theta
        model = m * x + b
        yerr2 = np.sqrt(yerr**2 + (m*xerr)**2)
        sigma2 = yerr2 ** 2 + model ** 2 * np.exp(2 * log_f)
        return -0.5 * np.sum((y - model) ** 2 / sigma2  + np.log(sigma2))

    nll = lambda *args: -log_likelihood(*args)
    initial = np.array([0.21, 0.15, -0.25])
    soln = minimize(
        nll, initial,
        args=(
            bothAvBinned["TNG_wmean"], bothAvBinned["GTC_wmean"],
            bothAvBinned["TNG_wstd"], bothAvBinned["GTC_wstd"] 
        )
    )
    m_ml, b_ml, log_f_ml = soln.x

    print("ML Results:")
    print(m_ml,b_ml,log_f_ml)
    
    xvals = np.logspace(-2,2, num=20)
    # yML = m_ml*xvals+b_ml
    
    
    def log_prior(theta):
        m, b, log_f = theta
        if 0 < m < 1 and 0 < b < 10 and -5. < log_f < 5.:
            return 0.0
        return -np.inf
    
    def log_probability(theta, x, y, xerr, yerr):
        lp = log_prior(theta)
        if not np.isfinite(lp):
            return -np.inf
        return lp + log_likelihood(theta, x, y, xerr, yerr)
    
    pos = soln.x + 1e-4 * np.random.randn(32, 3)
    nwalkers, ndim = pos.shape
    resample=False
    if resample:
        sampler = emcee.EnsembleSampler(
            nwalkers, ndim, log_probability,
            args=(
                bothAvBinned["TNG_wmean"], bothAvBinned["GTC_wmean"],
                bothAvBinned["TNG_wstd"], bothAvBinned["GTC_wstd"] 
            )
        )

        sampler.run_mcmc(pos, 5000, progress=True)
        
        # check boost in phase and reduce samples accordingly
        tau = sampler.get_autocorr_time()
        print("boost-in phase information:")
        print(tau)
        print("\n")
        thinBy = int(np.mean(tau)/2.)
        discardIndex = int(3*np.mean(tau))
        flat_samples = sampler.get_chain(
            discard=discardIndex, thin=thinBy, flat=True
        )
        np.save("./flatSamplesSensorCalibFit.npy", flat_samples)
    
    else:
        flat_samples = np.load("./flatSamplesSensorCalibFit.npy")

    
    inds = np.random.randint(len(flat_samples), size=100)
    firstflag = True
    for ind in inds:
        sample = flat_samples[ind]
        if firstflag:
            label = "100 random samples from\nthe MC ensemble"
            firstflag = False
        else:
            label = None
        ax.plot(
            xvals, np.dot(np.vander(xvals, 2), sample[:2]),
            color="tab:purple", alpha=0.1, zorder=2/100.+1, label=label
        )
    

    labelsNoFormat = ["m", "b", "log(f)"]
    resultDicts = []
    for i in range(ndim):
        mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
        q = np.diff(mcmc)
        resultDicts.append(
            {
                "key": labelsNoFormat[i],
                "mean": mcmc[1],
                "q16": mcmc[0],
                "q84": mcmc[2],
                "+": q[1], "-": q[0],
            }
        )
    pRes = pd.DataFrame(resultDicts)

    # print posterior results
    print("posterior results dataframe:")
    print(pRes)
    print("\n")
    optvalues = pRes["mean"].values

    ax.plot(xvals, optvalues[0]*xvals+optvalues[1], color="k",
        lw=2, zorder=4/100+1,
        label = "\nbest fit values:\n"+\
            "  "+r"$\mathrm{m}=0.30_{-0.05}^{+0.07}$"+" \n"+ \
            "  "+r"$\mathrm{c_{0}}=0.16_{-0.04}^{+0.05}$"+" µg/m³"
    )
    ax.legend()
    legendHandels, legendLabels = ax.get_legend_handles_labels()
    legendHandels = [legendHandels[0],legendHandels[1]]
    legendLabels = [legendLabels[0],legendLabels[1]]
    leg = ax.legend(legendHandels,legendLabels)
    for lh in leg.legendHandles: 
        lh.set_alpha(1)

    ax.set_xlabel("average concentration/(µg/m³) TNG")
    ax.set_ylabel("average concentration/(µg/m³) GTC")
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    ax.set_xlim(1e-2,1e2)
    ax.set_ylim(1e-2,1e2)
    ax.set_xscale("log")
    ax.set_yscale("log")
    fig.tight_layout()
    fig.savefig("./BinnedCorrelationFit.pdf")
    fig.savefig("./BinnedCorrelationFit.png", dpi=300)


if exportTNGAvNights:

    tngCopy = aI.copy()
    tngCopy = tngCopy.drop(
        ["Any_av","OnlyGTC_av","OnlyTNG_av","GTC_av","No_av",
        "BothCounters_av"], axis=1
    )
    tngCopy = tngCopy.drop(["GTC_N", "GTC_mean", "GTC_std"], axis=1)

    tngCopy.to_hdf("./TNGDustAv.h5", "TNGDustAv")
