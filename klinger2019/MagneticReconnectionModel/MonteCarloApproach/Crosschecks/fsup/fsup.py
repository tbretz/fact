import numpy as np
import matplotlib.pyplot as plt
import setFigureConfig as sfc

x = np.linspace(0, 10, 200)
nvar = 21
sigmas = np.linspace(1, 50, nvar)
As = np.linspace(0.5, 1, nvar)
Bs = np.linspace(0.01, 0.05, nvar)


def fsup(x, sigma=10, A=0.77, B=0.033):
    return 2./(1-np.tanh((x/np.sqrt(sigma) - A)/B))


sfc.fullWidth(aspectRatio=1)
fig, (axs, axa, axb) = plt.subplots(nrows=3, sharex=True, figsize=(10, 7))
# axa.text(
#   5, 10**9,
#   r"$f_{sup} = \frac{1}{\left[ \frac{1}{2} \left( 1-\tanh \left( \frac{\frac{|p|}{\sqrt{\sigma}}-A}{B} \right) \right) \right] }$",
#   fontsize=16
# )
colormap = plt.get_cmap('Blues')
colorCycle = colormap(np.linspace(0.2, 1, nvar))
axs.set_prop_cycle('color', colorCycle)
axs.semilogy(x, fsup(x), color="red", label=r"$\sigma=10$")
axs.plot([1, 10], [1, 1], linestyle=":", color="grey")
for i, sig in enumerate(sigmas):
    if not i % 5:
        axs.semilogy(x, fsup(x, sigma=sig), label=r"$\sigma=$" + str(sig))
    else:
        axs.semilogy(x, fsup(x, sigma=sig))
#axs.set_xlabel("p/mc")
axs.set_ylabel(r"$f_\mathrm{sup}$")
axs.legend()

axa.set_prop_cycle('color', colorCycle)
axa.semilogy(x, fsup(x), color="red", label=r"$A=0.77$")
axa.plot([1, 10], [1, 1], linestyle=":", color="grey")
for i, a in enumerate(As):
    if not i % 5:
        axa.semilogy(x, fsup(x, A=a), label=r"$A=$" + str(a))
    else:
        axa.semilogy(x, fsup(x, A=a))
#axa.set_xlabel("p/mc")
axa.set_ylabel(r"$f_\mathrm{sup}$")
axa.legend()

axb.set_prop_cycle('color', colorCycle)
axb.semilogy(x, fsup(x), color="red", label=r"$B=0.033$")
axb.plot([1, 10], [1, 1], linestyle=":", color="grey")
for i, b in enumerate(Bs):
    if not i % 5:
        axb.semilogy(x, fsup(x, B=b), label=r"$B=$" + str(b))
    else:
        axb.semilogy(x, fsup(x, B=b))

axb.set_xlabel("p/mc")
axb.set_ylabel(r"$f_\mathrm{sup}$")
axb.legend()
plt.tight_layout()
fig.savefig("fsup.pdf")
