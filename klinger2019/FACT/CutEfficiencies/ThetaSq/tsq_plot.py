import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_pdf import PdfPages

from HistogramManager import HistogramManager
import setFigureConfig as sfc

EBins = 64
IBins = 54
ZdBins = 60
si = -2.5

hm = HistogramManager()
hm.loadHistogramDict("../data/EventsMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)

sfc.fullWidth(aspectRatio=0.6)

# plot of theta squared distribution
fig, ax = plt.subplots()
ax = hm.plotHistograms(
    ["thetasq"], labels=[""], ax=ax,
    xlabel=r"$\Theta^2 $ $ [\mathrm{deg}^2]$"
)
ax.axvline(x=0.028, color="black", zorder=5)
ax.xaxis.set_zorder(11)
ax.get_legend().remove()
ax.set_ylabel(r"$10^{-4}$ d$N$/d$\Theta^2$")
ax.set_yticklabels([str(int(1e-4*yt)) for yt in ax.get_yticks()])
ax.set_xlim(-0.002, 0.05)
fig.tight_layout()
fig.savefig("tsq_distribution.pdf")


# efficiency - big
fig, (axe, axz, axi) = plt.subplots(ncols=3, figsize=(16, 5))

ylim = [0.7, 1.01]
axe = hm.plotEfficiencies(
    ["lgE_Q", "lgE_Q_tsq"],
    cuts=["triggered", r"$\Theta^2 cut$"],
    mode="lgEnergy", legloc=[None, "center right"], style="stairs",
    h0Name="triggered data", ax=axe, yscale="linear"
)
axe.axhline(y=1, color="black", linestyle=":")
axz = hm.plotEfficiencies(
    ["Zd_Q", "Zd_Q_tsq"],
    cuts=["triggered", r"$\Theta^2 cut$"],
    mode="Zd", legloc=[None, "center right"], style="stairs",
    h0Name="triggered data", ax=axz, yscale="linear"
)
axz.axhline(y=1, color="black", linestyle=":")
axi = hm.plotEfficiencies(
    ["Impact_Q", "Impact_Q_tsq"],
    cuts=["triggered", r"$\Theta^2 cut$"],
    mode="Impact", legloc=[None, "upper right"], style="stairs",
    h0Name="triggered data", ax=axi
)
axi.axhline(y=1, color="black", linestyle=":")
fig.tight_layout()
fig.savefig("tsq_efficiency_3big.pdf")


# efficiency - energy + Zd (as in thesis)
pdf = PdfPages("tsq_efficiency.pdf")
sfc.fullWidth(aspectRatio=0.5, alpha=0.8)
fig, axe = plt.subplots()

ylim = [0.7, 1.01]
# E
axe.axhline(y=1, color="black", linestyle=":")
axe = hm.plotEfficiencies(
    ["lgE_Q", "lgE_Q_tsq"],
    cuts=["triggered", r"$\Theta^2 cut$"],
    mode="lgEnergy", legloc=[None, "center right"], style="stairs",
    h0Name="triggered data", ax=axe, yscale="linear"
)
divider = make_axes_locatable(axe)
axz = divider.append_axes("right", size="100%", pad=0.0)
axe.get_legend().remove()
axe.grid()
axz.grid()
# Zd
axz.axhline(y=1, color="black", linestyle=":")
axz = hm.plotEfficiencies(
    ["Zd_Q", "Zd_Q_tsq"],
    cuts=["triggered", r"$\Theta^2$ cut"],
    mode="Zd", legloc=[None, "center right"], style="stairs",
    h0Name="triggered data", ax=axz, yscale="linear"
)
axz.set_xticks(np.linspace(0, 60, 7))
axz.set_xticklabels([str(int(xt)) + r"°" for xt in axz.get_xticks()])
# axz.get_yaxis().set_visible(False)
axz.set_xlabel(r"$Zd$")
axz.set_ylabel("")
axz.set_yticklabels("")
for tic in axz.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False

axe.set_ylabel("cut efficiency")
fig.tight_layout()
pdf.savefig(fig)


# efficiency - impact only (as in thesis)
sfc.halfWidth(aspectRatio=0.9, alpha=1.2)

fig, axi = plt.subplots()

ylim = [0.7, 1.01]
# E
axi.axhline(y=1, color="black", linestyle=":")
axi = hm.plotEfficiencies(
    ["Impact_Q", "Impact_Q_tsq"],
    cuts=["triggered", r"$\Theta^2$ cut"],
    legloc=[None, "upper right"], style="stairs",
    h0Name="triggered data", ax=axi, yscale="log"
)
# axe.get_legend().remove()
axi.grid()
axi.set_ylabel("cut efficiency")
axi.set_xlabel("Impact $I$ [m]")
axi.set_xticks(np.linspace(0, 54000, 5))
axi.set_xticklabels([str(int(1e-2*xt)) for xt in axi.get_xticks()])
fig.tight_layout()
pdf.savefig(fig)

pdf.close()
