import numpy as np
import pandas as pd


'''
class to simulate a reconnection layer with plasmoids
basic usage:

1. create object with correct parameters:
L = Layer()

2. initialize with grid as initial conditions:
L.initialize()

3. simulate up to time Tau with dtau stepsize (about 50steps/s):
L.calculateTime(Tau=2, dtau=1e-4)

4. save results to hdf file:
L.saveToHDF("filename.h5", "run1")

can be read in again by
allPlasmoids = pd.read_hdf("filename.h5", key="run1")
'''


class Layer:

    def __init__(
        self,
        sigma=10, betaA=0.12, betaG=0.08, Asup=0.77, Bsup=0.033,
        sMin=1e-3, dtau=1e-3, rMax=1, p0_variance=0.3,
        birth=True, merging=True, boundaries=True,
        initialDistanceFactor=10
    ):

        # for eq. of motion:
        # magnetisation
        self.sigma = float(sigma)
        # rate of acceleration
        self.betaA = float(betaA)

        # for growth:
        # rate of plasma growth
        self.betaG = float(betaG)
        # for growth suppression
        self.Asup = float(Asup)
        self.Bsup = float(Bsup)

        # boundaries
        self.rMax = rMax

        # for birth:
        # birth rate of plasmoids
        self.birth = birth
        # birth size of plasmoids
        self.sMin = float(sMin)
        # initial distance factor
        self.initialDistanceFactor = float(initialDistanceFactor)
        # factor in front of standard dev. of new genereated particle mom.
        self.p0_variance = float(p0_variance)

        # container for all plasmoids currently in evolution
        # r: position in lab frame
        # s: vertical height in lab frame and also comoving frame
        # beta: velocity / c
        self.columns = ["r", "s", "beta"]
        self.Plasmoids = []
        self.initalValues = pd.DataFrame(columns=["r0", "p0"])

        # number of all Plasmoids ever in system
        self.NTotal = 0

        # timing:
        # current time
        self.tau = 0.
        # time step size
        self.dtau = float(dtau)
        # array with all times
        self.times = []

        # merging
        self.merging = merging
        # drop plasmoids at boundaries
        self.boundaries = boundaries

        # save before filling up empty space
        self.allPlasmoidsBeforeBirth = []

    # set initial conditions
    def initialize(self, initialConditions="grid"):
        if initialConditions == "grid":
            # generate plasmoids along a grid = equidistantly distributed
            firstStep = pd.DataFrame(columns=self.columns)
            # constant distance between 2 plasmoids
            r0 = self.initialDistanceFactor * self.sMin
            newR = np.append(
                -np.arange(0, self.rMax, r0)[1:][::-1],  # exclude 0
                np.arange(0, self.rMax, r0)  # include 0
            )
            newP = self.sampleNewMomentum(newR)
            newbeta = newP / np.sqrt(1 + newP**2)
            newId = np.arange(len(newP)) + self.NTotal + 1
            newPlasmoids = pd.DataFrame(
                {
                    "r": newR,
                    "s": np.ones(len(newR)) * self.sMin,
                    "beta": newbeta
                },
                index=newId
            )
            newInitials = pd.DataFrame(
                {
                    "r0": newR,
                    "p0": newP
                },
                index=newId
            )
            # to keep id unique
            self.NTotal += len(newR)
            # add to timeStep
            firstStep = pd.concat([firstStep, newPlasmoids])
            self.initalValues = pd.concat([self.initalValues, newInitials])
            self.Plasmoids.append(firstStep)
            self.times.append(self.tau)

    # sample a new momentum given a new position
    def sampleNewMomentum(self, x0):
        # sample from gaussian with p0_mean and p0_sigma (cf. paper)
        l0 = 0.25 * (self.sigma / 10)
        p0_mean = 0.66 * np.sqrt(self.sigma) * np.tanh(x0 / l0)
        p0_sigma = self.p0_variance * np.sqrt(self.sigma)
        return np.random.normal(p0_mean, p0_sigma)

    # function to calculate the momenta of a step
    def p(self, step):
        arg = (
            self.betaA / np.sqrt(self.sigma) *
            (step["r"] - self.initalValues.loc[step.index]["r0"]) /
            step["s"]
        )
        return (
            np.sqrt(self.sigma) * np.tanh(arg) +
            self.initalValues.loc[step.index]["p0"]
        )

    # returns gamma of a step
    def gamma(self, step):
        return 1./np.sqrt(1. - step["beta"]**2)

    # function for every iteration
    # 1. integrate eq. of motion
    # 2. checks for losses at the boundaries
    # 3. checks for mergers
    # 4. sample new plasmoids
    def calcNextStep(self, saveBeforeBirth=False):
        i = len(self.times)  # number of steps
        # sort last step with new appended plasmoids
        lastStep = self.Plasmoids[i-1].sort_values("r")
        # create new step
        nextStep = pd.DataFrame(columns=self.columns)

        # 1. eq. of motion
        # calc new position
        nextStep["r"] = lastStep["r"] + self.dtau * lastStep["beta"]

        # calc new size
        p = lastStep["beta"] / np.sqrt(1 - lastStep["beta"]**2)
        fsup = 2./(
            1 - np.tanh(
                (np.abs(p)/np.sqrt(self.sigma) - self.Asup) / self.Bsup
            )
        )
        nextStep["s"] = (lastStep["s"] +
                         self.dtau * self.betaG / self.gamma(lastStep) / fsup)

        # calc new velocity
        nextStep["beta"] = self.p(nextStep) / np.sqrt(1. + self.p(nextStep)**2)

        # 2. check for losses at boundaries
        if self.boundaries:

            out = (
                # r + 3/2*s/gamma/2 < -rMax  = left bound
                (
                    (
                        nextStep["r"] + 0.75 * nextStep["s"] *  # 3/2* s /2
                        np.sqrt(1. - nextStep["beta"]**2)  # / gamma
                    ) < -self.rMax
                ) |
                # r - 3/2*s/gamma/2 < rMax  = right bound
                (
                    (
                        nextStep["r"] - 0.75 * nextStep["s"] *  # 3/2* s /2
                        np.sqrt(1. - nextStep["beta"]**2)  # / gamma
                    ) > self.rMax
                )
            )
            nextStep = nextStep.drop(nextStep[out].index)

        # 3. check for mergers
        if self.merging:
            # delta = r2 - 3/2*s2/gamma2/2 - (r1 + 3/2*s1/gamma1/2)
            delta = (
                nextStep["r"].values[1:] -  # r2
                0.75 * nextStep["s"].values[1:] *  # 3/2* s2 /2
                np.sqrt(1. - nextStep["beta"].values[1:]**2) -  # / gamma2
                nextStep["r"].values[:-1] -  # r1
                0.75 * nextStep["s"].values[:-1] *  # 3/2* s1 /2
                np.sqrt(1. - nextStep["beta"].values[:-1]**2)  # gamma1
            )
            if np.any(delta < 0):
                # at least one overlap of two plasmoids
                # -> keep bigger plasmoid
                keepFirst = np.less(
                    nextStep["s"].iloc[:-1].iloc[delta < 0].values,
                    nextStep["s"].iloc[1:].iloc[delta < 0].values
                )
                # drop first plasmoid
                drop1 = np.array(
                    nextStep.iloc[:-1].iloc[delta < 0].iloc[keepFirst].index,
                    dtype=int
                )
                # drop second plasmoid
                drop2 = np.array(
                    nextStep.iloc[1:].iloc[delta < 0].iloc[
                        np.logical_not(keepFirst)
                    ].index,
                    dtype=int
                )
                # indices to drop
                dropList = np.append(drop1, drop2)
                if len(dropList) > 0:  # redundand?
                    nextStep.drop(dropList, inplace=True)

        # save plasmoids before birt of new ones
        if saveBeforeBirth:
            self.allPlasmoidsBeforeBirth.append(nextStep)

        # 4. add new plasmoids
        if self.birth:

            # threshold difference: g
            g = self.initialDistanceFactor * self.sMin

            # check difference between first plasmoid and left boundary
            # r - 3/2*s/gamma/2 - (-rMax) > g
            if (
                nextStep["r"].iloc[0] -  # r
                0.75 * nextStep["s"].iloc[0] *  # 3/2*s/2
                np.sqrt(1 - nextStep["beta"].iloc[0]**2) +  # /gamma
                self.rMax > g
            ):

                # enough space between lower boundary and first plasmoid
                gamma = 1. / np.sqrt(1 - nextStep["beta"].iloc[0]**2)
                s0 = 0.75 * nextStep["s"].iloc[0]
                newR = np.random.uniform(
                    -self.rMax,
                    nextStep["r"].iloc[0] - s0 / gamma
                )
                newP = self.sampleNewMomentum(newR)
                nextStep.loc[self.NTotal + 1] = [
                    newR, self.sMin, newP / np.sqrt(1. + newP**2)
                ]
                self.initalValues.loc[self.NTotal + 1] = [newR, newP]
                self.NTotal += 1

            # check difference between last plasmoid and right boundary
            # rMax - (r + 3/2*s/gamma/2) > g
            if (
                self.rMax - nextStep["r"].iloc[-1] -  # rMax - r
                0.75 * nextStep["s"].iloc[-1] *  # 3/2* s /2
                np.sqrt(1 - nextStep["beta"].iloc[-1]**2) > g  # /gamma
            ):

                # enough space between upper boundary and last plasmoid
                gamma = 1. / np.sqrt(1 - nextStep["beta"].iloc[-1]**2)
                s0 = 0.75 * nextStep["s"].iloc[-1]
                newR = np.random.uniform(
                    nextStep["r"].iloc[-1] + s0 / gamma,
                    self.rMax
                )
                newP = self.sampleNewMomentum(newR)
                nextStep.loc[self.NTotal + 1] = [
                    newR, self.sMin, newP / np.sqrt(1. + newP**2)
                ]
                self.initalValues.loc[self.NTotal + 1] = [newR, newP]
                self.NTotal += 1

            # check difference between other plasmoids
            # delta = r2 - 3/2*s2/gamma2/2 - (r1 + 3/2*s1/gamma1/2)
            delta = (
                nextStep["r"].values[1:] -  # r2
                0.75 * nextStep["s"].values[1:] *  # 3/2* s2 /2
                np.sqrt(1. - nextStep["beta"].values[1:]**2) -  # / gamma2
                nextStep["r"].values[:-1] -  # r1
                0.75 * nextStep["s"].values[:-1] *  # 3/2* s1 /2
                np.sqrt(1. - nextStep["beta"].values[:-1]**2)  # gamma1
            )
            if np.any(delta > g):
                newR = np.random.uniform(
                    # lower bound: r1 + 3/2*s1/gamma1/2
                    nextStep["r"].iloc[:-1].iloc[delta > g] +
                    0.75 * nextStep["s"].iloc[:-1].iloc[delta > g] *
                    np.sqrt(1 - nextStep["beta"].iloc[:-1].iloc[delta > g]**2),
                    # upper bound: r2 - 3/2*s2/gamma2/2
                    nextStep["r"].iloc[1:].iloc[delta > g] -
                    0.75 * nextStep["s"].iloc[1:].iloc[delta > g] *
                    np.sqrt(1 - nextStep["beta"].iloc[1:].iloc[delta > g]**2),
                    # length
                    np.sum(delta > g)
                )
                newP = self.sampleNewMomentum(newR)
                newbeta = newP / np.sqrt(1 + newP**2)
                newId = np.arange(len(newP)) + self.NTotal + 1
                newPlasmoids = pd.DataFrame(
                    {
                        "r": newR,
                        "s": np.ones(len(newR)) * self.sMin,
                        "beta": newbeta
                    },
                    index=newId
                )
                newInitials = pd.DataFrame(
                    {
                        "r0": newR,
                        "p0": newP
                    },
                    index=newId
                )
                # increment id
                self.NTotal += len(newR)
                # add to timeStep
                nextStep = pd.concat([nextStep, newPlasmoids])
                self.initalValues = pd.concat([self.initalValues, newInitials])

        # increment tau
        self.tau += self.dtau
        self.times.append(self.tau)
        # add to self.Plasmoids
        self.Plasmoids.append(nextStep)

    def calculateNSteps(self, N, saveBeforeBirth=False, printProgressEvery=50):
        print("calculating " + str(N) + " steps")
        for i in range(N):
            if not i % printProgressEvery:
                print(i, " of ", N)
            self.calcNextStep(saveBeforeBirth=False)

    def calculateTime(self, tauMax=2, dtau=1e-4, saveBeforeBirth=False,
                      printProgressEvery=50):
        N = int(tauMax / dtau)
        self.calculateNSteps(N, saveBeforeBirth=saveBeforeBirth,
                             printProgressEvery=printProgressEvery)

    def saveToHDF(self, filename, key):
        # add time as a column and concatenate all dataframes
        times = np.array(self.times)
        allPlasmoids = []
        for i in range(len(times)):
            nextP = self.Plasmoids[i]
            nextP["tau"] = times[i]
            allPlasmoids.append(nextP.reset_index())
        allPlasmoids = pd.concat(allPlasmoids, ignore_index=True)
        # save to hdf file
        allPlasmoids.to_hdf(filename, key=key)
