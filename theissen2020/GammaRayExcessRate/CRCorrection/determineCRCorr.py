import emcee
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import minimize

import ExcessCalculation as ec
import setFigureConfig as figConfig
from correctionModels import perfCorrectionFactor
from DataFrameBinning import addLinearBinning
from LibFunctions import weightedMean

# set random seed
np.random.seed(123)

# set flags to skip parts of the program
resample = False
recreateObsTimePlot = True
recreateDataSpacePlot = True
recreateCorrSpacePlot = True

# load calibration set
calibrationCrabSample = ["../MergedExcessInfos/CrabExcess.h5", "CrabExcess"]
runsCR = pd.read_hdf(calibrationCrabSample[0], calibrationCrabSample[1])
print("\nCalibration Crab sample loaded from: "+calibrationCrabSample[0]+"\n")

# only data starting from season 14/15
runsCR = runsCR[runsCR["fNight"] > 20140723]

# conservative cuts to avoid influence by Calima, zenith distance and theshold
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 30]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 450]

# choose binning and group
runsCR, binEdges, dropped = addLinearBinning(
    runsCR, "RCR_corr", "CRBin", 15, minVal=220, maxVal=300
)
grpRuns = runsCR.groupby("CRBin")

# rebinning
binnedDf = pd.DataFrame()
binnedDf["N"] = grpRuns.apply(lambda group: len(group))

# - find mean within CR bins
wMean = grpRuns.apply(
    lambda group: weightedMean(group.RCR_corr, group.RCR_err_corr)
).apply(pd.Series, index=["RCR_wMean", "RCR_wMeanErr"])
binnedDf = binnedDf.merge(
    wMean, how="outer", left_on="CRBin", right_on="CRBin"
)

# - calculate excess for every bin
binnedDf = ec.excessPerBinUncorrected(binnedDf, grpRuns)

# - reset index to let CRBin become a column
binnedDf = binnedDf.reset_index()

# - reject outliers that are not contained in the binned parameter space
binnedDf = binnedDf[binnedDf["CRBin"] != -1]

# - add bin edges to data frame
binnedDf["BinStart"] = binEdges[:-1]
binnedDf["BinStop"] = binEdges[1:]
binnedDf["BinCenter"] = (binEdges[:-1]+binEdges[1:])/2

# plot observation time distribution
if recreateObsTimePlot:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.bar(
        binnedDf["BinCenter"], binnedDf["t_eff"],
        width=(binEdges[1]-binEdges[0])*0.95,
        color="tab:blue", zorder=1.1
    )
    ax.set_xlabel(r"$R_{\mathrm{CR}}$"+" in events/min")
    ax.set_ylabel("effective observation\ntime in h")
    ax.set_xlim(215, 305)
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("RCR_ObsTime.pdf")
    fig.savefig("RCR_ObsTime.png", dpi=300)

    t_tot = np.sum(binnedDf["t_eff"])
    print("Total effective observation time of the sample:\033[s")
    print("\033[u "+str(round(t_tot, 2)) + "h\n")


# define generative model by negative log likelihood function


def nll(theta, x, y, xerr, yerr):
    m, b, log_f = theta
    model = m * x + b
    yerr2 = np.sqrt(yerr**2 + (m*xerr)**2)
    sigma2 = yerr2 ** 2 + model ** 2 * np.exp(2 * log_f)
    ll = -0.5 * np.sum((y - model) ** 2 / sigma2 + np.log(sigma2))
    return -ll


# start with simple maximum likelihood fit
initial = np.array([0.5, 0, -0.25])
soln = minimize(
    nll, initial,
    args=(
        binnedDf["RCR_wMean"], binnedDf["RExc"],
        binnedDf["RCR_wMeanErr"], binnedDf["RExcErr"]
    )
)
m_ml, b_ml, log_f_ml = soln.x

print("ML Results:")
print(m_ml, b_ml, log_f_ml)
print()

# define CR paramter space for model plots
xvals = np.linspace(200, 320, num=100)

# define prior and posterior probabilities


def log_prior(theta):
    m, b, log_f = theta
    if 0 < m < 1 and -100 < b < 0 and -6. < log_f < 0.:
        return 0.0
    return -np.inf


def log_probability(theta, x, y, xerr, yerr):
    lp = log_prior(theta)
    if not np.isfinite(lp):
        return -np.inf
    return lp - nll(theta, x, y, xerr, yerr)


# initialise MCMC walkers
pos = soln.x + 1e-4 * np.random.randn(32, 3)
nwalkers, ndim = pos.shape

if resample:
    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, log_probability,
        args=(
            binnedDf["RCR_wMean"], binnedDf["RExc"],
            binnedDf["RCR_wMeanErr"], binnedDf["RExcErr"]
        )
    )
    sampler.run_mcmc(pos, 5000, progress=True)

    # check boost in phase and reduce samples accordingly
    tau = sampler.get_autocorr_time()
    print("boost-in phase information:")
    print(tau)
    print("\n")
    thinBy = int(np.mean(tau)/2.)
    discardIndex = int(3*np.mean(tau))
    flat_samples = sampler.get_chain(
        discard=discardIndex, thin=thinBy, flat=True
    )
    np.save("./flatSamplesFit.npy", flat_samples)

else:
    flat_samples = np.load("./flatSamplesFit.npy")

# save fit results
labelsNoFormat = ["p1", "p0", "log(f)"]
resultDicts = []
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)
    resultDicts.append(
        {
            "key": labelsNoFormat[i],
            "mean": mcmc[1],
            "q16": mcmc[0],
            "q84": mcmc[2],
            "+": q[1], "-": q[0],
        }
    )
pRes = pd.DataFrame(resultDicts)
pRes.to_hdf("./perfCorrectionResults.h5", key="perfCorrectionResults")

# print posterior results
print("Posterior results dataframe:")
print(pRes)
print()

# select random samples
optvalues = pRes["mean"].values
inds = np.random.randint(len(flat_samples), size=100)

if recreateDataSpacePlot:
    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        binnedDf["RCR_wMean"], binnedDf["RExc"],
        xerr=binnedDf["RCR_wMeanErr"], yerr=binnedDf["RExcErr"],
        fmt="o", color="k", zorder=1.3, label=None
    )
    ax.plot(
        xvals, optvalues[0]*xvals+optvalues[1], color="tab:blue",
        lw=3, zorder=1.2,
        label="\nbest fit values:\n" +
        "  "+r"$p_{1}=0.333_{-0.046}^{+0.044}$" + " " +
        r"$\frac{\mathrm{evts}/\mathrm{h}}{\mathrm{evts}/\mathrm{min}}$" +
        "\n" + "  "+r"$p_{0}=-28 \pm 12\;$"+"evts/h"
    )
    firstflag = True
    for ind in inds:
        sample = flat_samples[ind]
        if firstflag:
            label = "100 random samples from\nthe MC ensemble"
            firstflag = False
        else:
            label = None
        ax.plot(
            xvals, np.dot(np.vander(xvals, 2), sample[:2]),
            color="tab:purple", alpha=0.1, zorder=1.1, label=label
        )
    ax.legend()
    legendHandels, legendLabels = ax.get_legend_handles_labels()
    legendHandels = [legendHandels[1], legendHandels[0]]
    legendLabels = [legendLabels[1], legendLabels[0]]
    leg = ax.legend(legendHandels, legendLabels)
    for lh in leg.legendHandles:
        lh.set_alpha(1)

    ax.set_xlabel(r"$R_{\mathrm{CR}}$"+" in events/min")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_ylim(40, 85)
    ax.set_xlim(215, 305)
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("RCR_Dependence.pdf")
    fig.savefig("RCR_Dependence.png", dpi=300)

if recreateCorrSpacePlot:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.axvline(
        x=250, ls="-", color="tab:blue", lw=1, zorder=1.1,
        label="reference CR rate:\n250 events/min"
    )
    firstflag = True
    yvalList = []
    for ind in inds:
        sample = flat_samples[ind]
        if firstflag:
            label = "100 random samples\nfrom the MC ensemble"
            firstflag = False
        else:
            label = None
        yvals = perfCorrectionFactor(xvals, sample[0], sample[1])
        yvalList.append(yvals)
        ax.plot(
            xvals, yvals,
            color="tab:purple", alpha=0.1, zorder=1.2, label=label
        )
    # print(yvalList)
    ax.legend()
    legendHandels, legendLabels = ax.get_legend_handles_labels()
    legendHandels = [legendHandels[1], legendHandels[0]]
    legendLabels = [legendLabels[1], legendLabels[0]]
    leg = ax.legend(legendHandels, legendLabels)
    for lh in leg.legendHandles:
        lh.set_alpha(1)

    ax.set_xlabel(r"$R_{\mathrm{CR}}$"+" in events/min")
    ax.set_ylabel(r"$c_{\mathrm{perf}}$")
    ax.set_ylim(0.7, 1.3)
    ax.set_xlim(200, 320)
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("RCR_CorrFactor.pdf")
    fig.savefig("RCR_CorrFactor.png", dpi=300)
