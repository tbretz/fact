import json


class LookUpTable():

    def __init__(self, file=None):
        self.table = dict([])
        if file is not None:
            self.from_file(file)

    def fill(self, signal, background, content):
        key = str((signal, background))
        self.table.update({key: content})

    def access(self, signal, background):
        key = str((int(signal), int(background)))
        return self.table[key]

    def to_file(self, filename):
        with open(filename, 'w+') as file:
            file.write(json.dumps(self.table))

    def from_file(self, filename):
        with open(filename, 'r') as file:
            self.table = json.loads(file.read())
        return self

    def reducedInfo(self, signal, background):
        line = self.access(signal, background)
        return line[1], line[2], line[3][0], line[3][1]
