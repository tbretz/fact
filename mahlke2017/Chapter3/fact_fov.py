from astropy.coordinates import get_moon
from astropy.time import Time
import datetime
import matplotlib.pyplot as plt
from peripylib import figures
import numpy as np

# FACT coordinates
latitude = 28 + 45/60 + 41.9/3600  # 28°45'41.9" N
longitude = -(17 + 53/60 + 28.0/3600) # 17°53'28.0" W

# Date of Observation: Today
# date = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
# Alternatively, we can specify any date using ISO representation, eg
date = '2011-10-11 05:00:0'

# Calculate apparent local sidereal time at FACT coordinates
time = Time(date, format='iso', scale='utc', location=(longitude, latitude))
lst = float(time.sidereal_time('apparent').to_string(unit='degree', decimal=True))

# Short status update
print('At time {!s}, the local sidereal time for FACT at coordinates\
 (17°53\'28.0" W, 28°45\'41.9" N) is {:.2f}°'.format(date, lst))

# The conversions below require latitude and lst in radians
latitude = np.radians(latitude)
lst = np.radians(lst)


# Define two helper functions
def ra_dec_to_mwd(RA, Dec):
    '''
    RA, Dec are the coordinates in degrees
    '''
    ra_origin = 180 # We want 180° RA in the middle of the plot
    ra = np.remainder(RA + 360 - ra_origin, 360) # shift RA values
    ra = np.array(ra)
    ra[ra > 180] -= 360    # scale conversion to [-180, 180]
    ra *= -1    # Using astronomical convention: East to the left
    return np.radians(ra), np.radians(Dec)

def plot_source(RA, dec, name, color='#f46d43', s=5):
    # Converts source coordinates to mollweide projection,
    # annotates the dot with the source's name
    ra, dec = ra_dec_to_mwd(RA, dec)
    ax.scatter(ra, dec, marker='o', color=color, s=s)
    # Every annotation has a transparent white box underneath
    # to improve readability
    if name == 'PKS 0736+017' or name == 'Crab Nebula':
        ax.annotate(name, (ra, dec - 8.5 * np.pi / 180), size=8, ha='center', va='bottom',
                    bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))
    else:
        ax.annotate(name, (ra, dec + 3 * np.pi / 180), size=8, ha='center', va='bottom',
                    bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))

# Initiliaze the figure and axis instances
fig, ax = figures.newfig(projection='mollweide', ratio=0.5)

# ----------------
# Add FACT FoV: We compute the equatorial coordinates of the FoV in the horizontal system

# Define horizontal FoV
azimuth = np.linspace(0, 2*np.pi, 200)  # [0, 360) degree, but for plotting purposes we
                                        # include the last point
altitude = 0.                           # The FoV is defined by the horizon

# Calculate RA and Dec
declination = np.arcsin(np.sin(latitude) * np.sin(altitude)
                        + np.cos(latitude) * np.cos(altitude) * np.cos(azimuth))

# For the RA, we have to compute the hour angle first. We use the arctan2 function to
# choose the correct quadrant
x = -np.sin(azimuth) * np.cos(altitude) / np.cos(declination)
y = (np.sin(altitude) - np.sin(declination) * np.sin(latitude)) / (np.cos(declination) * np.cos(latitude))
hour_angle = np.arctan2(x, y)
right_ascension = lst - hour_angle

# For drawing purposes, we sort the output coordinates by right_ascension.
# Then we perform some black magic to convert between tuples, lists, and zip objects to plot the FoV
plot_ra, plot_dec = ra_dec_to_mwd(np.degrees(right_ascension), np.degrees(declination))
ax.plot(*zip(*[(ra, dec) for ra, dec in sorted(zip(plot_ra, plot_dec))]), color='black', lw=0.7)

# ----------------
# Add FACT's zenith: Sources close to this point in declination will culminate
# close to the zenith, which is important for the data quality
# The zenith moves along the declintaion=latitude line

dec_z = latitude
ra_z = lst
ax.plot(*ra_dec_to_mwd(np.degrees(ra_z), np.degrees(dec_z)),'o',ms=3, color='black')

ax.plot(np.linspace(-np.pi, np.pi, 2), np.ones(2)*latitude, '--', alpha=0.5, color='black', lw=0.7)

# -----------------
# Add sources, using the helper function defined above
sources = {
    # Name:            [RA, Dec, Colour]
    'Mrk 501':         [16*15 + 53/60 + 52/3600, 39 + 45/60 + 38/3600, '#2b83ba'],
    'Mrk 421':         [11*15 + 4 / 60+ 27/3600, 38 + 12/60 + 32/3600, '#f46d43'],
    'Crab Nebula':     [5*15 + 34/60 + 32/3600, 22 + 0 + 52/3600, '#f46d43'],
    'PKS 2155-304':    [21*15 + 58/60 + 52/3600, -30 + 13/60 + 18/3600, '#2b83ba'],
    '1ES 1959+650':    [19*15 + 59/60 + 60/3600, 65 + 8/60 + 55/3600, '#f46d43'],
    '1ES 2344+514':    [23*15 + 47/60 + 5/3600, 51 + 42/60 + 17/3600, '#f46d43'],
    '1H 0323+342':     [3*15 + 24/60 + 41/3600, 34 + 10/60 + 46/3600, '#f46d43'],
    'PKS 0736+017':    [7*15 + 39/60 + 18/3600, 1 + 37/60 + 5/360, '#f46d43']
}

for source, props in sources.items():
    plot_source(props[0], props[1], source, color=props[2])


# ----------------
# Add the ecliptic
epsilon = 23.4392 * np.pi / 180
alpha = np.arange(-180 * np.pi / 180, 180. * np.pi / 180, 0.01)
delta = np.array([np.arctan(np.sin(a)*np.tan(epsilon)) for a in alpha])
ax.plot(alpha, delta, ls='-.', lw=0.7, color='#2b83ba', zorder=-1)

# ---------------
# Add position of Moon

moon = get_moon(time)
plot_source(float(moon.ra.to_string(unit='degree', decimal=True)),
            float(moon.dec.to_string(unit='degree', decimal=True)), 'Moon',
           color='black', s=5)
# ---------------
# Set-up the labels, colors, and sizes

# Tick labels
tick_labels = [r'330$^{\circ}$', '', r'270$^{\circ}$', '', r'210$^{\circ}$', '', r'150$^{\circ}$', '', r'90$^{\circ}$', '', r'30$^{\circ}$']
ax.set_xticklabels(tick_labels, fontsize=8)
for label in ax.get_yticklabels():
    label.set_fontsize(8)
ax.tick_params(axis='x', colors='gray')
ax.tick_params(axis='y', colors='gray')

# Axis labels
ax.set_xlabel('Right Ascension / deg', fontsize=10)
ax.set_ylabel('Declination / deg', fontsize=10)

# Grid and some explanations
plt.grid(True)
ax.annotate(r'Outside FoV', (ra_dec_to_mwd(159.5, -30)), size=5, ha='center', va='bottom', bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0), rotation=-66.5, alpha=0.5)
ax.annotate(r'Inside FoV', (ra_dec_to_mwd(141, -40)), size=5, ha='center', va='bottom', bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0), rotation=-66.5)


figures.savefig('fact_fov', dpi=600, pgf=True)