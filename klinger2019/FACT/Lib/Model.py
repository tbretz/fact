import numpy as np
from scipy.optimize import least_squares
from scipy.integrate import quad
import matplotlib.pyplot as plt
import _pickle as pckl

from KDE import KDE

'''
Model conatins basically a function
two Models can be used to reweight a distribution
either use given parameters, fit or KDE
overall normalisation can be adjusted to data
can be saved/loaded 
'''

class Model:

    def __init__(self, function, parameters=None):
        # factor to normalize function, if input comes from array
        self.funcnorm = 1

        # initialize type of function
        if isinstance(function, str):
            if function == "expo10":
                self.function = self.__expo10
                self.parameters = np.zeros(2)
            if function == "powerlaw":
                self.function = self.__powerlaw
                self.parameters = np.zeros(2)
            if function == "steppowerlaw":
                self.function = self.__steppowerlaw
                self.parameters = np.zeros(5)
            if function == "linear":
                self.function = self.__linear
                self.parameters = np.zeros(2)
            if function == "steplinear":
                self.function = self.__steplinear
                self.parameters = np.zeros(5)
            if function == "uniform":
                self.function = self.__uniform
                self.parameters = np.zeros(1)
            if function == "step":
                self.function = self.__step
                self.parameters = np.zeros(1)
            if function == "gaussian":
                self.function = self.__gaussian
                self.parameters = np.zeros(3)
            if function == "KDE":
                self.function = None
                self.parameters = None
            self.mode = function
        else:
            self.function = function
            if parameters is None:
                print("Paramters not specified (None)")
                self.parameters = None
                self.mode = "error"
            else:
                self.parameters = parameters
                self.mode = "arbitrary"

    def getMode(self):
        return self.mode

    def getNorm(self):
        return self.norm

    # calculate new funcnorm to normalize model to other normalization
    def setNorm(self, norm, a, b, bins=30):
        # function to integrate
        def integrationFunc(x):
            return self.function(x, self.parameters)
        if self.mode == "KDE":
            self.funcnorm = 1. / quad(integrationFunc, a, b)[0]
        else:
            self.funcnorm = 1. / norm / quad(integrationFunc, a, b)[0]
        self.norm = norm

    # calculate new norm from other data
    def setNormFromData(self, histogram):
        width = histogram.getBins()[1] - histogram.getBins()[0]
        newnorm = 1. / (histogram.getNEvents() * width)
        self.setNorm(
            newnorm,
            min(histogram.getBins()), max(histogram.getBins()),
            bins=len(histogram.getCenters())
        )

    # function to evaluate Model at x
    def evaluate(self, x):
        if self.mode == "KDE":
            # KDE is already pdf
            return self.function(x, self.parameters) * self.funcnorm
        else:
            # normalize to get pdf
            return self.norm * self.function(x, self.parameters) * \
                self.funcnorm

    ''' inserting parameters into Model '''
    # obtain parameters from array
    def getParametersFromArray(self, parameters, norm, a, b, bins=30):
        self.parameters = parameters

        # function to integrate
        def integrationFunc(x):
            return self.function(x, parameters)
        self.funcnorm = 1. / norm / quad(integrationFunc, a, b)[0]
        self.norm = norm

    # obtain parameters from Fit
    # histogram: hist + bins from numpy histogram
    def getParametersFromFit(self, histogram, pGuess):

        # pull function for fit
        def pull(p):
            return (
                histogram.getCounts() - self.function(
                    histogram.getCenters(), p
                )
            ) / histogram.getSigCounts()

        lsq = least_squares(pull, pGuess)
        self.parameters = lsq.x

        # set norm
        self.setNormFromData(histogram)
        return lsq

    # obtain whole function from KDE
    # can save KDE to file with filename save
    def getParametersFromKDE(self, data, bandwidth=0.1, n_cv=3, atol=0,
                             rtol=1e-8, bins=30, save="", fromFile="",
                             toTable=True, tableSize=1000):
        if fromFile != "":
            # laod KDE from File
            self.KDE = KDE.fromFile(fromFile)
        else:
            # calculate KDE
            self.KDE = KDE(bandwidth=bandwidth, n_cv=n_cv, atol=atol,
                           rtol=rtol)
            print("Started calculating KDE..")
            self.KDE.fit(data, toTable=toTable, tableSize=tableSize)
            print("Finished calculating KDE!")
        hist, bins = np.histogram(data, bins=bins)
        width = bins[1] - bins[0]
        self.norm = 1. / (np.sum(hist) * width)
        self.function = self.__KDE

        # save calculated KDE to file
        if save != "":
            self.KDE.save(save)

    # loads KDE from file
    def loadKDEFromFile(self, path):
        self.KDE = KDE()
        self.KDE.load(path)
        self.function = self.__KDE

    ''' Plotting routines '''
    # function to plot Model
    def plotpdf(self, xmin, xmax, xlabel="", yscale="log", ylim=None):
        fig = plt.figure()
        ax = plt.gca()
        x = np.linspace(xmin, xmax, 200)
        ax.plot(x, self.evaluate(x))
        ax.set_xlabel(xlabel)
        ax.set_yscale(yscale)
        if isinstance(ylim, (np.ndarray, list, tuple)):
            ax.set_ylim(ylim[0], ylim[1])
        plt.tight_layout()
        return fig

    # function to compare data to Model
    def plotModelWithData(self, data, xlabel="", bins=30, yscale="log",
                          ylim=None):
        fig = plt.figure()
        ax = plt.gca()
        ax.set_xlabel(xlabel)
        ax.set_ylabel("#")
        if self.mode == "KDE":
            h, bins = np.histogram(data, bins=bins)
            label = "KDE"
            center = (bins[:-1] + bins[1:]) / 2
            ax.errorbar(center, h, yerr=np.sqrt(h), fmt=".", label="data")
            x = np.linspace(np.min(data), np.max(data), 200)
            ax.plot(x, self.evaluate(x) / self.norm,
                    label="KDE")
        else:
            label = "Fit"
            # ax.errorbar(center, h, yerr=np.sqrt(h), fmt=".", label="data")
            ax = data.addToAx(ax, label="Data")
            x = np.linspace(np.min(data.centers), np.max(data.centers), 200)
            ax.plot(x, self.evaluate(x) / self.norm,
                    label=label)
        ax.set_yscale(yscale)
        if isinstance(ylim, (np.ndarray, list, tuple)):
            ax.set_ylim(ylim[0], ylim[1])
        ax.legend()
        # ax.set_title(str(int(np.sum(h))) + " Events", loc="right")
        plt.tight_layout()
        return fig

    ''' saving and loading '''
    # save Model to file
    def save(self, filename):
        save = [self.mode,
                self.norm,
                self.funcnorm]
        if self.mode == "KDE":
            save.append(self.KDE)
        else:
            save.append(self.parameters)
        with open(filename, 'wb') as f:
            pckl.dump(save, f)

    # load Model from file
    def loadfromFile(self, filename):
        with open(filename, 'rb') as f:
            load = pckl.load(f)
        if load[0] == "KDE":
            mode, norm, funcnorm, KDE = load
            self.function = self.__KDE
            self.KDE = KDE
            self.parameters = None
        else:
            mode, norm, funcnorm, parameters = load
            if mode == "expo10":
                self.function = self.__expo10
            if mode == "powerlaw":
                self.function = self.__powerlaw
            if mode == "steppowerlaw":
                self.function = self.__steppowerlaw
            if mode == "linear":
                self.function = self.__linear
            if mode == "steplinear":
                self.function = self.__steplinear
            if mode == "uniform":
                self.function = self.__uniform
            if mode == "step":
                self.function = self.__step
            if mode == "gaussian":
                self.function = self.__gaussian
            self.parameters = parameters
        self.norm = norm
        self.mode = mode
        self.funcnorm = funcnorm

    # load model from file
    @classmethod
    def fromFile(cls, filename):
        with open(filename, 'rb') as f:
            load = pckl.load(f)
        returnModel = cls(load[0])
        if load[0] == "KDE":
            mode, norm, funcnorm, KDE = load
            returnModel.KDE = KDE
        else:
            mode, norm, funcnorm, parameters = load
            returnModel.parameters = parameters
        returnModel.norm = norm
        returnModel.mode = mode
        returnModel.funcnorm = funcnorm
        return returnModel

    ''' static methods -> all types of functions '''
    # p0 * 10**(p1*x)
    @staticmethod
    def __expo10(x, p):
        return p[0] * np.power(10, p[1] * x)

    # p0 * x**p1
    @staticmethod
    def __powerlaw(x, p):
        return p[0] * np.power(x, p[1])

    # x <= p0:  p1 * x**p2 , x > p0: p3 * x**p4
    @staticmethod
    def __steppowerlaw(x, p):
        if isinstance(x, np.ndarray):
            return np.less_equal(x, p[0]) * p[1] * np.power(x, p[2]) + \
                np.greater(x, p[0]) * p[3] * np.power(x, p[4])
        elif isinstance(x, (list, tuple)):
            x = np.array(x)
            return np.less_equal(x, p[0]) * p[1] * np.power(x, p[2]) + \
                np.greater(x, p[0]) * p[3] * np.power(x, p[4])
        else:
            return (x <= p[0]) * p[1] * np.power(x, p[2]) + \
                (x > p[0]) * p[3] * np.power(x, p[4])

    # p0 * x + p1
    @staticmethod
    def __linear(x, p):
        return p[0] * x + p[1]

    # x <= p0:  p1 * x + p2 , x > p0: p3 * x + p4
    @staticmethod
    def __steplinear(x, p):
        if isinstance(x, np.ndarray):
            return np.less_equal(x, p[0]) * (p[1] * x + p[2]) + \
                np.greater(x, p[0]) * (p[3] * x + p[4])
        elif isinstance(x, (list, tuple)):
            x = np.array(x)
            return np.less_equal(x, p[0]) * (p[1] * x + p[2]) + \
                np.greater(x, p[0]) * (p[3] * x + p[4])
        else:
            return (x <= p[0]) * (p[1] * x + p[2]) + \
                (x > p[0]) * (p[3] * x + p[4])

    # p0 = const
    @staticmethod
    def __uniform(x, p):
        if isinstance(x, (np.ndarray, list, tuple)):
            return p[0] * np.ones(len(x))
        else:
            return p[0]

    # x <= p0:  p1 = const , x > p0: p2 = const
    @staticmethod
    def __step(x, p):
        if isinstance(x, np.ndarray):
            return np.less_equal(x, p[0]) * p[1] + np.greater(x, p[0]) * p[2]
        elif isinstance(x, (list, tuple)):
            x = np.array(x)
            return np.less_equal(x, p[0]) * p[1] + np.greater(x, p[0]) * p[2]
        else:
            return (x <= p[0]) * p[1] + (x > p[0]) * p[2]

    # p0 * exp(- (x-p1)/p2)
    @staticmethod
    def __gaussian(x, p):
        return p[0] * np.exp(- (x - p[1])**2 / p[2])

    # contains KDE
    def __KDE(self, x, p):
        return self.KDE.evaluate(x)
