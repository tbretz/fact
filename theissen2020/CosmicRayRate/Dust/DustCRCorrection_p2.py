import corner
import emcee
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import minimize
from scipy.stats import gaussian_kde as gkde

import DataFrameBinning as dfb
import setFigureConfig as figConfig
from correctionModels import dustModel, dustCorrectionFactor
from LibFunctions import findFWHM, weightedMean, roundAndFollowDIN
from rootRoutines import dFrameFromRoot

# set flag if mc sampler should be rerun
resample = False
# fix numpy random seed for purposes of reproducibility
np.random.seed(123)

tngBinnedCR = pd.read_hdf(
    "./CRDustBinned.h5", key="CRDustBinned"
)


def logPrior(pars):
    r0, alpha, log_f = pars
    if (
        225 < r0 < 275 and
        0 < alpha < 1 and
        -10.0 < log_f < 1.0
    ):
        return 0.0
    return -np.inf


def negativeLogLikelihood(pars, x, y, yerr):
    r0, alpha, log_f = pars
    model = dustModel(x, r0, alpha, gamma=2.7)
    yerr2 = np.exp(2*log_f) * yerr**2
    ll = -0.5*np.sum((y-model)**2/yerr2 + np.log(yerr2))
    return -ll


def logProbability(pars, x, y, yerr):
    lp = logPrior(pars)
    if not np.isfinite(lp):
        return -np.inf
    ll = - negativeLogLikelihood(pars, x, y, yerr)
    return lp + ll


print(tngBinnedCR)

initial = np.array([250, 3e-3, -2])
soln = minimize(
    negativeLogLikelihood, initial,
    args=(
        tngBinnedCR["TNG_wmean"], tngBinnedCR["KDE_max"],
        tngBinnedCR["KDE_std"]
    )
)
mLPars = soln.x

# print maximum likelyhood results
print("maximum likelyhood results:")
print(mLPars[:-1], np.exp(mLPars[-1]))
print("\n")

pos = mLPars + 1e-4*np.random.randn(32, 3)
nwalkers, ndim = pos.shape

if resample:
    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, logProbability,
        args=(
            tngBinnedCR["TNG_wmean"], tngBinnedCR["KDE_max"],
            tngBinnedCR["KDE_std"]
        )
    )
    sampler.run_mcmc(pos, 5000, progress=True)

    # check boost in phase and reduce samples accordingly
    tau = sampler.get_autocorr_time()
    print("boost-in phase information:")
    print(tau)
    print("\n")
    thinBy = int(np.mean(tau)/2.)
    discardIndex = int(3*np.mean(tau))
    flat_samples = sampler.get_chain(
        discard=discardIndex, thin=thinBy, flat=True
    )

    np.save("./flatSamplesDustFit.npy", flat_samples)

else:
    flat_samples = np.load("./flatSamplesDustFit.npy")


labels = [r"$R_{0}$", r"$\alpha$", r"$\log{f}$"]
labelsNoFormat = ["r0", "alpha", "log(f)"]
resultDicts = []
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)
    resultDicts.append(
        {
            "key": labelsNoFormat[i],
            "mean": mcmc[1],
            "q16": mcmc[0],
            "q84": mcmc[2],
            "+": q[1], "-": q[0]
        }
    )
pRes = pd.DataFrame(resultDicts)
print(pRes)
pRes.to_hdf("../DustCorrectionResults.h5", "DustCorrectionResults")

cDust = np.logspace(-1., 2, 100)
bestFit = dustModel(
    cDust,
    pRes[pRes["key"] == "r0"]['mean'].values,
    pRes[pRes["key"] == "alpha"]['mean'].values,
    gamma=2.7
)
bestFitCorrFactor = dustCorrectionFactor(
    cDust,
    pRes[pRes["key"] == "alpha"]['mean'].values,
    gamma=2.7
)
# select random inds for example fits
inds = np.random.randint(len(flat_samples), size=100)

# calculate fitCurves and cFactors
cFactorsOfSamples = []
trigRatesOfSamples = []

for i in range(len(inds)):
    sample = flat_samples[inds[i]]
    for ind in inds:
        sample = flat_samples[ind]
        trigRatesSample = dustModel(
            cDust,
            sample[0],
            sample[1],
            gamma=2.7
        )
        cFactorsSample = dustCorrectionFactor(
            cDust, sample[1],
            gamma=2.7
        )
        cFactorsOfSamples.append(cFactorsSample)
        trigRatesOfSamples.append(trigRatesSample)
cFactorsOfSamples = np.array(cFactorsOfSamples)

cFactorMeans = np.mean(cFactorsOfSamples, axis=0)
cFactorStds = np.std(cFactorsOfSamples, axis=0, ddof=1)
relCFactorStds = cFactorStds/cFactorMeans


"""
    corner plot
"""

figConfig.fullWidth()
fig = corner.corner(
    flat_samples, labels=labels
)
ndim = np.shape(flat_samples)[1]
axes = np.array(fig.axes).reshape((ndim, ndim))
for j in range(ndim):
    ax = axes[j, j]
    parDf = pRes[pRes["key"] == labelsNoFormat[j]]
    mean = parDf["mean"].values[0]
    left = parDf["q16"].values[0]
    right = parDf["q84"].values[0]
    span = ax.axvspan(
        xmin=left, xmax=right, facecolor="tab:purple", alpha=0.5
    )
    ax.axvline(x=mean, color="tab:purple")
    ax2 = ax.twiny()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_xticks(ax.get_xticks())
    ax2.set_xticklabels([])
    rounded = roundAndFollowDIN(
        mean, [parDf["-"].values[0], parDf["+"].values[0]]
    )
    resultString = "{3}"+r"$= {0}_{{-{1}}}^{{+{2}}}$"
    resultString = resultString.format(
        str(rounded[0]), str(rounded[1][0]), str(rounded[1][1]), labels[j]
    )
    ax2.set_xlabel(resultString)

    for i in range(j):
        ax = axes[j, i]
        parDf2 = pRes[pRes["key"] == labelsNoFormat[i]]
        mean2 = parDf2["mean"].values[0]
        ax.axvline(x=mean2, color="tab:purple")
        ax.axhline(y=mean, color="tab:purple")
        ax.plot(mean2, mean, "s", color="tab:purple")

legend = fig.legend(
    (
        mlines.Line2D(
            [0], [0], marker='s', color='tab:purple', linewidth=2,
            markerfacecolor="tab:purple", markersize=10
        ),
        span
    ),
    (
        "best fit as median of\nthe MC ensemble",
        "uncertainty estimation as\nquantile 16 % to 84 %"
    ),
    loc="upper right",
    framealpha=0.9, bbox_to_anchor=(0.98, 0.98),
    handlelength=3, fontsize=15
)

fig.savefig("./DustCornerResults.png", dpi=300)
fig.savefig("./DustCornerResults.pdf")


"""
    model plot
"""

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.text(
    3, 280,
    r"$R_{\mathrm{CR}}=R_{0}(1+\alpha\cdot c_{\mathrm{dust}})^{-2.7}$",
    fontsize=14
)
ax.plot(
    cDust, bestFit, color="k",
    lw=2, zorder=4/100.+1,
    label="best fit:\n" +
    r"$\alpha= 4.21^{+0.26}_{-0.25}\; \mathrm{m}^{3} \mathrm{ng}^{-1}$"
)

firstflag = True
for i in range(len(inds)):
    if firstflag:
        label = "100 random samples from\nthe MC ensemble"
        firstflag = False
    else:
        label = None
    samplefit = trigRatesOfSamples[i]
    ax.plot(
        cDust, samplefit, lw=0.5,
        color="tab:purple", alpha=0.3, zorder=2/100.+1, label=label
    )
ax.errorbar(
    tngBinnedCR["TNG_wmean"], tngBinnedCR["KDE_max"],
    yerr=tngBinnedCR["KDE_std"], xerr=tngBinnedCR["TNG_wstd"],
    fmt="o", color="tab:blue", zorder=3/100.+1,
    label="KDE maxima in\n"+"corresponding\n"+r"$c_{\mathrm{dust}}$"+" bins"
)
ax.set_xscale("log")
ax.set_xlabel("dust concentration "+r"$c_{\mathrm{dust}}$"+" in µg/m³")
ax.set_xlim(10**(-1.), 10**2)
ax.set_ylim(0, 320)
ax.set_ylabel(r"$R_{CR}$ in events/min")
ax.grid(axis="both", alpha=0.5)
ax.set_axisbelow(True)
leg = ax.legend()
for lh in leg.legendHandles:
    lh.set_alpha(1)
fig.tight_layout()
fig.savefig("./DustFitModel.png", dpi=300)
fig.savefig("./DustFitModel.pdf")

"""
    cFactorsPlot
"""

figConfig.halfWidth(aspectRatio=0.8)
fig = plt.figure()
ax = fig.gca()
ax.plot(
    cDust, bestFitCorrFactor, color="k",
    lw=2, zorder=4/100+1,
    label="best fit:\n" +
    r"$\alpha= 4.21^{+0.26}_{-0.25}\; \mathrm{m}^{3} \mathrm{ng}^{-1}$"
)
firstflag = True
for i in range(len(inds)):
    if firstflag:
        label = "100 random samples from\nthe MC ensemble"
        firstflag = False
    else:
        label = None

    cFactors = cFactorsOfSamples[i]
    ax.plot(
        cDust,
        cFactors,
        lw=0.5,
        color="tab:purple", alpha=0.3, zorder=2/100.+1, label=label
    )
ax.set_xlabel("dust concentration "+r"$c_{\mathrm{dust}}$"+" in µg/m³")
ax.set_xlim(0, 100)
ax.set_ylabel("correction factor"+r"$c_{c}$")
ax.grid(alpha=0.5, axis="both")
ax.set_axisbelow(True)
leg = ax.legend()
for lh in leg.legendHandles:
    lh.set_alpha(1)
fig.tight_layout()
fig.savefig("./DustFitModelCorrFactors.png", dpi=300)
fig.savefig("./DustFitModelCorrFactors.pdf")

"""
    cFactorsStdPlot
"""

fig = plt.figure()
ax = fig.gca()
ax2 = ax.twinx()
ax.plot(
    cDust, cFactorStds,
    color="tab:blue", lw=2,
    label="absolute uncertainty"
)
ax2.plot(
    cDust, relCFactorStds,
    color="tab:purple", lw=2, ls="--",
    label="relative uncertainty"
)

ax.set_ylim(0, 0.12)
ax2.set_ylim(0, 0.05)
ax2.set_ylabel("relative uncertainty "+r"$\sigma_{c_{c}}/c_{c}$")
ax.set_ylabel("absolute uncertainty "+r"$\sigma_{c_{c}}$")

ax.set_xlabel("dust concentration "+r"$c_{\mathrm{dust}}$"+" in µg/m³")
ax.set_xlim(0, 100)
ax.grid(alpha=0.5, axis="both", zorder=10)

fig.legend(loc='upper left', bbox_to_anchor=(0.18, 0.95))
fig.tight_layout()
fig.savefig("./DustFitUncertainty.png", dpi=300)
fig.savefig("./DustFitUncertainty.pdf")
