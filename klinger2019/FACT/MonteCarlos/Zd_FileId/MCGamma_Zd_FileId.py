import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor


OriginalMCFile = "../data/MCGamma_Zd_FileId.root"
dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))


Zd_mins = dp.data.groupby("FileID")["Zd"].min()
Zd_maxs = dp.data.groupby("FileID")["Zd"].max()
Ids = np.unique(dp.data["FileID"])

fig, ax = plt.subplots()
ax.plot(Ids, Zd_mins, linestyle="", marker="^", label="min Zd")
ax.plot(Ids, Zd_maxs, linestyle="", marker="v", label="max Zd")
ax.axhline(y=0, color="k")
ax.axhline(y=30, color="k")
ax.axhline(y=60, color="k")

ax.set_xlabel("FileId")
ax.set_ylabel("Zd")
plt.show()
