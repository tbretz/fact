import numpy as np
import matplotlib as mpl
from cycler import cycler
from matplotlib import rc


def figsize(scale, ratio):
    fig_width_pt = 410.00                          # Get this from LaTeX using \the\textwidth  # 410 for classic thesis
    inches_per_pt = 1.0/72.27                       # Convert pt to inch
    golden_mean = (np.sqrt(5.0)-1.0)/2.0            # Aesthetic ratio (you could change this)
    fig_width = fig_width_pt * inches_per_pt * scale    # width in inches
    fig_height = fig_width * ratio              # height in inches
    fig_size = [fig_width, fig_height]
    return fig_size


my_rc = {
# ----------
# GENERAL SET-UP
'backend'              : 'pgf',         # To export figures to .pgf
'backend_fallback'     : True,          # If not compatible, mpl will find a compatible one
'toolbar'              : 'toolbar2',    # 'toolbar2', 'toolmanager', 'None'
'timezone'             : 'UTC',         # a pytz timezone string, eg US/Central or Europe/Paris
# ----------
# LINES
'lines.linewidth'      : 1.0,           # line width in points
'lines.antialiased'    : True,          # render lines in antialised (no jaggies)
#lines.markeredgewidth  : 0.5           # the line width around the marker symbol
#lines.markersize  : 6                  # markersize, in points
# ----------
# PATCHES
#patch.linewidth        : 1.0           # edge width in points
'patch.linewidth'      : 0.5,           # edge width in points
'patch.facecolor'      : '348ABD',      # blue
'patch.edgecolor'      : 'eeeeee',
'patch.antialiased'    : True,          # render patches in antialised (no jaggies)
# ----------
# LaTeX SET-UP

'text.usetex'          : True,          # use inline math for ticks
'pgf.rcfonts'          : False,         # don't setup fonts from rc parameters
'pgf.preamble':        [
                    r'\usepackage{amsmath}',
                    r'\usepackage[utf8x]{inputenc}',
                    r'\usepackage[T1]{fontenc}',
                    r'\usepackage{txfonts}',         # txfonts are used by A&A Journal
                    r'\usepackage{pgfplots}',
                    r'\usepgfplotslibrary{external}',
                    r'\tikzexternalize'
                    ],

# ----------
# FONTS
'font.size'            : 10.0,  # 10 is default
'figure.figsize'       : figsize(1.0, (np.sqrt(5.0)-1.0)/2.0),
'text.color'           : 'black',
'text.latex.unicode'   : False,         # is handled by inputenc package
'axes.facecolor'       : 'ffffff',      # axes background color
'axes.edgecolor'       : 'b0b0b0',#'b0b0b0',      # axes edge color
'axes.linewidth'       : 0.5,    # edge linewidth
'axes.grid'            : True,   # display grid or not
'axes.titlesize'       : 'large',  # fontsize of the axes title
'axes.labelsize'       : 'medium', # fontsize of the x any y labels
'axes.labelcolor'      : 'black',
'axes.axisbelow'       : True,   # whether axis gridlines and ticks are below the axes elements (lines, text, etc)
'axes.prop_cycle'      :  cycler('color', ['#2b83ba', '#f46d43', '#66c2a5', '#d7191c', '#abdda4', '#fdae61']),
'xtick.major.size'     : 0,      # major tick size in points
'xtick.minor.size'     : 0,      # minor tick size in points
'xtick.major.pad'      : 6,      # distance to major tick label in points
'xtick.minor.pad'      : 6,      # distance to the minor tick label in points
'xtick.color'          : '555555',# '555555' # color of the tick labels
'xtick.labelsize'      : 'small',  # fontsize of the tick labels
'xtick.direction'      : 'in',     # direction: in or out
'ytick.major.size'     : 0,      # major tick size in points
'ytick.minor.size'     : 0,      # minor tick size in points
'ytick.major.pad'      : 6,      # distance to major tick label in points
'ytick.minor.pad'      : 6,      # distance to the minor tick label in points
'ytick.color'          : '555555', # color of the tick labels
'ytick.labelsize'      : 'small',  # fontsize of the tick labels
'ytick.direction'      : 'in',     # direction: in or out
'grid.color'           : 'b0b0b0', # grid color
'grid.linestyle'       : ':',      # dotted
'grid.linewidth'       : '0.2',    # in points
'legend.fontsize'      : 'medium',
'legend.fancybox'      : False,     # if True, use a rounded box for the legend, else a rectangle
}

mpl.rcParams.update(my_rc)
import matplotlib.pyplot as plt





# I make my own newfig and savefig functions
def newfig(rows=1, cols=1, width=1., ratio=(np.sqrt(5.0)-1.0)/2.0, sequential=False, sharex=False, sharey=False, **kwargs):
    '''
    **kwargs are passed down to add_subplot() function
    '''
    if sequential:  # plot in sequential colors instead of diverging ones
        mpl.rc('axes', prop_cycle=cycler('color', ['#bd0026', '#f03b20', '#fd8d3c', '#fecc5c', '#ffffb2'])) # chosen with http://colorbrewer2.org/#type=sequential&scheme=YlGnBu&n=5

    fig, ax = plt.subplots(rows, cols, figsize=figsize(width, ratio), sharex=sharex, sharey=sharey, subplot_kw=kwargs)
    return fig, ax


def savefig(filename, pdf=True, pgf=False, png=False, svg=False, x_ticks_fmt=None, y_ticks_fmt=None, dpi=400):
    # Saves figure as pdf, optionally also as pgf for matplot import
    # Uses matplotlibs tight layout to make sure axis labels are displayed correctly
    plt.tight_layout(.2) # Ensures that everything fits http://matplotlib.org/users/tight_layout_guide.html

    if pdf:
        plt.savefig('{}.pdf'.format(filename), dpi=dpi)
    if pgf:
        plt.savefig('{}.pgf'.format(filename), dpi=dpi)
    if png:
        plt.savefig('{}.png'.format(filename), dpi=dpi)
    if svg:
    	plt.savefig('{}.svg'.format(filename), dpi=dpi)
    plt.close()

def colors(i):
    # Helper function to get the color you want for your line
    return ['#2b83ba', '#f46d43', '#66c2a5', '#d7191c', '#abdda4', '#fdae61'][i]


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.palplot(sns.color_palette())
    savefig('palplot')