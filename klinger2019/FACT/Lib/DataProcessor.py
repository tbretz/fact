import inspect
import os
import numpy as np
import pandas as pd
import _pickle as pckl

from Histogram import Histogram

class DataProcessor:
    mm2deg = 0.0117193246260285378

    def __init__(self, data=None, verbose=True):
        # wrapper for data
        if data is None:
            self.data = pd.DataFrame()
        else:
            self.data = data

        # contains the weights to be considered by default
        self.weights = []
        self.verbose = verbose
        self.InModel = None
        self.OutModel = None

    def updateData(self, data):
        self.data = data

    # returns data or column of data
    def getData(self, columns=None):
        if columns is None:
            return self.data
        else:
            return self.data[columns]

    # returns copy of itself
    def copy(self):
        newDP = DataProcessor(data=self.data, verbose=self.verbose)
        newDP.weights = self.weights
        newDP.updateModels(self.InModel, self.OutModel)
        return newDP

    # return length of data
    def getLen(self):
        return len(self.data.index)

    # save data to hdf file
    def saveData(self, filename, key="data"):
        self.data.to_hdf(filename, key=key)
        print("data saved to file: " + filename)

    # load data from hdf file
    def loadData(self, filename, key="data"):
        self.data = pd.read_hdf(filename, key=key)
        print("data loaded from file: " + filename)

    # add new column called lg + name with log10 of columns
    def calcLogs(self, columns=["Size", "Area", "Energy"]):
        print("calculating Logs:" + ", ".join(columns))
        for column in columns:
            if column in self.data:
                if column == "Area":
                    self.data["logArea"] = np.log10(
                        self.data.Area * self.mm2deg**2
                    )
                elif column == "Areamm":
                    self.data["logAreamm"] = np.log10(self.data.Area)
                else:
                    self.data["log" + column] = np.log10(self.data[column])
            else:
                print("no column in dataframe called:" + column)
        print("finished!")

    # adds lots of columns with the final goal disp/Xi
    def calcDispOverXi(self):
        if self.verbose:
            print("Calculating Disp over Xi..")
        self.data['dx'] = self.data.MeanX - self.data.X*1.02
        self.data['dy'] = self.data.MeanY - self.data.Y*1.02

        self.data['norm'] = np.sqrt(self.data.dx**2 + self.data.dy**2)
        self.data['dist'] = self.data.norm * self.mm2deg

        self.data['lx'] = np.minimum(np.maximum(
            (self.data.CosDelta * self.data.dy -
             self.data.SinDelta*self.data.dx) / self.data.norm, -1), 1)
        self.data['ly'] = np.minimum(np.maximum(
            (self.data.CosDelta * self.data.dx +
             self.data.SinDelta * self.data.dy) / self.data.norm, -1), 1)

        self.data['sgn'] = np.sign(self.data.ly)

        self.data['m3l'] = self.data.M3Long * self.data.sgn * self.mm2deg
        self.data['slope'] = self.data.SlopeLong * self.data.sgn / self.mm2deg

        self.data['sign1'] = self.data.m3l + 0.07
        self.data['sign2'] = (self.data.dist - 0.5) * 7.2 - self.data.slope

        # sign(min(s1, s2)) = (s1<0) || (s2<0)
        self.data['disp_over_xi'] = (np.sign(
            np.minimum(self.data.sign1, self.data.sign2)) *
            (1 - self.data.Width / self.data.Length))
        if self.verbose:
            print(".. finished!")

    # calculates theta squared using the following parameters:
    # default: Marvins optimal parameters with xi = xi0 + xi*slpoe + xi2*Leak1
    # if xi3 != 0 , it will change the last term to xi2*(1 - 1/(xi3*Leak1))
    # if ISDC is True, the ISDC paramters will be used
    def calcThetaSq(self, xi0=1.254, xi1=0.042, xi2=0, xi3=0,
                    ISDC=False):
        if "disp_over_xi" not in self.data:
            self.calcDispOverXi()
        if ISDC:
            if self.verbose:
                print("Calculating Xi with ISDC parameters..")
            # use ISDC parameters
            xi0 = 1.299
            xi1 = 0.0632
            xi2 = 1.67972
            xi3 = 4.86232
            self.data['xi'] = xi0 + xi1 * self.data.slope + \
                xi2 * (1 - 1 / (1 + xi3 * self.data.Leakage1))
        else:
            if xi3 == 0:
                if xi2 == 0:
                    if self.verbose:
                        print("Calculating Xi with 2 parameters..")
                    # use newest method from Marvin
                    self.data['xi'] = xi0 + xi1 * self.data.slope
                else:
                    if self.verbose:
                        print("Calculating Xi with 3 parameters..")
                    # use first method from Marvin
                    self.data['xi'] = xi0 + xi1 * self.data.slope + \
                        xi2 * self.data.Leakage1
            else:
                if self.verbose:
                    print("Calculating Xi with 4 parameters..")
                # use old 4 parameter method
                self.data['xi'] = xi0 + xi1 * self.data.slope + \
                    xi2 * (1 - 1 / (1 + xi3 * self.data.Leakage1))
        if self.verbose:
            print("Calculating theta squared parameters..")
        # calc disp and theta squared
        self.data['Disp'] = self.data.xi * self.data.disp_over_xi
        self.data['thetasq'] = self.data.Disp**2 + self.data.dist**2 - \
            2 * self.data.Disp * self.data.dist * np.sqrt(1 - self.data.lx**2)
        if self.verbose:
            print("..finished!")

    # energy regressor, adds column with lgEnergy
    def calcEnergy(self, filename="EnergyEst/energy_est.pckl", otherPath=None):
        path = ""
        if otherPath is not None:
            path = otherPath
        else:
            path = os.path.dirname(
                inspect.getsourcefile(DataProcessor)
            )
            path += "/" + filename
        with open(path, 'rb') as f:
            getEnergy = pckl.load(f)
        # make sure logSize is already calculated
        if "logSize" not in self.data:
            self.calcLogs(["Size"])
        self.data["logE_reco"] = getEnergy()

    # for data: only take 5min runs
    def doRunDurationCut(self, sigma=1):
        if self.verbose:
            print("Applying Run Duration cut")
        self.data = self.data[self.data["fRunDuration"].between(300-sigma,
                                                                300+sigma)]

    # TNG dust cut
    def doDustCut(self, maxTNGDust=1):
        if self.verbose:
            print("Applying TNGDust cut")
        self.data = self.data[self.data["TNGDust"] < maxTNGDust]

    # cut on column
    def doCut(self, minVal, maxVal, column):
        if self.verbose:
            print("Applying " + column + "cut")
        self.data = self.data[self.data[column].between(minVal, maxVal)]

    # perform quality cuts with standard parameters
    def doQualityCuts(self, NumIslands=3.5, NumUsedPixels=5.5, Leakage1=0.1,
                      Size=True, Area=True):
        if self.verbose:
            print("Applying quality cuts..")
        self.data = self.data[self.data.NumIslands < NumIslands]
        self.data = self.data[self.data.NumUsedPixels > NumUsedPixels]
        self.data = self.data[self.data.Leakage1 < Leakage1]
        if Size:
            self.data = self.data[self.data.Size > 0]
        if Area:
            self.data = self.data[self.data.Area > 0]
        if self.verbose:
            print("..finished!")

    # perform background cut (Size-Area)
    # kind determins parameter and type
    # kind=spectrum: default with Marvins optimal parameters
    # kind=lightcurve: default with Marvins optimal parameters
    # kind=ISDC: with ISDC parameters
    def doSizeAreaCut(
        self, kind="spectrum",
        alphaSpectrum=0.322, betaSpectrum=-1.845,
        alphaLightcurve=0.00115, betaLightcurve=1.554
    ):
        if self.verbose:
            print("Applying " + kind + " BG suppression cuts..")

        if "logSize" not in self.data:
            self.calcLogs(["Size"])

        if kind == "spectrum":
            # make sure logArea is already calculated
            if "logArea" not in self.data:
                self.calcLogs(["Area"])

            # logArea < alpha * logSize + beta
            self.data = self.data[
                self.data.logArea <
                alphaSpectrum * self.data.logSize + betaSpectrum
            ]

        elif kind == "lightcurve":
            # logSize > alpha * Area + beta
            self.data = self.data[
                self.data.logSize >
                alphaLightcurve * self.data.Area + betaLightcurve
            ]
        elif kind == "ISDC":
            alpha = 912
            beta = -1512
            # Area < alpha * logSize + beta
            self.data = self.data[
                self.data.Area <
                alpha * self.data.logSize + beta
            ]

        if self.verbose:
            print("..finished!")

    # perform theta squared cut
    def doThetaSqCut(self, thetasq=0.024):
        if self.verbose:
            print("Applying theta squared cut :" + str(thetasq) + " ..")
        self.thetaSq = thetasq
        if "thetasq" not in self.data:
            self.calcThetaSq()
        self.data = self.data[self.data.thetasq < thetasq]
        if self.verbose:
            print("..finished!")

    # function to calculate product of all weights
    # "all" returns all weights that are available
    # ["w1", ...] uses a list of all given weights
    # anything else returns an array of ones
    def getWeights(self, weightList="all"):
        if isinstance(weightList, str):
            if weightList == "all":
                if len(self.weights) > 0:
                    totalweights = self.data[self.weights[0]]
                else:
                    print("weights = 1")
                    return np.ones(len(self.data.index))
                if len(self.weights) > 1:
                    for weightname in self.weights[1:]:
                        totalweights = totalweights * self.data[weightname]
                return totalweights
            else:
                print("weights = 1")
                return np.ones(len(self.data.index))
        elif isinstance(weightList, (np.ndarray, list, tuple)):
            if len(weightList) > 0:
                totalweights = self.data[weightList[0]]
            else:
                print("weights = 1")
                return np.ones(len(self.data.index))
            if len(weightList) > 1:
                for weightname in weightList[1:]:
                    totalweights = totalweights * self.data[weightname]
            return totalweights
        else:
            print("weights = 1")
            return np.ones(len(self.data.index))

    # update Models, e.g. for adding different weights
    def updateModels(self, InModel, OutModel):
        self.InModel = InModel
        self.OutModel = OutModel

    ''' reweighting '''
    # calculates and returns weights
    def calcWeights(self, column, save=""):
        x = self.data[column].values
        newWeights = self.OutModel.evaluate(x) / self.InModel.evaluate(x)

        # save calculated weights to file
        if save != "":
            with open(save, 'wb') as f:
                pckl.dump(newWeights, f)
        return newWeights

    # adds weights to the dataframe
    def addWeights(self, column, name="weights"):
        self.data[name] = self.calcWeights(column)
        self.weights.append(name)

    # produce histogram
    def getHistogram(self, name, column, useWeights="all",
                     nBins=10, histRange=None, weights=None):
        if self.verbose:
            print("Calculating histogram " + name)
        # consider weights
        totalWeights = self.getWeights(useWeights)
        if weights is not None:
            totalWeights = totalWeights * weights
        # calculate histogram
        hist = Histogram(
            name, self.data[column].values,
            nBins=nBins, histRange=histRange, weights=totalWeights
        )
        return hist
