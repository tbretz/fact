import numpy as np
import pandas as pd
import uproot


def findTreeWithMaxCycle(path, key, prints=True):
    """If a root files contains trees with cycle relicted the keys of
       interest can be selected by only choosing the trees with maximum
       cycle.

    Args:
        path (str): Path to the root file of interest.
        keys (list of str): Keys of the trees for which the maximum cycle ones 
                            shall be selected.
        prints (boolean): If true generates print output

    Returns:
        keysToLoad (bytes): Binary keys to the maximum cycle trees of interest.
    """

    file = uproot.open(path)
    fileKeys = [
        i.decode(encoding='utf-8', errors='strict') for i in file.keys()
    ]

    if len(fileKeys) == 0:
        if prints:
            print("Empty File.")
        return []
    maxCycle = 0
    for fileKey in fileKeys:
        currentKey, cycle = fileKey.split(";")
        if currentKey == key and int(cycle) > maxCycle:
            maxCycle = int(cycle)
    if prints:
        print(key + ";" + str(maxCycle))

    return key + ";" + str(maxCycle)


def dFrameFromRoot(path, keys=["sql"], prints=True):
    """Load data in form of a pandas.Dataframe() object from one or more
       root trees.

    Args:
        path (str): Path to root file containing one or more trees to be read.
        keys (list of strings): List of tree keys that shall be combined to
                                output Dateframe. The trees have to contain the
                                same branches. By default only the "sql" tree
                                with biggest cycle number is chosen.
        print (bool): Enable or Disable printing the progress.

    Returns:
        df (pandas.Dataframe): Dataframe contaning the data from the trees
                               from path with a key in keys.
    """

    if prints:
        print("Load Data from " + str(path))
    file = uproot.open(path)
    fileKeys = [
        i.decode(encoding='utf-8', errors='strict') for i in file.keys()
    ]
    if prints:
        print("Found " + str(len(fileKeys)) + " Trees:")
        print(fileKeys)

    '''
    # cycling of keys seems to be handled by uproot now
    keysToLoad = []
    for key in keys:
        keysToLoad.append(findTreeWithMaxCycle(path, key, prints))
    '''
    keysToLoad = keys

    if prints:
        print("Adding the following keys to Dataframe: ")
        print(keysToLoad)

    dfList = []

    for key in keysToLoad:
        tree = file[key]
        df = tree.pandas.df(["*"])
        dfList.append(df)
    df = pd.concat(dfList)
    if prints:
        print("Dataframe with " + str(len(df.index)) +
              " Events successfully created with following colummns: ")
        print(df.columns.values)

    return df


def treeFromRoot(path, key="sql[0]", prints=True):
    if prints:
        print("Load Data from " + str(path))
    file = uproot.open(path)
    fileKeys = [
        i.decode(encoding='utf-8', errors='strict') for i in file.keys()
    ]
    if prints:
        print("Found " + str(len(fileKeys)) + " Trees:")
        print(fileKeys)
    return file[key]


def evalOnChunkedRootFile(func, path, keys, nChunks, prints=True):
    if prints:
        print("Load Data from " + str(path))
    file = uproot.open(path)
    fileKeys = [
        i.decode(encoding='utf-8', errors='strict') for i in file.keys()
    ]
    if prints:
        print("Found " + str(len(fileKeys)) + " Trees:")
        print(fileKeys)

    keysToLoad = []
    for key in keys:
        keysToLoad.append(findTreeWithMaxCycle(path, key, prints))

    if prints:
        print("Working with the following keys: ")
        print(keysToLoad)

    for key in keysToLoad:
        tree = file[key]
        for arrays in tree.iterate(entrysteps=nChunks):
            func(arrays)
