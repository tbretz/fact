import pandas as pd

import argparse

parser = argparse.ArgumentParser(
    prog="getTestPlasmoids",
    description='return a table of sample plasmoids from hdf file'
)

parser.add_argument(
    "-f", "--file",
    help="Path to hdf file",
    type=str
)
parser.add_argument(
    "-k", "--key",
    help="key for hdf file", default="allPlasmoids_0",
    type=str
)
parser.add_argument(
    "--sBig",
    help="min size of big plasmoids", default=0.075,
    type=float
)
parser.add_argument(
    "--sMedMin",
    help="min size of medium plasmoids", default=0.03,
    type=float
)
parser.add_argument(
    "--sMedMax",
    help="max size of medium plasmoids", default=0.033,
    type=float
)
parser.add_argument(
    "--sSmallMin",
    help="min size of small plasmoids", default=0.005,
    type=float
)
parser.add_argument(
    "--sSmallMax",
    help="max size of small plasmoids", default=0.0051,
    type=float
)

args = parser.parse_args()

allPlasmoids = pd.read_hdf(args.file, key=args.key)

s = allPlasmoids.sort_values(
    ["tau"]
).groupby("index").tail(1).sort_values(["s"])

# biggest plasmoids
print("\n\nbiggest plasmoids (s>%g)" % (args.sBig))
print(s[s["s"] > args.sBig].iloc[::-1])

# medium size plasmoids
print("\n\nmedium plasmoids (%g<s<%g)" % (args.sMedMin, args.sMedMax))
print(s[s["s"].between(args.sMedMin, args.sMedMax)].iloc[::-1])

# small plasmoids
print("\n\nsmall plasmoids (%g<s<%g)" % (args.sSmallMin, args.sSmallMax))
print(s[s["s"].between(args.sSmallMin, args.sSmallMax)].iloc[::-1])

# small plasmoids
print("\n\n inwards directed plasmoids (beta * r <0)")
print(s[s["r"]*s["beta"] < 0].iloc[::-1].iloc[:10])
