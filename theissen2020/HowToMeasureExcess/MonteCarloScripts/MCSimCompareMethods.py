import yaml
import pathlib
import os
import argparse

import matplotlib.colorbar as cbar
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=-1):
    if n == -1:
        n = cmap.N
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({name},{a:.2f},{b:.2f})'.format(
            name=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap


def mapArrayToColor(cmap, values, minVal=0, maxVal=0.9):
    values = values - (min(values)-minVal)
    values = values/max(values)
    values = values*maxVal
    return cmap(values)


def simResultColorScatter(
        df, xCol, yCol, colorCol, cmap, location,
        xLabel="", yLabel="", colorLabel="", title="", savename=""):

    fig, (ax, axCb) = plt.subplots(
        ncols=2, gridspec_kw={"width_ratios": (19, 1)}
    )

    ax.set_title(title)
    ax.scatter(
        df[xCol],
        df[yCol],
        color=mapArrayToColor(cmap, df[colorCol]),
        edgecolor='', label=str(len(df))+" Nights"
    )
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    colorbar = cbar.ColorbarBase(
        axCb,
        norm=mcolors.Normalize(
            vmin=min(df[colorCol]),
            vmax=max(df[colorCol]),
        ),
        cmap=cmap
    )

    colorbar.set_label(r"$\lambda_{Bg}$", rotation=270, labelpad=15)
    fig.tight_layout()
    fig.savefig(location+savename+".png", dpi=300)
    plt.close()


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--configFile", help="Path to input config file", type=str
    )
    args = parser.parse_args()

    configFile = args.configFile

    if configFile is not None:
        config = {}
        with open(configFile, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    else:
        print(
            "Please call the script with -i config.yaml (A valid config file)"
        )

    # Define colormap for colorplots
    cmap = truncate_colormap(plt.get_cmap("viridis"), maxval=0.9)

    # Define input and output data locations
    location = config["savingLocation"]["simulationFolder"]

    # Load Data
    df = pd.read_hdf(
        location+config["savingLocation"]["evalHDF5FileName"],
        key=config["savingLocation"]["evalHDF5ResultsKey"]
    )

    methods = np.array([])

    # True Parameters
    simResultColorScatter(
        df,
        "LambdaExcess", "LambdaBackground", "LambdaBackground",
        cmap, location,
        xLabel=r"$\lambda_{Exc}$", yLabel=r"$\lambda_{Bg}$",
        colorLabel=r"$\lambda_{Exc}$", title="Parameter Distribution",
        savename="ParameterDist"
    )

    # Relative Mean Deviation vs Excess
    for method in ["NW_DevMean", "RWOld_DevMean", "RWNew_DevMean",
                   "3_DevMean", "4_DevMean", "5_DevMean"]:
        simResultColorScatter(
            df,
            "LambdaExcess", method, "LambdaBackground",
            cmap, location,
            xLabel=r"$\lambda_{Exc}$",
            yLabel=r"Mean Deviation $\Delta_{Exc}\,$" +
            r"$in\, Units\,  of\, \, \sigma_{Exc}$",
            colorLabel=r"$\lambda_{Bg}$",
            title="Relative Deviation vs True Exc- Calculation " + method[:-8],
            savename=method
        )

    # Relative Mean Deviation vs Bg
    for method in ["NW_DevMean", "RWOld_DevMean", "RWNew_DevMean",
                   "3_DevMean", "4_DevMean", "5_DevMean"]:
        simResultColorScatter(
            df,
            "LambdaBackground", method, "LambdaExcess",
            cmap, location,
            xLabel=r"$\lambda_{Bg}$",
            yLabel=r"Mean Deviation $\Delta_{Exc}\,$" +
            r"$in\, Units\,  of\, \, \sigma_{Exc}$",
            colorLabel=r"$\lambda_{Exc}$",
            title="Relative Deviation vs True Bg- Calculation " + method[:-8],
            savename=method+"_Bg"
        )

    # Absolute Mean Deviation vs Excess
    for method in ["NW_AbsDevMean", "RWOld_AbsDevMean", "RWNew_AbsDevMean",
                   "3_AbsDevMean", "4_AbsDevMean", "5_AbsDevMean"]:
        simResultColorScatter(
            df,
            "LambdaExcess", method, "LambdaBackground",
            cmap, location,
            xLabel=r"$\lambda_{Exc}$",
            yLabel=r"Mean Deviation $\Delta_{Exc}$",
            colorLabel=r"$\lambda_{Bg}$",
            title="Absolute Deviation vs True Exc- Calculation " + method[:-8],
            savename=method
        )

    # Absolute Mean Deviation vs Measured Excess
    for method in ["NW_AbsDevMean", "RWOld_AbsDevMean", "RWNew_AbsDevMean",
                   "3_AbsDevMean", "4_AbsDevMean", "5_AbsDevMean"]:
        simResultColorScatter(
            df,
            method[:-11]+"_ExcMean", method, "LambdaBackground",
            cmap, location,
            xLabel=r"$\lambda^{rec}_{Exc}$",
            yLabel=r"Mean Deviation $\Delta_{Exc}$",
            colorLabel=r"$\lambda_{Bg}$",
            title="Absolute Deviation vs Rec. Exc - Calculation " +
            method[:-8],
            savename=method+"_VSrecExc"
        )


if __name__ == "__main__":
    main()
