import matplotlib.pyplot as plt

import matplotlib.patches as pats
import numpy as np

positions = np.loadtxt("./../positions.txt", delimiter=",")
fig = plt.figure(figsize=(10, 10))
ax = fig.gca()
# determine line parameters

# wobble pos
r = 0.6
xw = r*np.cos(np.radians(60))+0.12
yw = r*np.sin(np.radians(60))
# shower pos
xs = -1.2
ys = 0
# slope
m = (yw-ys)/(xw-xs)
angle = -np.arctan(m)
# offset
b = yw - m*xw
# size of gaussian
sx = 0.3 + 0.3*np.abs(xs)
sy = 0.3 + 0.3*np.abs(ys)


def gauss2D(loc):
    x, y = loc
    norm = 1
    mu_x = xs
    mu_y = ys
    alpha = angle
    sig_x = sx
    sig_y = sy
    a = np.cos(alpha)**2/(2*sig_x**2) + np.sin(alpha)**2/(2*sig_y**2)
    b = -np.sin(2*alpha)/(2*sig_x**2) + np.sin(2*alpha)/(2*sig_y**2)
    c = np.sin(alpha)**2/(2*sig_x**2) + np.cos(alpha)**2/(2*sig_y**2)
    return norm*np.exp(-a*(x-mu_x)**2-b*(x-mu_x)*(y-mu_y)-c*(y-mu_y)**2)


for i in range(len(positions)):
    hexPos = (positions[i][1], positions[i][2])
    hexColor = (0, 0, gauss2D(hexPos))
    hexagon = pats.RegularPolygon(
        hexPos,
        6, radius=0.06, color=hexColor)
    ax.add_patch(hexagon)

r = 0.6
theta = np.radians(np.array([0, 60, 120, 180, 240, 300]))
for i in range(len(theta)):
    wobblePos = (r*np.cos(theta[i])+0.12, r*np.sin(theta[i]))
    if i == 1:
        circleColor = "tab:red"
    else:
        circleColor = "tab:grey"
    circle = pats.Circle(
        wobblePos, radius=np.sqrt(0.028), color=circleColor, alpha=0.4
    )
    ax.add_patch(circle)
    ax.plot(
        wobblePos[0], wobblePos[1], color=circleColor, marker="x",
        markersize=7, mew=3
    )

ax.set_xlim(-2.4, 2.4)
ax.set_ylim(-2.4, 2.4)
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)

# shower axis
xp = np.linspace(-2.5, 1.5, 50)
yp = m*xp + b
ax.plot(xp, yp, c="r", lw=3)
# ellipse
eli = pats.Ellipse(
    xy=(xs, ys),
    width=2*sx,
    height=2*sy,
    angle=-np.degrees(angle),
    lw=3,
    edgecolor="white",
    facecolor="none"
)
ax.add_patch(eli)
# center of gravity
ax.plot(xs, ys, color="white", marker="x", markersize=10, mew=5)
# draw line with length
# dx = np.cos(angle) * sx
# off = 0.35
# length_x = np.array([xs-dx-np.tan(angle)*off, xs+dx-np.tan(angle)*off])
# ax.plot(
#     length_x, [m*(xs-dx)+b-off, m*(xs+dx)+b-off],
#     color="tab:orange", lw=3
# )
# x_text_length = xs-np.tan(angle)*1.2*off
# ax.text(
#     x_text_length, m*x_text_length+b-1.2*off, "Length",
#     color="tab:orange", fontsize=20,
#     rotation=-np.degrees(angle),
#     verticalalignment="top", horizontalalignment="center"
# )

fig.tight_layout()
plt.savefig("./exampleEvent.png", dpi=300)
plt.savefig("./exampleEvent.pdf")
