# -*- coding: utf-8 -*-
"""
Created on Wed May 29 16:23:23 2019

@author: Georg Schwefer
"""

import numpy as np
from numba import jit, f8
import time
import matplotlib.pyplot as plt
import scipy.interpolate
import Parameter_File_001 as par
import pandas


'''
In dieser Datei sind alle Routinen geschrieben. Eingwbunden werden muss eine Datei mit Parametern als par
'''


def TDMAsolver(a, b, c, d):
    '''
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    and to http://www.cfd-online.com/Wiki/Tridiagonal_matrix_algorithm_-_TDMA_(Thomas_algorithm)
    https://gist.github.com/TheoChristiaanse/d168b7e57dd30342a81aa1dc4eb3e469
    '''
    nf = len(d) # number of equations
    ac, bc, cc, dc = map(np.array, (a, b, c, d)) # copy arrays
    for it in range(1, nf):
        mc = ac[it-1]/bc[it-1]
        bc[it] = bc[it] - mc*cc[it-1] 
        dc[it] = dc[it] - mc*dc[it-1]
        	    
    xc = bc
    xc[-1] = dc[-1]/bc[-1]

    for il in range(nf-2, -1, -1):
        xc[il] = (dc[il]-cc[il]*xc[il+1])/bc[il]

    return np.array(xc)


'''
Definition Grids und Interpolation
'''
    
def y(j):
    return par.ymin*(par.ymax/par.ymin)**((j)/(par.jmax))   #Elektronen 
    
def x(j):
    return par.xmin*(par.xmax/par.xmin)**((j)/(par.jmax))   #Photonen


def ipol_x(ys,N_y,i):
    Xe=[]
    for j in range(par.jmax+1):
        Xe.append(x(j))
    Xe_ipol=np.concatenate((np.array(Xe),np.array(10*par.ymax)),axis=None)
    Ny_ipol=np.concatenate((np.array(N_y[i]),np.zeros(1)),axis=None)
    f=scipy.interpolate.interp1d(Xe_ipol,Ny_ipol)
    return f(ys)

def ipol_y(xs,N_e,i):
    Ye=[]
    for j in range(par.jmax+1):
        Ye.append(y(j)) 
    Ye_ipol=np.concatenate((np.array([0,1]),np.array(Ye)),axis=None)
    Ne_ipol=np.concatenate((np.zeros(2),np.array(N_e[i])),axis=None)
    f=scipy.interpolate.interp1d(Ye_ipol,Ne_ipol)
    return f(xs)
    


'''
Terme Elektronengleichung
'''

def U_T(i,j,N_y):      #Energiedichte Strahlungsfeld
    U=0
    k=0
    while (x(k)<=3/(4*y(j)) and k<par.jmax): 
        U+=x(k)*(x(k+1)-x(k))*N_y[i][k]
        k+=1
    return U

#Synchro und ICS-Thomson zusammen, jeweiliger Koeffizient  
def e_loss(i,j,w,N_y):
    return y(j)*y(j)*(par.lb+(4*U_T(i,j,N_y))/3)

def L_KN(i,j,N_y): #Klein-Nishina Verlust
    xT=3/(4*y(j))
    U=0
    k=0
    while(k<par.jmax):
        if(x(k)>=xT):
            U+=(x(k+1)-x(k))*N_y[i][k]*(1/x(k))
        k+=1
    return U/y(j)


def R(w): #PP-Reaktionsrate
    return 0.652*(np.log(w)*np.heaviside(w-1,0)*(-1+w**2))/(w**3)

def Q_pp(i,j,N_y): # PP-Gewinne
    Sum=0
    l=2*y(j)
    for k in range(par.jmax):
        Sum+=((x(k+1)-x(k))*N_y[i][k]*R(2*y(j)*x(k)))
    S=4*ipol_x(l,N_y,i)*Sum
    return S


'''
Terme für Photonengleichung
'''

def Q_icsT(i,j,N_e,N_y): #ICS Thomson Mast. und Kirk
    border=min(3*x(j)/(4),3/(4*x(j)))
    Qsum=0
    k=0
    while(x(k)<=border and k<par.jmax):
        l=np.sqrt((3*x(j))/(4*x(k)))
        Qsum+=np.abs(N_y[i][k])*ipol_y(l,np.abs(N_e),i)*(x(k+1)-x(k))/(x(k)**(0.5))
        k+=1
    return ((np.sqrt(3)*Qsum)/(4*x(j)**0.5))


def Q_icsKN(i,j,N_e,N_y):#ICS Klein Nishina Mast. und Kirk
    border=3/(4*x(j))
    Qsum=0
    k=0
    while(k<par.jmax):
        if(x(k)>=border):
            Qsum+=(x(k+1)-x(k))*N_y[i][k]*(1/x(k))
        k+=1
    return ((ipol_y(x(j),N_e,i))/x(j))*Qsum 


def fkt(q,C):
    return 2*q*np.log(q)+(1+2*q)*(1-q)+(C**2*q**2*(1-q))/(2*(1+C*q))

def Q_ICS_komp(i,j,N_e,N_y): #ICS komplett ohne Näherungen
    Qsum=0
    n=0
    while(n<par.jmax):
        if(x(j)<=y(n)):
            y_fak=((y(n+1)-y(n))/(y(n)**2))*N_e[i][n]
            k=0
            xsum=0
            E=x(j)/y(n)
            while(k<par.jmax):
                C=4*x(k)*y(n)
                q=(E)/(C*(1-E))
                if(q<=1 and q>=1/(4*y(n)**2)):
                    xsum+=(fkt(q,C)*N_y[i][k]*(x(k+1)-x(k))/(x(k)))
                k+=1
            Qsum+=y_fak*xsum 
        n+=1   
    return ((3*Qsum)/(4))


def L_ssa(i,j,w,N_e): #Synchrotron Self Absorption Verluste
    ycorr=np.sqrt(x(j)/par.b)
    ycorr_up=np.sqrt(x(j+1)/par.b)
    diff=(1/(ycorr_up-ycorr))*((ipol_y(ycorr_up,N_e,i)/ycorr_up**2)-((ipol_y(ycorr,N_e,i)/ycorr**2)))
    return -np.abs((w[i])*par.ssa_const*diff/(x(j)**0.5*par.b**0.5))

def Q_syn(i,j,N_e): #Synchrotron Gewinne
    ycorr=(np.sqrt(x(j)/par.b))
    return (par.lb*ipol_y(ycorr,N_e,i))/(2*x(j)**0.5*par.b**1.5)

def L_esc(i,w):# Escape Term
    return 2/w[i-1]

def L_pp(i,j,N_y):#PP-Verluste
    Sum=0
    for k in range(par.jmax):
        Sum+=(x(k+1)-x(k))*N_y[i-1][k]*R(x(j)*x(k))
    return Sum


'''
Injektion verschiedene Varianten
aktuell ist immer Q
'''

def Q(i,j,w,N_e,Ts,t0,tfin):
    if(i<=(par.tsteps)):
        if(y(j)>=par.ymin_inj and y(j)<=par.ymax_inj):
            delta_Rand=par.ymax_inj**(1-par.p)-par.ymin_inj**(1-par.p) 
            fak=((1-par.p)*y(j)**(-par.p))/delta_Rand
            return fak*(par.Amp2/(par.deltat*w[i]**3))*(w[i+1]**3-w[i]**3)
        else:
            return 0
    else:
        return 0
    
def Q2(i,j,w,N_e): # ALternativ, Plasmoid unabhängig
    if(i<=par.tsteps):
        if(y(j)>=par.ymin_inj and y(j)<=par.ymax_inj):
             delta_Rand=par.ymax_inj**(1-par.p)-par.ymin_inj**(1-par.p)
             fak=par.Amp1*((1-par.p)*y(j)**(-par.p))/delta_Rand
             return fak
        else:
            return 0
    else:
        return 0
    
   
def Q1(i,j,w,N_e,Ts,t0,tfin):
    if(i<=(Ts[int((tfin-t0)/par.deltat_layer)]/par.deltat)):
        Es=0
        for k in range(par.jmax+1):
            Es+=N_e[i][k]*(y(k+1)-y(k))
        if(y(j)>=par.ymin_inj and y(j)<=par.ymax_inj):
            delta_Rand=par.ymax_inj**(1-par.p)-par.ymin_inj**(1-par.p) 
            fak=((1-par.p)*y(j)**(-par.p))/delta_Rand
            return fak*(par.Amp2/(par.deltat))*(3*10**(-12)-Es)
        else:
            return 0
    else:
        return 0
    
"""
Gleichungen aufstellen und lösen
"""


#Elektronentrme für TDMA


def V1_e(j):
    return 0

def V2_e(i,j,w,N_y):
    return 1+e_loss(i,j,w,N_y)*par.deltat*(1/((y(j+1)-y(j))))+L_KN(i,j,N_y)*par.deltat

def V3_e(i,j,w,N_y):
    return -e_loss(i,j+1,w,N_y)*par.deltat*(1/((y(j+1)-y(j))))

def S_e(i,j,w,N_e,N_y,Ts,t0,tfin):
    return N_e[i][j]+Q(i,j,w,N_e,Ts,t0,tfin)*par.deltat+Q_pp(i,j,N_y)*par.deltat



#Photonenen
    
def Prop_y(i,j,w,N_e,N_y):
    return 1+par.deltat*L_esc(i,w)-L_ssa(i,j,w,N_e)*par.deltat+L_pp(i,j,N_y)*par.deltat

def const1_y(i,j,w,N_e,N_y,N_BB):
    return (Q_ICS_komp(i,j,N_e,N_BB)+Q_syn(i,j,N_e))*par.deltat+N_y[i][j]


def const_y(i,j,w,N_e,N_y,N_BB):
    return (Q_icsT(i,j,N_e,N_BB)+Q_icsKN(i,j,N_e,N_BB)+Q_syn(i,j,N_e))*par.deltat+N_y[i][j]
   
    
"""
Thermischer Background
"""

def N_BB(Xs,beta):
    n_BB=[]
    for i in range(par.tsteps):
        n_BB.append((d_p(i,beta)**2*45*np.array(Xs)**2)/(np.pi**4*par.T**4*(np.exp(np.array(Xs)*d_p(i,beta)/(par.T))-1)))
 
    n_BB=par.Amp_BB*np.array(n_BB)    
    return n_BB

'''
Funktion zur Lösung der Differentialgleichungen im Eigensystem
Argumente: Aus MC: Größe,beta,Entstehungs-und Endzeitpunkt des Plasmoid
Rückgabe: Elektronengrid, Elektronenspektrum, Photonengrid, Photonenspetrum
'''


def SSC_Spectrum(w_t,b_co_t,t0,tfin):
    
    Xs=[]
    Ys=[]
    for j in range(par.jmax+1):
        Xs.append(x(j))
        Ys.append(y(j))
        
    Ts_layer=np.linspace(t0,tfin+par.add*par.deltat_layer,((tfin-t0)/par.deltat_layer)+par.add)
  
    #Transformation der Wert aus MC in Eigensystem
    Ts_eigen=[0]    
    for i in range(1,len(Ts_layer)):
       Ts_eigen.append(Ts_eigen[i-1]+(par.deltat_layer)/(gamma(b_co_t[i])))
      
    w_int=scipy.interpolate.interp1d(np.concatenate((Ts_eigen,np.array([int(par.numLc)])),axis=None),np.concatenate((np.array(w_t),np.array([w_t[-1]])),axis=None))
    b_co_int=scipy.interpolate.interp1d(np.concatenate((Ts_eigen,np.array([int(par.numLc)])),axis=None),np.concatenate((np.array(b_co_t),np.array(b_co_t[len(Ts_layer)-1])),axis=None))
    
    w=[w_int(0)*gamma(b_co_int(0))]
    b_co=[b_co_int(0)]
    for i in range(1,par.tsteps):
        w.append(w[i-1]+(w_int(i*par.deltat)-w_int((i-1)*par.deltat))*gamma(b_co_int(i*par.deltat)))
        b_co.append(b_co_int(i*par.deltat))
        
    n_e=[]
    for k in range(par.tsteps):
        n_e.append(np.zeros(par.jmax+1))
    n_y=np.zeros((par.tsteps,par.jmax+1))
    
    '''
    delta_Rand=par.ymax_inj**(1-par.p)-par.ymin_inj**(1-par.p) 
    
    for j in range(par.jmax+1):
        if(y(j)>=par.ymin_inj and y(j)<=par.ymax_inj):
            fak=((1-par.p)*y(j)**(-par.p))/delta_Rand
            n_e[0][j]=fak*(par.Amp2/(par.deltat))*(2*10**(-3))
        else:
            n_e[0][j]=0
     '''

    #SSA=np.zeros((tsteps,jmax+1))

    n_BB=N_BB(Xs,b_co)
    
    
    V1=np.zeros(par.jmax-2)
    V2=np.zeros(par.jmax-1)
    V3=np.zeros(par.jmax-2)
    S=np.zeros(par.jmax-1)


    #Lösen der Gleichungen
    #print(time.time())

    for i in range(1,par.tsteps):#Zeitschritt
        for j in range(1,par.jmax-1): # Befüllen der Matrix für TDMA
            V1[j-1]=(V1_e(j))
            V3[j-1]=(V3_e(i-1,j,w,n_y+n_BB))
        for j in range(1,par.jmax):
            V2[j-1]=(V2_e(i-1,j,w,n_y+n_BB))
            S[j-1]=S_e(i-1,j,w,n_e,n_y,Ts_eigen,t0,tfin)
        A=TDMAsolver(V1,V2,V3,S) #Lösen der e-GLeichung
        for j in range(1,par.jmax):
            n_e[i][j]=A[j-1]
        for j in range(1,par.jmax): 
            n_y[i][j]=const_y(i-1,j,w,n_e,n_y,n_y+n_BB)/Prop_y(i,j,w,n_e,n_y)# Lösen der Photongleichung
            #SSA[i][j]=L_ssa(i,j,w_t,n_e)
        if(i==2):
            t1=time.time()
        if(i==3):
            print('Benötigte Rechenzeit in Sekunden:')
            print((time.time()-t1)*par.tsteps)

    N_Y=[]
    N_E=[]
    for i in range(par.tsteps):
        N_Y.append(np.array(n_y[i])*w[i]**3*par.L**3)
        N_E.append(np.array(n_e[i])*w[i]**3*par.L**3)
   
    
    return Ys,N_E,Xs,N_Y




'''
Definitionen für Transformation
'''



def gamma(b):
    return np.sqrt(1/(1-b**2))

def beta(G):
    return np.sqrt(1-1/G**2)

def v_add(v1,v2):
    return (v1+v2)/(1+v1*v2)

def G_p(i,b_co):
    return gamma(par.b_j)*gamma(b_co[i])*(1+par.b_j*b_co[i]*np.cos(par.Theta_prim))

def theta(i,b_co):
   tan=(b_co[i]*np.sin(par.Theta_prim))/(gamma(par.b_j)*(par.b_j+b_co[i]*np.cos(par.Theta_prim)))
   return np.arctan(tan)

def d_p(i,b_co):
    w=theta(i,b_co)-par.Theta_obs
    F=G_p(i,b_co)*(1-beta(G_p(i,b_co))*np.cos(w))
    return 1/F


'''
Wandelt Ergebnisse des Eigensystems in Beobachtersystem.
Argumente: Frequenzgrid im Eignsystem, Photonenzahl im Eigensystem, beta,Ort,Größe aus MC,Entstehungszeitpunkt aus MC, Endzeitpunkt aus MC
Rückgabewerte: Frequenzen im Observersystem, Zeiten im Observersystem, Intensitätsspektrum im Observersystem, Aufintegrierte Intensitäten
'''

def Obs_Data(Xe,n_y,b_co,Pos,w,t0,tfin):
    
    Ts_layer=np.linspace(t0,tfin+par.add*par.deltat_layer,((tfin-t0)/par.deltat_layer)+par.add)
    Ts=np.linspace(0,par.numLc,par.tsteps)
    N_Y=scipy.interpolate.interp1d(Ts,n_y,axis=0)
    
    
    Ts_eigen=[0]    
    for i in range(1,len(Ts_layer)):
       Ts_eigen.append(Ts_eigen[i-1]+(par.deltat_layer)/(gamma(b_co[i])))
    #Umwandlung n_y in I_y und Xe zu Xe_obs
    
    ts_obs=[t0/(d_p(0,b_co)*gamma(b_co[0]))]
    for i in range(1,len(Ts_layer)):
       ts_obs.append(ts_obs[i-1]+(par.deltat_layer)/(gamma(b_co[i])*d_p(i,b_co)))
     
    Pos_obs=[Pos[0]+ts_obs[0]*par.b_j]   
    for i in range(1,len(Ts_layer)):
        Pos_obs.append(Pos_obs[i-1]+(ts_obs[i]-ts_obs[i-1])*(v_add(b_co[i],par.b_j)))
    '''   
    x_obs=[]   
    for i in range(len(Ts_layer)):
        x_obs.append(Pos[i]/gamma(par.b_j)+par.b_j*ts_obs[i])
    '''
    
    Xe_obs=[]
    I_obs=[]
    for i in range (len(Ts_layer)):
        Xe_obs.append(d_p(i,b_co)*np.array(Xe))
        I_obs.append(Xe_obs[i]**2*N_Y(Ts_eigen[i])*d_p(i,b_co)**2)
        
   
     #Integration für Light Curve    
    Total_Is=[]
    for i in range(len(Ts_layer)):
        Sum=0
        for j in range(par.jmax-1):
            if(Xe_obs[i][j]>=200 and Xe_obs[i][j]<=8*10**5):
                Sum+=((Xe_obs[i][j+1]-Xe_obs[i][j])*x(j)*24.66*np.abs(N_Y(Ts_eigen[i])[j])*d_p(i,b_co)**3)
        Total_Is.append(Sum)    
    
    '''
    T_obs=[]
    x_obs=[]
    for i in range (len(Ts_layer)):
        T_obs.append(gamma(par.b_j)*(Ts_layer[i]-par.b_j*Pos[i]))
        x_obs.append(gamma(par.b_j)*(Pos[i]+par.b_j*Ts_layer[i]))
    '''    
     
     
    
        
   
    '''
    Real_Ts=[]   
    Thetas=[]
    Dops=[]
    for i in range(len(Ts_layer)):
       # Real_Ts.append(((par.numLc-Pos[i])+Ts[i]))
        Thetas.append(theta(i,b_co)-par.Theta_obs)
        #Dops.append(d_p(i,b_co))
    '''    
    '''    
    #Transformation der Ziten ins Beobachtersystem    
    Ts_obs=[Real_Ts[0]]    
    for i in range(1,par.tsteps):
       Ts_obs.append(Real_Ts[i-1]+(Real_Ts[i]-Real_Ts[i-1])/(d_p(i,b_co)*gamma(b_co[i]))) 
    
    Ts_obs=np.array(Ts_obs)
    New_order=np.argsort(Ts_obs)
    Ts_obs=np.sort(Ts_obs)
    I_obs_fin=[]
    for i in range(par.tsteps):
        I_obs_fin.append(I_obs[int(New_order[i])])
    
    '''   


   

    return Xe_obs,np.array(ts_obs),I_obs,Total_Is