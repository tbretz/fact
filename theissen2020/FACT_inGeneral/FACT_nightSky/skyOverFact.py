import datetime

import astropy
import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.coordinates import SkyCoord, get_moon
from astropy.time import Time

import setFigureConfig as figConfig


def toMwPlotConvention(RaInDeg, DecInDeg):
    # Function to convert RA and DEC to plot convention
    # (Mollweide projection)
    ra_origin = 180  # We want 180° RA in the middle of the plot
    ra = np.remainder(RaInDeg + 360 - ra_origin, 360)  # shift RA values
    ra = np.array(ra)
    ra[ra > 180] -= 360  # scale conversion to [-180, 180]
    ra *= -1  # Using astronomical convention: East to the left

    return np.radians(ra), np.radians(DecInDeg)


def extractConstellationPos(name, allConstellationsDf):

    df = allConstellationsDf[allConstellationsDf["Constellation"] == name]
    coords = []
    for index, row in df.iterrows():
        pRa, pDec = toMwPlotConvention(row['RA'], row['dec'])
        coords.append((pRa, pDec))

    return coords


def plotSource(RaInDeg, DecInDeg, name, color='#f46d43', s=7):
    # Converts source coordinates to mollweide projection,
    # annotates the dot with the source's name
    ra, dec = toMwPlotConvention(RaInDeg, DecInDeg)
    ax.scatter(ra, dec, marker='*', color=color, s=s, zorder=30)

    # Every annotation has a transparent white box underneath
    # to improve readability
    if name == "1ES 2344+514":
        ra_shift = 33*np.pi/180
        dec_shift = -8*np.pi/180
    elif name == "1ES 1959+650":
        ra_shift = 10*np.pi/180
        dec_shift = -9*np.pi/180
    elif name == "Crab Nebula":
        ra_shift = 10*np.pi/180
        dec_shift = +9*np.pi/180
    else:
        ra_shift = 0
        dec_shift = -7*np.pi/180

    ax.annotate(
        name, (ra+ra_shift, dec+dec_shift), size=8,
        ha='center', va='center', color="k",
        bbox=dict(facecolor='white', alpha=1, edgecolor='white', pad=0)
    )


def getFovForTimeAndLoc(
        date,
        latitudeInDeg=28 + 45/60 + 41.9/3600,       # 28°45'41.9" N
        longitudeInDeg=-(17 + 53/60 + 28.0/3600)):  # 17°53'28.0" W

    # Calculate apparent local sidereal time at FACT coordinates
    time = Time(
        date, format='iso', scale='utc',
        location=(longitudeInDeg, latitudeInDeg)
    )
    lst = float(
        time.sidereal_time('apparent').to_string(unit='degree', decimal=True)
    )
    # lst = 76.55
    # Short status update
    print('At time {!s}, the local sidereal time for FACT at coordinates\
    (17°53\'28.0" W, 28°45\'41.9" N) is {:.2f}°'.format(date, lst))

    latitude = np.radians(latitudeInDeg)
    lst = np.radians(lst)

    # Define horizontal FoV
    # [0, 360) degree, but for plottingpurposes we include last point
    azimuth = np.linspace(0, 2*np.pi, 200)

    # The FoV is defined by the horizon
    altitude = 0.
    # or is it defined by the maximum observation angle of 80°
    # altitude = 10.

    # Calculate RA and Dec
    declination = np.arcsin(
        np.sin(latitude) * np.sin(altitude)
        + np.cos(latitude) * np.cos(altitude) * np.cos(azimuth)
    )

    # For the RA, we have to compute the hour angle first.
    # We use the arctan2 function to choose the correct quadrant
    x = -np.sin(azimuth) * np.cos(altitude) / np.cos(declination)
    y = (np.sin(altitude) - np.sin(declination) * np.sin(latitude)) / \
        (np.cos(declination) * np.cos(latitude))
    hour_angle = np.arctan2(x, y)
    right_ascension = lst - hour_angle

    # For drawing purposes, we sort the output coordinates by right_ascension.

    plot_ra, plot_dec = toMwPlotConvention(
        np.degrees(right_ascension), np.degrees(declination))

    return plot_ra, plot_dec


# Date of Observation: Today
# date = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
# Alternatively, we can specify any date using ISO representation, eg
# date = '2019-01-05 01:00:0'


# Initiliaze the figure and axis instances
figConfig.fullWidth()
ax = plt.subplot(111, projection='mollweide')
fig = plt.gcf()

"""
# FOV
plot_ra, plot_dec = getFovForTimeAndLoc(date)
# For drawing purposes we perform some black magic to convert between tuples,
# lists, and zip objects to plot the FoV
ax.plot(
    *zip(*[(ra, dec) for ra, dec in sorted(zip(plot_ra, plot_dec))]),
    color='black', lw=0.7
)
ax.annotate(
    r'Outside FoV', (toMwPlotConvention(159.5, -30)),
    size=5, ha='center', va='bottom',
    bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0),
    rotation=-66.5, alpha=0.5
)
ax.annotate(
    r'Inside FoV', (toMwPlotConvention(141, -40)),
    size=5, ha='center', va='bottom',
    bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0),
    rotation=-66.5)


# Add FACT's zenith: Sources close to this point in declination will culminate
# close to the zenith, which is important for the data quality
# The zenith moves along the declintaion=latitude line

dec_z = latitude
ra_z = lst
ax.plot(*toMwPlotConvention(np.degrees(ra_z),
                            np.degrees(dec_z)), 'o', ms=3, color='black')

ax.plot(np.linspace(-np.pi, np.pi, 2), np.ones(2) *
        latitude, '--', alpha=0.5, color='black', lw=0.7)

"""

# Add sources, using the helper function defined above
sources = {
    # Name:            [RA, Dec, Colour]
    'Mrk 501':      [16*15+53/60+52/3600, 39+45/60+38/3600, 'tab:blue'],
    'Mrk 421':      [11*15+4/60+27/3600, 38+12/60+32/3600, 'tab:blue'],
    'Crab Nebula':  [5*15+34/60+32/3600, 22+0+52/3600, 'tab:blue'],
    # 'PKS 2155-304': [21*15+58/60+52/3600, -30+13/60+18/3600, '#2b83ba'],
    '1ES 1959+650': [19*15+59/60+60/3600, 65+8/60+55/3600, 'tab:blue'],
    '1ES 2344+514': [23*15+47/60+5/3600, 51+42/60+17/3600, 'tab:blue'],

    # '1H 0323+342':  [3*15+24/60+41/3600, 34+10/60+46/3600, '#f46d43'],
    # 'PKS 0736+017': [7*15+39/60+18/3600, 1+37/60+5/360, '#f46d43']
}

for source, props in sources.items():
    plotSource(props[0], props[1], source, color=props[2])


# ----------------
# Add the ecliptic
epsilon = 23.4392 * np.pi / 180
alpha = np.arange(-180 * np.pi / 180, 180. * np.pi / 180, 0.01)
delta = np.array([np.arctan(np.sin(a)*np.tan(epsilon)) for a in alpha])
ax.plot(alpha, delta, ls='-.', lw=0.7, color='tab:green', zorder=-1)

# Add galactic plane
galPlane = np.linspace(-35, 70, num=100)
gc = [
    SkyCoord(l=ll*u.degree, b=0*u.degree, frame='galactic') for ll in galPlane
]
for element in gc:
    ra, dec = toMwPlotConvention(element.fk5.ra.value, element.fk5.dec.value)
    ax.scatter(
        ra, dec, marker='o', color="tab:purple", s=8,
        alpha=0.6, zorder=5
    )

center = SkyCoord(l=0*u.degree, b=0*u.degree, frame='galactic')
ra, dec = toMwPlotConvention(center.fk5.ra.value, center.fk5.dec.value)
ax.scatter(ra, dec, marker='*', edgecolor="k",
           color="tab:purple", s=40, zorder=5)
ax.annotate(
    "GC", (ra-10*np.pi/180, dec-6*np.pi/180), size=7,
    ha='center', va='center', rotation=-40., color="k",
    bbox=dict(
        facecolor='white', alpha=1, edgecolor='white', pad=0
    )
)


"""
# Add position of Moon
moon = get_moon(time)
plotSource(
    float(moon.ra.to_string(unit='degree', decimal=True)),
    float(moon.dec.to_string(unit='degree', decimal=True)),
    'Moon', color='black', s=5
)

# Load constallations
constellations = pd.read_hdf("./ConstellationList.h5", key="constellations")
ori = extractConstellationPos("Ori", constellations)
[ax.scatter(o[0], o[1], s=1, color="white") for o in ori]

cyg = extractConstellationPos("Cyg", constellations)
[ax.scatter(o[0], o[1], s=1, color="white") for o in cyg]
"""

# Tick labels
tick_labels = [r'330$^{\circ}$', '', r'270$^{\circ}$', '', r'210$^{\circ}$',
               '', r'150$^{\circ}$', '', r'90$^{\circ}$', '', r'30$^{\circ}$']
ax.set_xticklabels(tick_labels, fontsize=8)
for label in ax.get_yticklabels():
    label.set_fontsize(8)
ax.tick_params(axis='x', colors='gray')
ax.tick_params(axis='y', colors='gray')

# Axis labels
ax.set_xlabel('Right Ascension / deg', fontsize=10)
ax.set_ylabel('Declination / deg', fontsize=10)

# Grid and some explanations
plt.grid(lw=0.3, color="tab:grey")


fig.savefig('fact_fov_mollweide.pdf')
