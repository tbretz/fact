import setFigureConfig as figConfig
import matplotlib.pyplot as plt
import numpy as np


def plotCorrectionFactorDist(
    runsCR, column, xlabel, saveTo, bins=None, full=False, legend=True
):
    if full:
        figConfig.fullWidth()
    else:
        figConfig.halfWidth()
    if bins is None:
        bins = np.linspace(0.7, 3.5, num=50)

    t_eff = np.sum(runsCR["fEffectiveTime_h"])
    binwidth = bins[1]-bins[0]
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        runsCR[column],
        bins=bins,
        width=0.85*binwidth,
        label="total effective observation time: " +
        str(int(round(t_eff, 0)))+"h\n" +
        "total number of runs: "+str(len(runsCR))
    )
    ax.set_ylabel("number of runs per bin")
    ax.set_xlabel(xlabel)
    ax.set_xlim(bins[0]-binwidth, bins[-1]+binwidth)
    ax.grid(axis="y", alpha=0.5)
    if legend:
        ax.legend()
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig(saveTo+".png", dpi=300)
    fig.savefig(saveTo+".pdf")

    return fig, ax
