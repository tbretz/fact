import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import datetime

from PlasmoidStatistics import createHistograms
from readYAML import loadConfiguration
from Histogram import Histogram
from HistogramManager import HistogramManager
import coloring as cl
import setFigureConfig as sfc

configPath = "steadyState.yaml"
calcHists = 0
plotHists = 1

if calcHists:
    configs = loadConfiguration(configPath)
    name = configs["name"]
    repetitions = int(configs["repetitions"])

    sRange = [-3.125, -0.375]
    sNBins = 11
    pRange = [-0.1, 3.5]
    pNBins = 18
    hm = HistogramManager()
    nBins = 10
    binSize = 2
    tauMins = np.arange(nBins) * binSize
    tauMaxs = tauMins + binSize
    for j in range(nBins):
        # calculate mean of histograms
        hs_mean = Histogram(
            "log10s_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j]),
            histRange=sRange, nBins=sNBins
        )
        hp_mean = Histogram(
            "p_normed_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j]),
            histRange=pRange, nBins=pNBins,
        )
        for i in range(repetitions):
            print("calculating histogram of " + name + " " + str(i))

            hs, hp = createHistograms(
                configPath, i, tauMin=tauMins[j], tauMax=tauMaxs[j],
                sRange=sRange, sNBins=sNBins, pRange=pRange, pNBins=pNBins
            )
            hm.addHistogram(
                hs.getLabel(), hs
            )
            hm.addHistogram(
                hp.getLabel(), hp
            )
            hs_mean += hs
            hp_mean += hp
        hs_mean = hs_mean / repetitions
        hs_mean.setLabel(
            "log10s_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        )
        hp_mean = hp_mean / repetitions
        hp_mean.setLabel(
            "p_normed_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        )
        hm.addHistogram(
            hs_mean.getLabel(), hs_mean
        )
        hm.addHistogram(
            hp_mean.getLabel(), hp_mean
        )
    # save everything
    hm.saveHistogramDict(name + "2_histograms.p")

if plotHists:
    configs = loadConfiguration(configPath)
    name = configs["name"]
    repetitions = int(configs["repetitions"])
    hm = HistogramManager()
    hm.loadHistogramDict(name + "2_histograms.p")

    sfc.fullWidth(aspectRatio=1)
    # fig, (ax, axcb) = plt.subplots(
    #     ncols=2, gridspec_kw={"width_ratios": [19, 1]},
    #     constrained_layout=True
    # )
    fig = plt.figure(constrained_layout=True)
    gs = fig.add_gridspec(nrows=3, ncols=20)
    ax = fig.add_subplot(gs[0, :19])
    axp = fig.add_subplot(gs[1, :19])
    axcb = fig.add_subplot(gs[:, 19])

    ax.set_xlabel(r"$\log_{10} (w/L)$")
    ax.set_ylabel(r"$\frac{N[\log_{10} (w/L)] }{N_{Isl}} $")
    ax.set_yscale("log")
    ax.set_title(r"$\sigma = %g$" % (configs["eom"]["sigma"]), loc="right")
    ax.set_title(r"mean of %g realizations" % (repetitions), loc="center")

    axp.set_xlabel(r"$|p|/\sqrt{\sigma}$")
    axp.set_ylabel(r"$N(|p|/\sqrt{\sigma}) / N_{Isl}$")
    axp.set_yscale("log")

    ax.grid()
    axp.grid()

    nBins = 10
    binSize = 2
    tauMins = np.arange(nBins) * binSize
    tauMaxs = tauMins + binSize
    cmap = cl.c_time(gamma=0.3)
    ax.set_prop_cycle(
        'color',
        cmap(np.linspace(0, 1, nBins))
    )
    axp.set_prop_cycle(
        'color',
        cmap(np.linspace(0, 1, nBins))
    )
    for j in range(nBins):
        sName = "log10s_" + name + "_mean"
        sName += "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        ax = hm.get(sName).addToAx(ax, style="stairs", zorder=5)
        pName = "p_normed_" + name + "_mean"
        pName += "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        axp = hm.get(pName).addToAx(axp, style="stairs", zorder=5)

    # colorbar
    bounds = np.linspace(0, 20, nBins+1)
    cb2 = cb.ColorbarBase(
        axcb, cmap=cmap,
        boundaries=bounds,
        values=np.linspace(0, 20, nBins),
        ticks=np.linspace(0, 20, nBins+1),
        orientation='vertical'
    )
    cb2.set_label(r"$\frac{c}{L}t$", rotation=0, labelpad=10)

    fig.savefig("SteadyStatePlot.pdf")
