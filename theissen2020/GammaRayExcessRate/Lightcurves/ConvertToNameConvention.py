import pandas as pd
from consoleOutput import printDfKeys


def convert(file, newfile):
    print()
    print("Load old file with key 'LightCurve' from: "+file+"\n")
    oldLC = pd.read_hdf(
        file,
        "LightCurve"
    )
    print("It had these keys:")
    printDfKeys(oldLC)

    newLC = pd.DataFrame()
    newLC["fNight"] = oldLC["fNight"]
    newLC["mjd"] = oldLC["mjd"]
    newLC["N_Runs"] = oldLC["NumberOfRuns"]
    newLC["t_total"] = oldLC["TotalDuration"]
    newLC["RExcCorr"] = oldLC["ExcessRate"]
    newLC["RExcErrCorr"] = oldLC["ExcessRateErr"]

    print("The new file has these keys:")
    printDfKeys(newLC)

    newKey = (newfile.split("/")[-1])[:-3]
    print("It will be saved in: "+newfile)
    print("The key is the file name: " + newKey)

    newLC.to_hdf(
        newfile,
        key=newKey
    )


convert("./Mrk501_ICRC2019_original.h5", "./Mrk501_ICRC2019.h5")
convert("./Mrk421_ICRC2019_original.h5", "./Mrk421_ICRC2019.h5")
