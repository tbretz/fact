from astroML.time_series import lomb_scargle
import matplotlib.pyplot as plt
from peripylib import figures, query_fact, detect_peaks
import numpy as np
from scipy import signal


# Plot the spectral windows of Mrk 421, Mrk 501, and the Crab Nebula observation dates of FACT
# Compute the convolution of a random-walk process with the spectral window of Mrk 421

def power_law(f, A, alpha, c):
    return A * np.power(f, -alpha) +  c


def compute_spectral_window(t):
    '''
    t: array of observation times
    '''

    # Compute window function
    # Timestamps are FACT MJDs filled with dates when source was not observed

    window_time = []
    previous_night = t.min() - 0.5
    for ti in t:
        while ti - previous_night > 1:
            window_time.append(previous_night + 1)
            previous_night += 1
        window_time.append(ti)

    window_time = np.asarray(window_time)
    window_func = np.array([1 if wi in [ti for ti in t] else 0 for wi in window_time])

    # Set up trial frequency grid
    # pmin is calculated as 2p with p being the minimal time step between data points,
    pmin = 2*np.min(np.diff(t))  # in days
    pmax = int(sorted(t)[-1]-sorted(t)[0])

    N = len(window_func)                                # Number of trial frequencies is equal to number of data points
    period = 10 ** np.linspace(np.log10(pmin), np.log10(pmax), N)
    omega = 2 * np.pi / period

    spectral_window = lomb_scargle(window_time, window_func, 1, omega, subtract_mean=False, generalized=False)
    return period, spectral_window


def plot_spectral_window(period, spectral_window, source):
    '''
    period and spectral window are computed in the compute_spectral_window function
    '''

    # Temporarily change some plot settings to remove grid lines and add ticks
    with plt.rc_context({'xtick.major.size'     : 4,      # major tick size in points
                         'xtick.minor.size'     : 2,      # minor tick size in points
                         'ytick.major.size'     : 4,      # major tick size in points
                         'ytick.minor.size'     : 2,      # minor tick size in points
                         'axes.labelpad'        : 3,
                        }):
        fig, ax = figures.newfig(ratio=0.5)

        ax.plot(period, spectral_window, color='black', lw=0.75)

        # Mark hgihest peaks with the period
        minimum_peak_heights = {'Mrk421': 0.04, 'Mrk501': 0.025, 'Crab': 0.05}
        peaks = detect_peaks.detect_peaks(spectral_window, mph=minimum_peak_heights[source])

        for i, p in enumerate(peaks):
            ax.annotate('{:.1f}'.format(period[p]), (period[p] * 0.87 if i % 2 == 0 else period[p] * 1.13, spectral_window[p] + 0.004),
                        color=figures.colors(0), size=8, ha='center', va='bottom', bbox=dict(facecolor='white', alpha=0.5,
                        edgecolor='white', pad=0))

        ax.set(xlabel='Period / days', ylabel='LS Power', xscale='log', ylim=(-0.01, 0.25))
        figures.savefig('spectral_window_%s' % source, pgf=True)


def plot_convolution(period, spectral_window, source):
    '''
    Computes and plots the convolution of the spectral window with a power-law PSD
    to illustrate the influence of uneven-sampling
    '''

    freq = 1/period

    # Power-law PSD
    PSD_params = [1e-5, 1.5, 0.]
    PSD = power_law(freq, *PSD_params)

    # Plot
    fig, ax = figures.newfig(ratio=0.5)

    # Only plot every 4th value for readability
    ax.plot(freq[::4], spectral_window[::4], ls='-', label='Spectral Window %s' % source, color='gray')
    ax.plot(freq[::4], PSD[::4], ls='--', label='PSD', color=figures.colors(3), lw=1.5)

    # -------
    # Convolution
    convolution = signal.convolve(PSD, spectral_window, mode='same') / sum(spectral_window)
    ax.plot(freq, convolution, ls='-', label='Convolution', lw=1.5)


    ax.legend(frameon=False)
    ax.set(xscale='log', yscale='log', xlabel='Frequency', ylabel='LS Power')

    figures.savefig('convolution_spectral_window_%s' % source, pgf=True)


if __name__ == '__main__':

    # ----------
    # Read in FACT data
    for source in ['Mrk421', 'Mrk501', 'Crab']:

        # ------
        # Get latest data, apply cuts and corrections
        runs, nightly = query_fact.query_fact(source=source, database='ISDC', verbose=True)

        period, spectral_window = compute_spectral_window(nightly.mjd)

        plot_spectral_window(period, spectral_window, source)

        if source == 'Mrk501':
            plot_convolution(period, spectral_window, source)

