import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.stats import bayesian_blocks
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable

import ModifiedJulianDay as mjd
import performancePeriodsPlots as ppp
import setFigureConfig as figConfig
from LibFunctions import weightedMean
from rootRoutines import dFrameFromRoot
from secondAxis import createYearAxisToMjdAxis

onlyFrom2014 = True

if onlyFrom2014:
    folder = "./From2014/"
else:
    folder = "./AllData/"

recreateDistPlots = False
recreateCleaningPlot = False
recreateRefCalcPlots = False
recreateCRp0ScanPlots = True
recreateBBResultPlots = False
recalculateBW = False

p0ChoiceCR = 0.01

runsCR = pd.read_hdf("../CorrectedCRRate.h5", key="CorrectedCRRate")
runsCR = runsCR[runsCR["fTNGDust"] < 100]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 80]
runsCR = runsCR[runsCR["RCR_corr"] > 125]
runsCR = runsCR[runsCR["RCR_corr"] < 375]

if onlyFrom2014:
    runsCR = runsCR[runsCR["fNight"] > 20140101]

# Rebinning

nightsCRG = runsCR.groupby("fNight")
nightsCR = pd.DataFrame()
nightsCR["mean"] = nightsCRG["RCR_corr"].apply(lambda group: np.mean(group))
nightsCR["std"] = nightsCRG["RCR_corr"].apply(
    lambda group: np.std(group, ddof=1))
nightsCR["N"] = nightsCRG["RCR_corr"].apply(lambda group: len(group))
nightsCR["meanErr"] = nightsCR["std"]/np.sqrt(nightsCR["N"])
nightsCR["fR750Ref"] = nightsCRG["fR750Ref"].apply(
    lambda group: np.unique(group)[0] if len(np.unique(group)) == 1 else None
)
wMean = nightsCRG.apply(
    lambda group: weightedMean(group.RCR_corr, group.RCR_err_corr)
).apply(pd.Series, index=["wMean", "wMeanErr"])
nightsCR = nightsCR.merge(
    wMean, how="outer", left_on="fNight", right_on="fNight"
)
nightsCR = nightsCR.reset_index()
nightsCR["mjd"] = mjd.fNight_to_mjd(nightsCR["fNight"])
nightsCR = nightsCR.reset_index()
nightsCR = nightsCR.dropna()

# Distributions and cleaning
meanCut = 180
nCut = 12
stdCut = 17

if recreateDistPlots:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.set_xlabel("number of runs per night")
    ax.set_ylabel("number of nights per bin")
    ax.set_xlim(0, 120)
    ax.set_ylim(0, 40)
    ax.hist(
        nightsCR["N"], bins=np.linspace(0, 120, num=121),
        color="tab:blue", zorder=1.1, label=None
    )
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    ax.axvline(x=nCut, color="tab:red", lw=2,
               label=r"$N>$"+str(nCut), zorder=1.2)
    ax.legend()
    fig.tight_layout()
    fig.savefig(folder+"NDist.png", dpi=300)
    fig.savefig(folder+"NDist.pdf")

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.set_xlabel("standard deviation " + r"$\sigma_{R}$"+" in events/min")
    ax.set_ylabel("number of nights per bin")
    ax.set_xlim(0, 50)
    ax.hist(
        nightsCR["std"], bins=np.linspace(0, 49, num=50),
        color="tab:blue", label=None, zorder=1.1
    )
    ax.axvline(
        x=stdCut, color="tab:red", lw=2,
        label=r"$\sigma_{R}<$"+str(stdCut)+" events/min", zorder=1.2
    )
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    fig.tight_layout()
    fig.savefig(folder+"StdDist.png", dpi=300)
    fig.savefig(folder+"StdDist.pdf")

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.set_xlabel(
        "weighted mean "+r"$R^{\mathrm{w}}_{\mathrm{CR}}$"+"in events/min"
    )
    ax.set_ylabel("number of nights per bin")
    ax.set_xlim(130, 350)
    ax.set_ylim(0, 200)
    ax.hist(
        nightsCR["wMean"], bins=np.linspace(120, 340, num=50),
        color="tab:blue", label=None, zorder=1.1
    )
    ax.axvline(
        x=meanCut, color="tab:red", lw=2, zorder=1.2,
        label=r"$R^{\mathrm{w}}_{\mathrm{CR}}>$"+str(meanCut)+" events/min"
    )
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    ax.legend()
    fig.tight_layout()
    fig.savefig(folder+"wMeanDist.png", dpi=300)
    fig.savefig(folder+"wMeanDist.pdf")

orgLen = len(nightsCR)

highStdCR = nightsCR[nightsCR["std"] > stdCut]
lowMeanCR = nightsCR[nightsCR["wMean"] < meanCut]
lowRunNumbers = nightsCR[nightsCR["N"] < nCut]
highStdShare = len(highStdCR)/orgLen*100
lowMeanShare = len(lowMeanCR)/orgLen*100
lowRunNumberShare = len(lowRunNumbers)/orgLen*100

highStdCR = nightsCR[nightsCR["std"] > stdCut]
nightsCR = nightsCR[nightsCR["std"] <= stdCut]
lowMeanCR = nightsCR[nightsCR["wMean"] < meanCut]
nightsCR = nightsCR[nightsCR["wMean"] >= meanCut]
lowRunNumbers = nightsCR[nightsCR["N"] < nCut]
nightsCR = nightsCR[nightsCR["N"] >= nCut]

finLen = len(nightsCR)

totalRejected = (orgLen-finLen)/orgLen*100

print(highStdShare, lowMeanShare, lowRunNumberShare, totalRejected)

stableNightsRunsCR = runsCR[runsCR["fNight"].isin(nightsCR.fNight)]

if recreateCleaningPlot:
    figConfig.fullWidth(aspectRatio=1/1.5)
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        lowMeanCR["mjd"], lowMeanCR["wMean"], yerr=lowMeanCR["std"],
        color="tab:orange", fmt=".", label="low mean", zorder=1.1
    )
    ax.errorbar(
        highStdCR["mjd"], highStdCR["wMean"], yerr=highStdCR["std"],
        color="tab:purple", alpha=0.7, fmt=".", label="high variance",
        zorder=1.2
    )
    ax.errorbar(
        lowRunNumbers["mjd"], lowRunNumbers["wMean"],
        yerr=lowRunNumbers["std"],
        color="tab:green", fmt=".", label="low run Number", zorder=1.3
    )
    ax.errorbar(
        nightsCR["mjd"], nightsCR["wMean"], yerr=nightsCR["std"],
        color="tab:blue", fmt=".", zorder=1.4
    )
    ax.set_xlim(min(nightsCR["mjd"]), max(nightsCR["mjd"]))
    ax2 = createYearAxisToMjdAxis(ax, yearlabel=False)
    ax.set_ylim(100, 380)
    ax.set_xlabel("modified julian day")
    ax2.grid(axis="x", alpha=0.5)
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    handels, labels = ax.get_legend_handles_labels()
    handels = [handels[1], handels[0], handels[2]]
    labels = [labels[1], labels[0], labels[2]]
    ax_div = make_axes_locatable(ax)
    ax_div2 = make_axes_locatable(ax2)
    ax_bot2 = ax_div2.append_axes("top", size="3%", pad="15%")
    ax_bot = ax_div.append_axes("top", size="3%", pad="15%")
    ax_bot.legend(handels, labels, ncol=3,
                  loc="center", handletextpad=0)
    ax_bot.axis("off")
    ax_bot2.axis("off")
    fig.tight_layout()
    fig.savefig(folder+"CRCleaned.png", dpi=300)
    fig.savefig(folder+"CRCleaned.pdf", dpi=300)


"""
-----------------------------------------------------------------------------
read out periods from data base
-----------------------------------------------------------------------------
"""

periodsDani = []
refValDani = []


def readOutPeriodsAndRefVals(periodGroup):
    if len(periodsDani) == 0:
        periodsDani.append(mjd.fNight_to_mjd(np.min(periodGroup.fNight)))
    uniques = np.unique(periodGroup.fR750Ref)
    if len(uniques) == 1:
        periodsDani.append(mjd.fNight_to_mjd(np.max(periodGroup.fNight)))
        refValDani.append(uniques[0])
    else:
        for i in range(len(uniques)):
            uPG = periodGroup[periodGroup["fR750Ref"] == uniques[i]]
            refValDani.append(uniques[i])
            periodsDani.append(mjd.fNight_to_mjd(np.max(uPG.fNight)))


runsCR.groupby("fPeriod").apply(
    lambda r: readOutPeriodsAndRefVals(r)
).values

periodsDani, kickedInd = ppp.reduceBlocksByAllowedMinDuration(
    np.array(periodsDani), 2)
for index in sorted(kickedInd, reverse=True):
    del refValDani[index]

table = ppp.generateTableFromPeriods(periodsDani, refValDani)
table.to_html(folder+"DaniTable.html")


"""
-----------------------------------------------------------------------------
determine periods based on bayesian blocks
-----------------------------------------------------------------------------
"""

allBayesBlockEdges = [np.min(nightsCR["mjd"])]
bayesianEdges = bayesian_blocks(
    nightsCR["mjd"],
    x=nightsCR["wMean"],
    sigma=nightsCR["std"],
    fitness="measures", p0=p0ChoiceCR
)
for edge in bayesianEdges:
    allBayesBlockEdges.append(edge)
allBayesBlockEdges.append(np.max(nightsCR["mjd"]))

allBayesBlockEdges, kickedInd2 = ppp.reduceBlocksByAllowedMinDuration(
    allBayesBlockEdges, 7
)
print(len(kickedInd2))

refValMean, refValKdeG, refValKdeA, refFigs = ppp.calcNewRefsfromBB(
    allBayesBlockEdges, stableNightsRunsCR, runsCR,
    withPlots=recreateRefCalcPlots
)

if recreateRefCalcPlots:
    refCalcPDF = PdfPages(folder+"BaysianBlockRefCalcs.pdf")
    for i in range(len(refFigs)):
        refCalcPDF.savefig(refFigs[i])
        if i == 13:
            refFigs[i].savefig(folder+"refCalcP14.pdf")
            refFigs[i].savefig(folder+"refCalcP14.png", dpi=300)
    refCalcPDF.close()
    plt.close()


perfPeriodsTable = ppp.generateTableFromPeriods(allBayesBlockEdges, refValKdeA)
perfPeriodsTable.to_html(folder+"NewTable.html")

# Scan for best p0
if recreateCRp0ScanPlots:
    probsStd = np.logspace(-10, 0, num=150)
    NoBStd = []
    meanLengthStd = []
    for p in probsStd:
        blocks = bayesian_blocks(
            nightsCR["mjd"],
            x=nightsCR["wMean"],
            sigma=nightsCR["std"],
            fitness="measures", p0=p
        )
        NoBStd.append(len(blocks))
        meanLengthStd.append(
            np.mean(
                [blocks[i+1]-blocks[i] for i in range(len(blocks)-1)]
            )
        )
    NoBStd = np.array(NoBStd)
    meanLengthStd = np.array(meanLengthStd)

    figConfig.halfWidth()
    bbFigStd = plt.figure()
    ax = bbFigStd.gca()
    ax.set_xscale("log")
    ax.set_xlim(probsStd[0], probsStd[-1])
    ax.set_ylabel("number of bayesian blocks")
    ax.set_xlabel(r"false alarm probability $p_{0}$")
    ax.axvline(x=p0ChoiceCR, color="tab:red", lw=1, label=r"chosen $p_{0}$")
    ax.plot(
        probsStd, NoBStd, color="tab:blue", lw=2,
        label="number"
    )
    ax.legend()
    ax.grid(axis="both", alpha=0.5)
    ax.set_ylim(10, 70)
    bbFigStd.tight_layout()
    bbFigStd.savefig(folder+"BBPriorScanNumber.pdf")

    figConfig.halfWidth()
    bbFigStd2 = plt.figure()
    ax2 = bbFigStd2.gca()
    ax2.set_xscale("log")
    ax2.set_xlim(probsStd[0], probsStd[-1])
    ax2.set_ylabel("average length of the\n bayesian blocks in days")
    ax2.set_xlabel(r"false alarm probability $p_{0}$")
    ax2.axvline(x=p0ChoiceCR, color="tab:red", lw=1, label=r"chosen $p_{0}$")
    ax2.plot(
        probsStd, meanLengthStd, color="tab:purple", lw=2,
        label="average length"
    )
    ax2.set_ylim(30, 200)
    ax2.legend()
    ax2.grid(axis="both", alpha=0.5)
    bbFigStd2.tight_layout()
    bbFigStd2.savefig(folder+"BBPriorScanLength.pdf")
    plt.close()

"""
-----------------------------------------------------------------------------
determine periods based on bayesian blocks
-----------------------------------------------------------------------------
"""

if recreateBBResultPlots:

    # FIGURE 1: Bayesian Blocks with new Reference Values
    bbCRfig, bbCRax = ppp.plotNightsCR(
        nightsCR, color="tab:blue"
    )
    ppp.addRefValuesToPlot(
        bbCRax, allBayesBlockEdges, refValKdeA,
        color="tab:red", colorJumps="tab:purple"
    )
    bbCRax.set_ylim(180, 340)
    bbCRfig.savefig(folder+"PerformancePeriodsBB.pdf")
    bbCRfig.savefig(folder+"PerformancePeriodsBB.png", dpi=300)

if recalculateBW:

    allBayesBlockEdges[-1] = allBayesBlockEdges[-1]+1

    runsCR["RefCR"] = pd.cut(
        runsCR["mjd"], allBayesBlockEdges, labels=refValKdeA,
        right=False, include_lowest=True
    ).astype(float)

    runsCR["BWF"] = runsCR["RCR_corr"]/runsCR["RefCR"]
    runsCR["StableNight"] = runsCR["fNight"].isin(nightsCR["fNight"])

    runsCR.to_hdf(folder+"CRRateWithBW.h5", "CRRateWithBW")
