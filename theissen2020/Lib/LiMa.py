import numpy as np


"""
    LiMa(s, b, alpha=0.2)

    Description:
        Implementation of a the Li-Ma-Significance. See paper in Sciebo.

    Parameters:
        s: int
            N_Signal
        b: int
            N_Background
        alpha: float
            ratio of on-source-time and off-source-time: alpha = t_on / t_off

    Returns:
        result: float
            Li-Ma-Significance
"""


def LiMa(s, b, alpha=0.2):
    sum = s+b
    if (s < 0 or b < 0 or alpha <= 0):
        return -1

    L = s*np.log(s/sum*(alpha+1)/alpha) if s != 0 else 0
    M = b*np.log(b/sum*(alpha+1)) if b != 0 else 0

    if (L+M < 0):
        return 0
    else:
        return np.sqrt(2*(L+M))
