import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde as gkde

import correctionModels as cm
import ExcessCalculation as ec
import lightcurveMonitor as lm
import setFigureConfig as figConfig
from secondAxis import createYearAxisToMjdAxis, createCrabUnitToExcessAxis
from consoleOutput import printDfKeys
import ModifiedJulianDay as mjd

# define plots to produce
corrFactorPlots = True
lightcurveDistPlots = True
lightcurvePlot = True

# 1 Crab unit corresponds to excess per h
crabUnit = 55.8

# load excess run info
mergedExcessInfo = ["../../MergedExcessInfos/Mrk501Excess.h5", "Mrk501Excess"]
runsCR = pd.read_hdf(mergedExcessInfo[0], mergedExcessInfo[1])
print("\nCalibration Crab sample loaded from: "+mergedExcessInfo[0]+"\n")

# spectral index of the source
gamma = 2.7

# reduce to seasons with feedback Start season 2014/2015
runsCR = runsCR[runsCR["fNight"] > 20131230]

# apply cuts where corrections are not reliable
runsCR = runsCR[runsCR["fTNGDust"] < 100]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 60]

# total effective observation time
t_tot = np.sum(runsCR["fEffectiveTime_h"])
print(
    "Total effective observation time of the sample: " +
    str(round(t_tot, 2)) + "h\n"
)

# applying efficiency corrections on a runwise basis

# - performance correction

perfCorrLocation = [
    "../../CRCorrection/perfCorrectionResults.h5", "perfCorrectionResults"
]
perfCorrRes = pd.read_hdf(perfCorrLocation[0], perfCorrLocation[1])
print(
    "  * Performance correction results loaded from: " +
    perfCorrLocation[0]+"\n"
)
bestFitPerf = perfCorrRes["mean"].values
p1 = bestFitPerf[0]
p0 = bestFitPerf[1]
rCrRef = 250.
print("\tModel: c_perf = (p1*RCR_ref + p0)/(p1*RCR + p0)")
print("\tBest fit was: p0="+str(p0)+" p1="+str(p1))
print("\tCR ref rate: "+str(rCrRef)+" events/min\n")

runsCR["C_perf"] = cm.perfCorrectionFactor(
    runsCR["RCR_corr"], p0, p1, rCrRef=rCrRef
)

# - dust correction

dustCorrLocation = [
    "../../../CosmicRayRate/DustCorrectionResults.h5", "DustCorrectionResults"
]
dustCorrRes = pd.read_hdf(dustCorrLocation[0], dustCorrLocation[1])
print(
    "  * Calima correction results loaded from: " + dustCorrLocation[0]+"\n"
)
bestFitDust = dustCorrRes["mean"].values
alpha = bestFitDust[1]
print("\tModel: c_c = 1/(1+alpha*c)^(-gamma)")
print("\tBest fit was: alpha="+str(alpha))
print("\tSpectral index: gamma="+str(gamma)+"\n")

runsCR["C_c_dust"] = cm.dustCorrectionFactor(
    runsCR["fTNGDust"], alpha, gamma=gamma
)

# - zenith distance correction
zdCorrLocation = [
    "../../ZdCorrection/ZenithCorrectionResults.h5", "ZenithCorrectionResults"
]
zdCorrRes = pd.read_hdf(zdCorrLocation[0], zdCorrLocation[1])
bestFitZd = zdCorrRes["mean"].values
print(
    "  * Zenith distance correction results loaded from: " +
    zdCorrLocation[0]+"\n"
)
chi = 0.9  # chi was fixed
xi = bestFitZd[1]
kappa = bestFitZd[2]
print("\tModel: c_theta=1 / ("
      + "(z^((gamma-1)*(5*chi+alpha-2)-2*chi) *"
      + "(exp(1-1./z))^(xi*(gamma-1)) *"
      + "(1-log(z))^(2*kappa*(gamma-1)))")
print("\t       with z=cos(theta)")
print("\tBest fit was: xi="+str(xi)+", kappa="+str(kappa))
print("\tFixed: chi="+str(chi), "alpha="+str(0.065))
print("\tSpectral index: gamma="+str(gamma)+"\n")

runsCR["C_theta"] = cm.zenithCorrectionFactor(
    runsCR["fZenithDistanceMean"], chi, xi, kappa, gamma=gamma
)

# - threshold correction
thCorrLocation = [
    "../../ThCorrection/ThresholdCorrectionResults.h5",
    "ThresholdCorrectionResults"
]
thCorrRes = pd.read_hdf(thCorrLocation[0], thCorrLocation[1])
bestFitTh = thCorrRes["mean"].values
print(
    "  * Threshold correction results loaded from: " +
    thCorrLocation[0]+"\n"
)
T_mu = bestFitTh[1]
sigma = bestFitTh[2]

print("\tModel: c_T = 1 / ((1-beta) + beta * T^(-delta) * (T_mu)^delta)")
print("\t       with beta=0.5*tanh((log10(T)-log10(T_mu))/sigma) + 0.5")
print("\tBest fit was: T_mu="+str(T_mu)+", kappa="+str(sigma))
print("\tSpectral index: gamma="+str(gamma)+"\n")

runsCR["C_T"] = cm.thresholdCorrectionFactor(
    runsCR["fThresholdMinSet"], T_mu, sigma, gamma=gamma
)

# - total correction
runsCR["C_total"] = (
    runsCR["C_perf"]*runsCR["C_c_dust"]*runsCR["C_theta"]*runsCR["C_T"]
)

# - apply correction and obtain corrected excess rates
runsCR["rExcCorr"] = runsCR["C_total"]*runsCR["rExc"]
runsCR["rExcErrCorr"] = runsCR["C_total"]*runsCR["rExcErr"]

# rebinning on a nightly basis
grpRuns = runsCR.groupby("fNight")
lightcurve = pd.DataFrame()

# - set basic information
lightcurve["mjd"] = grpRuns["mjd"].nth(0)
lightcurve["N_Runs"] = grpRuns.apply(lambda group: len(group))
lightcurve["StableNight"] = grpRuns["StableNight"].nth(0)

# - calculate the excess for every night

# -- method (i): ignoring the corrections
lightcurve = ec.excessPerBinUncorrected(lightcurve, grpRuns)

# -- method (ii): including the corrections and calculate runwise
lightcurve = ec.excessPerBinCorrected(
    lightcurve, grpRuns, binningColumn="fNight"
)

# calculate mean correction
lightcurve["MeanCorrFactor"] = grpRuns["C_total"].apply(
    lambda group: np.mean(group)
)

# - reset index to let fNight become a column again
lightcurve = lightcurve.reset_index()

print(
    "Rebinning on nightly basis complete. The following columns are available:"
)
printDfKeys(lightcurve)

saveTo = ["../Mrk501From2013.h5", "Mrk501From2013"]
lightcurve.to_hdf(saveTo[0], key=saveTo[1])
print(
    "Lightcurve saved to "+saveTo[0]+"\n"
)

# dist evaluations

bins = np.linspace(-45, 155, num=100)
errBins = np.linspace(0, 3, num=50)
evals = np.linspace(-45, 125, 250)

errBinsWidth = errBins[1]-errBins[0]
binwidth = bins[1]-bins[0]
bincenter = (bins[:-1]+bins[1:])/2

corrGkde = gkde(lightcurve["RExcCorr"])
corrGkdeY = corrGkde(evals)
uncorrGkde = gkde(lightcurve["RExc"])
uncorrGkdeY = uncorrGkde(evals)

print(
    "The Crab unit definition that is used is: 1CU = " +
    str(round(crabUnit, 1))+" events/h"
)


"""
   plots
"""

# correction factors
if corrFactorPlots:
    lm.plotCorrectionFactorDist(
        runsCR, "C_total",
        "total correction factor "+r"$c_{\mathrm{tot}}$",
        "./TotalCorrDistMrk501_2013"
    )
    lm.plotCorrectionFactorDist(
        runsCR, "C_perf",
        "performance correction factor "+r"$c_{\mathrm{perf}}$",
        "./PerformanceCorrDistMrk501_2013"
    )
    lm.plotCorrectionFactorDist(
        runsCR, "C_c_dust",
        "dust concentration correction factor " + r"$c_{c}$",
        "./DustCorrDistMrk501_2013", bins=np.linspace(1., 1.5, num=30)
    )
    lm.plotCorrectionFactorDist(
        runsCR, "C_theta",
        "zenith distance correction factor " + r"$c_{\theta}$",
        "./ZenithCorrDistMrk501_2013"
    )
    lm.plotCorrectionFactorDist(
        runsCR, "C_T",
        "threshold correction factor " + r"$c_{T}$",
        "./ThresholdCorrDistMrk501_2013"
    )

# Lightcurves

if lightcurveDistPlots:

    color1 = "tab:blue"
    color2 = "tab:purple"

    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        lightcurve["RExcCorr"],
        bins=bins,
        label="corrected excess rates",
        color=color1,
        lw=2,
        histtype=u'step',
        zorder=1.4
    )
    ax.hist(
        lightcurve["RExcCorr"],
        bins=bins,
        width=0.85*binwidth,
        label=None,
        color=color1,
        alpha=0.3,
        zorder=1.2
    )
    ax.hist(
        lightcurve["RExc"],
        bins=bins,
        label="uncorrected excess rates",
        color=color2,
        lw=2,
        ls="--",
        histtype=u'step',
        zorder=1.3
    )
    ax.hist(
        lightcurve["RExc"],
        bins=bins,
        width=0.85*binwidth,
        label=None,
        color=color2,
        alpha=0.3,
        zorder=1.1
    )
    ax.axvline(
        x=crabUnit, lw="2", color="k",
        label="Crab unit: "+str(round(crabUnit, 1))+" events/h"
    )
    ax.set_ylabel("number of nights per bin")
    ax.set_xlabel("excess rate "+r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_xlim(-45, 165)
    ax.grid(axis="y", alpha=0.5)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1])
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("./LightcurveDistsMrk501_2013.png", dpi=300)
    fig.savefig("./LightcurveDistsMrk501_2013.pdf")

    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.hist(
        lightcurve["RExcErrCorr"]/lightcurve["RExcCorr"],
        bins=errBins,
        label="corrected uncertainties",
        color=color1,
        lw=2,
        histtype=u'step',
        zorder=1.4
    )
    ax.hist(
        lightcurve["RExcErrCorr"]/lightcurve["RExcCorr"],
        bins=errBins,
        width=0.85*errBinsWidth,
        label=None,
        color=color1,
        alpha=0.3,
        zorder=1.2
    )
    ax.hist(
        lightcurve["RExcErr"]/lightcurve["RExc"],
        bins=errBins,
        label="uncorrected uncertainties",
        color=color2,
        lw=2,
        ls="--",
        histtype=u'step',
        zorder=1.3
    )
    ax.hist(
        lightcurve["RExcErr"]/lightcurve["RExc"],
        bins=errBins,
        width=0.85*errBinsWidth,
        label=None,
        color=color2,
        alpha=0.3,
        zorder=1.1
    )
    ax.set_ylabel("number of nights per bin")
    ax.set_xlabel("uncertainty "+r"$\sigma_{R}$"+" in events/h")
    #ax.set_xlim(-45, 165)
    ax.grid(axis="y", alpha=0.5)
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1])
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("./LightcurveErrDistsMrk501_2013.png", dpi=300)
    fig.savefig("./LightcurveErrDistsMrk501_2013.pdf")

    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    kde1 = ax.plot(
        evals, 1e3*corrGkdeY/np.sum(corrGkdeY),
        color=color1, label="corrected"
    )
    kde2 = ax.plot(
        evals, 1e3*uncorrGkdeY/np.sum(uncorrGkdeY),
        color=color2, ls="--", label="uncorrected"
    )
    handles, labels = ax.get_legend_handles_labels()
    firstlegend = ax.legend(
        handles=handles[::-1], labels=labels[::-1],
        title="excess rates", loc="upper right"
    )
    ax.add_artist(firstlegend)

    maxLine = ax.axvline(
        x=crabUnit, ls="-.", color="k"
    )

    ax.set_ylabel("normalized kernel density\nestimation " +
                  r"$\rho_{\mathrm{KDE}}\cdot 10^{4}$")
    ax.set_xlabel("excess rate "+r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.grid(axis="y", alpha=0.5)
    ax.set_xlim(-45, 125)
    ax.set_ylim(0, 18)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("./LightcurveKDEsMrk501_2013.png", dpi=300)
    fig.savefig("./LightcurveKDEsMrk501_2013.pdf")

if lightcurvePlot:

    # sigma clipping for display
    lightcurve = lightcurve[lightcurve["RExcCorr"] > -10]
    lightcurve = lightcurve[lightcurve["RExcErrCorr"] /
                            lightcurve["RExcCorr"] < 0.6]

    config = {}
    config["alpha"] = 0.5
    config["aspRatio"] = 1./4.0
    config["small"] = 12
    config["medium"] = 14
    config["big"] = 16
    config["width"] = 6.5
    config["legendHandleLength"] = 2

    figConfig.configure(config)
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        lightcurve["mjd"], lightcurve["RExcCorr"], lightcurve["RExcErrCorr"],
        fmt=".", zorder=50, color="tab:blue", ms=3.
    )
    ax.set_ylabel("excess rate "+r"$R_{\mathrm{exc}}$"+"\nin events/h")
    ax.set_xlabel("modified julian day")
    ax.set_ylim(-20, 270)
    ax.patch.set_visible(False)
    ax.set_xlim(np.min(lightcurve["mjd"])-30, np.max(lightcurve["mjd"])+30)
    mjdAx = createYearAxisToMjdAxis(ax, yearlabel=False)
    mjdAx.grid(axis="x", alpha=0.5)
    mjdAx.set_zorder(-1)
    mjdAx.patch.set_visible(False)
    crabticks = np.array([0, 1, 2., 3., 4.])
    cuAx = createCrabUnitToExcessAxis(ax, cuRef=crabUnit, crabticks=crabticks)
    cuAx.grid(axis="y", alpha=0.5)
    cuAx.set_zorder(-1)
    cuAx.patch.set_visible(False)
    fig.tight_layout()
    fig.savefig("./LightcurveMrk501_2013.png", dpi=300)
    fig.savefig("./LightcurveMrk501_2013.pdf")
