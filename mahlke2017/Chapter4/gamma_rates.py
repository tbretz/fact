import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from peripylib import figures, query_fact model
from scipy.stats import norm, gaussian_kde
from scipy.optimize import curve_fit
import seaborn as sns



# -------------------------------------
# Correct the zenith-dependence of the
# gamma-ray flux of the Crab Nebula

# -------------------------------------
# Start with plotting RunRates versus ZenithDistance

# Prepatration
# ------------
# Get latest Crab data
data, nightly_data = query_fact.query_fact(source='Crab', database='ISDC', verbose=True,
							 compute_nightly=True, cut_threshold=False, cut_zenith=False, exclude_bad_weather=False, force_new=True)

print('\nRead in Crab data: ', data.shape)

# Short status update
print('Number of Runs in data: ', len(data['run_id']))

# ----------------------------
# Bin runs by zenith angle, compute excess event rates and errors

bin_width = 4 # in degree
bins = np.arange(2, 80., bin_width)
inds = np.digitize(data['zdmean'], bins)

binned_rates, binned_errors = [], []


for i in range(1, len(bins)):

	binned_data = data[inds == i]

	# If no data was taken for these zenith distances, append 0 to arrays and continue
	if binned_data.empty:
		binned_rates.append(0)
		binned_errors.append(0)
		continue

	# Calculate binned event rate
	evts = binned_data['excevts'].as_matrix()  # converts to numpy array
	durations = binned_data.duration * binned_data.effective_on

	rate = sum(evts)*3600. / sum(durations)

	error = np.sqrt(np.sqrt(sum(binned_data['bgevts'])**2) + np.sqrt(sum(binned_data['sigevts'])**2)) * 3600 /\
                sum(durations)
	# Drop rows where error is zero (investigate why this happens at some point)

	if error == 0.0:
		print('error zero')
		binned_rates.append(0)
		binned_errors.append(0)
		continue

	binned_rates.append(rate)
	binned_errors.append(error)

bins_centered = np.array([(bins[i] + bins[i-1])/2 for i in range(1,len(bins))])
bins = bins_centered
binned_rates = np.array(binned_rates)
binned_errors = np.array(binned_errors)

# Fit binned rates and errors with model
if False:
	print(bins[binned_rates!=0.], binned_rates[binned_rates!=0.])
	popt, pcov = curve_fit(model.fit_model, bins[binned_rates!=0.], binned_rates[binned_rates!=0.]/max(binned_rates),\
	                       sigma=binned_errors[binned_rates!=0.]/max(binned_rates), p0=[1.41316024, 0.91773753, 1.19740462], method='lm')
	print(popt)
else:
	popt = [0.61444529, 1.82438447, 1.14927505]

# Plot fit funciton and binned rates
if True:
	fig, ax = figures.newfig()
	ax.errorbar(bins_centered[binned_rates!=0.], binned_rates[binned_rates!=0.], yerr=np.array(binned_errors[binned_rates!=0.]), fmt='.', label='Crab Nebula Data', color=figures.colors(0))
	plot_bins = np.linspace(0, 89, 90)
	ax.plot(plot_bins, model.fit_model(plot_bins, *popt)*max(binned_rates), label='Model Fit', color=figures.colors(3), ls='--')
	ax.plot([0, 90], [0, 0], '-', alpha=0.8, lw=0.5, color='black', zorder=-100)
	ax.set(xlabel='Zenith Angle / deg', ylabel=r'Rate / h$^{-1}$', xlim=(0,90))
	ax.legend()
	figures.savefig('crab_data_fit', png=True, dpi=600, pgf=True)