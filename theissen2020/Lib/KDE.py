import numpy as np
import _pickle as pckl
from sklearn.neighbors import KernelDensity
from sklearn.model_selection import GridSearchCV


class KDE:

    def __init__(self, bandwidth=0.1, n_cv=3, atol=0, rtol=1e-8):
        if isinstance(bandwidth, np.ndarray):
            self.grid = True
            self.kde = GridSearchCV(
                KernelDensity(atol=atol, rtol=rtol),
                {'bandwidth': bandwidth}, cv=n_cv, n_jobs=-1
            )
        else:
            self.grid = False
            self.kde = KernelDensity(bandwidth=bandwidth, atol=atol, rtol=rtol)
        self.hasTable = False

    def fit(self, x, toTable=True, tableSize=1000):
        self.kde.fit(x[:, None])
        if self.grid:
            self.eval = self.kde.best_estimator_
        else:
            self.eval = self.kde

        # saves values of KDE to a table
        if toTable:
            self.hasTable = True
            self.tableGrid = np.linspace(min(x), max(x), tableSize)
            print("Started calculating KDE table..")
            self.table = np.exp(
                self.eval.score_samples(self.tableGrid[:, None])
            )
            print("Finished calculating KDE table..")

    def evaluate(self, x):
        if self.hasTable:
            return self.fromTable(x)
        else:
            if isinstance(x, np.ndarray):
                return np.exp(self.eval.score_samples(x[:, None]))
            elif isinstance(x, (list, tuple)):
                x = np.array(x)
                return np.exp(self.eval.score_samples(x[:, None]))
            else:
                x = np.array([x])
                return np.exp(self.eval.score_samples(x[:, None]))[0]

    def fromTable(self, xin):
        if isinstance(xin, np.ndarray):
            x = np.array(xin)
            # mask = valid values
            mask = np.ones(len(x), dtype=bool)
            mask[x < min(self.tableGrid)] = False
            mask[x > max(self.tableGrid)] = False
            x[~mask] = 0
            x[mask] = self.table[np.digitize(x[mask], self.tableGrid) - 1]
            return x
        elif isinstance(xin, (list, tuple)):
            x = np.array(xin)
            # convert to np-array first
            x = np.array(x)
            # mask = valid values
            mask = np.ones(len(x), dtype=bool)
            mask[x < min(self.tableGrid)] = False
            mask[x > max(self.tableGrid)] = False
            x[~mask] = 0
            x[mask] = self.table[np.digitize(x[mask], self.tableGrid) - 1]
            return x
        else:
            # convert to np-array first
            x = np.array([xin])
            # mask = valid values
            mask = np.ones(len(x), dtype=bool)
            mask[x < min(self.tableGrid)] = False
            mask[x > max(self.tableGrid)] = False
            x[~mask] = 0
            x[mask] = self.table[np.digitize(x[mask], self.tableGrid) - 1]
            return x[0]

    def gen_weights(self, x):
        return 1/self.evaluate(x)

    def save(self, filename):
        with open(filename, 'wb') as f:
            pckl.dump([self.eval, self.tableGrid, self.table], f)

    def load(self, filename):
        with open(filename, 'rb') as f:
            self.eval, self.tableGrid, self.table = pckl.load(f)
        return self

    @classmethod
    def fromFile(cls, filename):
        with open(filename, 'rb') as f:
            cls.eval, cls.tableGrid, cls.table = pckl.load(f)
        return cls
