import numpy as np
import os as os

# extract path of Reference folder
path = os.path.split(os.path.relpath(__file__))[0] + "/ReferenceValues/"

P18_s3_s = np.genfromtxt(path + "P18_s3_s.txt", delimiter=", ").T
P18_s10_s = np.genfromtxt(path + "P18_s10_s.txt", delimiter=", ").T
P18_s50_s = np.genfromtxt(path + "P18_s50_s.txt", delimiter=", ").T
P18_s3_p = np.genfromtxt(path + "P18_s3_p.txt", delimiter=", ").T
P18_s10_p = np.genfromtxt(path + "P18_s10_p.txt", delimiter=", ").T
P18_s50_p = np.genfromtxt(path + "P18_s50_p.txt", delimiter=", ").T
