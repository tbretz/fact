import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
import setFigureConfig as sfc

nbins = 60*5
Zd_range = [0, 60]

recalc = True
if recalc:
    OriginalMCFile = "../data/OriginalMC_Energy_Impact_Zd.root"
    dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))

    # in model
    dp.data["Zd_deg"] = np.degrees(dp.data["Zd"])

    # 1. in 1deg bins correct for cos(Zd)-uniform distribution
    dp.data["wZd1"] = 1/np.sin(dp.data["Zd"]) / np.cos(dp.data["Zd"])
    dp.data["wZd1_norm_bin"] = pd.cut(
        dp.data["Zd_deg"], bins=np.arange(60.1), labels=np.arange(59.1)
    )
    dp.data["wZd1"] *= 1/pd.cut(
        dp.data["Zd_deg"], bins=np.arange(60.1),
        labels=dp.data.groupby("wZd1_norm_bin")["wZd1"].mean()
    ).astype(float)
    dp.data["wZd2"] = np.abs(pd.cut(
        dp.data["Zd_deg"], bins=[0, 29, 30, 59, 60],
        labels=[1, 67/57, -1, 537/427]
    ).astype(float))

    # create histogram of Zd called Zd_hist
    ZdHist = dp.getHistogram(
        "Zd_Hist", "Zd_deg", histRange=Zd_range, nBins=60,
        useWeights=["wZd1", "wZd2"]
    )
    print(ZdHist.getBins())

    # create Input Model with step function
    ZdModelIn = Model("step")
    # initial guess values: point of step, value1, value2
    ZdModelIn.getParametersFromFit(ZdHist, [30, 3e6, 1e7])

    # plt.show(ZdModelIn.plotModelWithData(data=ZdHist))
    ZdModelIn.plotModelWithData(data=ZdHist).savefig("Zd_reweighting_fit.pdf")

    print("finished in Model with parameters:")
    print(ZdModelIn.parameters)
    ZdModelIn.save("ZdModelIn_OriginalMC.p")

    # calculate weights of complete distribution
    factor = ZdModelIn.parameters[2] / ZdModelIn.parameters[1]
    dp.data["wZd3"] = pd.cut(
        dp.data["Zd_deg"], bins=[0, 30, 65], labels=[factor, 1]
    ).astype(float)

    # calc histograms for plot

    hm = HistogramManager()
    ZdHist = dp.getHistogram(
        "Zd_Hist", "Zd_deg", useWeights=None, nBins=nbins, histRange=Zd_range
    )
    hm.addHistogram("produced", ZdHist)
    ZdHist_w1 = dp.getHistogram(
        "Zd_Hist_w1", "Zd_deg", useWeights=["wZd1"], nBins=nbins,
        histRange=Zd_range
    )
    hm.addHistogram("w1", ZdHist_w1)
    ZdHist_w12 = dp.getHistogram(
        "Zd_Hist_w12", "Zd_deg", useWeights=["wZd1", "wZd2"], nBins=nbins,
        histRange=Zd_range
    )
    hm.addHistogram("w12", ZdHist_w12)
    ZdHist_w123 = dp.getHistogram(
        "Zd_Hist_w123", "Zd_deg", useWeights=["wZd1", "wZd2", "wZd3"],
        nBins=nbins, histRange=Zd_range
    )
    hm.addHistogram("w123", ZdHist_w123)
    hm.saveHistogramDict("ZD_reweighting.p")

else:
    hm = HistogramManager()
    hm.loadHistogramDict("ZD_reweighting.p")
# create plot
sfc.fullWidth(aspectRatio=0.6)
fig = hm.plotHistograms(
    ["produced", "w1", "w12", "w123"],
    ["produced", "1° CORSIKA effect", r"last $Zd$ bins",
     "full parameter-space"],
    yscale="linear", style="stairs", xlabel=r"$Zd$",
    legloc=[[0.5, 0], "lower left"], zlegend=10
)
ax = fig.gca()
ax.set_ylabel(
    r"$10^{-4}\frac{\mathrm{d}N}{\mathrm{d} Zd}$",
    # rotation=90, labelpad=10
)
ax.grid()
ax.set_yticklabels([str(int(xt*1e-4)) for xt in ax.get_yticks()])
ax.set_xticklabels([str(int(xt)) + r"°" for xt in ax.get_xticks()])
# ax.get_legend().set_zorder(10)
fig.tight_layout()
fig.savefig("OriginalMC_reweightingZd_sc_%g.pdf" % (nbins))
plt.show()
