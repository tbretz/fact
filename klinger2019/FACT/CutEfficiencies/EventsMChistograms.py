import numpy as np

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
'''
calculates the histograms of triggered and processed data
- after processing
- after quality cuts
- after theta squared cut
- after size-area cut (spectrum/lightcurve)
saves them to data/EventsMC_histograms_si#_nBins[#,#,#].p (make sure the 
folder exists)
takes root file from MonteCarlos/data/
takes the Models from MonteCarlos/reweighting_energy, ..._impact and ..._Zd
'''

EBins = 64
IBins = 54
ZdBins = 60
tsqBins = 20  # bins for theta squared histograms
si = -2.5

lgERange = [2.3, 4.7]
ZdRange = [0, 60]
ImpactRange = [0, 54000]

EventsMCFile = "../MonteCarlos/data/MC_Gamma.root"
dp = DataProcessor(data=dFrameFromRoot(EventsMCFile, keys=["Result"]))

# Energy reweighting
dp.calcLogs(["Energy"])
energyPath = "../MonteCarlos/reweighting_energy/"
MCG_E_in = Model.fromFile(energyPath + "/EnergyModelIn_MCGamma.p")
MCG_E_out = Model.fromFile(
    energyPath + "/EnergyModelOut_MCGamma_%g.p" % (si)
)
dp.updateModels(MCG_E_in, MCG_E_out)
dp.addWeights("logEnergy", name="wE")
print("finished energy reweighting")


# create and save histograms
hm = HistogramManager()

# triggered
hm.addHistogram(
    "lgE_triggered",
    dp.getHistogram(
        "lgE_triggered", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_triggered",
    dp.getHistogram(
        "Zd_triggered", "ZdFileID",
        histRange=ZdRange, nBins=ZdBins
    )
)
hm.addHistogram(
    "Impact_triggered",
    dp.getHistogram(
        "Impact_triggered", "Impact",
        histRange=ImpactRange, nBins=IBins
    )
)


# Quality cuts
dp.doQualityCuts()
hm.addHistogram(
    "lgE_Q",
    dp.getHistogram(
        "lgE_Q", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_Q",
    dp.getHistogram("Zd_Q", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_Q",
    dp.getHistogram("Impact_Q", "Impact", histRange=ImpactRange, nBins=IBins)
)

# thetasq cut
dp.calcThetaSq(xi0=1.254, xi1=0.042)

dp.data["lgtsq"] = np.log10(dp.getData("thetasq"))
hm.addHistogram("thetasq", dp.getHistogram("thetasq", "thetasq", nBins=tsqBins,
                                           histRange=[0, 0.05]))

hm.addHistogram("lgtsq", dp.getHistogram("lgtsq", "lgtsq", nBins=tsqBins))

dp.doThetaSqCut(0.028)

hm.addHistogram(
    "lgE_Q_tsq",
    dp.getHistogram(
        "lgE_Q_tsq", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_Q_tsq",
    dp.getHistogram("Zd_Q_tsq", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_Q_tsq",
    dp.getHistogram("Impact_Q_tsq", "Impact", histRange=ImpactRange,
                    nBins=IBins)
)


# create copies for other separation into lightcurve and spectrum
dpl = dp.copy()

# BG cut spectrum
dp.doSizeAreaCut(kind="spectrum")
hm.addHistogram(
    "lgE_S",
    dp.getHistogram(
        "lgE_S", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_S",
    dp.getHistogram("Zd_S", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_S",
    dp.getHistogram("Impact_S", "Impact", histRange=ImpactRange, nBins=IBins)
)

# BG cut lightcurve
dpl.doSizeAreaCut(kind="lightcurve")
hm.addHistogram(
    "lgE_L",
    dpl.getHistogram(
        "lgE_L", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_L",
    dpl.getHistogram("Zd_L", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_L",
    dpl.getHistogram("Impact_L", "Impact", histRange=ImpactRange, nBins=IBins)
)


hm.saveHistogramDict("data/EventsMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)
