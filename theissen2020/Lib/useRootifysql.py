import argparse
import getpass
import os

"""
    useRootifySQL python3 script

    Description:
        Using the rootifysql tool from fact++ the database acess is granted
        via the --uri statement including the password. Using python3 this
        tool runs in a subprocess after recieving the password via user
        input. The password does no longer appear in the users history or in
        the task header.
"""

parser = argparse.ArgumentParser()

parser.add_argument("-in", "--sqlFile",
                    help="Path to input sql-file", type=str,
                    default="")
parser.add_argument("-out", "--rootFile",
                    help="Path to output root-file", type=str,
                    default="")
parser.add_argument("-db", "--dataBase",
                    help="Chose data base", type=str,
                    default="factdata")
parser.add_argument("-i", "--ignoreNull",
                    help="Do not skip rows containing null field",
                    action="store_true")
parser.add_argument("-t", "--treeName",
                    help="Name of the tree containing the response",
                    type=str, default="sql")
args = parser.parse_args()

sqlFile = args.sqlFile
rootFile = args.rootFile
treeName = args.treeName
if args.ignoreNull:
    ignoreNull = "-i "
else:
    ignoreNull = ""

database = args.dataBase

if database == "factmc":
    username = "factmc"
else:
    username = "fact"

server = "ihp-pc45.ethz.ch"


password = getpass.getpass(
    "Please enter password for "+username+"@"+server+": ")

executionString = "rootifysql --file "+sqlFile+" --out "+rootFile +\
    " --tree "+treeName+" " + ignoreNull+"-f "+"--uri "+username+":" +\
    password+"@"+server+"/" + \
    database

os.system(executionString)
