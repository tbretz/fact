from stingray import Lightcurve
from stingray.simulator import simulator
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from astropy.timeseries import LombScargle
from scipy.optimize import curve_fit
from consoleOutput import printProgressBar

# set random seed
np.random.seed(123)

reSample = True

# Load Crab Lightcurve
lcMrk421 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk421From2013.h5",
    "Mrk421From2013"
)
lcMrk421 = lcMrk421[lcMrk421["RExcErrCorr"] /
                    lcMrk421["RExcCorr"] < 2]

t = lcMrk421["mjd"]
N = int(np.floor(t.max()) - np.floor(t.min()))
mean = np.mean(lcMrk421["RExcCorr"])
rms = 0.7

red_noise = 100
num_lcs = 10000

sim = simulator.Simulator(
    N=N, mean=mean, rms=rms, dt=1, red_noise=red_noise
)

fitted_spec_indices = []
uneven_errs = []
sim_index = 1.35
f = np.linspace(1e-3, 0.5, 10000)

if reSample:
    ls_powers = []
    for i in range(num_lcs):

        # Simulate the light-curve
        lc = sim.simulate(sim_index)

        # Set lightcurve times to FACT timestamps
        # to get the same spectral window
        lc.time = lc.time - lc.time[0] + int(np.floor(t.min()))
        lc.counts = lc.counts[np.isin(lc.time, np.floor(t))]
        lc.time = lc.time[np.isin(lc.time, np.floor(t))]
        lsw = LombScargle(
            lc.time, lc.counts, center_data=False, fit_mean=False
        )
        ls_powers.append(lsw.power(f))

    ls_powers = np.array(ls_powers)

    np.save("./simLSP_125_rms07.npy", ls_powers)

else:
    ls_powers = np.load("./simLSP_125_rms07.npy")


meanLSP = np.mean(ls_powers, axis=0)
stdLSP = np.std(ls_powers, axis=0)

mrk501Limit = pd.DataFrame()
mrk501Limit["mean"] = meanLSP
mrk501Limit["3sigma"] = meanLSP+3*stdLSP
mrk501Limit.to_hdf("./Mrk421Significance.h5", "Mrk421Significance")

fig = plt.figure()
ax = fig.gca()
ax.plot(
    f, meanLSP
)
ax.plot(
    f, meanLSP+3*stdLSP
)
ax.set_xscale("log")
ax.set_xlabel("f in 1 per day")
ax.set_ylabel("Index of LS Periodogram")
ax.legend()
fig.savefig("./Mrk421Lim.png", dpi=300)
fig.savefig("./Mrk421Lim.pdf")
