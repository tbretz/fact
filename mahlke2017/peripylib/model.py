import numpy as np
from scipy.integrate import quad
import pandas as pd


def differential_source_spectrum(E, spectral_index=2.47):
    '''
    Differential spectrum of the source we're looking at. For TH1000, CR proton spectrum, spectral_index is 2.7
    For Crab, 2.47, according to MAGIC: arXiv:1406.6892
    '''
    return 2.83e-11 * np.power(E*1e-3, -spectral_index) * 1e4  # ph/m^2 GeV s CR  # *1e4: cm^2 - >m^2


def detected_differential_rate_model(E, theta, slant_depth_scaling=1, spectral_index=2.47):
    '''
    Calculates the differential rate depending on the energy and the zenith angle.
    E is energy of primary particle in GeV.
    '''

    slant_depth = lambda x: 1035 / np.power(np.cos(x*np.pi/180), slant_depth_scaling)

    # ---------------------------
    # Calculate light pool area. Depends on energy of primary particle and zenith angle

    # Critical energy for electrons
    E_c = 86e-3  # in GeV

    # Radiation lengths
    R_e = 37.2  # in g/cm^2, for electrons
    R_p = 7/9 * R_e  # for photons

    # Calculate X_max
    X_max = np.log(E/E_c) / np.log(2)  # in radiation lengths
    X_max *= R_p  # in g/cm^2

    # Scale X_0 with zenith angle to account for larger distance
    X_0 = slant_depth(theta)

    # Convert X_max to distance from ground
    h_max = -np.log(X_max/X_0)  *   8e3 /np.cos(theta*np.pi/180)

    # Calculate area of cone = light pool area
    A_pool = np.pi*np.tan(1.4*np.pi/180)**2*h_max**2  # in m^2

    # ---------------------------
    # Multiplying differential source spectrum and light pool area gives the expected rate at that energy and zenith angle
    return A_pool * differential_source_spectrum(E, spectral_index)

def fit_model(theta, area, optical_depth, slant_depth_scaling=1, spectral_index=2.47):
    '''
    The model function itself. Executes the integration
    and calculates the normalization parameter
    '''

    # Calculate lower energy threshold
    slant_depth = lambda x: 1035 / np.power(np.cos(x*np.pi/180), slant_depth_scaling)
    E_thresh = 100*np.power(slant_depth(theta)/slant_depth(0), area) * np.exp(optical_depth*slant_depth(theta)/slant_depth(0))

    # If only one theta is given, E_thresh has to be converted into a list
    #if type(theta) != list and type(theta) != np.ndarray and type(theta) != pd.core.series.Series:
    if not isinstance(theta, (list, np.ndarray, pd.core.series.Series)):
        E_thresh = np.array([E_thresh])
        theta = np.array([theta])

    # Calculate normalization parameter
    sim_max = quad(detected_differential_rate_model, 100 * np.exp(optical_depth), 6e7, args=(0, 1, spectral_index))[0]

    # E_0 is energy of primary particle in GeV
    # scipy.quad does not take lists, vecotrize function with numpy

    rates_fit = []
    for i, E_min in enumerate(E_thresh):
        rates_fit.append(quad(detected_differential_rate_model, E_min, 6e7, args=(theta[i], slant_depth_scaling, spectral_index))[0]/sim_max)
    return np.array(rates_fit)

def fit_model_proton(theta, area, optical_depth, slant_depth_scaling=1, spectral_index=2.7):
    return fit_model(theta, area, optical_depth, slant_depth_scaling=1, spectral_index=2.7)