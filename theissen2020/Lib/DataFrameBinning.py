import pandas as pd
import numpy as np


def cleanBinningFails(rI, binningLabel, binEdges, dropped=0):
    failbins = []
    for i in range(len(binEdges)-1):
        dF = rI[rI[binningLabel] == i]
        if len(dF) == 0:
            failbins.append(i)

    dropped = dropped + len(failbins)

    for i in range(len(failbins)):
        toDel = failbins[i]
        binEdges = np.delete(binEdges, toDel)
        for j in range(len(failbins)):
            if failbins[j] > toDel:
                failbins[j] = failbins[j] - 1
        rI[binningLabel] = rI.apply(
            lambda c: c[binningLabel]-1
            if c[binningLabel] >= failbins[i]
            else c[binningLabel], axis=1)

    return rI, binEdges, dropped


def addLogBinning(rI, column, binningLabel, numberOfBins,
                  minVal=None, maxVal=None):
    if minVal is None:
        minVal = np.min(rI[column])
    if maxVal is None:
        maxVal = np.max(rI[column])
    logBins = np.logspace(
        np.log10(minVal), np.log10(maxVal), numberOfBins + 1
    )
    rI[binningLabel] = np.digitize(rI[column], logBins) - 1

    index = ~rI[column].between(minVal, maxVal, inclusive=False)
    rI[binningLabel] = rI[binningLabel].mask(index, -1)

    rI, binEdges, dropped = cleanBinningFails(rI, binningLabel, logBins)

    return rI, binEdges, dropped


def addUniformBinning(rI, column, binningLabel, numberOfBins):
    rI.sort_values(column)
    rI[binningLabel], uniBins = pd.qcut(
        rI[column], numberOfBins,
        retbins=True, labels=False, duplicates="drop"
    )
    dropped = numberOfBins - (len(uniBins) - 1)

    rI, binEdges, dropped = cleanBinningFails(
        rI, binningLabel, uniBins, dropped=dropped
    )

    return rI, binEdges, dropped


def addLogUniBinning(rI, column, binningLabel, logEdges, uniEdges):
    logstep = np.log10(logEdges[1]) - np.log10(logEdges[0])
    transValue = 0
    for i in range(len(uniEdges)-1):
        unistep = np.log10(uniEdges[i+1]) - np.log10(uniEdges[i])
        if unistep >= logstep:
            transValue = uniEdges[i]
            break
    newBinEdgesLog = logEdges[logEdges < transValue]
    newBinEdgesUni = uniEdges[uniEdges >= transValue]
    newBinEdges = np.concatenate((newBinEdgesLog, newBinEdgesUni))

    rI[binningLabel] = np.digitize(rI[column], newBinEdges, right=True) - 1

    rI, binEdges, dropped = cleanBinningFails(rI, binningLabel, newBinEdges)

    return rI, binEdges, dropped


def addLinearBinning(rI, column, binningLabel, numberOfBins,
                     minVal=None, maxVal=None):
    if minVal is None:
        minVal = np.min(rI[column])
    if maxVal is None:
        maxVal = np.max(rI[column])

    linBins = np.linspace(minVal, maxVal, numberOfBins + 1)
    rI[binningLabel] = np.digitize(rI[column], linBins) - 1

    index = ~rI[column].between(minVal, maxVal, inclusive=True)
    rI[binningLabel] = rI[binningLabel].mask(index, -1)

    rI, binEdges, dropped = cleanBinningFails(rI, binningLabel, linBins)

    return rI, binEdges, dropped


def addBinning(rI, column, binningLabel, binningEdges):

    minVal = np.min(binningEdges)
    maxVal = np.max(binningEdges)
    rI[binningLabel] = np.digitize(rI[column], binningEdges, right=True) - 1

    index = ~rI[column].between(minVal, maxVal, inclusive=True)
    rI[binningLabel] = rI[binningLabel].mask(index, -1)

    rI, binEdges, dropped = cleanBinningFails(rI, binningLabel, binningEdges)

    return rI, binEdges, dropped
