# -*- coding: utf-8 -*-
"""
Created on Wed May 29 18:41:02 2019

@author: Georg Schwefer
"""

'''
File für globale Konstanten für eine Simulation. Muss jeweils eingebunden werden
'''
 
import numpy as np

#Konstanten
Bc=4.41*10**13
c=3*10**10
me=9.11*10**(-28)
mp=1836*me
sigmaT=6.65*10**(-25)

#freie Parameter
#Ränder der Grids
ymin=1.001
ymax=10**12
xmin=10**(-15)
xmax=10**(8)
#Anzahlgridpunkte
jmax=40
tsteps=1000


#physikalische Parameter
B=2#Magnetfeld in Gauß
ymin_inj=560#Untere Grenze Injektion
ymax_inj=5*10**4#Obere Grenze
p=2.1#Exponent der Injektion
Amp1=10**3#nicht nötig
Amp2=2*10**-10#Amplitude der Elektroneninjektion
I=1.4 #Uinj/Ub aus Paper

L=5*10**16#Layerlänge
numLc=3#Anzahl simulierte Lightcrossing times 

T=10**(-5)#BlackBody Temperatur T=k*T/m*c^2
Amp_BB=10**-2#Amplitude BlackBody
zdiss=5*10**17
R_BLR=1*10**18
L_BLR=5*10**41
pm=-1

add=5000#nach Ende des Plasmoids noch berechnete Zeitschritte
#zusammengesetzte Größen
b=B/Bc
tsim=numLc
deltat=tsim/tsteps
#npic=(2*UinjUb*sigma*mp*(ymax_inj**(1-p)-ymin_inj**(1-p))*(2-p))/((ymax_inj**(2-p)-ymin_inj**(2-p))*(1-p)*me)
#nco=(npic*B**2)/(16*np.pi*sigma*mp*c**2)
Ub=(B**2)/(8*np.pi)
e_loss_const=(4*sigmaT*L)/3
lb=e_loss_const*Ub/(me*c**2)
ssa_const=(np.pi*137)/(6)

'''
Parameter für Beaming
'''

t_layer=2#LC-times in MC simuliert
tsteps_layer=20000#Schritte im MC code
deltat_layer=t_layer/tsteps_layer


Theta_obs=0 #Winkel zwischen Observer und Jet
Theta_prim=np.pi #Winkel zwischen Plasmoid und Jet
b_j=0.996 #Jet Geschw. in Observerframe



