import numpy as np

# returns the File Ids for all subsets included in parameter "sets"
def FileIdForZdBin(sets="all"):
    FileIdForZdBin = {i: np.array([]) for i in range(60)}
    if isinstance(sets, str):
        if sets == "all":
            sets = [
                "gustav", "werner", "bernd", "kai", "martin",
                "schantalle", "jackeline"
            ]

    # Gustav
    if "gustav" in sets:
        d = 67
        min_ids = np.arange(10910, 12909, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i] = np.append(
                FileIdForZdBin[i], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1] = np.append(
                FileIdForZdBin[len(min_ids)-1],
                np.arange(min_ids[-1], min_ids[-1] + 57)
            )

    # Werner
    if "werner" in sets:
        d = 67
        min_ids = np.arange(12910, 14909, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i] = np.append(
                FileIdForZdBin[i], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1] = np.append(
                FileIdForZdBin[len(min_ids)-1],
                np.arange(min_ids[-1], min_ids[-1] + 57)
            )


    # Bernd
    if "bernd" in sets:
        d = 34
        min_ids = np.arange(79734, 80733, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 14)
            )

    # Kai
    if "kai" in sets:
        d = 34
        min_ids = np.arange(80765, 81764, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 14)
            )

    # Martin
    if "martin" in sets:
        d = 67
        min_ids = np.arange(81765, 83764, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 57)
            )

    # Schantalle
    if "schantalle" in sets:
        d = 134
        min_ids = np.arange(83765, 87764, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 114)
            )
        d = 67
        min_ids = np.arange(87765, 89764, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 57)
            )

    # Jackeline
    if "jackeline" in sets:
        d = 201
        min_ids = np.arange(89765, 95764, d)
        for i, min_id in enumerate(min_ids[:-1]):
            FileIdForZdBin[i+30] = np.append(
                FileIdForZdBin[i+30], np.arange(min_id, min_id + d)
            )
        # last Zd bin has less entries
        FileIdForZdBin[len(min_ids)-1+30] = np.append(
                FileIdForZdBin[len(min_ids)-1+30],
                np.arange(min_ids[-1], min_ids[-1] + 171)
            )

    return FileIdForZdBin


# print([len(FileIdForZdBin[i]) for i in FileIdForZdBin])
