import matplotlib.pyplot as plt
from peripylib import figures
import numpy as np


# -----
# Plot the PSD of a multiply-periodic and of a stochastic process

def power_law(v, A, alpha):
    return A*v**-alpha

# -----
# Set up plot
fig, ax = figures.newfig()

# The PSD of a multiply-periodic process is non-zero only at the frequencies of the process

ax.plot([20, 20], [0, 1])
ax.plot([50, 50], [0, 0.75], color=figures.colors(0))

ax.set(xlabel='Frequency', ylabel='Amplitude', xlim=(0, 70), ylim=(0, 1.25))
ax.get_xaxis().set_ticks([0, 20, 50])
ax.get_yaxis().set_ticks([0.75, 1])
ax.get_xaxis().set_ticklabels(['0', r'$\nu_0$', r'$\nu_1$'])
ax.get_yaxis().set_ticklabels([r'$A_1$', r'$A_0$'])

figures.savefig('periodic_psd', pgf=True)

# -----
# Repeat for stochastic process
with plt.rc_context({'xtick.major.size'     : 4,      # major tick size in points
                     'xtick.minor.size'     : 2,      # minor tick size in points
                     'ytick.major.size'     : 4,      # major tick size in points
                     'ytick.minor.size'     : 2,      # minor tick size in points
                     'axes.labelpad'        : 0.01,
                    }):
    fig, ax = figures.newfig()

    # The PSD of a periodic process is non-zero only at the frequency of the process
    freq = np.arange(1, 100)
    psd = power_law(freq, 1, 1.)

    ax.plot(freq, psd, label=r'$\propto \nu^{-1}$')
    ax.legend(frameon=False)
    ax.set(xlabel='Frequency', ylabel='Amplitude', xscale='log', yscale='log')
    ax.get_xaxis().set_ticklabels([])
    ax.get_yaxis().set_ticklabels([])

    figures.savefig('stochastic_psd', pgf=True)