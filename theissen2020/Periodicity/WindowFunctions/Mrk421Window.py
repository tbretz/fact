import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.timeseries import LombScargle

import setFigureConfig as figConfig

# Load Crab Lightcurve
lcMrk421 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk421From2013.h5",
    "Mrk421From2013"
)

lcMrk421["mjd"] = lcMrk421["mjd"].astype(int)
t = lcMrk421["mjd"].values
tAll = np.arange(np.min(t), np.max(t)+1, step=1)
window = []
for time in tAll:
    if time in t:
        window.append(1)
    else:
        window.append(0)
window = np.array(window)

lsw = LombScargle(tAll, window, center_data=False, fit_mean=False)

f = np.linspace(1e-3, 0.5, 10000)
p = lsw.power(f)

config = {}
config["alpha"] = 0.8
config["aspRatio"] = 1./2.3
config["small"] = 12
config["medium"] = 14
config["big"] = 16
config["width"] = 6.5
config["legendHandleLength"] = 1

figConfig.configure(config)

fig = plt.figure()
ax = fig.gca()
ax.set_title("Markarian 421")
# ls
ax.plot(f, p, lw=2, color="k", label="LSP of "+r"$w(t)$", zorder=1.5)
ax.set_xlim(np.min(f), np.max(f))
# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)
ax.axvline(x=2/365.25, lw=1, color="tab:blue",
           ls="--", label="first alias", zorder=1.3)
ax.axvline(x=5/365.25, lw=1, color="tab:blue",
           ls=":", label="second alias", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)
ax.axvline(x=2/29.5, lw=1, ls="--", color="tab:orange",
           label="first alias", zorder=1.3)
ax.axvline(x=3/29.5, lw=1, color="tab:orange",
           ls=":", label="second alias", zorder=1.3)
ax.axvline(x=1/27.3, lw=1, color="tab:green",
           label="sidereal month", zorder=1.3)

ax.set_ylim(0, 0.18)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.legend(handletextpad=0.3)
ax.grid(axis="both", alpha=0.5)
fig.tight_layout()
fig.savefig("./Mrk421Window.png", dpi=300)
fig.savefig("./Mrk421Window.pdf")
