import ModifiedJulianDay as mjd
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from rootRoutines import dFrameFromRoot
import setFigureConfig as figConfig

"""
    MAGIC Reflectivity Datensatz aus Martins Plot
"""
Mref = pd.read_csv("./reflectivityMAGIC.csv", header=0)
Mref["fNight"] = Mref.apply(
    lambda row: mjd.mjd_to_fNight(row.mjd),
    axis=1
)
Mref.drop_duplicates(
    subset="fNight", keep=False, inplace=True
)
Mref = Mref[Mref["ref"] < 80]


"""
    FACT Cosmic Ray Rate from DB
"""
factCR = dFrameFromRoot("./ReflectivityCheck.root", prints=False)

factCR = factCR[factCR["fTNGDust"] < 1]
factCR = factCR[factCR["fThresholdMinSet"] < 750]
factCR = factCR[factCR["fZenithDistanceMean"] < 30]
factCR = factCR[factCR["fR750Cor"] < 1000]

factCR["RAWTime"] = (factCR["fRunStart"] + factCR["fRunStop"])/2
factCR["Time"] = pd.to_datetime(factCR["RAWTime"], unit="s")
factCR.sort_values("Time")

allNights = pd.DataFrame()
allNights["mean"] = factCR.groupby(
    "fNight")["fR750Cor"].apply(lambda r: np.mean(r))
allNights["std"] = factCR.groupby("fNight")["fR750Cor"].apply(
    lambda r: np.std(r, ddof=1))
allNights["meanErr"] = factCR.groupby("fNight")["fR750Cor"].apply(
    lambda r: np.std(r, ddof=1)/np.sqrt(len(r)))
allNights = allNights.reset_index()
allNights["mjd"] = allNights.apply(
    lambda row: mjd.fNight_to_mjd(row.fNight),
    axis=1
)
allNights = allNights.dropna()

lowMeanNights = allNights[allNights["mean"] < 185]
highSpreadNights = allNights[allNights["std"] > 17]
goodSpreadNights = allNights[allNights["std"] < 17]
goodSpreadNights = goodSpreadNights[goodSpreadNights["mean"] > 185]

badDates = [57890, 57891, 57893, 57894, 57895, 57898]
goodSpreadNights = goodSpreadNights[~goodSpreadNights.mjd.isin(badDates)]

"""
    Plots
"""

figConfig.fullWidth(aspectRatio=3/5)
fig = plt.figure()
ax = fig.gca()
MagicPlot = ax.plot(
    Mref["mjd"], Mref["ref"],
    marker="s", color="tab:green",
    label="preliminary MAGIC mirror reflectivity"
)
ax.set_xlabel("modified julian day")
ax.set_ylabel("total reflectivity in %")
ax.set_ylim(35, 90)
ax.set_xlim(min(Mref["mjd"]), max(Mref["mjd"]))
ax2 = ax.twiny()
ax.set_xlim(min(Mref["mjd"])-1, max(Mref["mjd"]) + 1)
ax.get_xaxis().get_major_formatter().set_useOffset(False)
dates = []
middates = []

year = int(str(min(Mref["fNight"]))[:4])
while year <= int(str(max(Mref["fNight"]))[:4]):
    firstNight = 1e4*year+101
    if firstNight < max(Mref["fNight"]) and firstNight > min(Mref["fNight"]):
        dates.append(firstNight)
    middleNight = 1e4*year+702
    if middleNight < max(Mref["fNight"]) and middleNight > min(Mref["fNight"]):
        middates.append(1e4*year+702)
    year += 1

yearticks = [mjd.fNight_to_mjd(date) for date in dates]
midticks = [mjd.fNight_to_mjd(middate) for middate in middates]

ax2.set_xlim(ax.get_xlim())
ax2.set_xticks(yearticks)
ax2.set_xticklabels('')
ax2.set_xticks(midticks, minor=True)
ax2.set_xticklabels([str(middate)[0:4] for middate in middates], minor=True)

ax3 = ax.twinx()
ax3.set_ylim(190, 450)
ax3.set_ylabel(r"$R_{\mathrm{CR}}$"+" in events/min",
               rotation=270, labelpad=18)
factPlot = ax3.errorbar(
    goodSpreadNights["mjd"], goodSpreadNights["mean"],
    yerr=goodSpreadNights["meanErr"], fmt=".",
    label="FACT cosmic-ray rate"
)
fig.tight_layout()

lines, labels = ax.get_legend_handles_labels()
lines2, labels2 = ax3.get_legend_handles_labels()
ax.legend(lines + lines2, labels + labels2, loc=1)
fig.savefig("./CorrMAGICReflectivity.png", dpi=300)
fig.savefig("./CorrMAGICReflectivity.pdf")
