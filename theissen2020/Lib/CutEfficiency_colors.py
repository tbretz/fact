import seaborn as sns

# add ticks
# sns.set_style("ticks")

# colors for cuts
basic = sns.color_palette()
dark = sns.color_palette("dark")

raw = basic[9]
orig = basic[7]
trig = basic[4]
qc = basic[1]
tsq = basic[2]
spec = basic[3]
lc = basic[0]

tsq_dark = dark[2]

# palettes
spec_palette = [orig, trig, qc, tsq, spec]
lc_palette = [orig, trig, qc, tsq, lc]

# colormaps
cm_raw = sns.light_palette(raw, input="rgb", as_cmap=True)
cm_tsq = sns.light_palette(tsq_dark, input="rgb", as_cmap=True, reverse=False)

bad_color = (0.8, 0.8, 0.8)

# other stuff
# sns.set(context="notebook", palette="dark", font_scale=1.4,
#         rc={"lines.linewidth": 7})
sns.set_context(rc={'patch.linewidth': 0.5})

