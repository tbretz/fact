import emcee
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import minimize

import ExcessCalculation as ec
import setFigureConfig as figConfig
from correctionModels import dustCorrectionFactor, dustModel
from DataFrameBinning import addLinearBinning
from LibFunctions import weightedMean

np.random.seed(123)
recreateObsTimePlot = True
recreateDataSpacePlot = True
mlFit = False

runsCR = pd.read_hdf("../MergedExcessInfos/CrabExcess.h5", "CrabExcess")

runsCR = runsCR[runsCR["fNight"] > 20140723]
runsCR = runsCR[runsCR["RCR_corr"] > 225]
runsCR = runsCR[runsCR["RCR_corr"] < 275]
runsCR = runsCR[runsCR["fZenithDistanceMean"] < 30]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 450]
zeroruns = runsCR[runsCR["fTNGDust"] == 0]
runsCR = runsCR[runsCR["fTNGDust"] > 0]


runsCR["DustLog10"] = np.log10(runsCR["fTNGDust"])

runsCR, binEdges, dropped = addLinearBinning(
    runsCR, "DustLog10", "DustBin", 18, minVal=-1., maxVal=1.8
)

grpRuns = runsCR.groupby("DustBin")

binnedDf = pd.DataFrame()
binnedDf["N"] = grpRuns.apply(lambda group: len(group))
binnedDf["meanDust"] = grpRuns["fTNGDust"].apply(
    lambda group: np.log10(np.mean(group))
)
binnedDf = ec.excessPerBinUncorrected(binnedDf, grpRuns)

# reset index to let CRBin become a column
binnedDf = binnedDf.reset_index()
binnedDf = binnedDf[binnedDf["DustBin"] != -1]
binnedDf["BinStart"] = binEdges[:-1]
binnedDf["BinStop"] = binEdges[1:]
binnedDf["BinCenter"] = (binEdges[:-1]+binEdges[1:])/2
t_tot = np.sum(binnedDf["t_eff"])
# observation time per bin
if recreateObsTimePlot:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    # ax.plot(
    #     10**(binnedDf["BinCenter"]), binnedDf["t_eff"],
    #     color="tab:blue", zorder=1.1, marker=".",
    # )
    ax.bar(
        10**(binnedDf["BinCenter"]), binnedDf["t_eff"],
        width=(10**binnedDf["BinStop"]-10**binnedDf["BinStart"])*0.95,
        color="tab:blue", zorder=1.1,
        label="total effective\nobservation time: " +
        str(int(round(t_tot, 0)))+"h"
    )
    ax.set_xlabel(r"$c_{\mathrm{dust}}$"+" in µg/m³")
    ax.set_ylabel("effective observation\ntime in h")
    ax.set_xlim(1e-1, 1e2)
    ax.set_xscale("log")
    ax.legend(handlelength=0, handletextpad=0)
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Dust_ObsTime.pdf")
    fig.savefig("Dust_ObsTime.png", dpi=300)

    print(t_tot)

# normalization
nsZero = np.sum(zeroruns["fNumSigEvts"])
nbZero = np.sum(zeroruns["fNumBgEvts"])
teffZero = np.sum(zeroruns["fEffectiveTime_h"])
neZero = ec.calcExcessEvents(nsZero, nbZero)
reZero = neZero/teffZero

# CR model
alpha = 4.21/1000
xvals = np.logspace(-1., 2, 100)
yvals = dustModel(xvals, reZero, alpha)

# chiq to model
binnedDf["CRPrediction"] = dustModel(binnedDf["BinCenter"], reZero, alpha)
binnedDf["Chi2TermModel"] = (
    (binnedDf["RExc"] - binnedDf["CRPrediction"])/binnedDf["RExcErr"]
)**2

chi2 = np.sum(binnedDf["Chi2TermModel"])
N = len(binnedDf)
chi2N = chi2/N

print(chi2, N, chi2N)

###############################################################################
#    Free alpha and r0 fit attempts
###############################################################################

if mlFit:

    def nll(pars, x, y,  yerr):
        r0, alpha, log_f = pars
        model = dustModel(x, r0, alpha, gamma=2.7)
        yerr2 = np.exp(2*log_f) * yerr**2
        ll = -0.5*np.sum((y-model)**2/yerr2 + np.log(yerr2))
        return -ll

    initial = np.array([70, 6e-3, -2.5])
    soln = minimize(
        nll, initial,
        args=(
            binnedDf["meanDust"], binnedDf["RExc"],
            binnedDf["RExcErr"]
        )
    )
    mLPars = soln.x

    # print maximum likelyhood results
    print("maximum likelyhood results:")
    print(mLPars[:-1], np.exp(mLPars[-1]))
    print("\n")

###############################################################################


if recreateDataSpacePlot:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        10**(binnedDf["meanDust"]), binnedDf["RExc"],
        yerr=binnedDf["RExcErr"],
        fmt=".", color="tab:blue", zorder=1.3, label=None
    )
    ax.plot(
        xvals, yvals,
        color="tab:purple",
        lw=3, zorder=1.2,
        label="best fit on CR:\n   " +
        r"$\alpha= 4.21^{+0.26}_{-0.25}\; \mathrm{m}^{3} \mathrm{ng}^{-1}$"
    )
    ax.legend(loc="lower left")
    # ax.set_xlabel("dust concentration "+r"$c_{\mathrm{dust}}$"+" in µg/m³")
    ax.set_xlabel(r"$c_{\mathrm{dust}}$"+" in µg/m³")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_xscale("log")
    ax.set_ylim(15, 70)
    ax.set_xlim(1e-1, 1e2)
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Dust_Dependence.pdf")
    fig.savefig("Dust_Dependence.png", dpi=300)
