from peripylib import figures
import pandas as pd


# Plot Mrk421 light curve from Whipple

data = pd.read_csv('mrk421_whipple.csv', sep=',')

fig, ax = figures.newfig()

ax.errorbar(data.mid, data.rate, yerr=data.error, ls='', marker='.')

# Add some years
ax2 = ax.twiny()
ax2.set_xlim(ax.get_xlim())

# Set ticks and ticks locations
ax2.set_xticks([50083, 51544, 53371, 54466])
ax2.set_xticklabels(['1996', '2000', '2005','2008'])

ax2.set_axisbelow(True)
ax2.grid(color='black', linestyle='--', linewidth=0.25)

ax.set(xlabel='Epoch / MJD', ylabel='Rate / Crab units')
figures.savefig('whipple_mrk421', pgf=True)
