import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns

from HistogramManager import HistogramManager
from Histogram import Histogram
import setFigureConfig as sfc

EBins = 64
IBins = 54
ZdBins = 60
si = -2.5

pdf = PdfPages("QulaityCuts.pdf")
hm = HistogramManager()
hm.loadHistogramDict("../data/QC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)


cp = sns.color_palette()
sfc.halfWidth(aspectRatio=1)

# Energy
figE = hm.plotHistograms(
    ["lgE_triggered", "lgE_NumIslands", "lgE_NumUsedPixels", "lgE_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="lgEnergy", legloc=[None, "lower left"], style="stairs"
)
figE.tight_layout()
pdf.savefig(figE)

# Zd
figZd = hm.plotHistograms(
    ["Zd_triggered", "Zd_NumIslands", "Zd_NumUsedPixels", "Zd_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="Zd", legloc=[None, "lower left"], style="stairs"
)
figZd.tight_layout()
pdf.savefig(figZd)

# Impact
figI = hm.plotHistograms(
    ["Impact_triggered", "Impact_NumIslands", "Impact_NumUsedPixels",
     "Impact_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="Impact", legloc=[None, "lower left"], style="stairs"
)
figI.tight_layout()
pdf.savefig(figZd)

pdf.close()

# all 3 in a row
sfc.fullWidth()
fig, (axe, axz, axi) = plt.subplots(ncols=3)

axe = hm.plotHistograms(
    ["lgE_triggered", "lgE_NumIslands", "lgE_NumUsedPixels", "lgE_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="lgEnergy", legloc=None, style="stairs",
    ax=axe
)
axz = hm.plotHistograms(
    ["Zd_triggered", "Zd_NumIslands", "Zd_NumUsedPixels", "Zd_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="Zd", legloc=[None, "lower left"], style="stairs",
    ax=axz
)
axi = hm.plotHistograms(
    ["Impact_triggered", "Impact_NumIslands", "Impact_NumUsedPixels",
     "Impact_Leakage1"],
    labels=["triggered", "Number of Islands", "Number of Pixels", "Leakage1"],
    mode="Impact", legloc=[None, "lower left"], style="stairs",
    ax=axi
)

fig.tight_layout()
fig.savefig("QualityCuts_EZdI.pdf")


# efficiency
sfc.fullWidth(aspectRatio=0.5, alpha=0.8)
fig, axe = plt.subplots()

ylim = [0.7, 1.01]
# energy
axe = hm.plotEfficiencies(
    ["lgE_triggered", "lgE_NumIslands"],
    cuts=["triggered", "Number of Islands"],
    mode="lgEnergy", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axe, z0=5
)
axe = hm.plotEfficiencies(
    ["lgE_triggered", "lgE_NumUsedPixels"],
    cuts=["triggered", "Number of Pixels"],
    mode="lgEnergy", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axe, z0=5
)
axe = hm.plotEfficiencies(
    ["lgE_triggered", "lgE_Leakage1"],
    cuts=["triggered", "Leakage1"],
    mode="lgEnergy", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", yscale="linear", ax=axe, ylim=ylim, z0=5
)
# add next plot aside
axe.set_xticks(np.linspace(2.5, 4.5, 5))
divider = make_axes_locatable(axe)
axz = divider.append_axes("right", size="100%", pad=0.0)
axe.get_legend().remove()
axe.grid()
axz.grid()
# Zd
axz = hm.plotEfficiencies(
    ["Zd_triggered", "Zd_NumIslands"],
    cuts=["triggered", "Number of Islands"],
    mode="Zd", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axz, z0=5
)
axz = hm.plotEfficiencies(
    ["Zd_triggered", "Zd_NumUsedPixels"],
    cuts=["triggered", "Number of Pixels"],
    mode="Zd", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axz, z0=5
)
axz = hm.plotEfficiencies(
    ["Zd_triggered", "Zd_Leakage1"],
    cuts=["triggered", "Leakage1"],
    mode="Zd", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", yscale="linear", ax=axz, ylim=ylim, z0=5
)
axz.set_xticks(np.linspace(0, 60, 7))
axz.set_ylabel("")
axz.set_xlabel(r"$Zd$")
axz.set_xticklabels([str(int(xt)) + r"°" for xt in axz.get_xticks()])
axz.set_yticklabels("")
for tic in axz.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False
axz.get_legend().remove()
# Impact
axi = divider.append_axes("right", size="100%", pad=0.0)
axi = hm.plotEfficiencies(
    ["Impact_triggered", "Impact_NumIslands"],
    cuts=["triggered", "Number of Islands"],
    mode="Impact", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axi, z0=5
)
axi = hm.plotEfficiencies(
    ["Impact_triggered", "Impact_NumUsedPixels"],
    cuts=["triggered", "Number of Pixels"],
    mode="Impact", legloc=[None, "lower left"], style="stairs",
    h0Name="triggered data", ax=axi, z0=5
)
axi = hm.plotEfficiencies(
    ["Impact_triggered", "Impact_Leakage1"],
    cuts=["triggered", "Leakage1"],
    mode="Impact", legloc=[[-1, 0], "lower left"], style="stairs",
    h0Name="triggered data", yscale="linear", ax=axi, ylim=ylim, z0=5
)
axi.grid()
axi.set_ylabel("")
axi.set_yticklabels("")
for tic in axi.yaxis.get_major_ticks():
    tic.tick1On = tic.tick2On = False

axi.set_xlabel("Impact $I$ [m]")
axi.set_xticks(np.linspace(0, 54000, 5))
axi.set_xticklabels([str(int(1e-2*xt)) for xt in axi.get_xticks()])
axe.set_ylabel("cut efficiency")
fig.tight_layout()
fig.savefig("QC_efficiency_EZdI.pdf")



# plots of the distributions of the 3 variables
# Num Islands
sfc.halfWidth(aspectRatio=0.9, alpha=1.4)
figQC_I, axI = plt.subplots()
axI.set_prop_cycle("color", [cp[0]])

hm.histograms["NumIslands"].centers -= 0.5
axI = hm.plotHistograms(
    ["NumIslands"], labels=["NumIslands"], ax=axI,
    yscale="log", xlabel="Number of Islands", xlim=[-0.5, 9.5]
)
axI.axvline(x=3.5, color="black", zorder=5)
axI.get_legend().remove()
figQC_I.tight_layout()
figQC_I.savefig("QC_histogram_NumIslands.pdf")

# Num Used Pixels
figQC_P, axP = plt.subplots()
axP.set_prop_cycle("color", [cp[1]])

axP = hm.plotHistograms(
    ["lgNumUsedPixels"], labels=["logNumIslands"], ax=axP,
    yscale="log", xlabel="Number of Pixels"
)
axP.axvline(x=np.log10(5.5), color="black", zorder=5)
axP.get_legend().remove()
# axP.set_xlim(0, 3.5)
axP.set_xticks(np.arange(3)+1)
# axP.set_xticklabels([r"$10^{%g}$" % (i) for i in np.arange(5)+1])
axP.set_xticklabels(["10", "100", "1000"])
figQC_P.tight_layout()
figQC_P.savefig("QC_histogram_NumPixels.pdf")

# Leakage
figQC_L, axL = plt.subplots()
axL.set_prop_cycle("color", [cp[2]])
axL = hm.plotHistograms(
    ["Leakage1"], labels=["Leakage1"], ax=axL,
    yscale="log", xlabel="Leakage1"
)
axL.axvline(x=0.1, color="black", zorder=5)
axL.get_legend().remove()
figQC_L.tight_layout()
figQC_L.savefig("QC_histogram_Leakage1.pdf")
