import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns
import palettable as pt

from readYAML import loadConfiguration
from LayerMC import Layer
from Plotting import plotEvolutionSizeColor, plotInitials, plotSize
import coloring as coloring


calc = True
plot = True
configPath = "eom.yaml"


if calc:
    configs = loadConfiguration(configPath)
    L = Layer(
        sigma=float(configs["eom"]["sigma"]),
        betaA=float(configs["eom"]["betaA"]),
        betaG=float(configs["eom"]["betaG"]),
        Asup=float(configs["eom"]["Asup"]),
        Bsup=float(configs["eom"]["Bsup"]),
        rMax=float(configs["grid"]["rMax"]),
        dtau=float(configs["grid"]["dtau"]),
        birth=bool(configs["birth"]),
        sMin=float(configs["sMin"]),
        initialDistanceFactor=float(configs["initialDistanceFactor"]),
        p0_variance=float(configs["p0_variance"]),
        merging=bool(configs["merging"]),
        boundaries=bool(configs["boundaries"])
    )
    # add new plasmoids at given positions
    x = np.linspace(-1, 1, 31)
    newR = x**3
    newP = L.sampleNewMomentum(newR)
    newbeta = newP / np.sqrt(1 + newP**2)
    newId = np.arange(len(newP)) + L.NTotal + 1
    newPlasmoids = pd.DataFrame(
        {
            "r": newR,
            "s": np.ones(len(newR)) * L.sMin,
            "beta": newbeta
        },
        index=newId
    )
    newInitials = pd.DataFrame(
        {
            "r0": newR,
            "p0": newP
        },
        index=newId
    )
    # to keep id unique
    L.NTotal += len(newR)
    # add to timeStep
    L.Plasmoids.append(newPlasmoids)
    L.initalValues = pd.concat([L.initalValues, newInitials])
    L.times.append(0)
    # do calculation
    L.calculateTime(
        tauMax=float(configs["grid"]["tauMax"]),
        dtau=float(configs["grid"]["dtau"]),
        saveBeforeBirth=bool(configs["saveBeforeBirth"]),
        printProgressEvery=int(configs["printProgressEvery"])
    )
    L.saveToHDF(configs["save"]["path"], key=configs["save"]["key"])

if plot:
    # load from file
    allPlasmoids = pd.read_hdf("eom.h5", "allPlasmoids")

    sns.set(style="ticks")
    pdf = PdfPages("eom.pdf")

    # plots with colors from initial position

    # define colors
    # cols = np.append(np.linspace(0, 1, 100), np.linspace(0, 1, 100)[::-1])
    # cmap = plt.get_cmap("jet")
    # cmap = sns.cubehelix_palette(start=0.5, rot=-.75, as_cmap=True)
    cmap = pt.scientific.sequential.Nuuk_20.mpl_colormap

    pdf.savefig(
        plotEvolutionSizeColor(
            allPlasmoids, Ncol=50, minLifeTime=0,
            cmap=coloring.c_size()
        )
    )
    pdf.savefig(plotSize(allPlasmoids, cmap=coloring.c_beta()))
    pdf.savefig(plotInitials(allPlasmoids))
    pdf.close()
