import numpy as np
from readYAML import allPlasmoidsFromYAML, loadConfiguration
from Histogram import Histogram


# momentum p from velocity (Gamma*beta)
def pOfBeta(beta):
    return beta/np.sqrt(1. - beta**2)


# expect a steady state after tau=1
def createHistograms(
    pathToConfigFile, id,
    s=True, sRange=[-3, 0], sNBins=10,
    p=True, pRange=[0, 1.5], pNBins=10,
    tauMin=1, tauMax=100
):
    # load all Plasmoids from file as given in config file
    allPlasmoids = allPlasmoidsFromYAML(pathToConfigFile, id)

    configs = loadConfiguration(pathToConfigFile)
    sigma = float(configs["eom"]["sigma"])

    # cut on time
    sf = allPlasmoids[allPlasmoids["tau"].between(tauMin, tauMax)]["s"]
    bf = allPlasmoids[allPlasmoids["tau"].between(tauMin, tauMax)]["beta"]
    pf = np.abs(pOfBeta(bf) / np.sqrt(sigma))

    # total number for normalization
    NTotal = len(allPlasmoids[allPlasmoids["tau"] > tauMin]["s"])

    # to get the name
    configs = loadConfiguration(pathToConfigFile)

    # histogram with log(s)
    sLabel = "log10s_" + configs["name"] + "_" + str(id)
    sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    hs = Histogram(
        label=sLabel,
        data=np.log10(sf), histRange=sRange, nBins=sNBins,
        ) / NTotal
    # histogram with p / sqrt(sigma)
    pLabel = "p_normed_" + configs["name"] + "_" + str(id)
    pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    hp = Histogram(
        label=pLabel,
        data=pf, histRange=pRange, nBins=pNBins,
        ) / NTotal

    return hs, hp
