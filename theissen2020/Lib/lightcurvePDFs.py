import numpy as np
import math


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


def gammaDist(x, kappa, theta):
    return theta**kappa * np.exp(-x/theta) * x**(kappa-1) / math.gamma(kappa)


def lognorm(x, mu, sig):
    a = (np.log(x) - mu)/np.sqrt(2*sig**2)
    return np.array([0.5 + 0.5*math.erf(e) for e in a])


def modelMax(x, lW, lMu, lSig, kappa, theta):
    return lW*lognorm(x, lMu, lSig) + \
        (1-lW)*gammaDist(x, kappa, theta)


def model(x, gMu, gSig, lMu, lSig, kappa, theta, gW, lW):
    pos = x[x > 0]
    neg = x[x <= 0]

    posRes = gW*gaussian(pos, gMu, gSig) + \
        lW*lognorm(pos, lMu, lSig) + \
        (1-gW-lW)*gammaDist(pos, kappa, theta)
    negRes = gW*gaussian(neg, gMu, gSig) + \
        (1-gW-lW)*gammaDist(neg, kappa, theta)

    return np.concatenate((posRes, negRes))


def fskde(y, err, evals):
    kde = np.zeros(len(evals))
    for i in range(len(y)):
        kde += gaussian(evals, y[i], err[i])

    return kde
