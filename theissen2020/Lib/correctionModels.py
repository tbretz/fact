import numpy as np


def zenithModel(theta, r0, chi, xi, kappa, gamma=2.49, alpha=0.065):
    z = np.cos(np.radians(np.array(theta)))
    return r0*(z**((gamma-1)*(5*chi+alpha-2)-2*chi) *
               (np.exp(1-1./z))**(xi*(gamma-1)) *
               (1-np.log(z))**(2*kappa*(gamma-1)))


def zenithCorrectionFactor(theta, chi, xi, kappa, gamma=2.49, alpha=0.065):
    z = np.cos(np.radians(np.array(theta)))
    return 1 / (
        (z**((gamma-1)*(5*chi+alpha-2)-2*chi) *
         (np.exp(1-1./z))**(xi*(gamma-1)) *
         (1-np.log(z))**(2*kappa*(gamma-1)))
    )


def dustModel(c, r0, alpha, gamma=2.7):
    return r0*(1+alpha*c)**(-gamma)


def dustCorrectionFactor(c, alpha, gamma=2.7):
    if c == 0:
        return 1
    else:
        return 1/(1+alpha*c)**(-gamma)


dustCorrectionFactor = np.vectorize(dustCorrectionFactor)


def perfModel(rCr, p0, p1):
    return p1*rCr+p0


def perfCorrectionFactor(rCr, p0, p1, rCrRef=250):
    nom = p1*rCrRef+p0
    denom = p1*rCr+p0

    return nom/denom


perfCorrectionFactor = np.vectorize(perfCorrectionFactor)


def thresholdModel(th, r_th_min, th_mu, sigma, gamma=2.7):
    delta = gamma - 1
    beta = 0.5*np.tanh((np.log10(th)-np.log10(th_mu))/sigma) + 0.5
    return r_th_min * ((1-beta) + beta * th**(-delta) * (th_mu)**delta)


def thresholdCorrectionFactor(th, th_mu, sigma, gamma=2.7):
    delta = gamma - 1
    beta = 0.5*np.tanh((np.log10(th)-np.log10(th_mu))/sigma) + 0.5
    return 1 / ((1-beta) + beta * th**(-delta) * (th_mu)**delta)
