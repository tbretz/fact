#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from iminuit import Minuit
from autograd import jacobian

import seaborn as sns
sns.set_style('ticks')


def parabola(x, p):
    return p[0] + p[1]*x + p[2]*x**2


x = np.linspace(-10, 20, 50)
x_plot = np.linspace(-10, 20, 1000)
y_true = parabola(x, [3, 12, -3])
y = y_true + np.random.randn(len(x))
y_err = [max(i, 10) for i in np.sqrt(abs(y))]


def least_sq(p):
    return (((y - parabola(x, p))/y_err)**2).sum()

def fitting(func, guess, fix=None, limit=None):
    '''
    func: function to be MINIMIZED (e.g. square sum of pull)
    guess: tuple guess for initial params
    fix: tuple of length guess with True for a param which is fixed and false otherwise (optional)
    limit: tuple of length guess containing 2-tuple with (lower limit, upper limit) for each value
            in case of no limit value is None (e.g. (2, None)) (optional)

    returns array of best params and cov matrix
    '''
    if fix is None:
        fix = tuple(np.ones(len(guess)) < 0)
    if limit is None:
        limit = tuple(len(guess)*[(None, None)])
    m = Minuit.from_array_func(func, guess, error=tuple(0.1*np.ones(len(guess))), fix=fix, limit=limit, errordef=1, print_level=0)
    _, param = m.migrad()
    best_param = np.array([i.value for i in param])
    m.minos()
    cov = np.array(m.matrix())
    return best_param, cov
    
best_param, cov = fitting(least_sq, (1, 2, -1))


def extreme(p):
    return -p[1]/(2*p[2])


def val_err(func, p, cov):
    val = func(p)
    jac = np.array(jacobian(func)(p))
    var = np.dot(jac, np.dot(cov, jac.T))
    return val, np.sqrt(var)

found_extreme, found_extreme_err = val_err(extreme, best_param, cov)
print(found_extreme, found_extreme_err)

#m.minos()
#cov = np.array(m.matrix())
#found_extreme, found_extreme_err = val_err(extreme, best_param, cov)
#print(found_extreme, found_extreme_err)


plt.errorbar(x, y, yerr=y_err, fmt='.')
plt.plot(x_plot, parabola(x_plot, best_param))
plt.axvline(found_extreme)
plt.show()
