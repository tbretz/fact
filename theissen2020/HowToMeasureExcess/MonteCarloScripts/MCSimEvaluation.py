import argparse

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
import yaml
from matplotlib.backends.backend_pdf import PdfPages


def printProgressBar(
        iteration, total, prefix='', suffix='',
        decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in
                                  percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(
        100 * (iteration / float(total))
    )
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def evaluateParamSet(simResults, paramId, plotExc=False, plotDev=False):

    results = {}
    refSim = simResults[simResults["fParamId"] == paramId]
    results["fParamId"] = paramId
    results["LambdaExcess"] = refSim["LambdaExcess"].values[0]
    results["LambdaBackground"] = refSim["LambdaBackground"].values[0]

    grid = (6, 1)
    if plotExc:
        excFig = plt.figure(figsize=(6, 20))
        excAxes = excFig.subplots(grid[0], grid[1])
        plt.subplots_adjust(top=0.93, bottom=0.05, hspace=0.27)
        excFig.text(
            0.5, 0.98, "Simulation of " + str(len(refSim)) + " Nights" +
            " (Parameter Set ID: " +
            str(refSim["fParamId"].values[0])+")",
            verticalalignment="top", horizontalalignment="center",
            fontsize=14
        )
    else:
        excFig = None
        excAxes = np.array([
            [None for columns in range(grid[1])] for rows in range(grid[0])
        ])

    if plotDev:
        devFig = plt.figure(figsize=(12, 20))
        devAxes = devFig.subplots(grid[0], 2)
        plt.subplots_adjust(top=0.93, bottom=0.05, hspace=0.27)
        devFig.text(
            0.5, 0.98, "Simulation of " + str(len(refSim)) + " Nights" +
            " (Parameter Set ID: " +
            str(refSim["fParamId"].values[0])+")",
            verticalalignment="top", horizontalalignment="center",
            fontsize=14
        )
    else:
        devFig = None
        devAxes = np.array([
            [None for columns in range(2)] for rows in range(grid[0])
        ])

    """
    Excess Distribution
    """
    excOut = distributionEvaluation(refSim, "Excess0",
                                    preTitle="Nightwise",
                                    ax=excAxes.flatten()[0])
    results["NW_ExcMean"] = excOut[0]
    results["NW_ExcStd"] = excOut[1]
    results["NW_ExcSkew"] = excOut[2]
    excOut = distributionEvaluation(refSim, "Excess1",
                                    preTitle="Gaussian Runwise",
                                    ax=excAxes.flatten()[1])
    results["RWOld_ExcMean"] = excOut[0]
    results["RWOld_ExcStd"] = excOut[1]
    results["RWOld_ExcSkew"] = excOut[2]
    excOut = distributionEvaluation(refSim, "Excess2",
                                    preTitle="New Runwise",
                                    ax=excAxes.flatten()[2])
    results["RWNew_ExcMean"] = excOut[0]
    results["RWNew_ExcStd"] = excOut[1]
    results["RWNew_ExcSkew"] = excOut[2]
    excOut = distributionEvaluation(refSim, "Excess3",
                                    preTitle="3rd Method",
                                    ax=excAxes.flatten()[3])
    results["3_ExcMean"] = excOut[0]
    results["3_ExcStd"] = excOut[1]
    results["3_ExcSkew"] = excOut[2]
    excOut = distributionEvaluation(refSim, "Excess4",
                                    preTitle="4th Method ",
                                    ax=excAxes.flatten()[4])
    results["4_ExcMean"] = excOut[0]
    results["4_ExcStd"] = excOut[1]
    results["4_ExcSkew"] = excOut[2]
    excOut = distributionEvaluation(refSim, "Excess5",
                                    preTitle="5th Method ",
                                    ax=excAxes.flatten()[5])
    results["5_ExcMean"] = excOut[0]
    results["5_ExcStd"] = excOut[1]
    results["5_ExcSkew"] = excOut[2]

    """
    Deviation
    """

    relDevOut = relDevEvaluation(refSim, "DeltaSigmaNW",
                                 preTitle="Nightwise",
                                 ax=devAxes.flatten()[0])
    absDevOut = absDevEvaluation(
        refSim, "DeltaNW", preTitle="Nightwise", ax=devAxes.flatten()[1]
    )
    results["NW_AbsDevMean"] = absDevOut[0]
    results["NW_DevMean"] = relDevOut[0]
    results["NW_DevStd"] = relDevOut[1]
    results["NW_DevSkew"] = relDevOut[2]

    relDevOut = relDevEvaluation(refSim, "DeltaSigmaRWOld",
                                 preTitle="Gaussian Runwise",
                                 ax=devAxes.flatten()[2])
    absDevOut = absDevEvaluation(
        refSim, "DeltaRWOld", preTitle="Gaussian Runwise",
        ax=devAxes.flatten()[3]
    )
    results["RWOld_AbsDevMean"] = absDevOut[0]
    results["RWOld_DevMean"] = relDevOut[0]
    results["RWOld_DevStd"] = relDevOut[1]
    results["RWOld_DevSkew"] = relDevOut[2]

    relDevOut = relDevEvaluation(refSim, "DeltaSigmaRWNew",
                                 preTitle="New Runwise",
                                 ax=devAxes.flatten()[4])
    absDevOut = absDevEvaluation(
        refSim, "DeltaRWNew", preTitle="New Runwise",
        ax=devAxes.flatten()[5]
    )
    results["RWNew_AbsDevMean"] = absDevOut[0]
    results["RWNew_DevMean"] = relDevOut[0]
    results["RWNew_DevStd"] = relDevOut[1]
    results["RWNew_DevSkew"] = relDevOut[2]
    relDevOut = relDevEvaluation(refSim, "DeltaSigma3",
                                 preTitle="3rd Method",
                                 ax=devAxes.flatten()[6])
    absDevOut = absDevEvaluation(
        refSim, "Delta3", preTitle="3rd Method",
        ax=devAxes.flatten()[7]
    )
    results["3_AbsDevMean"] = absDevOut[0]
    results["3_DevMean"] = relDevOut[0]
    results["3_DevStd"] = relDevOut[1]
    results["3_DevSkew"] = relDevOut[2]
    relDevOut = relDevEvaluation(refSim, "DeltaSigma4",
                                 preTitle="4th Method ",
                                 ax=devAxes.flatten()[8])
    absDevOut = absDevEvaluation(
        refSim, "Delta4", preTitle="4rd Method",
        ax=devAxes.flatten()[9]
    )
    results["4_AbsDevMean"] = absDevOut[0]
    results["4_DevMean"] = relDevOut[0]
    results["4_DevStd"] = relDevOut[1]
    results["4_DevSkew"] = relDevOut[2]
    relDevOut = relDevEvaluation(refSim, "DeltaSigma5",
                                 preTitle="5th Method ",
                                 ax=devAxes.flatten()[10])
    absDevOut = absDevEvaluation(
        refSim, "Delta5", preTitle="5th Method",
        ax=devAxes.flatten()[11]
    )
    results["5_AbsDevMean"] = absDevOut[0]
    results["5_DevMean"] = relDevOut[0]
    results["5_DevStd"] = relDevOut[1]
    results["5_DevSkew"] = relDevOut[2]

    return pd.Series(results), excFig, devFig


def distributionEvaluation(sim, column, ax=None, preTitle=""):

    iD = sim["fParamId"].values[0]
    meanExc = np.mean(sim[column])
    stdExc = np.std(sim[column], ddof=1)
    skewExc = stats.skew(sim[column])

    characteristics = np.array([meanExc, stdExc, skewExc])

    if ax is not None:
        numBins = 25
        numNights = len(sim)
        trueExcess = sim["LambdaExcess"].values[0]
        limitLeft = trueExcess-5
        limitRight = trueExcess+5

        ax.set_title(
            preTitle+" Evaluation: " +
            r"$R_{Exc}$ Distribution")

        hist = ax.hist(sim[column],
                       bins=np.linspace(
                           limitLeft, limitRight, num=numBins),
                       label=r"$\lambda_{E} = $" +
                       str((sim["LambdaExcess"].values[0].round(2))) +
                       ", " +
                       r"$\lambda_{B} = $" +
                       str((sim["LambdaBackground"].values[0]).round(2)),
                       color="tab:blue")
        ax.text(0.98, 0.98,
                "Mean: "+str(meanExc.round(2)) +
                "\n Std: "+str(stdExc.round(2)) +
                "\n Skew: "+str(np.round(skewExc, 2)),
                horizontalalignment='right',
                verticalalignment='top',
                transform=ax.transAxes)
        ax.axvline(np.mean(sim[column]), color="tab:green",
                   label=r"Mean $R_{Exc}$")
        ax.axvline(trueExcess, color="tab:orange", label=r"True $R_{Exc}$")
        ax.set_xlabel(
            r"Reconstructed Excess Rate $R_{Exc}$")
        ax.set_ylabel("Number of Runs")
        ax.set_xlim(limitLeft, limitRight)
        ax.set_ylim(0, 1.25*max(hist[0]))
        ax.legend(loc=2)
        plt.close()

    return characteristics


def relDevEvaluation(sim, column, ax=None, preTitle=""):

    iD = sim["fParamId"].values[0]
    meanDev = np.mean(sim[column])
    stdDev = np.std(sim[column], ddof=1)
    skewDev = stats.skew(sim[column])

    characteristics = np.array([meanDev, stdDev, skewDev])

    if ax is not None:
        numBins = 25
        numNights = len(sim)

        ax.set_title(
            preTitle+" Evaluation: " +
            r"$\Delta R_{Exc} / \sigma_{R_{Exc}}$ Distribution")

        hist = ax.hist(sim[column],
                       bins=np.linspace(-4.8, 4.8, num=numBins),
                       label=r"$\lambda_{E} = $" +
                       str((sim["LambdaExcess"].values[0].round(2))) +
                       ", " +
                       r"$\lambda_{B} = $" +
                       str((sim["LambdaBackground"].values[0]).round(2)),
                       color="tab:blue")
        ax.text(0.98, 0.98,
                "Mean: "+str(meanDev.round(2)) +
                "\n Std: "+str(stdDev.round(2)) +
                "\n Skew: "+str(np.round(skewDev, 2)),
                horizontalalignment='right',
                verticalalignment='top',
                transform=ax.transAxes)
        ax.axvline(np.mean(sim[column]), color="tab:green")
        ax.set_xlabel(
            r"Relative Deviation $\Delta R_{Exc} / \sigma_{R_{Exc}}$")
        ax.set_ylabel("Number of Runs")
        ax.set_xlim(-4.8, 4.8)
        ax.set_ylim(0, 1.25*max(hist[0]))
        ax.legend(loc=2)
        plt.close()

    return characteristics


def absDevEvaluation(sim, column, ax=None, preTitle=""):

    iD = sim["fParamId"].values[0]
    meanDev = np.mean(sim[column])
    stdDev = np.std(sim[column], ddof=1)
    skewDev = stats.skew(sim[column])

    characteristics = np.array([meanDev, stdDev, skewDev])

    if ax is not None:
        numBins = 25
        numNights = len(sim)

        ax.set_title(
            preTitle+" Evaluation: " +
            r"$\Delta R_{Exc}}$ Distribution")

        hist = ax.hist(sim[column],
                       bins=np.linspace(-5, 5, num=numBins),
                       label=r"$\lambda_{E} = $" +
                       str((sim["LambdaExcess"].values[0].round(2))) +
                       ", " +
                       r"$\lambda_{B} = $" +
                       str((sim["LambdaBackground"].values[0]).round(2)),
                       color="tab:blue")
        ax.text(0.98, 0.98,
                "Mean: "+str(meanDev.round(2)) +
                "\n Std: "+str(stdDev.round(2)) +
                "\n Skew: "+str(np.round(skewDev, 2)),
                horizontalalignment='right',
                verticalalignment='top',
                transform=ax.transAxes)
        ax.axvline(np.mean(sim[column]), color="tab:green")
        ax.set_xlabel(
            r"Absolute Deviation $\Delta R_{Exc}$")
        ax.set_ylabel("Number of Runs")
        ax.set_xlim(-5, 5)
        ax.set_ylim(0, 1.25*max(hist[0]))
        ax.legend(loc=2)
        plt.close()

    return characteristics


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--configFile", help="Path to input config file", type=str
    )
    args = parser.parse_args()

    configFile = args.configFile

    if configFile is not None:
        config = {}
        with open(configFile, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    else:
        print(
            "Please call the script with a valid config file (-i config.yaml)."
        )
        return 1

    # Load Simulation
    location = config["savingLocation"]["simulationFolder"]
    allSims = pd.read_hdf(
        location+config["savingLocation"]["rawHDF5FileName"],
        key=config["savingLocation"]["rawHDF5ResultsKey"]
    )
    if "createExcessDetailPlotsInFile" in config:
        plotExc = True
    else:
        plotExc = False

    if "createDeviationDetailPlotsInFile" in config:
        plotDev = True
    else:
        plotDev = False

    plotEveryXth = config["plotEveryXthParamSet"]

    # Calculate Deviation
    allSims["DeltaNW"] = allSims["Excess0"] - allSims["LambdaExcess"]
    allSims["DeltaSigmaNW"] = allSims["DeltaNW"]/allSims["Sigma0"]
    allSims["DeltaRWOld"] = allSims["Excess1"] - allSims["LambdaExcess"]
    allSims["DeltaSigmaRWOld"] = allSims["DeltaRWOld"]/allSims["Sigma1"]
    allSims["DeltaRWNew"] = allSims["Excess2"] - allSims["LambdaExcess"]
    allSims["DeltaSigmaRWNew"] = allSims["DeltaRWNew"]/allSims["Sigma2"]
    allSims["Delta3"] = allSims["Excess3"] - allSims["LambdaExcess"]
    allSims["DeltaSigma3"] = allSims["Delta3"]/allSims["Sigma3"]
    allSims["Delta4"] = allSims["Excess4"] - allSims["LambdaExcess"]
    allSims["DeltaSigma4"] = allSims["Delta4"]/allSims["Sigma4"]
    allSims["Delta5"] = allSims["Excess5"] - allSims["LambdaExcess"]
    allSims["DeltaSigma5"] = allSims["Delta5"]/allSims["Sigma5"]

    # Create Results Df
    paramIds = np.unique(allSims["fParamId"].values)
    df = pd.DataFrame(
        columns=["fParamId",
                 "LambdaExcess",
                 "LambdaBackground",
                 "NW_ExcMean", "NW_ExcStd", "NW_ExcSkew",
                 "RWOld_ExcMean", "RWOld_ExcStd", "RWOld_ExcSkew",
                 "RWNew_ExcMean", "RWNew_ExcStd", "RWNew_ExcSkew",
                 "3_ExcMean", "3_ExcStd", "3_ExcSkew",
                 "4_ExcMean", "4_ExcStd", "4_ExcSkew",
                 "5_ExcMean", "5_ExcStd", "5_ExcSkew",
                 "NW_DevMean", "NW_DevStd", "NW_DevSkew", "NW_AbsDevMean",
                 "RWOld_DevMean", "RWOld_DevStd", "RWOld_DevSkew",
                 "RWOld_AbsDevMean",
                 "RWNew_DevMean", "RWNew_DevStd", "RWNew_DevSkew",
                 "RWNew_AbsDevMean",
                 "3_DevMean", "3_DevStd", "3_DevSkew", "3_AbsDevMean",
                 "4_DevMean", "4_DevStd", "4_DevSkew", "4_AbsDevMean",
                 "5_DevMean", "5_DevStd", "5_DevSkew", "5_AbsDevMean"],
        index=paramIds,
        dtype=np.float64
    )

    # Create PDF
    if plotExc:
        ExcessPdf = PdfPages(
            location+config["createExcessDetailPlotsInFile"]
        )
    if plotDev:
        DeviationPdf = PdfPages(
            location+config["createDeviationDetailPlotsInFile"]
        )

    # Evaluation
    printProgressBar(
        0, int(1.1*len(paramIds)),
        prefix='Progress:', suffix='Complete', length=50
    )
    for i in range(len(paramIds)):
        paramId = paramIds[i]

        if i % plotEveryXth == 0:
            evaluation = evaluateParamSet(
                allSims, paramId, plotExc=plotExc, plotDev=plotDev
            )
            if plotExc:
                ExcessPdf.savefig(evaluation[1])
            if plotDev:
                DeviationPdf.savefig(evaluation[2])
        else:
            evaluation = evaluateParamSet(allSims, paramId)

        df.loc[paramId] = evaluation[0]
        printProgressBar(
            i, int(1.1*len(paramIds)),
            prefix='Progress:', suffix='Complete', length=50
        )

    # Close PDFs
    if plotExc:
        ExcessPdf.close()
    if plotDev:
        DeviationPdf.close()

    # Save Results
    df["fParamId"] = df["fParamId"].map(np.int)
    df.to_hdf(location+config["savingLocation"]["evalHDF5FileName"],
              key=config["savingLocation"]["evalHDF5ResultsKey"],
              format="table")

    printProgressBar(
        1, 1,
        prefix='Progress:', suffix='Complete', length=50
    )

    return 0


if __name__ == "__main__":
    main()
