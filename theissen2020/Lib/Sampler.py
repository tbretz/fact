import numpy as np

''' class to sample from different distributions '''


class Sampler:
    def __init__(self, N=1000):
        self.N = N

    def uniform(self, a, b, N=None):
        if isinstance(N, (int, float)):
            return np.random.uniform(a, b, N)
        else:
            return np.random.uniform(a, b, self.N)

    def normal(self, mu, sig, N=None):
        if isinstance(N, (int, float)):
            return np.random.normal(mu, sig, N)
        else:
            return np.random.normal(mu, sig, self.N)

    def linear(self, xmin, xmax, N=None):
        if isinstance(N, (int, float)):
            return xmin + (xmax - xmin) * np.sqrt(np.random.uniform(0, 1, N))
        else:
            return xmin + (xmax - xmin) * np.sqrt(np.random.uniform(0, 1,
                                                                    self.N))

    def powerlaw(self, a, b, g, N=None):
        if isinstance(N, (int, float)):
            r = np.random.random(size=N)
            ag, bg = a**g, b**g
            return (ag + (bg - ag)*r)**(1./g)
        else:
            r = np.random.random(size=self.N)
            ag, bg = a**g, b**g
            return (ag + (bg - ag)*r)**(1./g)

    def expo10(self, a, b, g, N=None):
        if isinstance(N, (int, float)):
            return np.log10(self.powerlaw(10**a, 10**b, g, N))
        else:
            return np.log10(self.powerlaw(10**a, 10**b, g, self.N))
