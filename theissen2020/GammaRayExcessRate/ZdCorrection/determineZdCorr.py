import emcee
import corner
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import pandas as pd
from scipy.optimize import minimize

import ExcessCalculation as ec
import setFigureConfig as figConfig
import correctionModels as cm
from DataFrameBinning import addLinearBinning
from LibFunctions import roundAndFollowDIN

# set random seed
np.random.seed(123)

# set flags to skip parts of the program
resample = False
recreateObsTimePlot = False
recreateCorrectionComparison = False
recreateCornerPlot = True
recreateDataSpacePlot = True

# load calibration set
calibrationCrabSample = ["../MergedExcessInfos/CrabExcess.h5", "CrabExcess"]
runsCR = pd.read_hdf(calibrationCrabSample[0], calibrationCrabSample[1])
print("\nCalibration Crab sample loaded from: "+calibrationCrabSample[0]+"\n")

# conservative cuts to avoid influence by Calima and theshold setting
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 450]

# select seasons
s2 = runsCR[runsCR["fNight"] > 20120723]
s2 = s2[s2["fNight"] < 20130723]
s3 = runsCR[runsCR["fNight"] > 20130723]
s3 = s3[s3["fNight"] < 20140723]
s4 = runsCR[runsCR["fNight"] > 20140723]
s4 = s4[s4["fNight"] < 20150723]
s9 = runsCR[runsCR["fNight"] > 20190723]
s9 = s9[s9["fNight"] < 20200723]

seasons = [s2, s3, s4, s9]
seasonkeys = ["s2", "s3", "s4", "s9"]

# total effective observation time
runsCR = pd.concat(seasons, keys=seasonkeys)
t_tot = np.sum(runsCR["fEffectiveTime_h"])
print(
    "Total effective observation time of the sample: " +
    str(round(t_tot, 2)) + "h\n"
)

# runwise performance correction

# - load results
perfCorrLocation = [
    "../CRCorrection/perfCorrectionResults.h5", "perfCorrectionResults"
]
perfCorrRes = pd.read_hdf(perfCorrLocation[0], perfCorrLocation[1])
print(
    "Performance correction results loaded from: " + perfCorrLocation[0]+"\n"
)
bestFit = perfCorrRes["mean"].values
p1 = bestFit[0]
p0 = bestFit[1]
rCrRef = 250.
print("\tModel: p0+p1*R_CR")
print("\tBest fit was: p0="+str(p0)+" p1="+str(p1))
print("\tCR ref rate: "+str(rCrRef)+" events/min\n")

# - apply correction
runsCR["C_perf"] = cm.perfCorrectionFactor(
    runsCR["RCR_corr"], p0, p1, rCrRef=rCrRef
)
runsCR["rExcCorr"] = runsCR["C_perf"]*runsCR["rExc"]
runsCR["rExcErrCorr"] = runsCR["C_perf"]*runsCR["rExcErr"]

# choose binning and group
runsCR, binEdges, dropped = addLinearBinning(
    runsCR, "fZenithDistanceMean", "ZdBin", 12, maxVal=75
)
grpRuns = runsCR.groupby("ZdBin")

# rebinning
binnedDf = pd.DataFrame()
binnedDf["N"] = grpRuns.apply(lambda group: len(group))

# - find mean within Zd bins
binnedDf["ZdMean"] = grpRuns["fZenithDistanceMean"].apply(
    lambda group: np.mean(group)
)

# - calculate the excess for every bin with two methods

# -- method (i): ignoring the performance differences and calculate binwise
binnedDf = ec.excessPerBinUncorrected(binnedDf, grpRuns)

# -- method (ii): including the performance correction and calculate runwise
binnedDf = ec.excessPerBinCorrected(binnedDf, grpRuns, binningColumn="ZdBin")

# - reset index to let CRBin become a column
binnedDf = binnedDf.reset_index()

# - reject outliers that are not contained in the binned parameter space
binnedDf = binnedDf[binnedDf["ZdBin"] != -1]

# - add bin edges to data frame
binnedDf["BinStart"] = binEdges[:-1]
binnedDf["BinStop"] = binEdges[1:]
binnedDf["BinCenter"] = (binEdges[:-1]+binEdges[1:])/2
binwidth = (binEdges[1]-binEdges[0])

# plot observation time distribution
if recreateObsTimePlot:
    figConfig.halfWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.bar(
        binnedDf["BinCenter"], binnedDf["t_eff"],
        width=binwidth*0.95,
        color="tab:blue", zorder=1.1
    )
    ax.set_xlabel("zenith distance "+r"$\theta$"+" in deg")
    ax.set_ylabel("effective observation\ntime in h")
    ax.set_xlim(binEdges[0]-0.2*binwidth, binEdges[-1]+0.2*binwidth)
    ax.grid(axis="y", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Zd_ObsTime.pdf")
    fig.savefig("Zd_ObsTime.png", dpi=300)

    t_tot = np.sum(binnedDf["t_eff"])
    print(
        "Total effective observation time after binning: " +
        str(round(t_tot, 2)) + "h\n"
    )

if recreateCorrectionComparison:
    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        binnedDf["ZdMean"], binnedDf["RExc"],
        yerr=binnedDf["RExcErr"],
        fmt="o", color="tab:blue", zorder=1.3,
        label="evaluated from event counts\ninside every bin"
    )
    ax.errorbar(
        binnedDf["ZdMean"], binnedDf["RExcCorr"],
        yerr=binnedDf["RExcErrCorr"],
        fmt="o", color="k", zorder=1.3,
        label="runwise evaluation corrected\nfor detector performance"
    )
    ax.legend(loc="lower left")
    ax.set_xlabel("zenith distance "+r"$\theta$"+" in deg")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_ylim(0, 70)
    ax.set_xlim(binEdges[0], binEdges[-1])
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Zd_CRCorrComp.pdf")
    fig.savefig("Zd_CRCorrComp.png", dpi=300)

# Fitting the model to data


def logPrior(pars):
    r0, chi, xi, kappa = pars
    if (
        40 < r0 < 60 and
        -1 < chi < 2.5 and
        -1 < xi < 2.5 and
        -1 < kappa < 3
    ):
        return 0.0
    return -np.inf


def nll(pars, x, y, yerr):
    r0, chi, xi, kappa = pars
    model = cm.zenithModel(x, r0, chi, xi, kappa, gamma=2.7)
    yerr2 = yerr**2
    ll = -0.5*np.sum((y-model)**2/yerr2 + np.log(yerr2))
    return -ll


def logProbability(pars, x, y, yerr):
    lp = logPrior(pars)
    if not np.isfinite(lp):
        return -np.inf
    ll = - nll(pars, x, y, yerr)
    return lp + ll


labels = [r"$R_{0}$", r"$\chi$", r"$\xi$", r"$\kappa$"]
labelsNoFormat = ["r0", "chi", "xi", "kappa"]
initial = [55, 0.78, 0.26, 0.71]
pos = initial + 1e-4*np.random.randn(32, 4)
nwalkers, ndim = pos.shape

if resample:
    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, logProbability,
        args=(binnedDf["ZdMean"], binnedDf["RExcCorr"],
              binnedDf["RExcErrCorr"])
    )
    sampler.run_mcmc(pos, 5000, progress=True)

    # check boost in phase and reduce samples accordingly
    tau = sampler.get_autocorr_time()
    print("boost-in phase information:")
    print(tau)
    print("\n")
    thinBy = int(np.mean(tau)/2.)
    discardIndex = int(4*np.mean(tau))
    flat_samples = sampler.get_chain(
        discard=discardIndex, thin=thinBy, flat=True
    )

    np.save("./flat_samples.npy", flat_samples)

    # Monitoring plot

    fig, axes = plt.subplots(len(initial), figsize=(10, 7), sharex=True)
    samples = sampler.get_chain()
    for i in range(ndim):
        ax = axes[i]
        ax.axvline(x=tau[i], color="tab:blue")
        ax.axvline(x=discardIndex, color="tab:purple")
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(labels[i])
        ax.yaxis.set_label_coords(-0.1, 0.5)

    axes[-1].set_xlabel("step number")
    fig.savefig("./samplerMonitor.png", dpi=300)
    fig.savefig("./samplerMonitor.pdf")

else:
    flat_samples = np.load("./flat_samples.npy")


# evaluation of the results:
resultDicts = []
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)
    resultDicts.append(
        {
            "key": labelsNoFormat[i],
            "mean": mcmc[1],
            "q16": mcmc[0],
            "q84": mcmc[2],
            "+": q[1], "-": q[0]
        }
    )
pRes = pd.DataFrame(resultDicts)
pRes.to_hdf("../ZenithCorrectionResults.h5", "ZenithCorrectionResults")
optvalues = pRes["mean"].values
inds = np.random.randint(len(flat_samples), size=100)
xvals = np.linspace(5, 80, num=200)
# print posterior results
print("posterior results dataframe:")
print(pRes)
print("\n")

# plot corner posterior probs
if recreateCornerPlot:
    figConfig.fullWidth()
    fig = corner.corner(
        flat_samples, labels=labels
    )
    ndim = np.shape(flat_samples)[1]
    axes = np.array(fig.axes).reshape((ndim, ndim))
    for j in range(ndim):
        ax = axes[j, j]
        parDf = pRes[pRes["key"] == labelsNoFormat[j]]
        mean = parDf["mean"].values[0]
        left = parDf["q16"].values[0]
        right = parDf["q84"].values[0]
        span = ax.axvspan(
            xmin=left, xmax=right, facecolor="tab:purple", alpha=0.5
        )
        ax.axvline(x=mean, color="tab:purple")
        ax2 = ax.twiny()
        ax2.set_xlim(ax.get_xlim())
        ax2.set_xticks(ax.get_xticks())
        ax2.set_xticklabels([])
        rounded = roundAndFollowDIN(
            mean, [parDf["-"].values[0], parDf["+"].values[0]]
        )
        resultString = "{3}"+r"$= {0}_{{-{1}}}^{{+{2}}}$"
        resultString = resultString.format(
            str(rounded[0]), str(rounded[1][0]), str(rounded[1][1]), labels[j]
        )
        ax2.set_xlabel(resultString)

        for i in range(j):
            ax = axes[j, i]
            parDf2 = pRes[pRes["key"] == labelsNoFormat[i]]
            mean2 = parDf2["mean"].values[0]
            ax.axvline(x=mean2, color="tab:purple")
            ax.axhline(y=mean, color="tab:purple")
            ax.plot(mean2, mean, "s", color="tab:purple")

    legend = fig.legend(
        (
            mlines.Line2D(
                [0], [0], marker='s', color='tab:purple', linewidth=2,
                markerfacecolor="tab:purple", markersize=10
            ),
            span
        ),
        (
            "best fit as median of\nthe MC ensemble",
            "uncertainty estimation as\nquantile 16 % to 84 %"
        ),
        loc="upper right",
        framealpha=0.9, bbox_to_anchor=(0.98, 0.98),
        handlelength=3, fontsize=15
    )
    fig.savefig("Zd_CornerPlot.png", dpi=300)
    fig.savefig("Zd_CornerPlot.pdf")

# plot observation
if recreateDataSpacePlot:
    figConfig.fullWidth()
    fig = plt.figure()
    ax = fig.gca()
    ax.errorbar(
        binnedDf["ZdMean"], binnedDf["RExcCorr"],
        yerr=binnedDf["RExcErrCorr"],
        fmt="o", color="k", zorder=1.3, label=None
    )
    ax.plot(
        xvals,
        cm.zenithModel(
            xvals, optvalues[0], optvalues[1], optvalues[2], optvalues[3]
        ),
        color="tab:blue",
        lw=3, zorder=1.2,
        label="best fit"
    )
    firstflag = True
    for ind in inds:
        sample = flat_samples[ind]
        if firstflag:
            label = "100 random samples from\nthe MC ensemble"
            firstflag = False
        else:
            label = None
        ax.plot(
            xvals,
            cm.zenithModel(xvals, sample[0], sample[1], sample[2], sample[3]),
            color="tab:purple", alpha=0.1, zorder=1.1, label=label
        )
    ax.legend()
    legendHandels, legendLabels = ax.get_legend_handles_labels()
    legendHandels = [legendHandels[1], legendHandels[0]]
    legendLabels = [legendLabels[1], legendLabels[0]]
    leg = ax.legend(legendHandels, legendLabels)
    for lh in leg.legendHandles:
        lh.set_alpha(1)

    ax.set_xlabel("zenith distance "+r"$\theta$"+" in deg")
    ax.set_ylabel(r"$R_{\mathrm{exc}}$"+" in events/h")
    ax.set_ylim(0, 62)
    ax.set_xlim(binEdges[0], binEdges[-1])
    ax.grid(axis="both", alpha=0.5)
    ax.set_axisbelow(True)
    fig.tight_layout()
    fig.savefig("Zd_Dependence.pdf")
    fig.savefig("Zd_Dependence.png", dpi=300)
