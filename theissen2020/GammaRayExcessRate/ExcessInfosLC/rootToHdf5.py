from rootRoutines import dFrameFromRoot
import argparse
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument(
    "-f", "--file", help="Path to root file", type=str, default=""
)
parser.add_argument(
    "-o", "--out", help="Path to the h5 file", type=str, default=""
)
args = parser.parse_args()

rootFile = args.file
keyName = None
newFile = args.out

if ".root" in rootFile:
    if keyName is None:
        keyName = (rootFile.split("/")[-1])[:-5]
    if ".h5" not in newFile:
        newFile = rootFile[:-4]+"h5"
    df = dFrameFromRoot(rootFile, prints=False)
    intColumnList = ["fNight", "fRunID", "fNumBgEvts", "fNumSigEvts"]
    df[intColumnList] = df[intColumnList].astype(int)
    df = df.reset_index("entry")
    df = df.drop("entry", axis=1)
    df.to_hdf(newFile, keyName)
else:
    print("Not a root file or missing extension '.root'.")
