import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from progressBar import printProgressBar
from astropy.timeseries import LombScargle


def LombScarglePower(data, fit_mean=False, center_data=False, n=1, f=None):
    # time for Lomb-Scargle
    t = data["mjd"].values

    # excess + error for Lomb-Scargle
    y = data["RExcCorr"].values
    dy = data["RExcErrCorr"].values

    # initialize Lomb-Scargle
    ls = LombScargle(
        t, y, dy=dy, fit_mean=fit_mean, center_data=center_data,
        nterms=n
    )

    # frequency grid
    if f is None:
        f = np.linspace(1e-3, 0.5, 10000)

    # power from Lomb-Scargle using f
    p = ls.power(f)

    return f, p


def LombScargleFAP(data, repetitions=1000, fit_mean=False,
                   center_data=False, n=1):

    t = data["mjd"].values
    y = data["RExcCorr"].values
    dy = data["RExcErrCorr"].values
    f = np.linspace(1e-3, 0.5, 10000)

    lps = []
    printProgressBar(
        0, repetitions,
        prefix='Progress:', suffix='Complete', length=50
    )

    for i in range(repetitions):
        np.random.shuffle(t)
        lp = LombScargle(t, y, dy=dy, fit_mean=fit_mean,
                         center_data=center_data, nterms=n)
        printProgressBar(
            i+1, repetitions,
            prefix='Progress:', suffix='Complete', length=50
        )
        lps.append(lp.power(f))

    lps = np.array(lps)
    lpsT = lps.T

    means = [np.mean(array) for array in lpsT]
    q5 = [np.percentile(array, 5) for array in lpsT]
    q90 = [np.percentile(array, 90) for array in lpsT]
    q95 = [np.percentile(array, 95) for array in lpsT]
    q99 = [np.percentile(array, 99) for array in lpsT]

    return lps, means, q5, q90, q95, q99


def LombScarglePlot(data, source="Crab", mode="normal", onlyLS=False,
                    fontfactor=1, fit_mean=False, center_data=False):

    # fm = False
    # excess + error for Lomb-Scargle
    # y = data["ExcessRate"].values
    # dy = data["ExcessRateErr"].values
    # # initialize Lomb-Scargle
    # ls = LombScargle(t, y, dy=dy, fit_mean=fm, center_data=False)
    # # frequency grid
    # f = np.linspace(1e-3, 0.5, 10000)
    # # power from Lomb-Scargle using f
    # p = ls.power(f)
    f, p = LombScarglePower(data)

    # time for Lomb-Scargle
    t = data["mjd"].values

    # window function (1 at exactly all timesteps)
    w = np.ones(len(t), dtype=np.float64)
    # wPoisson = np.random.poisson(1, len(t))
    # np.random.normal(loc=1.0, scale=1e-13, size=len(t))
    wGauss = np.random.normal(loc=1.0, scale=0.5, size=len(t))
    # initialize Lomb-Scargle for window function
    lsw = LombScargle(t, w, fit_mean=fit_mean, center_data=center_data)
    # lswPoisson = LombScargle(t, wPoisson, fit_mean=fm, center_data=False)
    lswGauss = LombScargle(t, wGauss,
                           fit_mean=fit_mean, center_data=center_data)
    # power from Lomb-Scargle using same f
    pw = lsw.power(f)
    # pwPoisson = lswPoisson.power(f)
    pwGauss = lswGauss.power(f)

    if mode == "normal":
        repetitions = 1000
    elif mode == "test":
        repetitions = 10

    lps, means, q5, q90, q95, q99 = LombScargleFAP(
        data, repetitions=repetitions,
        fit_mean=fit_mean, center_data=center_data
    )

    # plotting
    sns.set(style="whitegrid", font_scale=1.3)
    if not onlyLS:
        fig, (axl, axw) = plt.subplots(nrows=2, figsize=(15, 8))
    else:
        fig, axl = plt.subplots(figsize=(15, 4.5))

    # ticks for relevant frequencies
    axl.axvline(1/365.25, c="black", ls=":", alpha=0.7)
    if source in ["Mrk501", "Mrk501 by Fermi"]:
        axl.axvline(1/332., c="purple", ls="-", alpha=0.7)
        if not onlyLS:
            axw.axvline(1/332., c="purple", ls="-", alpha=0.7)
    axl.axvline(1/29.5, c="black", ls=":", alpha=0.7)
    axl.axvline(1/27.3, c="black", ls=":", alpha=0.7)

    if not onlyLS:
        axw.axvline(1/365.25, c="black", ls=":", alpha=0.7)
        axw.axvline(1/29.5, c="black", ls=":", alpha=0.7)
        axw.axvline(1/27.3, c="black", ls=":", alpha=0.7)

        # Lomb-Scargle of window
        axw.plot(
            f, pw, label="Window function with constant c=1", c="tab:grey"
        )
        # axw.plot(
        #     f, pwPoisson,
        #     label=r"Window function with poissonian" +
        #           r" distribution $\lambda=1$",
        #     c="tab:orange"
        # )
        axw.plot(
            f, pwGauss,
            label=r"Window function with gaussian " +
                  r"noise $\mu=1$ $\sigma=0.5$",
            c="tab:blue"
        )

        axw.legend()
        axw.set_xlabel("frequency [1/day]")
        axw.set_ylabel("Lomb-Scargle-Power")
        axw.set_xscale("log")
        axw.grid()

    else:
        axl.set_xlabel("frequency [1/day]")

    # Lomb-Scargle of data
    for i in range(int(repetitions/50)-1):
        axl.plot(f, lps[i], color="tab:grey", alpha=0.1)
    axl.plot(f, lps[int(repetitions/50)], color="tab:grey", alpha=0.1,
             label="20 realizations")

    axl.plot(f, q99, label=r"1% FAP", color="tab:blue")
    axl.plot(f, p, label="Data", c="tab:red")
    axl.legend()
    axl.set_xscale("log")
    axl.set_ylabel("Lomb-Scargle-Power")
    axl.grid()

    xticks = np.array([1/365.25, 1/29.5, 1/27.3])
    xticklabels = ["1 year", "syn. month", "sid. month"]
    xticks501 = np.array([1/365.25, 1/332., 1/29.5, 1/27.3])
    xticklabels501 = ["1 year", "Fermi 332 days", "syn. month",
                      "sid. month"]
    rot = 25
    if not onlyLS:
        axl.set_title("Lomb-Scargle of " + source)
        axwlabel = axw.twiny()
        axwlabel.set_xscale("log")
        axwlabel.set_xlim(axw.get_xlim())
        if source in ["Mrk501", "Mrk501 by Fermi"]:
            axwlabel.set_xticks(xticks501)
            axwlabel.set_xticklabels(xticklabels501, rotation=90)
        else:
            axwlabel.set_xticks(xticks)
            axwlabel.set_xticklabels(xticklabels, rotation=90)
        axwlabel.minorticks_off()
        fig.tight_layout(h_pad=-0.5)
    else:
        axl.text(
            0.8, 0.82, "Lomb-Scargle of " + source, fontsize=15,
            transform=plt.gcf().transFigure
        )
        axllabel = axl.twiny()
        axllabel.set_xscale("log")
        axllabel.set_xlim(axl.get_xlim())
        if source in ["Mrk501", "Mrk501 by Fermi"]:
            axllabel.set_xticks(xticks501)
            axllabel.set_xticklabels(
                xticklabels501, rotation=45, ha="left"
            )
            axl.text(
                0.23, 0.9, xticklabels501[0], rotation=rot,
                transform=plt.gcf().transFigure
            )
            axl.text(
                0.25, 0.9, xticklabels501[1], rotation=rot,
                transform=plt.gcf().transFigure
            )
            axl.text(
                0.52, 0.9, xticklabels501[2], rotation=rot,
                transform=plt.gcf().transFigure
            )
            axl.text(
                0.54, 0.9, xticklabels501[3], rotation=rot,
                transform=plt.gcf().transFigure
            )
        else:
            axllabel.set_xticks(xticks)
            axllabel.set_xticklabels(
                xticklabels, rotation=45, ha="left"
            )
        axllabel.minorticks_off()
        fig.tight_layout()

    return fig
