import inspect
import os
import numpy as np
from scipy.stats import binom
from scipy.integrate import quad
from scipy.special import betainc, betaincinv
import matplotlib.pyplot as plt
import _pickle as pckl

'''
class HistogramManager

container for Histogram 

-instances of type Histogram can be added and plotted all together
-efficiency between two histograms can be calculated
-save/load using pickle


'''

class HistogramManager:

    def __init__(self, plotSettingsFile="plotSettings.txt",
                 plotSettingsPath=None):
        path = ""
        if plotSettingsPath is not None:
            path = plotSettingsPath
        else:
            path = os.path.dirname(
                inspect.getsourcefile(HistogramManager)
            )
            path += "/" + plotSettingsFile
        self.updatePlotSettings(path)
        self.histograms = {}
        self.thetaSq = 0.024

    def updatePlotSettings(self, plotSettingsPath="plotSettings.txt"):
        self.plotSettings = self.load_dict_from_file(plotSettingsPath)

    def getPlotSettings(self, mode):
        return self.plotSettings[mode]

    def get(self, name):
        return self.histograms[name]

    # get list of all histograms
    def getHistograms(self):
        return str(self.histograms)

    # add histogram to Manager
    def addHistogram(self, name, newHist):
        newHistogramDict = {name: newHist}
        self.histograms = dict(self.histograms, **newHistogramDict)

    # load histogram(s) from file
    def loadHistogramDict(self, filename):
        with open(filename, 'rb') as f:
            newHists = pckl.load(f)
        self.histograms = dict(self.histograms, **newHists)

    # save histogram(s) to file
    # names can contain list of all histograms to be saved
    def saveHistogramDict(self, filename, names="all"):
        if isinstance(names, str):
            if names == "all":
                toSave = self.histograms
        else:
            toSave = {key: self.histograms[key] for key in names}

        with open(filename, "wb") as f:
            pckl.dump(toSave, f)

    # plot a single histogram, returns figure
    def plotSingleHistogram(self, name, xlabel=None, style="bar", title="",
                            xscale=None, yscale=None, mode=None,
                            xlim=None, ylim=None, figsize=None, legloc=None):
        if name in self.histograms:
            pXLabel = None
            pXScale = None
            pYScale = None
            if mode is not None:
                (selfXLabel, selfXScale, selfYScale) = self.plotSettings[mode]
                pXLabel = selfXLabel
                pXScale = selfXScale
                pYScale = selfYScale
            if xlabel is not None:
                pXLabel = xlabel
            if xscale is not None:
                pXScale = xscale
            if yscale is not None:
                pYScale = yscale
            return self.histograms[name].draw(
                xlabel=pXLabel, style=style, title=title,
                xscale=pXScale, yscale=pYScale,
                xlim=xlim, ylim=ylim, figsize=None, legloc=legloc
            )
        else:
            print(name + " not in histograms!")
            return plt.figure(facecolor="red")

    # plots multiple histograms
    def plotHistograms(self, histogramNames, labels, mode=None, style="bar",
                       xlabel=None, ylabel=None, xscale=None, yscale=None,
                       xlim=None, ylim=None, figsize=None, title="",
                       colorCycle=None, markers=None, legloc=None, ax=None,
                       zlegend=10):
        if ax is None:
            fig, ax = plt.subplots(figsize=figsize)
            newFig = True
        else:
            newFig = False

        # color cycle
        if colorCycle is not None:
            ax.set_prop_cycle('color', colorCycle)
        # marker
        if markers is None:
            markers = ["." for i in histogramNames]

        for i, histogramName in enumerate(histogramNames):
            # plot histograms
            ax = self.histograms[histogramName].addToAx(
                    ax, style, labels[i], zorder=i+3
                )

        if mode is not None:
            (selfXLabel, selfXScale, selfYScale) = self.plotSettings[mode]
            ax.set_xlabel(selfXLabel)
            ax.set_xscale(selfXScale)
            ax.set_yscale(selfYScale)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if xscale is not None:
            ax.set_xscale(xscale)
        if yscale is not None:
            ax.set_yscale(yscale)
        if xlim is not None:
            ax.set_xlim(xlim[0], xlim[1])
        if ylim is not None:
            ax.set_ylim(ylim[0], ylim[1])
        ax.set_title(title)
        if legloc is not None:
            bbox_to_anchor, loc = legloc
            if bbox_to_anchor is None:
                if loc is not None:
                    legend = ax.legend(loc=loc)
            else:
                legend = ax.legend(loc=loc, bbox_to_anchor=bbox_to_anchor)
        else:
            legend = ax.legend(loc="center left", bbox_to_anchor=[1, 0.5])
        legend.set_zorder(zlegend)
        if newFig:
            # plt.tight_layout()
            return fig
        else:
            return ax

    # calculate efficiency between histogram 0 and 1
    def calcEfficiency(self, hist0, hist1):
        # vectorize function with numpy
        calcEff = np.vectorize(self.getEfficiencyWithErrors)
        # delete nullnp.vectorize( bins from h0 assuming h1 < h0 for every bin
        empty = np.where(hist0.counts == 0)
        h0Counts = np.delete(hist0.getCounts(), empty)
        h1Counts = np.delete(hist1.getCounts(), empty)
        centers = np.delete(hist0.getCenters(), empty)
        return calcEff(h0Counts, h1Counts), centers

    # plots efficiency between all histograms and first one
    def plotEfficiencies(self, histogramNames, cuts="spectrum", mode=None,
                         xlabel=None, xscale=None, yscale=None,
                         xlim=None, ylim=None, figsize=None, title="",
                         colorCycle=None, markers=None, legloc=None, z0=1,
                         h0Name="untriggered data", style=".", ax=None):
        if ax is None:
            fig, ax = plt.subplots(figsize=figsize)
            newFig = True
        else:
            newFig = False

        # color cycle
        if colorCycle is not None:
            ax.set_prop_cycle('color', colorCycle)
        # marker
        if markers is None:
            markers = ["." for i in histogramNames]
        # labels
        labels = self.getCorrectLabels(cuts)

        for i, histogramName in enumerate(histogramNames[1:]):
            # calculate efficiency
            (effs, sigLows, sigUps), effCenters = self.calcEfficiency(
                self.histograms[histogramNames[0]],
                self.histograms[histogramName]
            )
            # plot efficiency
            if style == "stairs":
                color = next(ax._get_lines.prop_cycler)["color"]
                h = self.histograms[histogramName]
                # h0 = self.histograms[histogramNames[0]]
                # empty = np.where(
                #     h0.counts == 0
                # )
                # hbins = np.delete(hist0.getCounts(), empty)
                # This has a problem, if h0 has an empty bin, this is deleted
                # from the efficiency, but not for the calculation of x
                # (h.bins still contains all bins)
                x = np.reshape(
                    h.bins[:, None] * np.ones(2), (2*len(h.bins))
                )[1:-1]
                y = np.reshape(
                    effs[:, None] * np.ones(2), (2*len(effs))
                )
                ax.plot(
                    x, y, label=labels[i+1], zorder=i+z0, color=color
                )
                # ax.step(effCenters, effs, label=labels[i+1], color=color,
                #         where="mid", zorder=i)
                ax.errorbar(effCenters, effs, yerr=[sigLows, sigUps],
                            linestyle="", ecolor=color, label=None,
                            zorder=i+z0 + 0.5)
            else:
                ax.errorbar(
                    effCenters, effs, yerr=[sigLows, sigUps],
                    linestyle="", label=labels[i+1], marker=markers[i]
                )

            if mode is not None:
                (selfXLabel, selfXScale, selfYScale) = self.plotSettings[mode]
                ax.set_xlabel(selfXLabel)
                ax.set_xscale(selfXScale)
                ax.set_yscale(selfYScale)
            if xlabel is not None:
                ax.set_xlabel(xlabel)
            ax.set_ylabel("efficiency compared to " + h0Name)
            if xscale is not None:
                ax.set_xscale(xscale)
            if yscale is not None:
                ax.set_yscale(yscale)
            if xlim is not None:
                ax.set_xlim(xlim[0], xlim[1])
            if ax.get_yscale != "log":
                ax.set_ylim(0, 1.05)
            if ylim is not None:
                ax.set_ylim(ylim[0], ylim[1])
            ax.set_title(title)
            if legloc is not None:
                bbox_to_anchor, loc = legloc
                if bbox_to_anchor is None:
                    ax.legend(loc=loc)
                else:
                    ax.legend(loc=loc, bbox_to_anchor=bbox_to_anchor)
            else:
                ax.legend(loc="center left", bbox_to_anchor=[1, 0.5])
            plt.tight_layout()
        if newFig:
            plt.tight_layout()
            return fig
        else:
            return ax

    # correct labels for cuts
    # theta squared has to be set before, if it is not 0.024
    def getCorrectLabels(self, cuts):
        # correct labels
        if isinstance(cuts, str):
            if cuts == "spectrum":
                labels = ["original", "triggered", "+Quality cuts",
                          "+Spectrum BG suppr.",
                          r"+$\theta^2 $ < " + str(round(self.thetaSq, 3))]
            elif cuts == "lightcurve":
                labels = ["original", "triggered", "+Quality cuts",
                          "+Lightcurve BG suppr.",
                          r"+$\theta^2 $ < " + str(round(self.thetaSq, 3))]
            elif cuts == "ISDC":
                labels = ["original", "triggered", "+Quality cuts",
                          "+ISDC BG suppr.",
                          r"+$\theta^2 $ < " + str(round(self.thetaSq, 3))]
        else:
            labels = cuts
        return labels

    """
        getEffWithErrors(N,k)

        Description:
            Calculates the efficiency and it's error based on the
            1-sigma(68.3%) percentile from a bayesian approach of the
            distribution of the efficiency

        Parameters:
            N: int
                Number of events before cut
            k: int
                Number of events after cut

        Returns:
            p: float
                efficiency
            sig_low: float
                lower error
            sig_up: float
                upper error
    """

    @staticmethod
    def getEfficiencyWithErrors(N, k):
        p = float(k) / N
        if p == 0 or p == 1:
            interval = 0.68268949
        else:
            interval = 0.341344745

        # pdf of efficiency
        def pdf(eff):
            return (N+1)*binom.pmf(k, N, eff)

        # upper error pull function
        def sigma_up(s):
            return np.array([quad(pdf, p, p + si)[0] - interval for si in s])

        # lower error pull function
        def sigma_low(s):
            return np.array([quad(pdf, p - si, p)[0] - interval for si in s])

        # for eff = p = 0 the maximum of the efficiency distribution is at 0
        # -> define error only as upper limit
        # same applies for eff = p = 1

        a = k + 1
        b = N - k + 1
        sigma68 = 0.68268949

        if p == 0:
            sig_low = 0
            sig_up = betaincinv(a, b, sigma68)
        elif p == 1:
            sig_low = betaincinv(a, b, 1 - sigma68)
            sig_up = 0
        else:
            sig_low = p - betaincinv(a, b, betainc(a, b, p) - sigma68/2)
            sig_up = - p + betaincinv(a, b, betainc(a, b, p) + sigma68/2)

        return p, sig_low, sig_up

    # to save plotting parameters
    @staticmethod
    def save_dict_to_file(dic, filename):
        f = open(filename, 'w')
        f.write(str(dic))
        f.close()

    # to load plotting parameters
    @staticmethod
    def load_dict_from_file(filename):
        f = open(filename, 'r')
        data = f.read()
        f.close()
        return eval(data)
