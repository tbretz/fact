import argparse
import os
import pathlib
from shutil import copyfile
import yaml

parser = argparse.ArgumentParser()
parser.add_argument(
    "-i", "--configFile", help="Path to input config file", type=str
)
args = parser.parse_args()

configFile = args.configFile

if configFile is not None:
    config = {}
    with open(configFile, 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
else:
    print("Please call the script with -i config.yaml (A valid config file)")

savingLocation = pathlib.Path(config["savingLocation"]["simulationFolder"])
savingLocation.mkdir(parents=True, exist_ok=True)

print("Starting simulation.")
os.system("python3 ./MCSimulation.py -i" + configFile)
print("Simulation complete.")
print("Starting evaluation.")
os.system("python3 ./MCSimEvaluation.py -i" + configFile)
print("Evaluation complete.")
print("Starting comparison plot creation.")
os.system("python3 ./MCSimCompareMethods.py -i" + configFile)
print("Comparison plot creation complete.")
copyfile(
    configFile, config["savingLocation"]["simulationFolder"]+"config.yaml"
)
