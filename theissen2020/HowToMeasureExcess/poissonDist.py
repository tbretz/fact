import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from scipy.special import gamma, gammainc

import setFigureConfig as figConfig


def poisson(l, k):
    return (l**k)/np.math.factorial(k) * np.exp(-l)


def solution(excSpace, s, b):
    result = np.zeros(len(excSpace))
    for e_ind in range(len(excSpace)):
        e = excSpace[e_ind]
        firstFactor = 5**b * np.exp(5*e)/np.math.factorial(s)
        summ = 0
        for i in range(b+1):
            # print(i)
            first = (-e)**i/(np.math.factorial(b-i)*np.math.factorial(i))
            second = 6**(-s-b-i-1)
            firstArg = s+b-i+1
            secondArg = 0
            if e > 0:
                secondArg = 6*e
            third = gamma(firstArg)*(1 - gammainc(firstArg, secondArg))
            summ += first*second*third
            # if e_ind == 0:
            #    print(firstFactor, first, second, third)
        result[e_ind] = firstFactor*summ

    return result


kSpace = np.arange(0, 41)
lSpace = np.linspace(0, 20, num=200)

x, y = np.meshgrid(lSpace, kSpace)

z = np.array([poisson(l, k) for l in lSpace for k in kSpace])
z[z < 1e-9] = 1e-9
Z = z.reshape(len(lSpace), len(kSpace))

figConfig.fullWidth(aspectRatio=0.85)
fig = plt.figure()
ax = fig.gca()
plot = ax.imshow(Z.T, origin='lower', interpolation='bilinear', aspect=1/2,
                 cmap="viridis", norm=LogNorm(vmin=1e-9, vmax=1),
                 extent=(0, 20, 0, 41))
cbar = plt.colorbar(plot, fraction=0.046)
cbar.set_label(r"$P(\lambda, k)$")
ax.set_xlabel(r"$\lambda$")
ax.set_ylabel(r"$k$")
fig.tight_layout()
fig.savefig("./Poisson.png", dpi=300)
fig.savefig("./Poisson.pdf")

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.set_prop_cycle('color', plt.cm.viridis(np.linspace(0, 1, 10)))
for i in range(9):
    ax.plot(
        lSpace, poisson(lSpace, i+1), label=r"$k=$"+str(i+1), lw=2, zorder=1.1
    )
ax.set_xlabel(r"$\lambda$")
ax.set_ylabel(r"$P(\lambda)$")
ax.legend(ncol=3)
ax.set_xlim(-1.5, 20)
ax.set_ylim(0, 0.4)
ax.grid(axis="both", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./PoissonExamples.png", dpi=300)
fig.savefig("./PoissonExamples.pdf")


excSpace = np.linspace(-2, 6, 200)
result = solution(excSpace, 0, 0)
mean = np.average(excSpace, weights=result)

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
# ax.set_title(r"$P(\lambda_{E})$ for the case $n_{B}=n_{S}=0$")
ax.plot(excSpace, result, color="tab:blue", zorder=50)
ax.set_xlim(-2, 6)
ax.set_ylim(0, 0.18)
ax.axvline(
    0, color="tab:green", zorder=1.2, lw=2, ls="--",
    label="most probable value: \n  "+r"$\lambda^{max}_{E}$=0"
)
ax.axvline(
    mean, color="tab:purple", zorder=1.2, lw=2,
    label="expectation value: \n  "+r"<$\lambda_{E}$>=" + str(mean.round(1))
)
ax.axvspan(
    0.8-np.sqrt(1+1/25), 0.8+np.sqrt(1+1/25), zorder=1.1,
    color="tab:purple", alpha=0.25,
    label="68% interval:\n  "+r"$-0.22<\lambda_{E}<1.82$"
)
ax.set_xlabel(r"$\lambda_{E}$")
ax.set_ylabel(r"$P(\lambda_{E})$")
ax.legend()
ax.grid(axis="both", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./ZeroZero.png", dpi=300)
fig.savefig("./ZeroZero.pdf")

print(0.8-np.sqrt(1+1/25), 0.8+np.sqrt(1+1/25), np.sqrt(1+1/25))
