from HistogramManager import HistogramManager
import setFigureConfig as sfc

'''
Plots reweighted energy distributions for OriginaMC and triggered MC
in one figure (Fig. 2.9)
histograms computed with energy_Models_OriginalMC.py
'''

si = -2.5  # spectral index to reweight to

hm = HistogramManager()
hm.loadHistogramDict("OriginalMC_reweightingEnergy_%g.p" % (si))
hm.loadHistogramDict("MC_Gamma_reweightingEnergy_%g.p" % (si))

# create combined plot
sfc.halfWidth(aspectRatio=0.9)
fig = hm.plotHistograms(
    ["before_MCGamma", "after_MCGamma"], ["initial", "reweighted"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"]
)
ax = fig.gca()
ax = hm.plotHistograms(
    ["before_original", "after_original"], ["initial", "reweighted"],
    yscale="log", style="stairs", xlabel=r"$\log_{10} E/GeV$",
    legloc=[None, "upper right"], ax=ax
)
ax.set_ylabel(
    r"$\frac{\mathrm{d}N}{\mathrm{d} \log_{10} E/GeV}$",
    # rotation=90, labelpad=10
)
fig.tight_layout()
fig.savefig("combined_reweightingEnergy_%g.pdf" % (si))
