import numpy as np

mm2deg = 0.0117193246260285378

"""
    calcLogs(df)

    Description:
        Adds columns to the DataFrame with log10 of Size and Energy.

    Parameters:
        df: pandas.DataFrame
            DataFrame where columns will be added.
        columns: list of str
            array of names, for which columns a log10 column is added to df

    Returns:
        df: pandas.DataFrame
        DataFrame with additional columns.
"""


def calcLogs(df, columns=["Size", "Area", "Energy"]):
    if "Size" in columns:
        df["lgSize"] = np.log10(df.Size)
    if "Area" in columns:
        df["lgArea"] = np.log10(df.Area * mm2deg**2)
    # df["lgArea"][df["lgArea"]==-np.inf] = -100 # some const value
    if "Energy" in columns:
        df["lgEnergy"] = np.log10(df.Energy)
    if "Slope" in columns:
        df["lgSlope"] = np.log10(df.slope)
    if "m3l" in columns:
        df["lgm3l"] = np.log10(df.m3l)
    if "Impact" in columns:
        df["lgImpact"] = np.log10(df.Impact)
    return df


"""
    calcDispOverXi(df)

    Description:
        Adds columns to the DataFrame doing the classic reconstruction of
        FACT images up to Disp / xi. Ideal for xi variation.

    Parameters:
        df: pandas.DataFrame
            DataFrame where columns will be added.

    Returns:
        df: pandas.DataFrame
        DataFrame with additional columns.
"""


def calcDispOverXi(df):
    df['dx'] = df.MeanX - df.X*1.02
    df['dy'] = df.MeanY - df.Y*1.02

    df['norm'] = np.sqrt(df.dx**2 + df.dy**2)
    df['dist'] = df.norm * mm2deg

    df['lx'] = np.minimum(np.maximum((df.CosDelta*df.dy - df.SinDelta*df.dx) /
                                     df.norm, -1), 1)
    df['ly'] = np.minimum(np.maximum((df.CosDelta*df.dx + df.SinDelta*df.dy) /
                                     df.norm, -1), 1)

    df['alpha'] = np.arcsin(df.lx)
    df['sgn'] = np.sign(df.ly)

    df['m3l'] = df.M3Long * df.sgn * mm2deg
    df['slope'] = df.SlopeLong * df.sgn / mm2deg

    df['sign1'] = df.m3l + 0.07
    df['sign2'] = (df.dist - 0.5) * 7.2 - df.slope

    df['disp_over_xi'] = (np.sign(np.minimum(df.sign1, df.sign2))
                          * (1 - df.Width / df.Length))
    return df


"""
    calcThetaSq(df, xi0=1.299, xi1=0.0632, xi2=1.67972, xi3=4.86232)

    Description:
        Adds columns to the DataFrame doing the classic reconstruction of
        FACT images with xi, Dist, Disp and ThetaSq.

    Parameters:
        df: pandas.DataFrame
            DataFrame where columns will be added.

    Returns:
        df: pandas.DataFrame
        DataFrame with additional columns.
"""


def calcThetaSq(df, xi0=1.299, xi1=0.0632, xi2=1.67972, xi3=4.86232):
    calcDispOverXi(df)
    df['xi'] = xi0 + xi1 * df.slope + xi2 * (1 - 1 / (1 + xi3 * df.Leakage1))
    df['Disp'] = df.xi * df.disp_over_xi
    df['thetasq'] = (df.Disp**2 + df.dist**2
                     - 2 * df.Disp * df.dist * np.sqrt(1 - df.lx**2))
    return df
