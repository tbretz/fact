import numpy as np
import scipy.linalg as la
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from scipy.integrate import simps
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.colorbar as cb
from matplotlib.colors import Normalize
import datetime
import astropy.constants as const
import astropy.units as u
# import yt.units as u
import pandas as pd
import coloring as cl
import setFigureConfig as sfc

# a_j = 1 + dt / dg_j
# b_j = - dt / dg_j
#
# M = / *   b0  b1  ...  bJ-1 \
#     \ a0  a1  a2  ...  aJ   /
#
# S = N + s with N from last step
#
# M * N_new = S
# advection + sources - losses
def fast_TDMA(v2, v3, Nlast, sources):
    return la.solve_banded(
        (0, 1),
        np.reshape(
            np.append(
                np.append(-1, v3[:-1]),
                v2
            ),
            (2, len(Nlast))
        ),
        Nlast + sources
    )


# class to solve radiation modeling
class Radiation:
    def __init__(self, L=5*10**16, B=1, L_BLR=1e46, R_BLR=1e17, z_diss=5e17,
                 Theta_BB=1e-5, UinjUB=1,
                 inj_si=-2, inj_N0=1e3, inj_min=10, inj_max=1e3,
                 betaG=0.08, Asup=0.77, Bsup=0.033, sigma=10,
                 nIntegration=500,
                 yEscFactor=2, lBBto1=False):  # options for crosschecks
        # array with all timesteps for electrons (e) and photons (y)
        self.Ne_all = []  # erg/s
        self.Ny_all = []  # erg/s

        # define all the constants, etc. in cgs units
        self.L = L  # cm
        self.B = B  # gauss

        # magnetic field, etc..
        self.Bc = 4.414 * 10**13  # gauss
        self.b = B / self.Bc
        self.UB = self.B**2 / 8 / np.pi  # gauss**2 = erg/cm**3
        # magnetic compactness l_B
        me_c2 = 8.18710565e-07  # erg
        sigma_T = 6.65245854533e-25  # cm^2
        prefactor = sigma_T * self.L / me_c2
        self.lB = prefactor * self.B**2 / 8. / np.pi
        print("l_B  =", self.lB)
        self.sigma = sigma  # magnetization
        self.UinjUB = UinjUB  # fraction of injected to magnetic energy

        # comving number density from PIC (energy equipartition)
        dimfactor = sigma_T * L / me_c2  # cm^3/erg
        if inj_si != -2:
            indfactor = (2 + inj_si)
            rangefactor = 1. / (inj_max**(2+inj_si) - inj_min**(2+inj_si))
            self.n_co_Fnorm = dimfactor * indfactor * rangefactor * self.UB *\
                UinjUB
        else:
            self.n_co_Fnorm = dimfactor / np.log(inj_max/inj_min) * self.UB *\
                UinjUB

        # print("new norm", self.n_co)
        self.n_co2 = B**2 / (4. * np.pi * sigma * me_c2)  # 1/cm**3
        # normailze n_co with sigma_T and L
        self.n_co2 = self.n_co2 * sigma_T * L
        # print("old norm", self.n_co2)

        self.alphaF = 1./137  # fine structure constant

        # dimensionless
        # black body properties in plasmoid frame
        self.R_BLR = R_BLR
        self.z_diss = z_diss
        c = 29979245800  # cm/s
        U_BB = L_BLR / (4*np.pi * (R_BLR-z_diss)**2) / c  # erg/cm**3
        # sigmaT * cm/ me_c2
        if lBBto1:
            self.l_BB = 1
        else:
            self.l_BB = U_BB * sigma_T * L / me_c2
        print("l_BB =", self.l_BB)
        self.Theta_BB = Theta_BB
        self.delta_BB = 1  # default no value

        self.inj_si = inj_si  # spectral index of injection
        self.inj_N0 = inj_N0  # norm of injection (for power law only)
        self.inj_min = inj_min  # min energy of injection
        self.inj_max = inj_max  # max energy of injection

        # for growth of plasmoids:
        # rate of plasma growth
        self.betaG = float(betaG)
        # for growth suppression
        self.Asup = float(Asup)
        self.Bsup = float(Bsup)

        # turn on all processes by default
        self.include = {
            "Qe_injection": 1,  # electron injection
            "Qe_powerlaw": 0,  # pure power law injection
            "Qe_yy": 1,  # photon pair production
            "Le_sync": 1,  # synchr3otron
            "Le_icT": 1,  # inverse Compton - Thomson regime
            "Le_icKN": 1,  # inverse Compton - Klein-Nishina regime
            "Ly_esc": 1,  # photon escape
            "Ly_yy": 1,  # photon pair production
            "Ly_ssa": 1,  # synchrotron self-absorption
            "Qy_sync": 1,  # synchrotron
            "Qy_icT": 1,  # inverse Compton - Thomson regime
            "Qy_icKN": 1,  # inverse Compton - Klein-Nishina regime
            # "Qy_icFull": 0,  # inverse Compton - full (Gould-Blumenthal)
            "BB": 1,  # black body background field
            "SSC": 1  # self synchrotron compton
        }
        self.yEscFactor = yEscFactor
        self.nIntegration = nIntegration
        self.courant = []

    '''
    ########
    set grid
    ########
    '''
    # time
    def setLinTime(self, dt, saveEvery=1):
        self.dt = dt
        self.saveEvery = saveEvery
        self.t = np.zeros(1)

    # energy electrons
    def setLogElectronEnergy(self, eexpmin, eexpmax, NeE=1000):
        self.eexpmin = eexpmin
        self.eexpmax = eexpmax
        self.eEmin = 10**self.eexpmin
        self.eEmax = 10**self.eexpmax
        self.NeE = NeE
        i = np.arange(2 * NeE + 1) / 2 - 0.5
        self.eEall = 10**(i * (eexpmax-eexpmin)/(NeE-1) + eexpmin)
        self.eE = self.eEall[1::2]
        self.eE_up = self.eEall[2::2]
        self.eE_low = self.eEall[:-2:2]
        self.deE = self.eE_up - self.eE_low

        # print("dt=", self.dt[0])
        # Ne_min = (
        #     1 + np.log(self.eEmax / self.eEmin) /
        #     (2 * np.arcsinh(2. / 3 * self.lB * self.inj_max * self.dt))
        # )
        # print("from courant condition: Ne_min=", Ne_min)

    # energy photons
    def setLogPhotonEnergy(self, yexpmin, yexpmax, NyE=1000,
                           zero=1e-15, infty=1e10):
        self.yexpmin = yexpmin
        self.yexpmax = yexpmax
        self.yEmin = 10**self.yexpmin
        self.yEmax = 10**self.yexpmax
        self.NyE = NyE
        i = np.arange(2 * NyE + 1) / 2 - 0.5
        self.yEall = 10**(i * (yexpmax-yexpmin)/(NyE-1) + yexpmin)
        self.yE = self.yEall[1::2]
        self.dyE = self.yEall[2::2] - self.yEall[:-2:2]
        self.zero = zero
        self.infty = infty

    '''
    ######################
    set initial conditions
    ######################
    '''
    # electrons
    def setInitalElectronDistribution(self, Ninitial, plot=False, ax=None):
        if len(Ninitial) == self.NeE:
            self.Ne_all.append(Ninitial)
            self.updateInterpolationNe()
            if plot and ax is not None:
                ax.plot(self.eE, Ninitial, label="initial", marker=".",
                        linestyle="")
                return ax
        else:
            print(
                "Ninitial has wrong length for electrons",
                str(len(Ninitial)),
                ", but expected:",
                str(self.NeE)
            )

    # photons
    def setInitalPhotonDistribution(self, Ninitial, plot=False, ax=None):
        if len(Ninitial) == self.NyE:
            self.Ny_all.append(Ninitial)
            self.updateInterpolationNy()
            if plot and ax is not None:
                ax.plot(self.yE, Ninitial, label="initial", marker=".",
                        linestyle="")
                return ax
        else:
            print(
                "Ninitial has wrong length for photons",
                str(len(Ninitial)),
                ", but expected:",
                str(self.NyE)
            )

    '''
    #############
    interpolation
    #############
    '''
    def updateInterpolationNe(self):
        self.Ne_inter = interp1d(self.eE, self.Ne_all[-1])

    def updateInterpolationNy(self):
        self.Ny_inter = interp1d(self.yE, self.Ny_all[-1])

    def NeOfE(self, E):
        # interpolate in energy range, return 0 otherwise
        result = np.zeros(E.shape)
        result[(E > self.eEmin) & (E < self.eEmax)] = self.Ne_inter(
            E[(E > self.eEmin) & (E < self.eEmax)]
        )
        return result

    def NyOfE(self, E, BB=False):
        # interpolate in energy range, return 0 otherwise
        result = np.zeros(E.shape)
        result[(E > self.yEmin) & (E < self.yEmax)] = self.Ny_inter(
            E[(E > self.yEmin) & (E < self.yEmax)]
        )
        if BB:
            result += self.n_BB(E)
        return result

    '''
    ###########
    integration
    ###########
    '''
    def log_int_simps(self, func, a, b, parameter=None, N=500):
        u = np.linspace(np.log(a), np.log(b), N).T  # [Nb, N]
        if parameter is None:
            y = np.exp(u) * func(np.exp(u))
        else:
            # [Nb, N] * [Nb]
            y = np.exp(u) * func(np.exp(u), parameter)
        return simps(y, u)

    '''
    ##############
    electron terms
    ##############
    '''
    # Q injection: power law
    def Qe_inj_powerLaw(self):
        if self.include["Qe_powerlaw"]:
            pivot = 1  # pivot, not really used
            norm = (1 + self.inj_si) / (
                self.inj_max**(1+self.inj_si) - self.inj_min**(1+self.inj_si)
            )
            return(
                norm * self.inj_N0 * (self.eE/pivot)**self.inj_si *
                ((self.eE > self.inj_min) & (self.eE < self.inj_max))
            )
        else:
            return 0

    # growth suppression from MC for injection term
    # inverted to prevent division by 0
    def f_sup_inv(self, beta):
        p = beta / np.sqrt(1 - beta**2)
        return (
            0.5 * (1 - np.tanh(
                (np.abs(p)/np.sqrt(self.sigma) - self.Asup) / self.Bsup)
            )
        )

    # injection term due to plasmoid growth
    def Qe_inj_growth(self, s, beta, eE):
        if self.include["Qe_injection"]:
            return (
                3 * self.n_co_Fnorm / s *
                self.betaG * self.f_sup_inv(beta) *  # ds/dt
                (eE)**self.inj_si *
                ((eE > self.inj_min) & (eE < self.inj_max))
            )
        else:
            return 0

    # p-p - reaction rate
    def R_yy(self, w):
        return 0.652 * np.log(w) * np.heaviside(w-1, 0) * (w**2 - 1)/w**3

    def Qe_yy(self, eE, SSC=True, BB=True):
        if self.include["Qe_yy"]:
            if SSC:
                def integrand(x, y):
                    return self.NyOfE(x, BB) * self.R_yy(2 * y * x)
                return 4 * self.NyOfE(2 * eE, BB) * self.log_int_simps(
                    integrand, self.zero, self.infty, parameter=eE[:, None],
                    N=self.nIntegration
                )
            else:
                if BB:
                    def integrand(x, y):
                        return self.n_BB(x) * self.R_yy(2 * y * x)
                    return 4 * self.n_BB(2 * eE) * self.log_int_simps(
                        integrand, self.zero, self.infty,
                        parameter=eE[:, None], N=self.nIntegration
                    )
                else:
                    return 0
        else:
            return 0

    # L synchrotron
    def Le_sync_factor(self):
        if self.include["Le_sync"]:
            return 4./3 * self.lB
        else:
            return 0

    # L inverse compton - Thomson/Klein-Nishina
    # radiation field
    def U_T(self, eE, SSC=True, BB=True):
        if SSC:
            xT = 3./(4 * eE)  # upper limit of intergral/Thomson regime

            def integrand(x):
                return self.NyOfE(x, BB) * x
            return self.log_int_simps(integrand, self.zero, xT,
                                      N=self.nIntegration)
        else:
            if BB:
                xT = 3./(4 * eE)  # upper limit of intergral/Thomson regime

                def integrand(x):
                    return self.n_BB(x) * x
                return self.log_int_simps(integrand, self.zero, xT,
                                          N=self.nIntegration)
            else:
                return 0

    # Thomson
    def Le_icT_factor(self, eE, SSC=True, BB=True):
        if self.include["Le_icT"]:
            return 4./3 * self.U_T(eE, SSC, BB)
        else:
            return 0

    # Klein-Nishina regime
    def Le_icKN_factor(self, eE, SSC=True, BB=True):
        if self.include["Le_icKN"]:
            if SSC:
                xT = 3./(4 * eE)  # lower limit of intergral/KN regime

                def integrand(x):
                    return self.NyOfE(x, BB) / x
                return self.log_int_simps(integrand, xT, self.infty,
                                          N=self.nIntegration) / eE
            else:
                if BB:
                    xT = 3./(4 * eE)  # lower limit of intergral/KN regime

                    def integrand(x):
                        return self.n_BB(x) / x
                    return self.log_int_simps(integrand, xT, self.infty,
                                              N=self.nIntegration) / eE
                else:
                    # neither existing nor black body photons included
                    return 0
        else:
            # not included
            return 0

    '''
    ############
    photon terms
    ############
    '''
    # loss term
    def Ly_esc(self, s):
        if self.include["Ly_esc"]:
            return self.yEscFactor/s
        else:
            return 0

    # Q synchrotron
    def Qy_sync(self, yE):
        if self.include["Qy_sync"]:
            return (
                2./3. * self.lB * self.b**-1.5 / np.sqrt(yE) *
                self.NeOfE(np.sqrt(yE/self.b))
            )
        else:
            return 0

    # Q inverse compton
    def Qy_icT(self, yE, SSC=True, BB=True):
        if self.include["Qy_icT"]:
            if SSC:
                # upper limit of integration
                xT = np.minimum(3./(4*yE), 3*yE/4)

                def integrand(x, x2):
                    return (
                        1./np.sqrt(x*x2) * self.NeOfE(np.sqrt(3./4 * x2/x)) *
                        self.NyOfE(x, BB)
                    )
                return np.sqrt(3)/4 * self.log_int_simps(
                    integrand, self.zero, xT, parameter=yE[:, None],
                    N=self.nIntegration
                )
            else:
                if BB:
                    # upper limit of integration
                    xT = np.minimum(3./(4*yE), 3*yE/4)

                    def integrand(x, x2):
                        return (
                            1./np.sqrt(x*x2) *
                            self.NeOfE(np.sqrt(3./4 * x2/x)) *
                            self.n_BB(x)
                        )
                    return np.sqrt(3)/4 * self.log_int_simps(
                        integrand, self.zero, xT, parameter=yE[:, None],
                        N=self.nIntegration
                    )
                else:
                    # neither existing nor black body photons included
                    return 0
        else:
            # not included
            return 0

    def Qy_icKN(self, yE, SSC=True, BB=True):
        if self.include["Qy_icKN"]:
            if SSC:
                # lower limit of intergral/KN regime
                xT = 3./(4 * yE)

                def integrand(x):
                    return self.NyOfE(x, BB) / x
                return self.NeOfE(yE) / yE * self.log_int_simps(
                    integrand, xT, self.infty, N=self.nIntegration
                )
            else:
                if BB:
                    # lower limit of intergral/KN regime
                    xT = 3./(4 * yE)

                    def integrand(x):
                        return self.n_BB(x) / x
                    return self.NeOfE(yE) / yE * self.log_int_simps(
                        integrand, xT, self.infty, N=self.nIntegration
                    )
                else:
                    return 0
        else:
            return 0

    # L yy photon pair production
    def Ly_yy(self, yE, SSC=True, BB=True):
        if self.include["Ly_yy"]:
            if SSC:
                def integrand(x, x2):
                    return self.NyOfE(x, BB) * self.R_yy(x * x2)
                return self.log_int_simps(
                    integrand, self.zero, self.infty,
                    parameter=yE[:, None], N=self.nIntegration
                )
            else:
                if BB:
                    def integrand(x, x2):
                        return self.n_BB(x) * self.R_yy(x * x2)
                    return self.log_int_simps(
                        integrand, self.zero, self.infty,
                        parameter=yE[:, None], N=self.nIntegration
                    )
                else:
                    return 0
        else:
            return 0

    # L ssa self synchronton absorpion
    def Ly_ssa(self, yE, s):
        if self.include["Ly_ssa"]:
            return np.abs(
                np.pi/6/self.alphaF / np.sqrt(yE * self.b) * s/2 *
                np.gradient(  # df/dx | x=x0
                    self.NeOfE(np.sqrt(yE / self.b)) * self.b/yE,  # f(x0)
                    np.sqrt(yE / self.b)  # x0
                )
            )
        else:
            return 0

    # Background Black Body
    def n_BB(self, E):
        with np.errstate(over='ignore', invalid='ignore'):
            return self.l_BB * 15./np.pi**4 * (
                np.nan_to_num(  # numerically better to use Taylor expansion
                    (E / self.delta_BB)**2 / self.Theta_BB**4 /
                    (np.exp(E/self.delta_BB/self.Theta_BB) - 1)
                ) * (E/self.delta_BB/self.Theta_BB >= 1e-10) + (
                    E/self.delta_BB / self.Theta_BB**3
                ) * (E/self.delta_BB/self.Theta_BB < 1e-10)
            )

    '''
    ############
    choose terms
    ############
    '''
    # set processes
    def turnOff(self, process):
        if isinstance(process, (list, np.ndarray)):
            for p in process:
                if p in self.include:
                    self.include[p] = 0
                else:
                    print(p, " is no process to turn off!")
        elif isinstance(process, str):
            self.include[process] = 0

    def turnOn(self, process):
        if isinstance(process, (list, np.ndarray)):
            for p in process:
                if p in self.include:
                    self.include[p] = 1
                else:
                    print(p, " is no process to turn off!")
        elif isinstance(process, str):
            self.include[process] = 1

    def setProcesses(self, processDict):
        self.include = processDict

    '''
    ###############
    solve equations
    ###############
    '''
    # set electron coefficients
    def v2(self, dt, SSC=True, BB=True):
        return (
            np.ones(self.NeE) +
            # Klein Nishina
            dt * self.Le_icKN_factor(self.eE, SSC, BB) +
            # synchrotron and inverse Compton Thomson
            (
                self.Le_sync_factor() +
                self.Le_icT_factor(self.eE_low, SSC, BB)
            ) *
            self.eE_low**2 / self.deE * dt
        )

    def v3(self, dt, SSC=True, BB=True):
        return (
            - (
               self.Le_sync_factor() +
               self.Le_icT_factor(self.eE_up, SSC, BB)
            ) *
            self.eE_up**2 / self.deE * dt
        )

    # set photon coefficients
    def y_prop(self, s, dt, SSC=True, BB=True):
        return (
            1. -
            dt * (
                -self.Ly_esc(s) -
                self.Ly_ssa(self.yE, s) -
                self.Ly_yy(self.yE, SSC, BB)
            )
        )

    def y_const(self, Ny, dt, SSC=True, BB=True):
        return (
            Ny +
            dt * (
                # synchrotron
                self.Qy_sync(self.yE) +
                # IC Thomson
                self.Qy_icT(self.yE, SSC, BB) +
                # IC Klein-Nishina
                self.Qy_icKN(self.yE, SSC, BB)
                # +
                # IC full
                # self.include["Qy_icFull"] * self.Qy_ic_full(
                #     Ne,
                #     self.include["SSC"] * Ny +
                #     self.include["BB"] * self.n_BB(self.yE),
                #     yE
                # )
            )
        )

    # solve equation for next step
    def solveNextStep(self, Ne_last, Ny_last, dt, s, dBB, beta, save=True):
        # set temperature of black body
        self.delta_BB = dBB
        # solve electron equation
        next_Ne = fast_TDMA(
            v2=self.v2(dt, self.include["SSC"], self.include["BB"]),
            v3=self.v3(dt, self.include["SSC"], self.include["BB"]),
            Nlast=Ne_last,
            sources=(
                dt * (
                    self.Qe_yy(self.eE, self.include["SSC"],
                               self.include["BB"]) +
                    self.Qe_inj_growth(s, beta, self.eE) +
                    self.Qe_inj_powerLaw()
                )
            )
        )
        self.updateInterpolationNe()

        # solve photon equation
        next_Ny = (
            self.y_const(
                Ny_last, dt, self.include["SSC"], self.include["BB"]
            ) /
            self.y_prop(s, dt, self.include["SSC"], self.include["BB"])
        )
        self.updateInterpolationNy()

        # check courant criterion
        self.courant.append(
            (
                self.Le_sync_factor() +
                self.Le_icT_factor(
                    self.eE, self.include["SSC"], self.include["BB"]
                )
            ) *
            self.eE**2 / self.deE * dt
        )
        if save:
            self.Ne_all.append(next_Ne)
            self.Ny_all.append(next_Ny)
        else:
            return next_Ne, next_Ny

    def solveUntil(self, T, sArray, betaArray, dopplerBBArray, printEvery=50):
        print("start calculating..\n", datetime.datetime.now())
        print(self.include)

        sIter = iter(sArray)
        betaIter = iter(betaArray)
        dBBIter = iter(dopplerBBArray)
        count = 0
        Nexp = (T - self.t[-1])/self.dt
        while self.t[-1] <= T:  # always 1 further calc. after check
            # print progress
            if not count % printEvery:
                print(count+1, "of", Nexp)

            # increment counter
            count += 1
            # solve next step
            self.solveNextStep(
                Ne_last=self.Ne_all[-1],
                Ny_last=self.Ny_all[-1],
                dt=self.dt,
                s=next(sIter),
                dBB=next(dBBIter),
                beta=next(betaIter),
                save=True
            )

            # add time to array
            self.t = np.append(self.t, self.t[-1] + self.dt)

        print("finished calculating!\n", datetime.datetime.now())

    '''
    ############
    save results
    ############
    '''
    def saveToh5(self, filename):
        # electrons
        Ne_df = pd.DataFrame(
            self.Ne_all,
            columns=self.eE
        )
        Ne_df.to_hdf(filename, key="Ne")
        # photons
        Ny_df = pd.DataFrame(
            self.Ny_all,
            columns=self.yE
        )
        Ny_df["t"] = self.t[::self.saveEvery]
        Ny_df.to_hdf(filename, key="Ny")
        # Courant values
        C_df = pd.DataFrame(
            self.courant,
            columns=self.eE
        )
        C_df.to_hdf(filename, key="Courant")

    '''
    ########
    plotting
    ########
    '''
    # every NLines-th time step will be plotted
    def plotEvolution(self, maxLines=1000, ax=None,
                      logx=True, logy=True,
                      eSlope=2, ySlope=2, BB=True):
        # plotting
        if ax is None:
            # fig, (axe, axg, axcb) = plt.subplots(
            #     ncols=3,
            #     figsize=(10, 5),
            #     gridspec_kw={"width_ratios": [20, 20, 2]}
            # )
            # sfc.fullWidth(aspectRatio=0.8)
            fig, axe = plt.subplots(figsize=(10, 5))
            divider = make_axes_locatable(axe)
            axg = divider.append_axes("right", size="100%", pad=0.8)
            axcb = divider.append_axes("right", size="5%", pad=0.05)

            # don't plot more lines than maxlines
            Nt = len(self.t)
            if Nt > maxLines:
                dL = int(np.ceil(Nt / float(maxLines)))
                Ne_all = np.array(self.Ne_all)[::dL]
                Ny_all = np.array(self.Ny_all)[::dL]
                Nlines = len(Ne_all)
                t = self.t[::self.saveEvery][::dL]
            else:
                Ne_all = np.array(self.Ne_all)
                Ny_all = np.array(self.Ny_all)
                Nlines = len(Ne_all)
                t = self.t[::self.saveEvery]

            # cmap = plt.get_cmap("viridis_r")
            cmap = cl.c_time()
            cy = cmap(np.linspace(0, 1, Nlines))

            # electron plot
            axe.set_prop_cycle('color', cy)
            if eSlope == 0:
                axe.set_ylabel(r"$n_e$")
            else:
                axe.set_ylabel(r"$\gamma^{%g} n_e$" % (eSlope))
            axe.set_xlabel(r"$\gamma$")
            if logx:
                axe.set_xscale("log")
            if logy:
                axe.set_yscale("log")

            for Ne in Ne_all:
                axe.plot(self.eE, self.eE**eSlope * Ne, drawstyle="steps-mid")

            axe.set_xlim(1, axe.get_xlim()[1])

            # gamma plot
            axg.set_prop_cycle('color', cy)
            if ySlope == 0:
                axg.set_ylabel(r"$n_{\gamma}$")
            else:
                axg.set_ylabel(r"$x^{%g} n_{\gamma}$" % (ySlope))
            axg.set_xlabel(r"$x$")
            if logx:
                axg.set_xscale("log")
            if logy:
                axg.set_yscale("log")

            for Ny in Ny_all:
                axg.plot(self.yE, self.yE**ySlope * Ny, drawstyle="steps-mid")

            # black body line in plot
            if BB:
                axg.plot(self.yE, self.yE**ySlope * self.n_BB(self.yE),
                         label="Black Body", c="tab:red")
                axg.legend()

            # set y limits
            axe = fig.get_axes()[0]
            axe.set_ylim(
                10**-7 * np.max(self.Ne_all * self.eE**2),
                2 * np.max(self.Ne_all * self.eE**2)
            )
            axg = fig.get_axes()[1]
            axg.set_ylim(
                10**-7 * np.max(self.Ny_all * self.yE**2),
                2 * np.max(self.Ny_all * self.yE**2)
            )

            # colorbar
            cb2 = cb.ColorbarBase(
                axcb, cmap=cmap,
                norm=Normalize(vmin=min(t), vmax=max(t)),# boundaries=t,
                ticks=np.linspace(min(t), max(t), 5),
                orientation='vertical', format="%.1f"
            )
            cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=20)
            fig.tight_layout()
            return fig
        else:
            print("not implemented yet")
            return ax

    # every NLines-th time step will be plotted
    def plotEvolutionE(self, maxLines=1000, ax=None,
                       logx=True, logy=True, eSlope=2):
        # plotting
        if ax is None:
            sfc.fullWidth(aspectRatio=0.5)
            fig, axe = plt.subplots()
            divider = make_axes_locatable(axe)
            axcb = divider.append_axes("right", size="5%", pad=0.05)

            # don't plot more lines than maxlines
            Nt = len(self.t)
            if Nt > maxLines:
                dL = int(np.ceil(Nt / float(maxLines)))
                Ne_all = np.array(self.Ne_all)[::dL]
                Nlines = len(Ne_all)
                t = self.t[::self.saveEvery][::dL]
            else:
                Ne_all = np.array(self.Ne_all)
                Nlines = len(Ne_all)
                t = self.t[::self.saveEvery]

            # cmap = plt.get_cmap("viridis_r")
            cmap = cl.c_time()
            cy = cmap(np.linspace(0, 1, Nlines))

            # electron plot
            axe.set_prop_cycle('color', cy)
            if eSlope == 0:
                axe.set_ylabel(r"$n_e$")
            else:
                axe.set_ylabel(r"$\gamma^{%g} n_e$" % (eSlope))
            axe.set_xlabel(r"$\gamma$")
            if logx:
                axe.set_xscale("log")
            if logy:
                axe.set_yscale("log")

            for Ne in Ne_all:
                axe.plot(self.eE, self.eE**eSlope * Ne, drawstyle="steps-mid")

            axe.set_xlim(1, axe.get_xlim()[1])

            # set y limits
            axe = fig.get_axes()[0]
            axe.set_ylim(
                10**-7 * np.max(self.Ne_all * self.eE**2),
                2 * np.max(self.Ne_all * self.eE**2)
            )

            # colorbar
            cb2 = cb.ColorbarBase(
                axcb, cmap=cmap,
                norm=Normalize(vmin=min(t), vmax=max(t)),
                ticks=np.linspace(min(t), max(t), 5),
                orientation='vertical', format="%.1f"
            )
            cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=20)
            fig.tight_layout()
            return fig
        else:
            print("not implemented yet")
            return ax

    # every NLines-th time step will be plotted
    def plotEvolutionY(self, maxLines=1000, ax=None,
                       logx=True, logy=True, ySlope=2, BB=True):
        # plotting
        if ax is None:
            sfc.fullWidth(aspectRatio=0.5)
            fig, axg = plt.subplots()
            divider = make_axes_locatable(axg)
            axcb = divider.append_axes("right", size="5%", pad=0.05)

            # don't plot more lines than maxlines
            Nt = len(self.t)
            if Nt > maxLines:
                dL = int(np.ceil(Nt / float(maxLines)))
                Ny_all = np.array(self.Ny_all)[::dL]
                Nlines = len(Ny_all)
                t = self.t[::self.saveEvery][::dL]
            else:
                Ny_all = np.array(self.Ny_all)
                Nlines = len(Ny_all)
                t = self.t[::self.saveEvery]

            # cmap = plt.get_cmap("viridis_r")
            cmap = cl.c_time()
            cy = cmap(np.linspace(0, 1, Nlines))

            # gamma plot
            axg.set_prop_cycle('color', cy)
            if ySlope == 0:
                axg.set_ylabel(r"$n_{\gamma}$")
            else:
                axg.set_ylabel(r"$x^{%g} n_{\gamma}$" % (ySlope))
            axg.set_xlabel(r"$x$")
            if logx:
                axg.set_xscale("log")
            if logy:
                axg.set_yscale("log")

            for Ny in Ny_all:
                axg.plot(self.yE, self.yE**ySlope * Ny, drawstyle="steps-mid")

            # black body line in plot
            if BB:
                axg.plot(self.yE, self.yE**ySlope * self.n_BB(self.yE),
                         label="Black Body", c="tab:red")
                axg.legend()

            # set y limits
            axg.set_ylim(
                10**-7 * np.max(self.Ny_all * self.yE**2),
                1 * np.max(self.Ny_all * self.yE**2)
            )

            # colorbar
            cb2 = cb.ColorbarBase(
                axcb, cmap=cmap,
                norm=Normalize(vmin=min(t), vmax=max(t)),
                ticks=np.linspace(min(t), max(t), 5),
                orientation='vertical', format="%.1f"
            )
            cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=20)
            fig.tight_layout()
            return fig
        else:
            print("not implemented yet")
            return ax


'''
##################
plotting from file
##################
'''


def plotEvolutionFromFile(filename, maxLines=1000, addLast=True, ax=None,
                          logx=True, logy=True, saveEvery=1, ind0=0,
                          eSlope=2, ySlope=2, BB=True, cmap="viridis_r",
                          nTicks=5):
    # read file
    eE, Ne = Ne_fromFile(filename)
    yE, Ny = Ny_fromFile(filename)
    t = t_fromFile(filename)
    Nt = len(t)

    if addLast:
        tLast = t.iloc[-1]
        NeLast = Ne.iloc[-1]
        NyLast = Ny.iloc[-1]
        NLast = t.index[-1]

    # limit number of plotted stuff
    # don't plot more lines than maxlines
    if Nt > maxLines:
        dL = int(np.ceil(Nt / float(maxLines)))
        Ne = Ne[ind0::dL]
        Ny = Ny[ind0::dL]
        t = t[ind0::dL]
        Nt = len(t)

    # explicitly add latest time step
    if addLast:
        if t.iloc[-1] != tLast:
            t[NLast] = tLast
            Ne.append(NeLast)
            Ny.append(NyLast)
            Nt += 1

    # plotting
    c_map = cmap
    cy = c_map(np.linspace(0, 1, Nt))

    if ax is None:
        # fig, (axe, axg, axcb) = plt.subplots(
        #     ncols=3,
        #     gridspec_kw={"width_ratios": [20, 20, 1]}, constrained_layout=True
        # )

        fig, axe = plt.subplots()
        divider = make_axes_locatable(axe)
        axg = divider.append_axes("right", size="100%", pad=0.75)
        axcb = divider.append_axes("right", size="5%", pad=0.05)
        # electron plot
        axe.set_prop_cycle('color', cy)
        if eSlope == 0:
            axe.set_ylabel(r"$n_e$")
        else:
            axe.set_ylabel(r"$\gamma^{%g} n_e$" % (eSlope))
        axe.set_xlabel(r"$\gamma$")
        if logx:
            axe.set_xscale("log")
        if logy:
            axe.set_yscale("log")

        for i in range(len(Ne)):
            axe.plot(eE, eE**eSlope * Ne.iloc[i].values, drawstyle="steps-mid")

        axe.set_xlim(1, axe.get_xlim()[1])

        # gamma plot
        axg.set_prop_cycle('color', cy)
        if ySlope == 0:
            axg.set_ylabel(r"$n_{\gamma}$")
        else:
            axg.set_ylabel(r"$x^{%g} n_{\gamma}$" % (ySlope))
        axg.set_xlabel(r"$x$")
        if logx:
            axg.set_xscale("log")
        if logy:
            axg.set_yscale("log")

        for i in range(len(Ny)):
            axg.plot(yE, yE**ySlope * Ny.iloc[i].values, drawstyle="steps-mid")

        # set y limits
        axe = fig.get_axes()[0]
        axe.set_ylim(
            10**-15 * max(Ne * eE**2),
            10 * max(Ne * eE**2)
        )
        axg = fig.get_axes()[1]
        axg.set_ylim(
            10**-10 * max(Ny * yE**2),
            10 * max(Ny * yE**2)
        )

        # black body line in plot
        if BB:
            runInfo = pd.read_hdf(filename, key="RunInfo")
            l_BB = runInfo["l_BB"][0]
            Theta_BB = runInfo["Theta_BB"][0]
            nbb = (l_BB * 45./np.pi**4 * yE**2 / Theta_BB**4 /
                   (np.exp(yE/Theta_BB) - 1))
            axg.plot(yE, yE**ySlope * nbb, label="Black Body", c="tab:red")
            axg.legend()
        # colorbar
        # bounds = np.linspace(0, Tmax, len(Ne)//NLines+1)
        cb2 = cb.ColorbarBase(axcb, cmap=c_map,
                              boundaries=t,
                              ticks=np.linspace(min(t), max(t), nTicks),
                              orientation='vertical',
                              spacing="proportional", format="%.1f")
        cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=10)
        # fig.tight_layout()
        return fig
    else:
        print("not implemented yet!!")
        return ax


def plotEvolutionFromFileV(filename, maxLines=1000, addLast=True, ax=None,
                           logx=True, logy=True, saveEvery=1,
                           eSlope=2, ySlope=2, BB=True, cmap="viridis_r",
                           pad=0.6, nTicks=5, ind0=0):
    # read file
    eE, Ne = Ne_fromFile(filename)
    yE, Ny = Ny_fromFile(filename)
    t = t_fromFile(filename)
    Nt = len(t)

    if addLast:
        tLast = t.iloc[-1]
        NeLast = Ne.iloc[-1]
        NyLast = Ny.iloc[-1]
        NLast = t.index[-1]

    # limit number of plotted stuff
    # don't plot more lines than maxlines
    if Nt > maxLines:
        dL = int(np.ceil(Nt / float(maxLines)))
        Ne = Ne[ind0::dL]
        Ny = Ny[ind0::dL]
        t = t[ind0::dL]
        Nt = len(t)

    # explicitly add latest time step
    if addLast:
        if t.iloc[-1] != tLast:
            t[NLast] = tLast
            Ne.append(NeLast)
            Ny.append(NyLast)
            Nt += 1

    # plotting
    c_map = cmap
    cy = c_map(np.linspace(0, 1, Nt))

    if ax is None:
        fig, axe = plt.subplots()
        divider = make_axes_locatable(axe)
        axg = divider.append_axes("bottom", size="100%", pad=pad)
        axcb = divider.append_axes("right", size="5%", pad=0.05)
        # electron plot
        axe.set_prop_cycle('color', cy)
        if eSlope == 0:
            axe.set_ylabel(r"$N_e$")
        else:
            axe.set_ylabel(r"$\gamma^{%g} N_e$" % (eSlope))
        axe.set_xlabel(r"$\gamma$")
        if logx:
            axe.set_xscale("log")
        if logy:
            axe.set_yscale("log")

        for i in range(len(Ne)):
            axe.plot(eE, eE**eSlope * Ne.iloc[i].values, drawstyle="steps-mid")

        axe.set_xlim(1, axe.get_xlim()[1])

        # gamma plot
        axg.set_prop_cycle('color', cy)
        if ySlope == 0:
            axg.set_ylabel(r"$N_{\gamma}$")
        else:
            axg.set_ylabel(r"$x^{%g} N_{\gamma}$" % (ySlope))
        axg.set_xlabel(r"$x$")
        if logx:
            axg.set_xscale("log")
        if logy:
            axg.set_yscale("log")

        for i in range(len(Ny)):
            axg.plot(yE, yE**ySlope * Ny.iloc[i].values, drawstyle="steps-mid")

        # set y limits
        axe = fig.get_axes()[0]
        axe.set_ylim(
            10**-15 * max(Ne * eE**2),
            10 * max(Ne * eE**2)
        )
        axg = fig.get_axes()[1]
        axg.set_ylim(
            10**-10 * max(Ny * yE**2),
            10 * max(Ny * yE**2)
        )

        # black body line in plot
        if BB:
            runInfo = pd.read_hdf(filename, key="RunInfo")
            l_BB = runInfo["l_BB"][0]
            Theta_BB = runInfo["Theta_BB"][0]
            nbb = (l_BB * 45./np.pi**4 * yE**2 / Theta_BB**4 /
                   (np.exp(yE/Theta_BB) - 1))
            axg.plot(yE, yE**ySlope * nbb, label="Black Body", c="tab:red")
            axg.legend()
        # colorbar
        # bounds = np.linspace(0, Tmax, len(Ne)//NLines+1)
        cb2 = cb.ColorbarBase(axcb, cmap=c_map,
                              boundaries=t,
                              ticks=np.linspace(min(t), max(t), nTicks),
                              orientation='vertical', format="%.1f",
                              spacing="proportional")
        cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=10)
        # fig.tight_layout()
        return fig
    else:
        print("not implemented yet!!")
        return ax


# returns electron energy and histograms from file
def Ne_fromFile(filename):
    Ne = pd.read_hdf(filename, key="Ne")
    eE = np.array(Ne.columns, dtype=np.float64)
    return eE, Ne


# returns photon energy and histograms from file
def Ny_fromFile(filename):
    Ny = pd.read_hdf(filename, key="Ny").drop("t", axis=1)
    yE = np.array(Ny.columns, dtype=np.float64)
    return yE, Ny


# returns time from file
def t_fromFile(filename):
    Ny = pd.read_hdf(filename, key="Ny")
    return Ny["t"]


def fitPowerLaw(x, y, guess, height, textpos="center", pivot=1, ax=None,
                c="tab:orange", slope=0):

    def powerlaw(x, p0, si):
        # return p0 * (x/pivot)**si
        return np.log10(p0) + si * np.log10(x/pivot)

    popt, pcov = curve_fit(powerlaw, x, np.log10(y), p0=guess)
    print("norm: ", popt[0])
    print("si  : ", popt[1])
    print("uncertainty:", pcov)
    if ax is not None:
        ax.axvspan(x[0], x[-1], color=c, alpha=0.3)
        # calc text position's x-value
        if textpos == "center":
            xtext = (x[-1] + x[0])/2
            hAlign = "center"
        elif textpos == "logCenter":
            xtext = 10**(np.log10(x[-1]*x[0])/2)
            hAlign = "center"
        elif textpos == "right":
            xtext = 1.05 * x[-1]
            hAlign = "left"
        elif textpos == "left":
            xtext = 0.95 * x[0]
            hAlign = "right"
        ax.text(
            xtext, height, r"$\Gamma=%.1f$" % (popt[1]),
            horizontalalignment=hAlign, verticalalignment='center',
            bbox={"boxstyle": "round", "facecolor": "lightgrey", "alpha": 1}
        )
        return ax, popt, pcov
    else:
        return popt, pcov


# returns frequency of photon (default in Hz)
def nuOfX(x, unit="Hz"):
    return (const.m_e * const.c**2 / const.h * x).to(unit).value


# returns energy of photon (default in GeV)
def EofX(x, unit="GeV"):
    return (x * const.m_e * const.c**2).to(unit).value


# adds line to ax
def addSpectrumToAx(x, Ny, ax, label=None, unitx="Hz", unity="erg/s"):
    nu = nuOfX(x, unitx)
    ax.plot(nu, x**2 * Ny, label=label)
    return ax


def addEnergyAxis(ax, unitx):
    axE = ax.twiny()
    xlim = ax.get_xlim()
    axE.set_xscale("log")
    axE.set_xlim(xlim)

    # find tick locations
    numin = xlim[0]
    numax = xlim[1]
    Emin = (numin * u.Hz * const.h).to("eV").value
    Emax = (numax * u.Hz * const.h).to("eV").value
    allticks = np.array([-3, 0, 3, 6, 9, 12, 15, 18])
    Eticks = []
    for t in allticks:
        if np.log10(Emin) < t and np.log10(Emax) > t:
            Eticks.append(t)

    nuTicks = (10.**np.array(Eticks) * u.eV / const.h).to(unitx).value
    axE.set_xticks(nuTicks)

    # labels
    unitFactor = {-3: "m", 0: "", 3: "k", 6: "M", 9: "G", 12: "T", 15: "P",
                  18: "E"}
    labels = [unitFactor[Etick] + "eV" for Etick in Eticks]
    axE.set_xticklabels(labels)

    return axE


# plots evolution of spectrum
# converts x to freuqency in unitx, x^2*Ny=nu L_nu to unity, and times to unitt
def plotSpectralEvolution(x_all, Ny_all, times,
                          unitx="Hz", unitE="eV", unity="erg/s", unitt="s",
                          maxLines=1000, saveEvery=1):
    Nt0 = len(x_all)
    # fig, (ax, axcb) = plt.subplots(ncols=2, figsize=(10, 5),
    #                                gridspec_kw={"width_ratios": [100, 2]})
    sfc.fullWidth(aspectRatio=0.5)
    fig, ax = plt.subplots()

    # don't plot more lines than maxlines
    if Nt0 > maxLines:
        dL = int(np.ceil(Nt0 / float(maxLines)))
        Ny_all = np.array(Ny_all)[::dL]
        x_all = np.array(x_all)[::dL]
        t = (times[::saveEvery] - times[0])[::dL]
    else:
        Ny_all = np.array(Ny_all)
        x_all = np.array(x_all)
        t = times[::saveEvery] - times[0]
    Nt = len(t)

    # cmap = plt.get_cmap("viridis_r")
    cmap = cl.c_time()
    times = (t * u.s).to(unitt).value
    cy = cmap((times-times[0])/max(times))
    ax.set_prop_cycle('color', cy)

    for i in range(Nt):
        addSpectrumToAx(x_all[i], Ny_all[i], ax, unitx=unitx, unity=unity)
    ax.set_xscale("log")
    ax.set_yscale("log")

    ax.set_xlabel(r"$\nu \:[Hz]$")
    ax.set_ylabel(r"$\nu L_{\nu} \:[erg/s]$")

    # set y limits
    maximum = np.max(Ny_all * x_all**2 * (u.erg/u.s).to(unity))
    ax.set_ylim(
        10**-6 * maximum,
        5 * maximum
    )

    axE = addEnergyAxis(ax, unitx=unitx)
    # colorbar
    divider = make_axes_locatable(ax)
    divider2 = make_axes_locatable(axE)
    axcb = divider.append_axes("right", size="5%", pad=0.05)
    axcb_empty = divider2.append_axes("right", size="5%", pad=0.05)
    axcb_empty.set_visible(False)
    cb2 = cb.ColorbarBase(axcb, cmap=cmap,
                          #boundaries=bounds,
                          norm=Normalize(vmin=min(times), vmax=max(times)),
                          ticks=np.linspace(min(times), max(times), 5),
                          orientation='vertical', format="%.1f")
                          #spacing="proportional")
    cb2.set_label(r"$ t \:[" + unitt + "]$", rotation=0, labelpad=20)

    fig.tight_layout()
    #fig.tight_layout()
    return fig
