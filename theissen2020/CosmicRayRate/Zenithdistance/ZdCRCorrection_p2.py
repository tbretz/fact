import corner
import emcee
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from scipy.optimize import minimize
from scipy.stats import gaussian_kde as gkde

import DataFrameBinning as dfb
import setFigureConfig as figConfig
from correctionModels import zenithModel, zenithCorrectionFactor
from LibFunctions import findFWHM, weightedMean, roundAndFollowDIN
from rootRoutines import dFrameFromRoot

"""
    file and reproducibility management
"""
# set flag if mc sampler should be rerun
sample = False

# fix numpy random seed for purposes of reproducibility
np.random.seed(123)

# define output pdf for all plots produced by this script
outputPDF = PdfPages("./ZdCRRateFits.pdf")

# TBretz 2019 results
# r0 was read off from the plot the remaining numbers are given in the paper
tbretz2019 = np.array([246, 0.78, 0.26, 0.71])

"""
    data management
"""

# path to root file with artificial trigger rates information from fact db
runsCR = dFrameFromRoot(
    "../ArtificialTriggerRates.root",
    keys=["sql"], prints=False
)

# sources that are taken into account for this investigation are the five
# most observed ones
sources = {
    "Mrk421": 1,
    "Mrk501": 2,
    "1ES 2344+51.4": 3,
    "Crab": 5,
    "1ES 1959+650": 7
}
runsCR = runsCR[runsCR["fSourceKey"].isin(list(sources.values()))]

# minimum Quality Cuts
runsCR = runsCR[runsCR["fNight"] > 20140101]
runsCR = runsCR[runsCR["fEffectiveOn"] > 0.9]
runsCR = runsCR[runsCR["fRunDuration"] > 297]
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fR750Cor"] < 1000]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 950]

# calculation of addional quantities
runsCR["RAWTime"] = (runsCR["fRunStart"] + runsCR["fRunStop"])/2
runsCR["Time"] = pd.to_datetime(runsCR["RAWTime"], unit="s")
runsCR.sort_values("Time")

runsCR["RCR"] = 60*runsCR["fNumThreshold750"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

runsCR["RCR_err"] = 60*np.sqrt(runsCR["fNumThreshold750"]) / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

# zenith distance binning in 3° steps
binEdges = dfb.addBinning(
    runsCR, "fZenithDistanceMean", "ZdBin",
    np.arange(9.3417, 76, 3.5)
)[1]

# exclude all events in the -1 flagged bin as they exceed the given bin range
runsCR = runsCR[runsCR["ZdBin"] != -1]

# calculate bin related quantities
zdBinnedDf = pd.DataFrame()
zdBinnedGroup = runsCR.groupby("ZdBin")
zdBinnedDf["ZdMean"] = zdBinnedGroup["fZenithDistanceMean"].apply(
    lambda r: np.mean(r)
)
zdBinnedDf["ZdCount"] = zdBinnedGroup["fZenithDistanceMean"].apply(
    lambda r: len(r)
)

zdBinnedRates = zdBinnedGroup["RCR"]

zdBinnedDf["RCR_mean"] = zdBinnedRates.apply(lambda r: np.mean(r))
zdBinnedDf["RCR_std"] = zdBinnedRates.apply(lambda r: np.std(r, ddof=1))
zdBinnedDf["RCR_meanErr"] = zdBinnedRates.apply(
    lambda r: np.std(r, ddof=1)/np.sqrt(len(r))
)

zdBinnedDf["minZd"] = binEdges[:-1]
zdBinnedDf["maxZd"] = binEdges[1:]

# add kde evaluation
KDEs = []
KDEs = zdBinnedRates.apply(lambda r: gkde(r))
KDEx = np.linspace(0, 400, num=1000)
KDEy = []
for i in range(len(KDEs)):
    KDEy.append(KDEs[i](KDEx))

zdBinnedDf["KDE_max"] = np.array(
    [KDEx[np.argmax(kde_y)] for kde_y in KDEy]
)
zdBinnedDf["KDE_fwhm"] = np.array(
    [findFWHM(KDEx, kde_y)[0] for kde_y in KDEy]
)
zdBinnedDf["KDE_std"] = zdBinnedDf["KDE_fwhm"]/(2*np.sqrt(2*np.log(2)))

"""
    fit section
"""


def logPrior(pars):
    r0, chi, xi, kappa, log_f = pars
    if (
        225 < r0 < 275 and
        -0.2 < chi < 1 and
        -0.2 < xi < 1 and
        -1 < kappa < 3 and
        -10.0 < log_f < 1.0
    ):
        return 0.0
    return -np.inf


def negativeLogLikelihood(pars, x, y, yerr):
    r0, chi, xi, kappa, log_f = pars
    model = zenithModel(x, r0, chi, xi, kappa, gamma=2.7)
    yerr2 = np.exp(2*log_f) * yerr**2
    ll = -0.5*np.sum((y-model)**2/yerr2 + np.log(yerr2))
    return -ll


def logProbability(pars, x, y, yerr):
    lp = logPrior(pars)
    if not np.isfinite(lp):
        return -np.inf
    ll = - negativeLogLikelihood(pars, x, y, yerr)
    return lp + ll


# for initial values use tbretz2019 results and -2 as initial log(f) estimation
initial = np.append(tbretz2019, -2)
soln = minimize(
    negativeLogLikelihood, initial,
    args=(zdBinnedDf["ZdMean"], zdBinnedDf["KDE_max"], zdBinnedDf["KDE_std"])
)
optPars = soln.x

# print maximum likelyhood results
print("maximum likelyhood results:")
print(optPars[:-1], np.exp(optPars[-1]))
print("\n")

pos = optPars + 1e-4*np.random.randn(32, 5)
nwalkers, ndim = pos.shape

if sample:
    sampler = emcee.EnsembleSampler(
        nwalkers, ndim, logProbability,
        args=(zdBinnedDf["ZdMean"], zdBinnedDf["KDE_max"],
              zdBinnedDf["KDE_std"])
    )
    sampler.run_mcmc(pos, 5000, progress=True)

    # check boost in phase and reduce samples accordingly
    tau = sampler.get_autocorr_time()
    print("boost-in phase information:")
    print(tau)
    print("\n")
    thinBy = int(np.mean(tau)/2.)
    discardIndex = int(3*np.mean(tau))
    flat_samples = sampler.get_chain(
        discard=discardIndex, thin=thinBy, flat=True
    )

    np.save("./flat_samples.npy", flat_samples)

else:
    flat_samples = np.load("./flat_samples.npy")

"""
    evaluate sampling results
"""

labels = [r"$R_{0}$", r"$\chi$", r"$\xi$", r"$\kappa$", r"$\log{f}$"]
labelsNoFormat = ["r0", "chi", "xi", "kappa", "log(f)"]
resultDicts = []
for i in range(ndim):
    mcmc = np.percentile(flat_samples[:, i], [16, 50, 84])
    q = np.diff(mcmc)
    resultDicts.append(
        {
            "key": labelsNoFormat[i],
            "mean": mcmc[1],
            "q16": mcmc[0],
            "q84": mcmc[2],
            "+": q[1], "-": q[0],
            "tbretz": initial[i]
        }
    )
pRes = pd.DataFrame(resultDicts)
pRes.to_hdf("../ZenithCorrectionResults.h5", "ZenithCorrectionResults")

# print posterior results
print("posterior results dataframe:")
print(pRes)
print("\n")


"""
    fit control and result plots
"""

if sample:
    fig, axes = plt.subplots(len(optPars), figsize=(10, 7), sharex=True)
    samples = sampler.get_chain()
    for i in range(ndim):
        ax = axes[i]
        ax.axvline(x=tau[i], color="tab:blue")
        ax.axvline(x=discardIndex, color="tab:orange")
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(labels[i])
        ax.yaxis.set_label_coords(-0.1, 0.5)

    axes[-1].set_xlabel("step number")
    outputPDF.savefig(fig)


def makeCornerPlot(flat_samples):
    figConfig.fullWidth()
    fig = corner.corner(
        flat_samples, labels=labels
    )
    ndim = np.shape(flat_samples)[1]
    axes = np.array(fig.axes).reshape((ndim, ndim))
    for j in range(ndim):
        ax = axes[j, j]
        parDf = pRes[pRes["key"] == labelsNoFormat[j]]
        mean = parDf["mean"].values[0]
        tbretz = parDf["tbretz"].values[0]
        left = parDf["q16"].values[0]
        right = parDf["q84"].values[0]
        span = ax.axvspan(
            xmin=left, xmax=right, facecolor="tab:orange", alpha=0.5
        )
        ax.axvline(x=mean, ls="--", color="tab:orange")
        ax2 = ax.twiny()
        ax2.set_xlim(ax.get_xlim())
        ax2.set_xticks(ax.get_xticks())
        ax2.set_xticklabels([])
        rounded = roundAndFollowDIN(
            mean, [parDf["-"].values[0], parDf["+"].values[0]]
        )
        resultString = "{3}"+r"$= {0}_{{-{1}}}^{{+{2}}}$"
        resultString = resultString.format(
            str(rounded[0]), str(rounded[1][0]), str(rounded[1][1]), labels[j]
        )
        ax2.set_xlabel(resultString)
        if labelsNoFormat[j] != "log(f)":
            ax.axvline(x=tbretz2019[j], color="tab:blue")

        for i in range(j):
            ax = axes[j, i]
            parDf2 = pRes[pRes["key"] == labelsNoFormat[i]]
            mean2 = parDf2["mean"].values[0]
            tbretz2 = parDf2["tbretz"].values[0]
            ax.axvline(x=mean2, ls="--", color="tab:orange")
            ax.axhline(y=mean, ls="--", color="tab:orange")
            ax.plot(mean2, mean, "s", color="tab:orange")
            if labelsNoFormat[i] != "log(f)" and labelsNoFormat[j] != "log(f)":
                ax.axvline(x=tbretz2, color="tab:blue")
                ax.axhline(y=tbretz, color="tab:blue")
                ax.plot(tbretz2, tbretz, "s", color="tab:blue")

    legend = fig.legend(
        (
            mlines.Line2D(
                [0], [0], marker='s', color='tab:blue', linewidth=2,
                markerfacecolor="tab:blue", markersize=10
            ),
            mlines.Line2D(
                [0], [0], marker='s', ls="--", color='tab:orange', linewidth=2,
                markerfacecolor="tab:orange", markersize=10
            ),
            span
        ),
        (
            "best fit Bretz 2019",
            "best fit as median of\nthe MC ensemble",
            "uncertainty estimation as\nquantile 16 % to 84 %"
        ),
        loc="upper right",
        framealpha=0.9, bbox_to_anchor=(0.98, 0.98),
        handlelength=3, fontsize=15
    )

    return fig


fig = makeCornerPlot(flat_samples)
outputPDF.savefig(fig)
fig = makeCornerPlot(flat_samples[:, :-1])
outputPDF.savefig(fig)
fig.savefig("./SeperatePlots/ZdCornerResults.png", dpi=300)
fig.savefig("./SeperatePlots/ZdCornerResults.pdf")

"""
    model plot
"""

theta = np.linspace(0, 90, 200)
modelFit = zenithModel(
    theta, optPars[0], optPars[1], optPars[2], optPars[3],
    gamma=2.7, alpha=0.065
)
modelBretz = zenithModel(
    theta, 246, 0.78, 0.26, 0.71, gamma=2.7, alpha=0.065
)
inds = np.random.randint(len(flat_samples), size=500)

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.plot(theta, modelBretz, color="tab:blue", zorder=4, label="Bretz 2019")
ax.plot(theta, modelFit, color="tab:orange",
        lw=0.3, zorder=4, label="500 random samples from\nthe MC ensemble")
for i in range(len(inds)):
    sample = flat_samples[inds[i]]
    ax.plot(
        theta,
        zenithModel(
            theta, sample[0], sample[1], sample[2], sample[3],
            gamma=2.7, alpha=0.065
        ),
        lw=0.3,
        color="tab:orange", alpha=0.1
    )
ax.errorbar(
    zdBinnedDf["ZdMean"], zdBinnedDf["KDE_max"], yerr=zdBinnedDf["KDE_std"],
    fmt=".", markersize=10, color="k",
    zorder=5, label="KDE maxima in\n"+"corresponding\n"+r"$\theta$"+" bins"
)
ax.minorticks_off()

ax.set_xlabel("zenith distance "+r"$\theta$"+" in degrees")
ax.set_ylabel(r"$R_{CR}$ in events/min")
ax.grid(alpha=0.5, axis="both", zorder=10)
legendHandels, legendLabels = ax.get_legend_handles_labels()
legendHandels[1] = mlines.Line2D(
    [0], [0], color='tab:orange', linewidth=2, alpha=0.8
)
ax.legend(legendHandels, legendLabels, loc="lower left")
fig.tight_layout()
fig.savefig("./SeperatePlots/ZdFitKDE.png", dpi=300)
fig.savefig("./SeperatePlots/ZdFitKDE.pdf")
outputPDF.savefig(fig)

"""
c Factor Figs
"""

cFactors = []
theta = np.linspace(20, 75, 500)
figConfig.halfWidth(aspectRatio=0.8)
fig = plt.figure()
ax = fig.gca()
ax.plot(
    theta,
    zenithCorrectionFactor(
        theta, optPars[1], optPars[2], optPars[3],
        gamma=2.7, alpha=0.065
    ),
    color="k", lw=1, zorder=4,
    label="best fit"
)
firstFlag = True
for i in range(len(inds)):
    if firstFlag:
        label = "500 random samples from\nthe MC ensemble"
    else:
        label = None
    sample = flat_samples[inds[i]]
    cFactor = zenithCorrectionFactor(
        theta, sample[1], sample[2], sample[3],
        gamma=2.7, alpha=0.065
    )
    firstFlag = False
    cFactors.append(cFactor)
    ax.plot(
        theta,
        cFactor,
        lw=0.3,
        color="tab:orange", alpha=0.1, label=label
    )
ax.set_ylim(0.8, 7)
ax.set_xlim(20, 75)
ax.set_xlabel("zenith distance "+r"$\theta$"+" in degrees")
ax.set_ylabel("correction factor "+r"$c_{\theta}$")
ax.grid(alpha=0.5, axis="both", zorder=10)
legendHandels, legendLabels = ax.get_legend_handles_labels()
legendHandels[1] = mlines.Line2D(
    [0], [0], color='tab:orange', linewidth=2, alpha=0.8
)
ax.legend(legendHandels, legendLabels, loc="upper left")
fig.tight_layout()
fig.savefig("./SeperatePlots/ZdFitCorrFactors.png", dpi=300)
fig.savefig("./SeperatePlots/ZdFitCorrFactors.pdf")
outputPDF.savefig(fig)

cFactors = np.array(cFactors)

cFactorMeans = np.mean(cFactors, axis=0)
cFactorStds = np.std(cFactors, axis=0, ddof=1)
relCFactorStds = cFactorStds/cFactorMeans

fig = plt.figure()
ax = fig.gca()
ax2 = ax.twinx()
ax.plot(
    theta, cFactorStds,
    color="tab:blue", lw=2,
    label="absolute uncertainty"
)
ax2.plot(
    theta, relCFactorStds,
    color="tab:orange", ls="--", lw=2,
    label="relative uncertainty"
)

ax.set_ylim(0, 0.55)
ax2.set_ylim(0, 0.055)
ax2.set_ylabel("relative uncertainty "+r"$\sigma_{c_{\theta}}/c_{\theta}$")
ax.set_ylabel("absolute uncertainty "+r"$\sigma_{c_{\theta}}$")

ax.set_xlabel("zenith distance "+r"$\theta$"+" in degrees")
ax.set_xlim(20, 75)
ax.grid(alpha=0.5, axis="both", zorder=10)

fig.legend(loc='upper left', bbox_to_anchor=(0.18, 0.95))
fig.tight_layout()
fig.savefig("./SeperatePlots/ZdFitUncertainty.png", dpi=300)
fig.savefig("./SeperatePlots/ZdFitUncertainty.pdf")
outputPDF.savefig(fig)

outputPDF.close()
