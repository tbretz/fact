#!rootifysql
WITH RunParams AS
(
    SELECT
        RunInfo.fNight,
        RunInfo.fRunID,
        RunInfo.fPeriod,
        RunInfo.fRunStart,
        RunInfo.fRunStop,
        TIME_TO_SEC(TIMEDIFF(fRunStop,fRunStart)) AS fRunDuration,

        RunInfo.fR750Cor,
        RunInfo.fR750Ref,
        RunInfo.fNumThreshold350,
        RunInfo.fNumThreshold500,
        RunInfo.fNumThreshold750,
        RunInfo.fNumThreshold1000,
        
        RunInfo.fEffectiveOn,
        RunInfo.fTNGDust,
        RunInfo.fThresholdMinSet,
        RunInfo.fThresholdMedian,
        RunInfo.fZenithDistanceMean,

        RunInfo.fSourceKey

    FROM
        RunInfo
    WHERE
        fRunTypeKey=1
    AND
        -- Only Runs with consistent threshold setting
        fThresholdMedian = fThresholdMinSet
    AND
        -- Only runs with a non zero artificial trigger rate
        fNumThreshold750 > 0
    AND
        -- Only runs with a non zero zenith distance
        fZenithDistanceMean > 0
    AND
        RunInfo.fNight BETWEEN 20120101 AND 20200101 
)   

SELECT
    * 
FROM
    RunParams
WHERE
    fRunDuration BETWEEN 298 AND 301
OR
    fRunDuration BETWEEN 198 AND 201