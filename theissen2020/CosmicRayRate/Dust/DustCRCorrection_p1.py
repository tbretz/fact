import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import gaussian_kde as gkde
from correctionModels import zenithModel, zenithCorrectionFactor
import DataFrameBinning as dfb
import setFigureConfig as figConfig
from LibFunctions import weightedMean, findFWHM
from rootRoutines import dFrameFromRoot
import matplotlib.patheffects as path_effects

# path to root file with artificial trigger rates information
runsCR = dFrameFromRoot(
    "../ArtificialTriggerRates.root",
    keys=["sql"], prints=False
)

# sources that are taken into account for this investigation are the five
# most observed ones
sources = {
    "Mrk421": 1,
    "Mrk501": 2,
    "1ES 2344+51.4": 3,
    "Crab": 5,
    "1ES 1959+650": 7
}
runsCR = runsCR[runsCR["fSourceKey"].isin(list(sources.values()))]

# minimum Quality Cuts
runsCR = runsCR[runsCR["fNight"] > 20140101]
runsCR = runsCR[runsCR["fEffectiveOn"] > 0.9]
runsCR = runsCR[runsCR["fRunDuration"] > 297]
runsCR = runsCR[runsCR["fR750Cor"] < 1000]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 950]

# calculation of addional quantities
runsCR["RAWTime"] = (runsCR["fRunStart"] + runsCR["fRunStop"])/2
runsCR["Time"] = pd.to_datetime(runsCR["RAWTime"], unit="s")
runsCR.sort_values("Time")

runsCR["RCR"] = 60*runsCR["fNumThreshold750"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

runsCR["RCR_err"] = 60*np.sqrt(runsCR["fNumThreshold750"]) / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

# load Zd correction
zdCorrRes = pd.read_hdf(
    "../ZenithCorrectionResults.h5", key="ZenithCorrectionResults"
)

runsCR["ZdCorrFactor"] = zenithCorrectionFactor(
    runsCR["fZenithDistanceMean"],
    zdCorrRes[zdCorrRes["key"] == "chi"]['mean'].values,
    zdCorrRes[zdCorrRes["key"] == "xi"]['mean'].values,
    zdCorrRes[zdCorrRes["key"] == "kappa"]['mean'].values,
    gamma=2.7
)

runsCR["RCR_corr"] = runsCR["RCR"]*runsCR["ZdCorrFactor"]
runsCR["RCR_err_corr"] = runsCR["RCR_err"]*runsCR["ZdCorrFactor"]

# load TNGfromSensorCalib
tngDustAv = pd.read_hdf(
    "./SensorCorrelation/TNGDustAv.h5", key="TNGDustAv"
)
runsCR = runsCR.merge(
    tngDustAv, how="left", left_on="fNight", right_on="fNight"
)
runsCR = runsCR[runsCR["TNG_av"] == 1]
tngDustAv = tngDustAv[tngDustAv["TNG_av"] == 1]
runsCR = runsCR[runsCR["fTNGDust"] > 0]

runsCR["TNG_log"] = np.log10(runsCR["fTNGDust"])
runsCR["TNG_mean_log"] = np.log10(runsCR["TNG_mean"])
runsCR = runsCR[runsCR["TNG_mean"] > 1e-2]
runsCR = runsCR[runsCR["TNG_mean"] < 1e3]
runsCR = runsCR[runsCR["TNG_std"] > 1e-4]

tngDustAv["TNG_mean_log"] = np.log10(tngDustAv["TNG_mean"])
tngDustAv = tngDustAv[tngDustAv["TNG_mean"] > 1e-2]
tngDustAv = tngDustAv[tngDustAv["TNG_mean"] < 1e3]
tngDustAv = tngDustAv[tngDustAv["TNG_std"] > 1e-4]

binEdges = dfb.addLinearBinning(
    runsCR, "TNG_mean_log", "TNGBin", 18, minVal=-1., maxVal=1.8
)[1]
dfb.addLinearBinning(
    tngDustAv, "TNG_mean_log", "TNGBin", 18, minVal=-1., maxVal=1.8
)[1]

tngBinned = pd.DataFrame()
tngBinnedGroup = tngDustAv[tngDustAv["TNGBin"] != -1].groupby("TNGBin")
tngBinned["NDust"] = tngBinnedGroup["TNGBin"].apply(
    lambda group: len(group)
)
wMeanTNG = tngBinnedGroup.apply(
    lambda group: weightedMean(group.TNG_mean, group.TNG_std)
).apply(pd.Series, index=["TNG_wmean", "TNG_wstd"])
tngBinned = tngBinned.merge(
    wMeanTNG, how="outer", left_on="TNGBin", right_on="TNGBin"
)

tngBinnedCR = pd.DataFrame()
tngBinnedCRGroup = runsCR[runsCR["TNGBin"] != -1].groupby("TNGBin")
tngBinnedCR["NRuns"] = tngBinnedCRGroup["TNGBin"].apply(
    lambda group: len(group)
)
tngBinnedCR = tngBinnedCR.merge(
    tngBinned, how="outer", left_on="TNGBin", right_on="TNGBin"
)
tngBinnedCR = tngBinnedCR.reset_index()

tngBinnedRates = tngBinnedCRGroup["RCR_corr"]
tngBinnedCR["RCR_mean"] = tngBinnedRates.apply(lambda r: np.mean(r))
tngBinnedCR["RCR_std"] = tngBinnedRates.apply(lambda r: np.std(r, ddof=1))
tngBinnedCR["RCR_meanErr"] = tngBinnedRates.apply(
    lambda r: np.std(r, ddof=1)/np.sqrt(len(r))
)

tngBinnedCR["minDust"] = binEdges[:-1]
tngBinnedCR["maxDust"] = binEdges[1:]

print(tngBinnedCR)

KDEs = []
KDEs = tngBinnedRates.apply(lambda r: gkde(r))
KDEx = np.linspace(0, 400, num=1000)
KDEy = []
for i in range(len(KDEs)):
    KDEy.append(KDEs[i](KDEx))

tngBinnedCR["KDE_max"] = np.array(
    [KDEx[np.argmax(kde_y)] for kde_y in KDEy]
)
tngBinnedCR["KDE_fwhm"] = np.array(
    [findFWHM(KDEx, kde_y)[0] for kde_y in KDEy]
)
tngBinnedCR["KDE_std"] = tngBinnedCR["KDE_fwhm"]/(2*np.sqrt(2*np.log(2)))


calculated = True
densityInLog = True
if calculated:
    if densityInLog:
        density_kde = np.load("./densityInfoLog.npy")
    else:
        density_kde = np.load("./densityInfo.npy")
else:
    if densityInLog:
        density = np.vstack([
            np.log10(runsCR["fTNGDust"]),
            runsCR["RCR"]
        ])
    else:
        density = np.vstack([
            runsCR["fTNGDust"],
            runsCR["RCR"]
        ])

    density_kde = gkde(density)(density)
    if densityInLog:
        np.save("./densityInfoLog.npy", density_kde)
    else:
        np.save("./densityInfo.npy", density_kde)


figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.set_facecolor((0.7, 0.7, 0.7))
ax.scatter(
    runsCR["fTNGDust"], runsCR["RCR_corr"], s=2,
    c=density_kde, edgecolor='', zorder=-5, alpha=0.5,
)
eb = ax.errorbar(
    tngBinnedCR["TNG_wmean"], tngBinnedCR["KDE_max"],
    yerr=tngBinnedCR["KDE_std"], xerr=tngBinnedCR["TNG_wstd"],
    fmt=".", markeredgecolor="k", markeredgewidth=1., marker=".", lw=2,
    markersize=11, color="tab:red", ecolor="tab:red", capsize=3, zorder=5,
    label="KDE maxima in\n"+"corresponding\n"+r"$c_{\mathrm{dust}}$"+" bins"
)

ax.set_xlim(10**(-1.), 10**2)
ax.set_ylim(0, 350)
ax.set_xscale("log")
ax.set_xlabel("dust concentration "+r"$c_{\mathrm{dust}}$"+" in µg/m³")
ax.set_ylabel(r"$R_{CR}$ in events/min")
ax.grid(axis="both", color="k", zorder=10, alpha=0.5)
handels, labels = ax.get_legend_handles_labels()
legend = ax.legend([handels[1]], [labels[1]], framealpha=0.7,
                   loc="lower left")
frame = legend.get_frame()
frame.set_facecolor((0.7, 0.7, 0.7))
frame.set_edgecolor("white")
fig.tight_layout()
fig.savefig("./DustScatterKDE.png", dpi=300)
fig.savefig("./DustScatterKDE.pdf")

tngBinnedCR.to_hdf("./CRDustBinned.h5", "CRDustBinned")
