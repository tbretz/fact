import numpy as np
import pandas as pd

"""
    excess(s, b, alpha=0.2)

    Description:
        Calculating the number of excess events

    Parameters:
        s: int
            Number of signal events
        b: int
            Number of background events
        alpha: float [0,1]
            weight to the individual background wobble positions

    Returns:
        e: float
            Number of excess events
"""


def excess(s, b, alpha=0.2):
    return s - alpha*b


"""
    relExcErr(s, b, alpha=0.2)

    Description:
        Calculating the relative error on excess events

    Parameters:number of
        s: int
            Number of signal events
        b: int
            Number of background events
        alpha: float [0,1]
            weight to the individual background wobble positions

    Returns:
        sExc/Exc: float
            Relative error on excess events
"""


def relExcErr(s, b, alpha=0.2):
    return np.sqrt(s + alpha**2*b) / (s - alpha*b)


"""
    excessBinPoint(binId, Runs, binsCol, xCol, asRate=True)

    Description:
        Calculation of the point (<x>, Exc, <x>_err, Exc_err) in an
        Excess-X diagram for bin with binId

    Parameters:
        binId: int
            Id of the bin of interest
        Runs: pandas Dataframe
            Dataframe with run selection containing at least:
                - Binning information
                - The parameter of interest x
                - Evaluated Number of Signal and Background Events
                  fNumSigEvts and fNumBgEvts
                - fRunDuration and fEffectiveOn if asRate=True
        binsCol: str
            Title of the column containing binning information
        xCol: str
            Title of the column containing parameter of interest
        asRate: bool
            Calculate the excess rate from timing information

    Returns:
        xMean: float
            Position of the point inside the bin, taking into account
            the distribution inside the bin
        xMeanErr: float
            Uncertainty on xMean
        exc: float
            Obtained excess for the bin as excess events per hour if asRate
            is True
        excErr:
            Uncertainty on ExcErr

"""


def excessBinPoint(binId, Runs, binsCol, xCol, asRate=True):
    dF = Runs[Runs[binsCol] == binId]

    # Calculate xMean and xMeanErr
    xMean = np.mean(dF[xCol])
    xMin = np.min(dF[xCol])
    xMax = np.max(dF[xCol])
    if xMin == xMax:
        xMeanErr = 1./np.sqrt(12)
    else:
        xMeanErr = (xMax - xMin)/np.sqrt(12)

    # Calculate Excess
    sigSum = np.sum(dF["fNumSigEvts"])
    bgSum = np.sum(dF["fNumBgEvts"])
    rExcErr = relExcErr(sigSum, bgSum)
    exc = excess(sigSum, bgSum)

    excErr = exc*rExcErr
    if asRate:
        durationSum = np.sum(dF["fRunDuration"]*dF["fEffectiveOn"])
        exc = 3600*exc/durationSum
        excErr = 3600*excErr/durationSum
        bgSum = 3600*bgSum/durationSum

    return xMean, xMeanErr, exc, excErr


def excessAllBins(Runs, NumBins, binsCol, xCol, asRate=True):

    xMeans = []
    xMeanErrs = []
    excs = []
    excErrs = []
    for i in range(NumBins-1):
        xMean, xMeanErr, exc, excErr = excessBinPoint(
            i, Runs, binsCol, xCol, asRate)
        xMeans.append(xMean)
        xMeanErrs.append(xMeanErr)
        excs.append(exc)
        excErrs.append(excErr)
    xMeans = np.array(xMeans)
    xMeanErrs = np.array(xMeanErrs)
    excs = np.array(excs)
    excErrs = np.array(excErrs)

    return [xMeans, xMeanErrs, excs, excErrs]


def excessResultsToLog(results):
    xMeansLog = np.log10(results[0])
    xMeanErrsLog = results[1]/(results[0]*np.log(10))
    excsLog = np.log10(results[2])
    excErrsLog = results[3]/(results[2]*np.log(10))

    return [xMeansLog, xMeanErrsLog, excsLog, excErrsLog]


def constThenLine(x, p0, p1, p2):
    x = np.array(x)
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p2:
            y[i] = p0*x[i]+p1
        else:
            y[i] = p0*p2+p1
    return y


def constThenLineFixedSlope(x, p0, p1):
    a = -1.49
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p1:
            y[i] = a*x[i]+p0
        else:
            y[i] = a*p1+p0
    return y


def getTimeBinsFromPeriods(periods):
    timeBins = []
    timeBins.append(periods["From"][0])
    for i in range(len(periods)):
        timeBins.append(periods["To"][i])
    timeBins = np.array(timeBins)
    return timeBins


def weightedMean(y, stat):
    w = 1/stat**2
    s = sum(w*y)
    wsum = sum(w)
    mw = s/wsum
    mw_stat = np.sqrt(1./wsum)

    return mw, mw_stat


def normExcess(Runs, periods):
    Runs = Runs[Runs["fThresholdMinSet"] > 350]
    excs = []
    excErrs = []
    durations = []
    for i in range(len(periods)):
        dF1 = Runs[Runs["fNight"] >= periods["From"][i]]
        dF = dF1[dF1["fNight"] <= periods["To"][i]]

        sigSum = np.sum(dF["fNumSigEvts"])
        bgSum = np.sum(dF["fNumBgEvts"])
        durationSum = np.sum(dF["fRunDuration"]*dF["fEffectiveOn"])
        rExcErr = relExcErr(sigSum, bgSum)

        exc = 3600*excess(sigSum, bgSum)/durationSum
        excErr = 3600*exc*rExcErr/durationSum

        excs.append(exc)
        excErrs.append(excErr)
        durations.append(durationSum/3600)

    periods = periods.assign(
        excNorm=pd.Series(np.array(excs)).values)
    periods = periods.assign(
        excNormErr=pd.Series(np.array(excErrs)).values)
    periods = periods.assign(
        effObervationTime=pd.Series(np.array(durations)).values)
    excMean, excMeanErr = weightedMean(
        np.array(periods["excNorm"].values),
        np.array(periods["excNormErr"].values),
    )

    return periods, excMean, excMeanErr


def timeDistAllBins(Runs, NumBins, binsCol, xCol, periods=None, rel=True):
    if periods is not None:
        timeBins = getTimeBinsFromPeriods(periods)
        timeBinLabels = np.array(periods["PeriodLabel"].values)
    else:
        timeBins = [
            20110101, 20130721, 20140721, 20150721,
            20160721, 20170721, 20180721, 20190721
        ]
        timeBinLabels = np.array([
            "< 2013/07/21", "2013/2014", "2014/2015", "2015/2016",
            "2016/2017", "2017/2018", "2018/2019"
        ])

    timeDists = []
    for i in range(NumBins-1):
        dF = Runs[Runs[binsCol] == i]
        timeDist, binedges = np.histogram(dF["fNight"], bins=timeBins)
        if rel:
            timeDists.append(timeDist/np.sum(timeDist))
        else:
            timeDists.append(timeDist)

    timeDists = np.array(timeDists)
    checkSums = [np.sum(timeDists[:, i]) for i in range(len(timeBinLabels))]

    checkSums = np.array(
        [checkSums[i] > 0 for i in range(len(checkSums))], dtype=bool
    )

    return [timeBinLabels[checkSums], timeDists[:, checkSums]]
