import numpy as np
import matplotlib.pyplot as plt
import setFigureConfig as sfc

# use data from https://www.fact-project.org/dch/datataking_efficiency.php
# 2019 data has to be updated in file

# year = eg. 2011
# rest: datataking time in hours
year, data, callibration, otherRuns, twilight = np.genfromtxt(
    "factDataEfficiency.txt").reshape((9, 5)).T

sfc.fullWidth(aspectRatio=0.6, alpha=0.8)
fig, ax = plt.subplots()
ax.set_xlabel("year")
ax.set_ylabel("data taking time [hr]")
ax.set_xticks(year)
ax.set_xticklabels([str(int(i)) for i in year], rotation=0)


ax.bar(year, data + callibration + otherRuns + twilight,
       label="twilight", color="tab:grey")
ax.bar(year, data + callibration + otherRuns,
       label="other runs", color="tab:red")
ax.bar(year, data + callibration,
       label="callibration runs", color="tab:orange")
ax.bar(year, data, label="physics data", color="tab:green")

ax.legend(bbox_to_anchor=[1, 0.5])
fig.tight_layout()
# plt.show(fig)
fig.savefig("FACTDataTaking.pdf")
fig.savefig("FACTDataTaking.png", dpi=300)
