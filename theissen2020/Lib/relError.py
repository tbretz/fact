import numpy as np


def relError(s, b, alpha=0.2):
    return np.sqrt(s + alpha**2*b) / (s - alpha*b)
