import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import Normalize
import matplotlib.colorbar as cb

import setFigureConfig as sfc
from PlasmoidStatistics import pOfBeta
from coloring import c_time

sigma = 10

ap = pd.read_hdf("eom.h5", "allPlasmoids")

sfc.halfWidth(aspectRatio=0.8)
fig, ax = plt.subplots()

for j in np.linspace(-3, 0, 30):
    ax.axvline(x=j, color="lightgrey")
for j in np.linspace(0, 3, 30):
    ax.axhline(y=j, color="lightgrey")
taumax = ap["tau"].max()
k = 10
for i, g in ap.groupby("index"):
    if i > 15:
        ax.scatter(
            np.log10(g["s"][::k]), pOfBeta(g["beta"][::k]) / np.sqrt(sigma),
            color=c_time()(g["tau"][::k]/taumax), zorder=5, #marker=".",
            s=0.5
        )

ax.axhline(y=0.77, c="tab:red", zorder=6)
ax.text(-2, 1.5, r"mean $p_\mathrm{max}$", color="tab:red")
ax.axhline(y=1.66, c="tab:red", zorder=6)
ax.text(-1.6, 0.85, "growth\nsuppression", color="tab:red")
# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=c_time(), norm=Normalize(vmin=0, vmax=2),
    orientation='vertical',
    spacing="proportional"
)
cb2.set_label(r"ct/L")
# ticklabels = np.array(["%.1f" % (i) for i in cb2.get_ticks()])
# ticklabels[::2] = ""
cb2.set_ticks(np.linspace(0, 2, 5))

ax.set_xlabel(r"$\log_{10} s$")
ax.set_ylabel(r"$p / \sqrt{\sigma}$")
ax.set_xlim(-3.05, -0.7)
ax.set_ylim(-0.03, 1.7)
fig.tight_layout()
fig.savefig("sp-diagram.pdf")
