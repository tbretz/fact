#!rootifysql
SELECT
    -- Identification
    EventsMC.FileID,
    EventsMC.EvtNumber,
    -- Zd
    EventsMC.Zd,
    EventsMC.Az,
    EventsMC.ParticleTheta,
    EventsMC.ParticlePhi,
FROM
    EventsMC
WHERE
    EventsMC.PartID=1 -- only photons
 