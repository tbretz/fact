import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import poisson
from scipy.integrate import quad


class Excess():

    def __init__(self, signal, background, alpha=0.2):
        # initialize object with number of signal event,
        # number of background events and ratio of signal/background time
        self.signal = signal
        self.background = background
        self.alpha = alpha

        self.excess_hist = None

    def classical_excess(self):
        # standard method for excess and error calculation
        excess = self.signal - self.alpha*self.background
        excess_err = np.sqrt(self.signal + self.alpha**2*self.background)

        return excess, excess_err

    @staticmethod
    def excess_pmf(k_s, k_b, alpha):

        def signal(s):
            return poisson.pmf(k_s, s)

        def background(b):
            return poisson.pmf(k_b, b)

        def excess(e): return quad(lambda s: signal(
            s)*background((s-e)/alpha), max([e, 0]), max([e, np.inf]))[0]

        return excess

    def sample_excess(self, resolution=1):
        # sample the excess pmf
        # all methods below this require that this was called
        self.resolution = resolution
        excess_hist_x = np.arange(
            self.classical_excess()[0]-(self.signal+self.background)/3-3,
            self.classical_excess()[0]+(self.signal+self.background)/3+5,
            self.alpha/resolution)
        excess_hist_y = np.array([
            self.excess_pmf(
                self.signal, self.background, self.alpha
            )(i) for i in excess_hist_x
        ])
        self.excess_hist = np.array(
            [i for i in zip(excess_hist_x, excess_hist_y)])
        return self.excess_hist

    def cl_band(self, p):
        # calculate the percentile of all values specified in (list) p
        def calc_cl_band(p):
            try:
                if p > 1:
                    print('Error')
                    return 0

                seed = np.argmax(self.excess_hist[:, 1])
                bounds = [0, len(self.excess_hist[:, 1])-1]
                confidence = self.excess_hist[:, 1][seed]
                band = [seed]
                sum_y = self.resolution  # sum(self.excess_hist[:,1])

                while confidence <= p:
                    right = max(band)+1
                    left = min(band)-1

                    if min(band) == bounds[0]:
                        confidence += self.excess_hist[right+1, 1]/sum_y
                        band.append(right+1)
                        continue
                    if max(band) == bounds[1]:
                        confidence += self.excess_hist[left, 1]/sum_y
                        band.append(left)
                        continue

                    if self.excess_hist[right, 1] > self.excess_hist[left, 1]:
                        confidence += self.excess_hist[right, 1]/sum_y
                        band.append(right)
                    else:
                        confidence += self.excess_hist[left, 1]/sum_y
                        band.append(left)

                resultList = [
                    self.excess_hist[min(band), 0],
                    self.excess_hist[max(band), 0]
                ]

                return resultList
            except TypeError:
                print('Excess not sampled!')

        results = []

        try:
            for i in p:
                results.append(calc_cl_band(i))
        except TypeError:
            results = calc_cl_band(p)

        return results

    def mean_std(self):
        # sqrt of variance estimates 1sigma interval
        # better than result of classical excess for low signal/background
        # but still WRONG!
        # use with caution

        try:
            mean = sum(
                self.excess_hist[:, 0]*self.excess_hist[:, 1]) /\
                sum(self.excess_hist[:, 1])
            var = sum(
                self.excess_hist[:, 1]*(self.excess_hist[:, 0]-mean)**2) /\
                sum(self.excess_hist[:, 1])
            return mean, np.sqrt(var)
        except TypeError:
            print('Excess not sampled!')

    def plot(self, error_kind=None, pdf=None, title=True):
        # plot the excess dist
        try:
            plt.plot(self.excess_hist[:, 0], self.excess_hist[:, 1])
        except TypeError:
            print('Excess not sampled!')
        plt.axvline(self.classical_excess()[
                    0], color='k', linestyle='--', label='Measured Excess')
        plt.axvline(self.mean_std()[0], color='k',
                    linestyle='-.', label='Mean of Distribution')
        if error_kind == 'asym':
            first_cl, second_cl = self.cl_band([.68, .90])
            print(self.signal, self.background, first_cl[0], first_cl[1])
            plt.axvspan(first_cl[0], first_cl[1],
                        color='g', alpha=0.4, label='68% CL')
            plt.axvspan(second_cl[0], second_cl[1],
                        color='g', alpha=0.2, label='90% CL')
        elif error_kind == 'sym':
            cexcess = self.classical_excess()[0]
            sstd = self.mean_std()[1]
            plt.axvspan(cexcess-sstd, cexcess+sstd,
                        color='y', alpha=0.4, label='68% CL')
            plt.axvspan(cexcess-2*sstd, cexcess+2*sstd,
                        color='y', alpha=0.2, label='90% CL')
        elif error_kind == 'classical':
            cexcess, cstd = self.classical_excess()
            plt.axvspan(cexcess-cstd, cexcess+cstd,
                        color='r', alpha=0.4, label='68% CL')
            plt.axvspan(cexcess-2*cstd, cexcess+2*cstd,
                        color='r', alpha=0.2, label='90% CL')
        plt.xlabel('Excess')
        plt.ylabel('Exclusion PDF')
        plt.legend()
        if title:
            plt.title('Signal Events = %i & Background Events = %i' %
                      (self.signal, self.background))
        plt.tight_layout()
        if pdf is not None:
            pdf.savefig()
            plt.close()

    def compare_plot(self, pdf):
        # plog excess hists with all kinds of error estimation
        plt.figure(figsize=(5, 10))

        plt.suptitle('Signal Events = %i & Background Events = %i' %
                     (self.signal, self.background))

        plt.subplot(311)
        plt.title('Improved Errors')
        self.plot(error_kind='asym', title=False)

        plt.subplot(312)
        plt.title('Gaussian Errors')
        self.plot(error_kind='sym', title=False)

        plt.subplot(313)
        plt.title('Classical Errors')
        self.plot(error_kind='classical', title=False)

        plt.subplots_adjust(top=0.94)
        pdf.savefig()
        plt.close()

    def compile_information(self):
        # return classical excess, symmetric uncertainty and
        # 68% confidence interval
        mean, std = self.mean_std()
        resultList = [
            elf.classical_excess()[0], mean, std,
            self.cl_band(0.68), self.cl_band(0.95)
        ]
        return resultList
