import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import setFigureConfig as figConfig

runsCR = pd.read_hdf("../MergedExcessInfos/CrabExcess.h5", "CrabExcess")

runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 450]

slabels = np.array(["2012/2013", "2013/2014", "2014/2015", "2019/2020"])
xForPlot = np.array([1, 2, 3, 4])
over30 = None
over50 = None

for zdmin in [30, 40, 50, 60]:

    print("Total observation time above "+str(int(zdmin))+"deg in hours:")

    highZd = runsCR[runsCR["fZenithDistanceMean"] > zdmin]

    highZdS1 = highZd[highZd["fNight"] > 20110723]
    highZdS1 = highZdS1[highZdS1["fNight"] < 20120723]
    tS1 = np.sum(highZdS1["fEffectiveTime_h"])

    highZdS2 = highZd[highZd["fNight"] > 20120723]
    highZdS2 = highZdS2[highZdS2["fNight"] < 20130723]
    tS2 = np.sum(highZdS2["fEffectiveTime_h"])

    highZdS3 = highZd[highZd["fNight"] > 20130723]
    highZdS3 = highZdS3[highZdS3["fNight"] < 20140723]
    tS3 = np.sum(highZdS3["fEffectiveTime_h"])

    highZdS4 = highZd[highZd["fNight"] > 20140723]
    highZdS4 = highZdS4[highZdS4["fNight"] < 20150723]
    tS4 = np.sum(highZdS4["fEffectiveTime_h"])

    highZdS5 = highZd[highZd["fNight"] > 20150723]
    highZdS5 = highZdS5[highZdS5["fNight"] < 20160723]
    tS5 = np.sum(highZdS5["fEffectiveTime_h"])

    highZdS6 = highZd[highZd["fNight"] > 20160723]
    highZdS6 = highZdS6[highZdS6["fNight"] < 20170723]
    tS6 = np.sum(highZdS6["fEffectiveTime_h"])

    highZdS7 = highZd[highZd["fNight"] > 20170723]
    highZdS7 = highZdS7[highZdS7["fNight"] < 20180723]
    tS7 = np.sum(highZdS7["fEffectiveTime_h"])

    highZdS8 = highZd[highZd["fNight"] > 20180723]
    highZdS8 = highZdS8[highZdS8["fNight"] < 20190723]
    tS8 = np.sum(highZdS8["fEffectiveTime_h"])

    highZdS9 = highZd[highZd["fNight"] > 20190723]
    highZdS9 = highZdS9[highZdS9["fNight"] < 20200723]
    tS9 = np.sum(highZdS9["fEffectiveTime_h"])

    print("\t2011/2012: "+str(round(tS1, 2)))
    print("\t2012/2013: "+str(round(tS2, 2)))
    print("\t2013/2014: "+str(round(tS3, 2)))
    print("\t2014/2015: "+str(round(tS4, 2)))
    print("\t2015/2016: "+str(round(tS5, 2)))
    print("\t2016/2017: "+str(round(tS6, 2)))
    print("\t2017/2018: "+str(round(tS7, 2)))
    print("\t2018/2019: "+str(round(tS8, 2)))
    print("\t2019/2020: "+str(round(tS9, 2)))

    if zdmin == 30:
        over30 = np.array([tS2, tS3, tS4, tS9])
    if zdmin == 40:
        over40 = np.array([tS2, tS3, tS4, tS9])
    if zdmin == 50:
        over50 = np.array([tS2, tS3, tS4, tS9])
    if zdmin == 60:
        over60 = np.array([tS2, tS3, tS4, tS9])

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.bar(
    xForPlot, over30, width=0.9, color="tab:blue",
    zorder=1.1, label=r"$\theta > 30^{\circ}$"
)
ax.bar(
    xForPlot, over40, width=0.9, color="tab:green",
    zorder=1.2, label=r"$\theta > 40^{\circ}$"
)
ax.bar(
    xForPlot, over50, width=0.9, color="tab:purple",
    zorder=1.3, label=r"$\theta > 50^{\circ}$"
)
ax.bar(
    xForPlot, over60, width=0.9, color="tab:orange",
    zorder=1.3, label=r"$\theta > 60^{\circ}$"
)
ax.set_xlim(0.5, 4.5)
ax.legend()
ax.set_xticks(xForPlot)
ax.set_xticklabels(slabels)
ax.set_ylabel("effective observation\ntime in h")
ax.grid(axis="y", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("HighZenith_ObsTime.pdf")
fig.savefig("HighZenith_ObsTime.png", dpi=300)
