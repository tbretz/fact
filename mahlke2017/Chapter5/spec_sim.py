#!/usr/bin/python
# -*- coding: UTF-8 -*-
from astroML.time_series import lomb_scargle
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from peripylib import figures, query_fact
from scipy.optimize import curve_fit
from stingray import Lightcurve
from stingray.simulator import simulator
import sys


def power_law(x, A, exponent, c):
    return A * x**exponent + c

# The different sources have different spectral windows

for source in ['Mrk421', 'Mrk501']:

    # Get latest FACT data, apply cuts and corrections
    _, nightly = query_fact.query_fact(source)

    t = nightly.mjd
    evts = nightly.evts
    evts_err = nightly.evts_err

    # ----------------
    # Simulate light-curves and calculte LS periodogram

    N = int(np.floor(t.max()) - np.floor(t.min()))    # number of datapoints in simulated lc is the number of nights between the first
                                                    # and the last night of observation. we later exclude the nights when FACT did not
                                                    # observe
    evts_mean = np.mean(evts)    # mean of simulated lc
    evts_rms = np.sqrt(1/N * sum(evts**2)) / evts_mean    # fractional RMS
    red_noise = 100    # to simulate red noise leakage
    num_lcs = 1000 # Number of light-curves to simulate

    sim = simulator.Simulator(N=N, mean=evts_mean, rms=evts_rms, dt=1, red_noise=red_noise) # light curves have time steps of 1 day

    print('\nSimulating {:.0f} datapoints with a mean of {:.2f}Hz, fractional RMS of {:.2f} for {:s}'.format(N, evts_mean, evts_rms, source))


    fitted_spec_indices = []    # the fitted indices of the simulated LS periodograms
                                # these will be compared to the input indices later
    fitted_spec_indices_equidistant = []
    even_errs = []
    uneven_errs = []
    simulated_range = np.arange(0.5, 1.5, 0.05)
    if True:
        for spectral_index in simulated_range:
            print(spectral_index)
            ls_powers = [] # List to store the calculated Lomb-Scargle periodograms in, to
                       # later compute statistics
            ls_powers_equidistant_times = []
            # Simulation of lcs
            for i in range(num_lcs):

                # Short status update
                sys.stdout.write('\r')
                sys.stdout.write('{:d}/{:.0f}'.format(i+1, num_lcs))
                sys.stdout.flush()

                # Simulate the light-curve
                lc = sim.simulate(spectral_index)

                # Set lightcurve times to FACT timestamps to get the same spectral window
                lc.time = lc.time - lc.time[0] + int(np.floor(t.min()))  # shift to start of FACT observations in MJD

                # To estimate the influence of the spectral window, calculate
                # the LSPs of the simulated LCs with equidistant timesteps
                equidistant_times = lc.time
                equidistant_counts = lc.counts

                # Set up period grid the LS periodogram is evaluated on
                sorted_time = sorted(lc.time)
                pmin = np.min(np.diff(sorted_time))*2  # in days
                pmax = int(sorted_time[-1]-sorted_time[0]) # N days
                N_periods = len(lc.time)
                period = 10 ** np.linspace(np.log10(pmin), np.log10(pmax), N_periods)
                omega = 2 * np.pi / period

                # Equidistant
                power = lomb_scargle(equidistant_times, lc.counts, 1, omega, generalized=True)
                ls_powers_equidistant_times.append(power)


                # Remove data points at nights when FACT did not observe
                lc.counts = lc.counts[np.isin(lc.time, np.floor(t))]  # exclude observations at nights when FACT did not observe
                lc.time = lc.time[np.isin(lc.time, np.floor(t))]  # exclude observations at nights when FACT did not observe

                # Compute Lomb-Scarlge periodogram of simulated LC with FACT timestamps, no error given
                power = lomb_scargle(lc.time, lc.counts, 1, omega, generalized=True)

                # Append to ls_powers list for statistics
                ls_powers.append(power)


            print('\nSimulations complete \n')

            # Fit mean LS Periodogram and store fit index
            mean_ls_power = np.mean(ls_powers, axis=0)
            var_ls_power = np.var(ls_powers, axis=0)

            # Fit power law to spectrum
            popt, pcov = curve_fit(power_law, period, mean_ls_power, sigma=abs(mean_ls_power-var_ls_power))
            fitted_spec_indices.append(popt[1])
            uneven_errs.append(np.sqrt(np.diag(pcov))[1])

            # EQUIDISTANT
            # Fit mean LS Periodogram and store fit index
            mean_ls_power = np.mean(ls_powers_equidistant_times, axis=0)
            var_ls_power = np.var(ls_powers_equidistant_times, axis=0)

            # Fit power law to spectrum
            popt, pcov = curve_fit(power_law, period, mean_ls_power, sigma=abs(mean_ls_power-var_ls_power))
            fitted_spec_indices_equidistant.append(popt[1])
            even_errs.append(np.sqrt(np.diag(pcov))[1])


    fig, ax = figures.newfig()

    ax.errorbar(simulated_range, fitted_spec_indices, yerr=uneven_errs, label='FACT Uneven Sampling', ls='--', marker='.', color=figures.colors(0))
    ax.errorbar(simulated_range, fitted_spec_indices_equidistant, yerr=even_errs,label='Even Sampling', ls='--', marker='.', color=figures.colors(3))

    ax.set(xlabel='Index of Simulated LC', ylabel='Index of LS Periodogram', xlim=(0, 2), ylim=(0, 2))
    ax.legend(frameon=True)
    ax.plot([0, 2], [0, 2], '--', color='gray', alpha=0.7, zorder=-100)

    figures.savefig('spec_index_comparison_%s' % source, pgf=True)