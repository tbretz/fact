import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from matplotlib.backends.backend_pdf import PdfPages
import argparse
import os
import datetime
from Radiation_simps import Ny_fromFile, t_fromFile
from RadiationForPlasmoid import timing, doppler_BB, doppler_p, \
    chunkedIntegral, lightcurve, plotLC
from readYAML import loadConfiguration


parser = argparse.ArgumentParser(
    prog="calcTotalLightCurve",
    description='calculate radiation and create plots'
)

parser.add_argument(
    "-c", "--configFile",
    help="Path to input config file",
    type=str
)
parser.add_argument(
    "-l", "--lowerBound",
    help="lower bound of the light curve",
    type=float
)
parser.add_argument(
    "-u", "--upperBound",
    help="upper bound of the light curve",
    type=float
)
parser.add_argument(
    "--dt",
    help="time step of final light curve",
    type=float
)
parser.add_argument(
    "-p", "--plot",
    help="False for no plot, True for calcuation and plot" +
         "plotOnly for only a plot",
    type=str
)


args = parser.parse_args()
configs = loadConfiguration(args.configFile)


# parse configs
path = os.path.join(configs["MCPath"], configs["MCFile"])
allPlasmoids = pd.read_hdf(path, key=str(configs["MCKey"]))

# convert list of plasmoid ids
if isinstance(configs["PlasmoidID"], (int, float)):
    numbers = np.array([configs["PlasmoidID"]], dtype=int)
elif isinstance(configs["PlasmoidID"], list):
    numbers = np.array(configs["PlasmoidID"], dtype=int)
if isinstance(configs["PlasmoidID"], str):
    if configs["PlasmoidID"] == "all":
        numbers = np.arange(np.max(allPlasmoids['index'])) + 1

# extract constants
L_Layer = float(configs["Layer"]["L_Layer"])
GammaJet = float(configs["AGN"]["GammaJet"])
R_BLR = float(configs["BLR"]["R_BLR"])
z_diss = float(configs["Layer"]["z_diss"])


# function to sum up several lightcurves in lists
# ts, Ls
# dt = final time spacing
def sumLCs(ts, Ls, dt):
    # maximum time (has to be done for each array due to different lengths)
    tmax = np.max([np.max(t) for t in ts])
    tmin = np.min([np.min(t) for t in ts])
    # time grid for final light curve
    newT = np.arange(tmin, tmax, dt)

    # interpolation with 0 everywhere else
    def L_interpol(t_eval, t, L):
        t_min = min(t)
        t_max = max(t)
        interpolation = interp1d(t, L)
        result = np.zeros(t_eval.shape)
        result[(t_eval > t_min) & (t_eval < t_max)] = interpolation(
            t_eval[(t_eval > t_min) & (t_eval < t_max)]
        )
        return result

    return newT, np.sum(
        [L_interpol(newT, ts[i], Ls[i]) for i in range(len(ts))],
        axis=0
    )


if args.plot == "True" or args.plot == "False":
    t_fin = None
    LC_fin = None
    print("started calculating: ", datetime.datetime.now())
    # loop over all plasmoids
    for i in range(len(numbers)):
        PlasmoidID = numbers[i]
        if len(allPlasmoids[allPlasmoids["index"] == PlasmoidID]) > 1:
            print("\ncalculate plasmoid ", PlasmoidID)

            # select Plasmoid
            Plasmoid = allPlasmoids[allPlasmoids["index"] == PlasmoidID]

            # calculate the timing
            t0, dt0, t0_t1, t1_ini, t0_add, t0_fin, t0_fin_add, t0_tot =\
                timing(
                    Plasmoid,
                    dtmin=float(configs["grid"]["dtmin"]),
                    addTimeFrac=float(configs["grid"]["addTimeFrac"]),
                    addTime=float(configs["grid"]["addTime"])
                )

            # do the interpolation
            Nadd = len(t0_add)
            # interpolation of s, beta, r to t0-grid
            s0_inter = interp1d(t0_t1, Plasmoid["s"])(t0)
            beta1_inter = interp1d(t0_t1, Plasmoid["beta"])(t0)
            # append constant values for additional time steps
            s0_inter_tot = np.append(s0_inter, s0_inter[-1]*np.ones(Nadd))
            beta1_inter_tot = np.append(
                beta1_inter, beta1_inter[-1]*np.ones(Nadd)
            )

            # black body Doppler factors at t0 grid
            betaJet = np.sqrt(1. - 1./GammaJet**2)
            # black body Doppler factor
            deltaBB = doppler_BB(
                beta1=beta1_inter,
                theta1=float(configs["AGN"]["theta1"]),
                betaJet=betaJet,
                orientation=np.sign(z_diss-R_BLR)
            )
            deltaBB_tot = np.append(deltaBB, deltaBB[-1]*np.ones(Nadd))

            # plasmoid's Doppler factors at t0 grid
            deltaP = doppler_p(
                beta1=beta1_inter,
                theta1=float(configs["AGN"]["theta1"]),
                betaJet=betaJet,
                thetaObs=float(configs["AGN"]["thetaObs"])
            )
            deltaP_tot = np.append(deltaP, deltaP[-1]*np.ones(Nadd))

            # load radiation modelling from file
            save = os.path.join(
                configs["save"]["path"],
                (
                    configs["name"] + "_" +
                    configs["MCFile"].split(".")[0] +
                    "_" + str(PlasmoidID) + ".h5"
                )
            )
            t = t_fromFile(save)
            yE, Ny_all = Ny_fromFile(save)

            # conversion from number density to escaped spectrum/LC
            me_c3 = 24544.325267191878  # erg cm / s
            sigma_T = 6.65245854533e-25  # cm**2
            conv = np.pi/2 * me_c3/sigma_T * L_Layer * s0_inter_tot**2

            # [time, energy]
            Ny0_esc = conv[:, None] * Ny_all.values[:-1, :]  # 1/(erg s)

            # boost to observer's frame
            Ny2_esc = Ny0_esc[:, :] * deltaP_tot[:, None]**2
            yE2 = yE[None, :] * deltaP_tot[:, None]

            t2_t0 = chunkedIntegral(dt0 / deltaP_tot) + t1_ini / GammaJet
            # convert units to seconds
            c = 29979245800  # cm / s
            t2_t0 = L_Layer / c * t2_t0  # s

            lc = lightcurve(
                yE2, Ny2_esc, loweV=args.lowerBound, upeV=args.upperBound
            )

            # add lightcurve to sum
            if t_fin is None:
                t_fin = t2_t0
                LC_fin = lc
            else:
                newT, newLC = sumLCs([t_fin, t2_t0], [LC_fin, lc], dt=args.dt)
                t_fin = newT
                LC_fin = newLC
        else:
            print("\nskipped plasmoid ", PlasmoidID)

    print("finished calculating: ", datetime.datetime.now())

    np.save(
        configs["name"] + "_TotalLightCurve_%.1e-%.1e_dt%g.npy" %
        (args.lowerBound, args.upperBound, args.dt),
        [t_fin, LC_fin]
    )

if args.plot == "True" or args.plot == "plotOnly":
    if args.plot == "plotOnly":
        t_fin, LC_fin = np.load(
            configs["name"] + "_TotalLightCurve_%.1e-%.1e_dt%g.npy" %
            (args.lowerBound, args.upperBound, args.dt)
        )
    # new pdf file
    pdf = PdfPages(
        configs["name"] + "_TotalLightCurve_%.1e-%.1e_dt%g.pdf" %
        (args.lowerBound, args.upperBound, args.dt)
    )
    fig = plotLC(t_fin, LC_fin, args.lowerBound, args.upperBound)
    pdf.savefig(fig)
    pdf.close()
