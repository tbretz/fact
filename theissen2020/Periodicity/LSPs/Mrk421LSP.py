import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.timeseries import LombScargle
from scipy.optimize import curve_fit, minimize

import setFigureConfig as figConfig


significance = True

# Load Crab Lightcurve
lcMrk421 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk421From2013.h5",
    "Mrk421From2013"
)

if significance:
    sig = pd.read_hdf(
        "../TKSimulations/Mrk421Significance.h5",
        "Mrk421Significance"
    )
    threeSigma = sig["3sigma"]

t = lcMrk421["mjd"]
exc = lcMrk421["RExcCorr"]
excErr = lcMrk421["RExcErrCorr"]

lsw = LombScargle(t, exc, dy=excErr, center_data=False, fit_mean=False)

f = np.linspace(1e-3, 0.5, 10000)
p = lsw.power(f)

config = {}
config["alpha"] = 0.8
config["aspRatio"] = 1./2.3
config["small"] = 12
config["medium"] = 14
config["big"] = 16
config["width"] = 6.5
config["legendHandleLength"] = 1

figConfig.configure(config)

fig = plt.figure()
ax = fig.gca()

# ls
ax.plot(f, p, lw=2, color="k", label="LSP of Mrk 421", zorder=1.5)
ax.set_xlim(np.min(f), np.max(f))

if significance:
    sigLine = ax.plot(
        f, threeSigma, lw=2, color="tab:red",
        label="99.7% significance from TK", zorder=1.5
    )

# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)
ax.axvline(x=4/365.25, lw=1, color="tab:blue",
           ls="--", label="third alias", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)
ax.axvline(x=1/27.3, lw=1, color="tab:green",
           label="sidereal month", zorder=1.3)

ax.set_ylim(0, 0.6)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend(handletextpad=0.3)
fig.tight_layout()
fig.savefig("./Mrk421LSP.png", dpi=300)
fig.savefig("./Mrk421LSP.pdf")


def power_law(x, A, alpha):
    return A * x**(-alpha)


popt, pcov = curve_fit(power_law, f, p)
print(popt)
print(np.sqrt(pcov[1, 1]))

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.set_xlim(np.min(f), np.max(f))
ax.plot(
    f, p, ".", markersize=0.8, label="LSP of Mrk421 lightcurve",
    color="tab:blue"
)
ax.plot(
    f, power_law(f, popt[0], popt[1]),
    label="PSD slope fit: "+r"$-0.79\pm 0.14$",
    color="tab:red"
)
ax.set_xscale("log")
ax.set_yscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend()
fig.tight_layout()
fig.savefig("Mrk421SlopeFit.png", dpi=300)
fig.savefig("Mrk421SlopeFit.pdf")
