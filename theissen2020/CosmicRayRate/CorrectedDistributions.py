import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import gaussian_kde as gkde
from correctionModels import zenithCorrectionFactor, dustCorrectionFactor
import DataFrameBinning as dfb
import setFigureConfig as figConfig
from LibFunctions import weightedMean, findFWHM
from rootRoutines import dFrameFromRoot
import matplotlib.patheffects as path_effects
import ModifiedJulianDay as mjd

# path to root file with artificial trigger rates information
runsCR = dFrameFromRoot(
    "./ArtificialTriggerRates.root",
    keys=["sql"], prints=False
)

# sources that are taken into account for this investigation are the five
# most observed ones
sources = {
    "Mrk421": 1,
    "Mrk501": 2,
    "1ES 2344+51.4": 3,
    "Crab": 5,
    "1ES 1959+650": 7
}
runsCR = runsCR[runsCR["fSourceKey"].isin(list(sources.values()))]

# minimum Quality Cuts
runsCR = runsCR[runsCR["fEffectiveOn"] > 0.9]
runsCR = runsCR[runsCR["fR750Cor"] < 1000]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 950]

# calculate mjds
runsCR["mjd"] = mjd.fNight_to_mjd(runsCR["fNight"])

# reject data with hardware issues
badfNights = [
    # BIAS recalibration - after it problems with the trigger rate
    20170515, 20170516, 20170517, 20170518, 20170519, 20170520, 20170521,
    20170522, 20170523, 20170524, 20170525
]
badDates = [mjd.fNight_to_mjd(badNight) for badNight in badfNights]
badMjds = [
    # 56095, 56099, 56102
]
for badNight in badMjds:
    badDates.append(badNight)

runsCR = runsCR[~runsCR.mjd.isin(badDates)]

# calculation of addional quantities
runsCR["RAWTime"] = (runsCR["fRunStart"] + runsCR["fRunStop"])/2
runsCR["Time"] = pd.to_datetime(runsCR["RAWTime"], unit="s")
runsCR.sort_values("Time")

runsCR["RCR"] = 60*runsCR["fNumThreshold750"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

runsCR["RCR_err"] = 60*np.sqrt(runsCR["fNumThreshold750"]) / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

# load Zd correction
zdCorrRes = pd.read_hdf(
    "./ZenithCorrectionResults.h5", key="ZenithCorrectionResults"
)

runsCR["ZdCorrFactor"] = zenithCorrectionFactor(
    runsCR["fZenithDistanceMean"],
    zdCorrRes[zdCorrRes["key"] == "chi"]['mean'].values,
    zdCorrRes[zdCorrRes["key"] == "xi"]['mean'].values,
    zdCorrRes[zdCorrRes["key"] == "kappa"]['mean'].values,
    gamma=2.7
)

# load TNGfromSensorCalib
tngDustAv = pd.read_hdf(
    "./Dust/SensorCorrelation/TNGDustAv.h5", key="TNGDustAv"
)
tngDustAv = tngDustAv[~tngDustAv.mjd.isin(badDates)]
tngDustAv = tngDustAv.drop("mjd", axis=1)

runsCR = runsCR.merge(
    tngDustAv, how="left", left_on="fNight", right_on="fNight"
)
runsCR = runsCR[runsCR["fTNGDust"] >= 0]

# load Zd correction
dustCorrRes = pd.read_hdf(
    "./DustCorrectionResults.h5", key="DustCorrectionResults"
)
alpha = dustCorrRes[dustCorrRes["key"] == "alpha"]['mean'].values


runsCR["DustCorrFactor"] = dustCorrectionFactor(
    runsCR["fTNGDust"],
    alpha,
    gamma=2.7
)

runsCR["CorrFactor"] = runsCR["ZdCorrFactor"]*runsCR["DustCorrFactor"]
runsCR["RCR_corr"] = runsCR["RCR"]*runsCR["CorrFactor"]
runsCR["RCR_err_corr"] = runsCR["RCR_err"]*runsCR["CorrFactor"]

runsCR.to_hdf("./CorrectedCRRate.h5", "CorrectedCRRate")

runsCR = runsCR[runsCR["fRunDuration"] > 297]
runsCR = runsCR[runsCR["fNight"] > 20140101]

KDEb = gkde(runsCR["RCR"])
KDEa = gkde(runsCR["RCR_corr"])
KDEx = np.linspace(0, 400, num=1000)
KDEbY = KDEb(KDEx)
KDEaY = KDEa(KDEx)
meanB = KDEx[np.argmax(KDEbY)]
meanA = KDEx[np.argmax(KDEaY)]

runsCR["DeltaSq"] = (meanB-runsCR["RCR"])**2
runsCR["relDeltaSq"] = runsCR["DeltaSq"]/runsCR["RCR_err"]**2
runsCR["DeltaSq_corr"] = (meanA-runsCR["RCR_corr"])**2
runsCR["relDeltaSq_corr"] = runsCR["DeltaSq_corr"]/runsCR["RCR_err_corr"]**2

chi2NDFb = np.sum(runsCR["relDeltaSq"])/len(runsCR)
chi2NDFa = np.sum(runsCR["relDeltaSq_corr"])/len(runsCR)
print(chi2NDFb)
print(chi2NDFa)
print((chi2NDFb-chi2NDFa)/chi2NDFb)


"""
    plots
"""

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["RCR"], bins=np.linspace(0, 400, num=401),
    alpha=1, color="tab:purple",
    label="before correction", zorder=1+1/100
)
ax.axvline(
    x=meanB, color="k", lw=1, zorder=3+1./100,
    label=r"$R_{\mathrm{CR}}^{max}$"+" before: " +
    str(np.round(meanB, 1))+" evts/min"
)
ax.hist(
    runsCR["RCR_corr"], bins=np.linspace(0, 400, num=401), alpha=0.7,
    color="tab:blue", label="after correction", zorder=1+50./100
)
ax.axvline(
    x=meanA, color="k", lw=1, ls="--", alpha=0.7, zorder=3+50./100,
    label=r"$R_{\mathrm{CR}}^{max}$"+" after: " +
    str(np.round(meanA, 1))+" evts/min"
)
ax.set_ylabel("number of runs per bin")
ax.set_xlabel(r"$R_{CR}$ in events/min")
ax.legend()
handels, labels = ax.get_legend_handles_labels()
legend = ax.legend(
    [handels[2], handels[0], handels[3], handels[1]],
    [labels[2], labels[0], labels[3], labels[1]]
)
ax.set_ylim(0, 2200)
ax.set_xlim(50, 350)
ax.grid(axis="both", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./CorrectedCRDistribution.png", dpi=300)
fig.savefig("./CorrectedCRDistribution.pdf")


figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["CorrFactor"], bins=np.linspace(1, 3, num=200),
    alpha=1, color="tab:blue"
)
ax.set_ylabel("number of runs per bin")
ax.set_xlabel("total correction factor "+r"$c_{\mathrm{tot}}$")
ax.set_yscale("log")
ax.set_ylim(1, 30000)

fig.tight_layout()
fig.savefig("./CorrFactorDistribution.png", dpi=300)
fig.savefig("./CorrFactorDistribution.pdf")
