import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colorbar as cb
from matplotlib.colors import Normalize, LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
import setFigureConfig as sfc
from MC_FileIdForZdBin import FileIdForZdBin

'''
calculates 2D histograms (energy vs Zd, energy vs impact, impact vs Zd) for
-OriginalMC: originally produced
-MCGamma: triggered and processed
-efficiency of the two
needs reweighting Models from
-MonteCarlos/reweighting_energy/energy_Models_MCGamma.py
-MonteCarlos/reweighting_energy/energy_Models_OriginalMC.py
-MonteCarlos/reweighting_Zd/Zd_Models_OriginalMC.py
-MonteCarlos/reweighting_impact/impact_Models_OriginalMC.py
'''

Ebins = 64
IBins = 54
ZdBins = 60
si = -2.5

recalcOriginal = False
recalcMCGamma = False

if recalcOriginal:
    OriginalMCFile = "../MonteCarlos/data/OriginalMC_Energy_Impact_Zd.root"
    dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))

    # energy
    # calc log energy
    dp.calcLogs(["Energy"])

    EnergyModelIn = Model.fromFile(
        "../MonteCarlos/reweighting_energy/EnergyModelIn_OriginalMC.p"
    )
    EnergyModelOut = Model.fromFile(
        "../MonteCarlos/reweighting_energy/EnergyModelOut_OriginalMC_%g.p" %
        (si)
    )
    dp.updateModels(EnergyModelIn, EnergyModelOut)
    dp.addWeights("logEnergy", name="wE")
    print("finished energy weights")

    # Zd
    dp.data["Zd_deg"] = np.degrees(dp.data["Zd"])
    ZdModelIn = Model.fromFile(
        "../MonteCarlos/reweighting_Zd/ZdModelIn_OriginalMC.p"
    )

    # calculate weights of complete distribution
    factor = ZdModelIn.parameters[2] / ZdModelIn.parameters[1]
    dp.data["wZd"] = pd.cut(
        dp.data["Zd_deg"], bins=[0, 30, 60], labels=[factor, 1]
    )
    print("finished Zd weights")

    # in model
    dp.data["I_m"] = dp.data["Impact"]/100.

    ImpactModelIn = Model.fromFile(
        "../MonteCarlos/reweighting_impact/ImpactModelIn_OriginalMC.p"
    )

    factor = ImpactModelIn.parameters[1] / ImpactModelIn.parameters[3]
    dp.data["wI"] = pd.cut(
        dp.data["I_m"], bins=[0, 270, 540], labels=[1, factor]
    )
    print("finished impact weights")

    # 2D Histograms
    E_range = [2.3, 4.7]
    I_range = [0, 540]
    Zd_range = [0, 60]

    chunks = 100
    NEvents = len(dp.data.index)
    k_range = np.linspace(0, NEvents, chunks+1, dtype=int)
    OHEI = np.zeros([Ebins, IBins])
    OHEZd = np.zeros([Ebins, ZdBins])
    OHIZd = np.zeros([IBins, ZdBins])

    for k in range(chunks):

        dp.data["I_m"][k_range[k]:k_range[k+1]][
            dp.data["I_m"][k_range[k]:k_range[k+1]] == 270] = 269.9

        print(str(k) + " of " + str(chunks))
        OHEI_t, OHEI_Eedges, OHEI_Iedges = np.histogram2d(
            dp.data["logEnergy"][k_range[k]:k_range[k+1]],
            dp.data["I_m"][k_range[k]:k_range[k+1]],
            bins=[Ebins, IBins], range=[E_range, I_range],
            weights=(
                dp.data["wE"][k_range[k]:k_range[k+1]] *
                dp.data["wI"].astype(float)[k_range[k]:k_range[k+1]]
            )
        )
        OHEI += OHEI_t

        OHEZd_t, OHEZd_Eedges, OHEZd_Zdedges = np.histogram2d(
            dp.data["logEnergy"][k_range[k]:k_range[k+1]],
            dp.data["Zd_deg"][k_range[k]:k_range[k+1]],
            bins=[Ebins, ZdBins], range=[E_range, Zd_range],
            weights=(
                dp.data["wE"][k_range[k]:k_range[k+1]] *
                dp.data["wZd"].astype(float)[k_range[k]:k_range[k+1]]
            )
        )
        OHEZd += OHEZd_t

        OHIZd_t, OHIZd_Iedges, OHIZd_Zdedges = np.histogram2d(
            dp.data["I_m"][k_range[k]:k_range[k+1]],
            dp.data["Zd_deg"][k_range[k]:k_range[k+1]],
            bins=[IBins, ZdBins], range=[I_range, Zd_range],
            weights=dp.data["wE"][k_range[k]:k_range[k+1]]
        )
        OHIZd += OHIZd_t

    np.save(
        "OriginalMC_Histograms_si%g_nBins[%g,%g,%g].npy" %
        (si, Ebins, ZdBins, IBins),
        [
            OHEI, OHEI_Eedges, OHEI_Iedges,
            OHEZd, OHEZd_Eedges, OHEZd_Zdedges,
            OHIZd, OHIZd_Iedges, OHIZd_Zdedges
        ]
    )
else:
    load = np.load(
        "OriginalMC_Histograms_si%g_nBins[%g,%g,%g].npy" %
        (si, Ebins, ZdBins, IBins),
        allow_pickle=True
    )
    OHEI, OHEI_Eedges, OHEI_Iedges, \
        OHEZd, OHEZd_Eedges, OHEZd_Zdedges, \
        OHIZd, OHIZd_Iedges, OHIZd_Zdedges = load

if recalcMCGamma:
    MCGammaFile = "../MonteCarlos/data/MC_Gamma.root"
    dp = DataProcessor(data=dFrameFromRoot(MCGammaFile, keys=["Result"]))

    # energy
    # calc log energy
    dp.calcLogs(["Energy"])

    EnergyModelIn = Model.fromFile(
        "../MonteCarlos/reweighting_energy/EnergyModelIn_MCGamma.p"
    )
    EnergyModelOut = Model.fromFile(
        "../MonteCarlos/reweighting_energy/EnergyModelOut_MCGamma_%g.p" %
        (si)
    )
    dp.updateModels(EnergyModelIn, EnergyModelOut)
    dp.addWeights("logEnergy", name="wE")
    print("finished energy weights")

    # Zd
    # in model
    dp.data["I_m"] = dp.data["Impact"]/100.

    # 2D Histograms
    E_range = [2.3, 4.7]
    I_range = [0, 540]
    Zd_range = [0, 60]

    # chunks = 100
    # NEvents = len(dp.data.index)
    # k_range = np.linspace(0, NEvents, chunks+1, dtype=int)
    # HEI = np.zeros([Ebins, IBins])
    HEZd = np.zeros([Ebins, ZdBins])
    HIZd = np.zeros([IBins, ZdBins])

    HEI, HEI_Eedges, HEI_Iedges = np.histogram2d(
        dp.data["logEnergy"],
        dp.data["I_m"],
        bins=[Ebins, IBins], range=[E_range, I_range],
        weights=dp.data["wE"]
    )
    gb = dp.data.groupby("FileID")
    for zd in np.arange(60):
        print(zd)
        df = dp.data[dp.data["FileID"].isin(FileIdForZdBin()[zd])]

        HEZd_t, HEZd_Eedges, HEZd_Zdedges = np.histogram2d(
            df["logEnergy"],
            (zd + 0.5) * np.ones(len(df)),
            bins=[Ebins, ZdBins], range=[E_range, Zd_range],
            weights=df["wE"]
        )
        HEZd += HEZd_t
        HIZd_t, HIZd_Iedges, HIZd_Zdedges = np.histogram2d(
            df["I_m"],
            (zd + 0.5) * np.ones(len(df)),
            bins=[IBins, ZdBins], range=[I_range, Zd_range],
            weights=df["wE"]
        )
        HIZd += HIZd_t

    '''
    for k in range(chunks):
        print(str(k) + " of" + str(chunks))
        HEI_t, HEI_Eedges, HEI_Iedges = np.histogram2d(
            dp.data["logEnergy"][k_range[k]:k_range[k+1]],
            dp.data["I_m"][k_range[k]:k_range[k+1]],
            bins=[Ebins, IBins], range=[E_range, I_range],
            weights=dp.data["wE"][k_range[k]:k_range[k+1]]
        )
        HEI += HEI_t

        HEZd_t, HEZd_Eedges, HEZd_Zdedges = np.histogram2d(
            dp.data["logEnergy"][k_range[k]:k_range[k+1]],
            dp.data["Zd_deg"][k_range[k]:k_range[k+1]],
            bins=[Ebins, ZdBins], range=[E_range, Zd_range],
            weights=dp.data["wE"][k_range[k]:k_range[k+1]]
        )
        HEZd += HEZd_t

        # HIZd_t, HIZd_Iedges, HIZd_Zdedges = np.histogram2d(
        #     dp.data["I_m"][k_range[k]:k_range[k+1]],
        #     dp.data["Zd_deg"][k_range[k]:k_range[k+1]],
        #     bins=[IBins, ZdBins], range=[I_range, Zd_range],
        #     weights=dp.data["wE"][k_range[k]:k_range[k+1]]
        # )
        # HIZd += HIZd_t
    '''
    np.save(
        "MCGamma_Histograms_si%g_nBins[%g,%g,%g].npy" %
        (si, Ebins, ZdBins, IBins),
        [
            HEI, HEI_Eedges, HEI_Iedges,
            HEZd, HEZd_Eedges, HEZd_Zdedges,
            HIZd, HIZd_Iedges, HIZd_Zdedges
        ]
    )
else:
    load = np.load(
        "MCGamma_Histograms_si%g_nBins[%g,%g,%g].npy" %
        (si, Ebins, ZdBins, IBins),
        allow_pickle=True
    )

    HEI, HEI_Eedges, HEI_Iedges, \
        HEZd, HEZd_Eedges, HEZd_Zdedges, \
        HIZd, HIZd_Iedges, HIZd_Zdedges = load

# efficiencies
eff_HEI = HEI / OHEI
eff_HEZd = HEZd / OHEZd  #+ 1e-16
eff_HIZd = HIZd / OHIZd
# print(np.argwhere(np.isnan(HIZd)))

# plots

sfc.halfWidth(aspectRatio=0.9)
cmap = cm.get_cmap('viridis')
cmap.set_bad(color='darkgrey')
cmap.set_under(color='black')
cmap.set_over(color='orange')

'''
############ Energy Impact ################
'''


# plot 2D original histogram Energy Impact
fig, ax = plt.subplots()

X, Y = np.meshgrid(HEI_Eedges, HEI_Iedges)

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1e5)
norm = LogNorm(vmin=1, vmax=1e6)
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHEI.T == 0, OHEI.T),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r"Impact $ I \:[m]$")

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
cb2.set_label(r"d$N_{prod}$/d$\log_{10}E$d$I$")

plt.tight_layout()
fig.savefig("original_lgE_I.png", dpi=300)


# plot 2D triggered histogram Energy Impact
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1e4)
norm = LogNorm(vmin=1, vmax=np.max(HEI))
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHEI.T == 0, HEI.T + 1e-15),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r"Impact $ I \:[m]$")
ax.set_yticks(np.linspace(0, 540, 5))
# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"d$N_{trig}$/d$\log_{10}E$d$I$")

plt.tight_layout()
fig.savefig("triggered_lgE_I.png", dpi=300)

# plot 2D efficiency histogram Energy Impact
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1)
norm = LogNorm(vmin=1e-3, vmax=1)
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHEI.T == 0, eff_HEI.T+1e-16),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r"Impact $ I \:[m]$")
ax.set_yticks(np.linspace(0, 540, 5))
ax.axhline(y=270, c="r", lw=0.5)

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"efficiency")

plt.tight_layout()
fig.savefig("triggerEfficiency_lgE_I.png", dpi=300)



'''
############ Energy Zd ################
'''


# plot 2D original histogram Energy Zd
fig, ax = plt.subplots()

X, Y = np.meshgrid(HEZd_Eedges, HEZd_Zdedges)

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=np.max(OHEZd))
norm = LogNorm(vmin=1, vmax=np.max(OHEZd))
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHEZd.T == 0, OHEZd.T),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r" $ Zd \:[deg]$")

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
cb2.set_label(r"d$N_{prod}$/d$\log_{10}E$d$Zd$")

plt.tight_layout()
fig.savefig("original_lgE_Zd.png", dpi=300)

# plot 2D triggered histogram Energy Impact
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1e4)
norm = LogNorm(vmin=1, vmax=np.max(HEZd))
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHEZd.T == 0, HEZd.T + 1e-16),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r" $ Zd$")
ax.set_yticklabels([str(int(yt)) + r"°" for yt in ax.get_yticks()])
# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"d$N_{trig}$/d$\log_{10}E$d$Zd$")

plt.tight_layout()
fig.savefig("triggered_lgE_Zd.png", dpi=300)

# plot 2D eff histogram Energy Zd
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
norm = LogNorm(vmin=1e-4, vmax=1)
# norm = LogNorm(vmin=1e-5, vmax=1)
# norm = Normalize(vmin=1e-5, vmax=1)
pcm = ax.pcolormesh(
    X, Y,
    eff_HEZd.T + 1e-16,
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"$\log_{10}(\frac{E}{GeV})$")
ax.set_ylabel(r" $ Zd $")
ax.set_yticklabels([str(int(xt)) + r"°" for xt in ax.get_yticks()])
ax.axhline(y=30, c="r", lw=0.5)

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"efficiency")

plt.tight_layout()
fig.savefig("triggerEfficiency_lgE_Zd.png", dpi=300)



'''
############ Impact Zd ################
'''


# plot 2D original histogram Impact Zd
fig, ax = plt.subplots()

X, Y = np.meshgrid(HIZd_Iedges, HIZd_Zdedges)

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1e4)
norm = LogNorm(vmin=1, vmax=np.max(OHIZd))
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHIZd.T == 0, OHIZd.T),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"Impact $ I \:[m]$")
ax.set_ylabel(r"$ Zd \:[deg]$")

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
cb2.set_label(r"d$N_{prod}$/d$I$d$Zd$")
ax.axvline(x=270, c="r", lw=0.3)
ax.axhline(y=30, c="r", lw=0.3)
plt.tight_layout()
fig.savefig("original_I_Zd.png", dpi=300)

# plot 2D triggered histogram Energy Impact
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = Normalize(vmin=0, vmax=1e4)
norm = LogNorm(vmin=1, vmax=np.max(HIZd))
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHIZd.T == 0, HIZd.T + 1e-16),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.set_xlabel(r"Impact $ I \:[m]$")
ax.set_ylabel(r" $ Zd$")
ax.set_xticks(np.linspace(0, 540, 5))
ax.set_yticklabels([str(int(yt)) + r"°" for yt in ax.get_yticks()])

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"d$N_{trig}$/d$I$d$Zd$")

plt.tight_layout()
fig.savefig("triggered_I_Zd.png", dpi=300)


# plot 2D histogram Impact Zd
fig, ax = plt.subplots()

# norm = Normalize(vmin=np.min(HEI) * 1e-4, vmax=np.max(HEI) * 1e-4)
# norm = LogNorm(vmin=1, vmax=np.max(eff_HIZd))
norm = LogNorm(vmin=1e-3, vmax=1)
# norm = Normalize(vmin=0, vmax=1)
pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(OHIZd.T == 0, eff_HIZd.T + 1e-16),
    cmap=cmap, norm=norm
    # , edgecolor="face", # rasterized=True  # , lw=0#edgecolor="face"
)
ax.axvline(x=270, c="r", lw=0.5)
ax.axhline(y=30, c="r", lw=0.5)
ax.set_xlabel(r"Impact $ I \:[m]$")
ax.set_ylabel(r"$Zd$")
ax.set_xticks(np.linspace(0, 540, 5))
ax.set_yticklabels([str(int(xt)) + r"°" for xt in ax.get_yticks()])

# colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional", extend="min"
)
cb2.set_label(r"efficiency")

plt.tight_layout()
fig.savefig("triggerEfficiency_I_Zd.png", dpi=300)
fig.savefig("triggerEfficiency_I_Zd.pdf")
