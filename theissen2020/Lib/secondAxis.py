import ModifiedJulianDay as mjd
import numpy as np


def createYearAxisToMjdAxis(mjdAx, offset=0, yearlabel=True):

    minMjd, maxMjd = mjdAx.get_xlim()
    minNight = mjd.mjd_to_fNight(int(minMjd+offset))
    maxNight = mjd.mjd_to_fNight(int(maxMjd+offset))

    dates = []
    middates = []
    year = int(str(minNight)[:4])
    while year <= int(str(maxNight)[:4]):
        firstNight = 1e4*year+101
        if firstNight < maxNight and firstNight > minNight:
            dates.append(firstNight)
        middleNight = 1e4*year+702
        if middleNight < maxNight and middleNight > minNight:
            middates.append(1e4*year+702)
        year += 1

    yearticks = np.array([mjd.fNight_to_mjd(date) for date in dates])
    midticks = np.array([mjd.fNight_to_mjd(middate) for middate in middates])
    yearticks = yearticks - offset
    midticks = midticks - offset
    yearLabels = np.array([str(middate)[0:4] for middate in middates])

    ax2 = mjdAx.twiny()
    ax2.set_xlim(minMjd, maxMjd)
    ax2.set_xticks(yearticks)
    ax2.set_xticklabels('')
    ax2.set_xticks(midticks, minor=True)
    ax2.set_xticklabels(yearLabels, minor=True)
    if yearlabel:
        ax2.set_xlabel("Year")

    return ax2


def createCrabUnitToExcessAxis(
    excAx, cuRef=55.8, cuLabel=True,
    crabticks=np.array([0, 0.5, 1.0, 1.5, 2.0, 2.5])
):

    minExc, maxExc = excAx.get_ylim()
    tickpos = cuRef*crabticks
    ticklabels = [str(round(tick, 2)) for tick in crabticks]
    ax2 = excAx.twinx()
    ax2.set_ylim(minExc, maxExc)

    ax2.set_yticks(tickpos)
    ax2.set_yticklabels(ticklabels)

    if cuLabel:
        ax2.set_ylabel(
            "flux in Crab units",
            rotation=270, labelpad=10
        )

    return ax2
