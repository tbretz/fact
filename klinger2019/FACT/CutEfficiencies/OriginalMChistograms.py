import numpy as np
import pandas as pd

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
'''
calculates the histograms for untriggered data
saves them to data/OriginalMC_histograms.p (make sure the folder exists)
takes root file from MonteCarlos/data/
takes the Models from MonteCarlos/reweighting_energy, ..._impact and ..._Zd
'''

EBins = 64
IBins = 54
ZdBins = 60
si = -2.5

lgERange = [2.3, 4.7]
ZdRange = [0, 60]
ImpactRange = [0, 54000]

OriginalMCFile = "../MonteCarlos/data/OriginalMC_Energy_Impact_Zd.root"
dpo = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))

# Energy reweighting
dpo.calcLogs(["Energy"])
EnergyModelIn = Model.fromFile(
    "../MonteCarlos/reweighting_energy/EnergyModelIn_OriginalMC.p"
)
EnergyModelOut = Model.fromFile(
    "../MonteCarlos/reweighting_energy/EnergyModelOut_OriginalMC_%g.p" %
    (si)
)
dpo.updateModels(EnergyModelIn, EnergyModelOut)
dpo.addWeights("logEnergy", name="wE")
print("finished energy reweighting")


# Zd reweighting
dpo.data["Zd_deg"] = np.degrees(dpo.data["Zd"])
ZdModelIn = Model.fromFile(
    "../MonteCarlos/reweighting_Zd/ZdModelIn_OriginalMC.p"
)

factor = ZdModelIn.parameters[2] / ZdModelIn.parameters[1]
dpo.data["wZd"] = pd.cut(
    dpo.data["Zd_deg"], bins=[0, 30, 60], labels=[factor, 1]
)
print("finished Zd weights")


# impact reweighting
dpo.data["I_m"] = dpo.data["Impact"]/100.

ImpactModelIn = Model.fromFile(
    "../MonteCarlos/reweighting_impact/ImpactModelIn_OriginalMC.p"
)

factor = ImpactModelIn.parameters[1] / ImpactModelIn.parameters[3]
dpo.data["wI"] = pd.cut(
    dpo.data["I_m"], bins=[0, 270, 540], labels=[1, factor]
)
print("finished impact weights")


# create and save histograms
hm = HistogramManager()
hm.addHistogram("lgE_original", dpo.getHistogram("lgE_original", "logEnergy",
                useWeights=["wE"], histRange=lgERange, nBins=EBins))

hm.addHistogram("Zd_original", dpo.getHistogram("Zd_original", "Zd",
                useWeights="no", histRange=ZdRange, nBins=ZdBins))
hm.addHistogram(
    "Impact_original",
    dpo.getHistogram(
        "Impact_original", "Impact", useWeights="no", histRange=ImpactRange,
        nBins=IBins
    )
)
hm.addHistogram(
    "Zd_original_nostep",
    dpo.getHistogram(
        "Zd_original_nostep", "Zd", useWeights=["wZd"], histRange=ZdRange,
        nBins=ZdBins
    )
)
hm.addHistogram(
    "Impact_original_nostep",
    dpo.getHistogram(
        "Impact_original_nostep", "Impact", useWeights=["wI"],
        histRange=ImpactRange, nBins=IBins
    )
)

hm.saveHistogramDict(
    "data/OriginalMC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)
