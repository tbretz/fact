from scipy.interpolate import UnivariateSpline
import numpy as np
import math


def weightedMean(y, stat, prints=False):
    oldL = len(y)
    y = y[stat > 0]
    stat = stat[stat > 0]
    if not len(y) == oldL and prints:
        print("Shorter by "+str(oldL-len(y)))
    if len(y) == 0:
        if prints:
            print("Killed the only run left")
        return 0., 0.
    w = 1/stat**2
    s = sum(w*y)
    wsum = sum(w)
    mw = s/wsum
    mw_stat = np.sqrt(1./wsum)
    return mw, mw_stat


def findFWHM(x, y):

    # create a spline of x and y-np.max(y)/2
    spline = UnivariateSpline(x, y-np.max(y)/2, s=0)
    # find the roots
    roots = spline.roots()
    # find the roots from roots that are clostest to the max position
    xmax = x[np.argmax(y)]
    roots_center = roots-xmax

    left = -np.min(np.abs(roots_center[roots_center < 0])) + xmax
    right = np.min(np.abs(roots_center[roots_center > 0])) + xmax
    fwhm = right-left

    return fwhm, left, right,


def roundUp(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier


def roundDown(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier


def roundAndFollowDIN(a, sa):
    """
    a: value, sa: uncertainty

    DUM definition for scientific rounding of uncertainties:

    To ensure that the measurement uncertainty is not estimated too small, it
    should always be rounded up. The uncertainty of measurement is rounded to
    the first digit z different from 0 if z is between 3 and 9 and to two valid
    digits if z has the value 1 or 2.

    Following this definition uncertainties are treated. The quantity a is then
    rounded on the digit of interest given by the rounded uncertainty.
    """

    if not isinstance(sa, (list, tuple, type(np.array([])))):
        sa = [sa]

    positions = []
    anySpecial = False
    for entry in sa:
        str_sa = str(entry)
        if "e" in str_sa:
            split = str_sa.split("e")
            str_sa = split[0]
            exp_sa = int(split[1])
        else:
            exp_sa = 1
        special = False
        for i in range(len(str_sa)):
            str_pos = str_sa[i]
            if not str_pos == ".":
                digit = int(str_pos)
            else:
                continue
            if digit == 0:
                continue
            if digit <= 2:
                i += 1
                special = True
                anySpecial = True
                break
            else:
                break
        positions.append(i)

    rounded_sa = [roundUp(entry, decimals=max(positions)+exp_sa-2)
                  for entry in sa]

    if rounded_sa[0] != 0:
        counter = math.floor(math.log10(rounded_sa[0]))
        if anySpecial:
            counter -= 1
    else:
        counter = 0

    rounded_a = round(a, -counter)
    if len(rounded_sa) == 1:
        rounded_sa = rounded_sa[0]

    return rounded_a, rounded_sa
