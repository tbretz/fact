import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

from readYAML import LayerFromYAML, loadConfiguration, allPlasmoidsFromYAML
from Plotting import plotSizeHists, plotMomentumHists, plotEvolutionFilled, \
    plotSize, plotInitials
from PlasmoidStatistics import createHistograms
from HistogramManager import HistogramManager
from Histogram import Histogram
import setFigureConfig as sfc

configPath = "steadyState.yaml"

calc = 0
calcHist = 0
plot = 1
plotPathChain = "ChainPlots/"
plotHist = 0

if calc:
    LayerFromYAML(configPath)


if plot:
    configs = loadConfiguration(configPath)
    name = configs["name"]
    repetitions = int(configs["repetitions"])

    for i in range(repetitions):
        print("plotting" + name + " " + str(i))

        # plot in one pdf
        pdf = PdfPages(plotPathChain + name + "_Chain" + str(i) + ".pdf")

        # fig_evo, fig_size, fig_ini = chainPlots(configPath, i, minLifeTime=1)
        # load from file
        allPlasmoids = allPlasmoidsFromYAML(configPath, id=i)

        print("start plotting evolution..")
        sfc.halfWidth(aspectRatio=1)
        fig_evo = plotEvolutionFilled(
            allPlasmoids, minLifeTime=0.35, labelpad=5
        )

        print("start plotting sizes..")
        fig_size = plotSize(
            allPlasmoids, tauMax=20, minLifeTime=0.35, labelpad=5, ymax=2
        )

        print("start plotting initials..")
        sfc.halfWidth(aspectRatio=0.8)
        fig_ini = plotInitials(allPlasmoids)

        # save evolution plot
        fig_evo.savefig(
            plotPathChain + name + "_Chain" + str(i) + "_evo.png", dpi=300
        )
        pdf.savefig(fig_evo)

        # save size plot
        fig_size.savefig(
            plotPathChain + name + "_Chain" + str(i) + "_size.png", dpi=300
        )
        pdf.savefig(fig_size)

        # save initials plot
        fig_ini.savefig(
            plotPathChain + name + "_Chain" + str(i) + "_ini.png", dpi=300
        )
        pdf.savefig(fig_ini)

        pdf.close()

if calcHist:
    configs = loadConfiguration(configPath)
    name = configs["name"]
    repetitions = int(configs["repetitions"])

    sRange = [-3.125, -0.375]
    sNBins = 11
    pRange = [-0.1, 3.5]
    pNBins = 18
    hm = HistogramManager()
    nBins = 10
    binSize = 5
    tauMins = np.arange(nBins) * binSize
    tauMaxs = tauMins + binSize
    for j in range(nBins):
        # calculate mean of histograms
        hs_mean = Histogram(
            "log10s_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j]),
            histRange=sRange, nBins=sNBins
        )
        hp_mean = Histogram(
            "p_normed_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j]),
            histRange=pRange, nBins=pNBins,
        )
        for i in range(repetitions):
            print("calculating histogram of " + name + " " + str(i))

            hs, hp = createHistograms(
                configPath, i, tauMin=tauMins[j], tauMax=tauMaxs[j],
                sRange=sRange, sNBins=sNBins, pRange=pRange, pNBins=pNBins
            )
            hm.addHistogram(
                hs.getLabel(), hs
            )
            hm.addHistogram(
                hp.getLabel(), hp
            )
            hs_mean += hs
            hp_mean += hp
        hs_mean = hs_mean / repetitions
        hs_mean.setLabel(
            "log10s_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        )
        hp_mean = hp_mean / repetitions
        hp_mean.setLabel(
            "p_normed_" + name + "_mean" +
            "_tau" + str(tauMins[j]) + "-" + str(tauMaxs[j])
        )
        hm.addHistogram(
            hs_mean.getLabel(), hs_mean
        )
        hm.addHistogram(
            hp_mean.getLabel(), hp_mean
        )
    # save everything
    hm.saveHistogramDict(name + "_histograms.p")

if plotHist:
    configs = loadConfiguration(configPath)
    name = configs["name"]
    nBins = 10
    binSize = 5
    tauMins = np.arange(nBins) * binSize
    tauMaxs = tauMins + binSize
    for i in range(nBins):
        tauName = "_tau" + str(tauMins[i]) + "-" + str(tauMaxs[i])
        # size histograms plot
        fig = plotSizeHists(configPath, tauMin=tauMins[i], tauMax=tauMaxs[i])
        fig.savefig(name + "_SizeHists" + tauName + ".pdf")

        # momentum histograms plot
        fig = plotMomentumHists(
            configPath, tauMin=tauMins[i], tauMax=tauMaxs[i]
        )
        fig.savefig(name + "_MomentumHists" + tauName + ".pdf")
