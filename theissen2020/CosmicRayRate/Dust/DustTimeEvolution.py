import argparse

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import datetime
import setFigureConfig as figConfig

from rootRoutines import dFrameFromRoot

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--rootInputFile", help="Path to input root-file",
                    type=str, default="../ArtificialTriggerRates.root")
parser.add_argument("-o", "--pdfOutputFile", help="Path to output pdf-file",
                    type=str, default="./DustTimeEvolution.pdf")

args = parser.parse_args()

rootFile = args.rootInputFile
pdfFile = args.pdfOutputFile
printNoData = False

dF = dFrameFromRoot(rootFile, prints=False)
dF = dF[dF["fNight"] < 20190301]
dF["RAWTime"] = (dF["fRunStart"] + dF["fRunStop"])/2
dF["Time"] = pd.to_datetime(dF["RAWTime"], unit="s")
dF.sort_values("Time")


resampler = dF.set_index('Time').resample('M')
dustEv = resampler["fTNGDust"].count().to_frame("RunCounts").reset_index()
dustEv["DustMean"] = resampler.fTNGDust.mean().values
dustEv["DustStd"] = resampler.fTNGDust.std().values
dustEv["DustMeanErr"] = dustEv["DustStd"]/np.sqrt(dustEv["RunCounts"])
dustEv["Month"] = dustEv["Time"].dt.to_period("M")

noDustData = dustEv[dustEv["DustMean"] == 0]
if printNoData:
    print("No Data for:")
    [print(noDustData.Month.values[i]) for i in range(len(noDustData.index))]
dustEv = dustEv[dustEv["DustMean"] > 0]

figConfig.fullWidth()
figure = plt.figure("Dust Distribution over Time")
plot = figure.gca()
dustEv[dustEv["Time"].dt.month.isin([7, 8])].plot(
    "Month", "DustMean", fmt="s", yerr="DustMeanErr", ms=4,
    ax=plot, color="tab:green", label="July and August")
dustEv[~dustEv["Time"].dt.month.isin([7, 8])].plot(
    "Month", "DustMean", yerr="DustMeanErr", fmt=".",
    ax=plot, color="tab:blue", label="rest of the year")
# noDustData.plot("Month", "DustMean", yerr="DustMeanErr",
#                color="tab:red", ax=plot, fmt=".", label="not available")

figure.autofmt_xdate()
plot.set_ylim(0, 23)
plot.set_xlim([datetime.date(2012, 2, 15), datetime.date(2019, 4, 1)])
plot.set_ylabel("dust concentration "+r"$c_{\mathrm{TNG}}$"+" in µg/m³")
plot.set_xlabel("")
plot.legend(loc=2)
pos = plot.get_position()
plot.grid(axis="both", alpha=0.5)
figure.tight_layout()
# figure.text(pos.x0, pos.y0+1.05*pos.height,
#             "Cuts: 299s<fDuration<300s,\n         fRunType=1",
#             verticalalignment="top",
#             fontsize=8, transform=figure.transFigure)
figure.savefig(pdfFile)
figure.savefig(pdfFile[:-3]+"png", dpi=300)
