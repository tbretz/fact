import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u

import setFigureConfig as sfc

pid = 214
tf, lcf = np.load(
    "../F10/F10_4_0.5/F10_4_TotalLightCurve_1.0e+11-1.0e+14_dt60.npy")
tb, lcb = np.load(
    "../B10/B10_4_0.5/B10_4_TotalLightCurve_1.0e+11-1.0e+14_dt60.npy")
tfb, lcfb = np.load(
    "../F10/F10_4_0.5/F10_4_TotalLightCurve_1.0e+11-1.0e+14_dt60_bigdudes.npy")
tbb, lcbb = np.load(
    "../B10/B10_4_0.5/B10_4_TotalLightCurve_1.0e+11-1.0e+14_dt60_bigdudes.npy")


def add_lc(ax, t, lc, label, color="tab:blue", alpha=1, unitt="h"):
    t_unit = t * (u.s).to(unitt)
    ax.plot(t_unit - t_unit[0], lc, color=color, label=label, alpha=alpha)
    return ax


loweV = 1e11
upeV = 1e14
unitt = "h"
unitE = "TeV"
# convert units
lo = loweV * (u.eV).to(unitE)
up = upeV * (u.eV).to(unitE)

# plot
sfc.fullWidth(aspectRatio=0.6)
fig, ax = plt.subplots()
ax.set_title(r"$%g - %g$" % (lo, up) + unitE + r" (VHE $\gamma$)")
# ax.plot(tf_unit - tf_unit[0], lcf, color="tab:blue", label="FSRQ")
# ax.plot(tb_unit - tb_unit[0], lcb, color="tab:red", label="BL Lac")
ax = add_lc(ax, tf, lcf, None, color="tab:blue", alpha=0.3)
ax = add_lc(ax, tfb, lcfb, "FSRQ", color="tab:blue", alpha=1)
ax = add_lc(ax, tb, lcb, None, color="tab:red", alpha=0.3)
ax = add_lc(ax, tbb, lcbb, "BL Lac", color="tab:red", alpha=1)


ax.set_xlabel("t [" + unitt + "]")
ax.set_ylabel("L [erg/s]")
ax.legend(ncol=2)
ax.grid()
ax.set_ylim(1e35, 1e43)
ax.set_xlim(0, 155)
ax.set_yscale("log")
fig.tight_layout()
fig.savefig("F10_B10_totallc.pdf")
plt.show()