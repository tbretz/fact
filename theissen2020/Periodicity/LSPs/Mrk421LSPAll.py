import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.timeseries import LombScargle
from scipy.optimize import curve_fit, minimize

import setFigureConfig as figConfig


# Load Crab Lightcurve
lcMrk421 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk421Complete.h5",
    "Mrk421Complete"
)


t = lcMrk421["mjd"]
exc = lcMrk421["RExcCorr"]
excErr = lcMrk421["RExcErrCorr"]

lsw = LombScargle(t, exc, dy=excErr, center_data=False, fit_mean=False)

f = np.linspace(1e-3, 0.5, 10000)
p = lsw.power(f)

config = {}
config["alpha"] = 0.8
config["aspRatio"] = 1./2.3
config["small"] = 12
config["medium"] = 14
config["big"] = 16
config["width"] = 6.5
config["legendHandleLength"] = 1

figConfig.configure(config)

fig = plt.figure()
ax = fig.gca()

# ls
ax.plot(f, p, lw=2, color="k", label="LSP of Mrk 421", zorder=1.5)
ax.set_xlim(np.min(f), np.max(f))

# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)
ax.axvline(x=4/365.25, lw=1, color="tab:blue",
           ls="--", label="third alias", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)
ax.axvline(x=1/27.3, lw=1, color="tab:green",
           label="sidereal month", zorder=1.3)

ax.set_ylim(0, 0.6)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend(handletextpad=0.3)
fig.tight_layout()
fig.savefig("./Mrk421LSPAll.png", dpi=300)
fig.savefig("./Mrk421LSPAll.pdf")
