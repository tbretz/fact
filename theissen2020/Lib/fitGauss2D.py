import numpy as np
from scipy.optimize import leastsq

"""
    gauss2d(x, y, p)

    Description:
        Implementation of a 2D Gaussian.
        Compare to https://en.wikipedia.org/wiki/Gaussian_function#Meaning_of_parameters_for_the_general_equation

    Parameters:
        x: numpy.ndarray
            x-values
        y: numpy array
            y-values
        p: numpy.ndarray
            must contain 7 entries
            p[0]: overall norm
            p[1]: mu_x
            p[2]: mu_y
            p[3]: offset
            p[4]: angle of rotation
            p[5]: sigma_x
            p[6]: sigma_y

    Returns:
        result: numpy.ndarray
            Array containing the values of the 2D-Gaussian
"""


def gauss2d(x, y, p):
    a = np.cos(p[4])**2/(2*p[5]**2) + np.sin(p[4])**2/(2*p[6]**2)
    b = -np.sin(2*p[4])/(2*p[5]**2) + np.sin(2*p[4])/(2*p[6]**2)
    c = np.sin(p[4])**2/(2*p[5]**2) + np.cos(p[4])**2/(2*p[6]**2)
    return p[0]*np.exp(-a*(x-p[1])**2-b*(x-p[1])*(y-p[2])-c*(y-p[2])**2) + p[3]


"""
    fit_gauss2d(x, y, p)

    Description:
        fits parameters p of Gaussian as defined above.

    Parameters:
        x_input: numpy.ndarray
            x-values
        y_input: numpy array
            y-values
        z: numpy.ndarray
            values to fit the x and y input to
        guess: numpy.ndarray
            initial guess for parameters
            must contain 7 entries
            p[0]: overall norm
            p[1]: mu_x
            p[2]: mu_y
            p[3]: offset
            p[4]: angle of rotation
            p[5]: sigma_x
            p[6]: sigma_y

    Returns:
        lsq: numpy.ndarray
            result of least squares fit from scipy optimize
"""


def fit_gauss2d(x_input, y_input, z, guess=2*np.ones(7)):
    if len(np.shape(x_input)) == 1 and len(np.shape(y_input)) == 1:
        x, y = np.meshgrid(x_input, y_input)
    else:
        x, y = x_input, y_input

    def pull(p):
        return z.flatten() - gauss2d(x.flatten(), y.flatten(), p)

    lsq = leastsq(pull, guess, full_output=1)

    return lsq
