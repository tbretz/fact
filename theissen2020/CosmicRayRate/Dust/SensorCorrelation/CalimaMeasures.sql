#!rootifysql
WITH DustParams AS
(
   SELECT
      fNight,
      fRunID,
      fRunStart,
      fRunStop,
      (fRunStart+fRunStop)/2 AS fRunTimeStemp,
      fTNGDust,
      fLidarTransmission3,
      fLidarTransmission6,
      fLidarTransmission9,
      fLidarTransmission12
   FROM
      RunInfo
   WHERE
      fNight BETWEEN 20120101 AND 20200101
)
SELECT
   * 
FROM
   DustParams