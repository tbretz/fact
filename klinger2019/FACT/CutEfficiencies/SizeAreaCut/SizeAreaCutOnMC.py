import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
import setFigureConfig as sfc

si = -2.5

ROOTFile = "../../MonteCarlos/data/MC_Gamma.root"
dp = DataProcessor(data=dFrameFromRoot(ROOTFile, keys=["Result"]))
dp.doQualityCuts(Size=True, Area=True)
dp.calcLogs(["Energy", "Size", "Area"])
dp.doThetaSqCut(0.028)
# reweighting
energyPath = "../../MonteCarlos/reweighting_energy/"
MCG_E_in = Model.fromFile(energyPath + "/EnergyModelIn_MCGamma.p")
MCG_E_out = Model.fromFile(
    energyPath + "/EnergyModelOut_MCGamma_%g.p" % (si)
)
dp.updateModels(MCG_E_in, MCG_E_out)
dp.addWeights("logEnergy", name="wE")

counts, xEdges, yEdges = np.histogram2d(
    dp.getData("logSize"), dp.getData("logArea"),
    bins=30, range=[[0.8, 5.3], [-4, 0.5]], weights=dp.getData("wE")
)

sfc.halfWidth()
fig, ax = plt.subplots()
ax.set_xlabel(r"$\log_{10}(Size)$")
ax.set_ylabel(r"$\log_{10}(Area / mm^2)$")
cmap=plt.get_cmap("viridis")
cmap.set_bad(color="darkgrey")
X, Y = np.meshgrid(xEdges, yEdges)

pcm = ax.pcolormesh(
    X, Y,
    np.ma.masked_where(counts.T == 0, counts.T),
    cmap=cmap,
    norm=LogNorm(vmin=np.maximum(np.min(counts), 1),
                    vmax=np.max(counts))
)

cb = fig.colorbar(pcm, ax=ax)
cb.set_label("Events")

xmin = np.min(xEdges)
xmax = np.max(xEdges)
x = np.linspace(xmin, xmax, 200)


def ISDC_cut(x):
    alpha = 912
    beta = -1512
    return np.log10(alpha * x + beta) + np.log10(dp.mm2deg**2)


def spectrum_cut(x):
    alpha = 0.322
    beta = -1.845
    return alpha * x + beta


def lightcurve_cut(x):
    alpha = 0.00115
    beta = 1.554
    return np.log10((x - beta) / alpha) + np.log10(dp.mm2deg**2)


# ax.plot(x, ISDC_cut(x), label="ISDC", color="tab:red")
ax.plot(x, spectrum_cut(x), label="spectrum", color="tab:red")
ax.plot(x, lightcurve_cut(x), label="lightcurve", color="tab:orange")
ax.legend(loc="lower right")
ax.set_ylim(-3, 0.5)

fig.tight_layout()
# fig.savefig("SizeAreaCutOnMC.png", dpi=300)
fig.savefig("SizeAreaCutOnMC.pdf")
