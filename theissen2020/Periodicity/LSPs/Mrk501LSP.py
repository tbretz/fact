import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.timeseries import LombScargle
from scipy.optimize import curve_fit, minimize

import setFigureConfig as figConfig


significance = True

# Load Crab Lightcurve
lcMrk501 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk501From2013.h5",
    "Mrk501From2013"
)

if significance:
    sig = pd.read_hdf(
        "../TKSimulations/Mrk501Significance.h5",
        "Mrk501Significance"
    )
    threeSigma = sig["3sigma"]

t = lcMrk501["mjd"]
exc = lcMrk501["RExcCorr"]
excErr = lcMrk501["RExcErrCorr"]

lsw = LombScargle(t, exc, dy=excErr, center_data=False, fit_mean=False)
lsw_trunc = LombScargle(
    t, exc, dy=excErr, center_data=False, fit_mean=False, nterms=2
)

f = np.linspace(1e-3, 0.5, 10000)
p = lsw.power(f)
p_trunc = lsw_trunc.power(f)

config = {}
config["alpha"] = 0.8
config["aspRatio"] = 1./2.3
config["small"] = 12
config["medium"] = 14
config["big"] = 16
config["width"] = 6.5
config["legendHandleLength"] = 2

figConfig.configure(config)

fig = plt.figure()
ax = fig.gca()

# ls
lsLine = ax.plot(f, p, lw=2, color="k", label="LSP of Mrk501", zorder=1.5)
ax.set_xlim(np.min(f), np.max(f))
if significance:
    sigLine = ax.plot(
        f, threeSigma, lw=2, color="tab:red",
        label="99.7% significance from TK", zorder=1.5
    )

# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)

ax.axvline(x=2/365.25, lw=1, ls="--", color="tab:blue",
           label="first alias", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)


ax.set_ylim(0, 0.45)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend(handletextpad=0.3)
fig.tight_layout()
fig.savefig("./Mrk501LSP.png", dpi=300)
fig.savefig("./Mrk501LSP.pdf")

newticks = np.linspace(1/340, 1/290, num=6)
newticklabels = [str(int(1/tick)) for tick in newticks]

ax.set_xlim(1/340, 1/290)
ax.set_ylim(0.05, 0.3)
ax.axvline(x=1/332, lw=2, ls="-.", color="tab:purple",
           label="332 days period\n(Fermi - Bhatta)", zorder=1.3)
ax.axvline(x=1/310, lw=2, ls="-.", color="tab:green",
           label="310 days period", zorder=1.3)
for line in fig.gca().lines:
    line.set_linewidth(3)
ax.set_xscale("linear")
ax.set_xlabel("period in days")
ax.set_xticks(newticks)
ax.set_xticklabels(newticklabels)
handles, labels = ax.get_legend_handles_labels()
for entry in [handles, labels]:
    del entry[2]
    del entry[2]
ax.legend(handles, labels)
fig.tight_layout()
fig.savefig("Mrk501LSPZoom.png", dpi=300)
fig.savefig("Mrk501LSPZoom.pdf", dpi=300)


##########################################
# Trunced
##########################################

fig = plt.figure()
ax = fig.gca()

# ls
ax.plot(f, p_trunc, lw=2, color="k", label="LSP of Mrk501 (K=2)", zorder=1.5)
ax.set_xlim(2*np.min(f), 0.45)

# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)
ax.axvline(x=1/(2*29.5), lw=1, ls="--", color="tab:orange",
           label="synodic month alias", zorder=1.3)


# signal ???
ax.axvline(x=1/332, lw=1.5, ls="-.", color="tab:purple",
           label="332 days period\n(Fermi - Bhatta)", zorder=1.3)

# signal ???
ax.axvline(x=1/310, lw=1.5, ls="--", color="tab:green",
           label="310 days period", zorder=1.3)

ax.set_ylim(0, 0.26)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend(handletextpad=0.3)
fig.tight_layout()
fig.savefig("./Mrk501LSPK2.png", dpi=300)
fig.savefig("./Mrk501LSPK2.pdf")


def power_law(x, A, alpha):
    return A * x**(-alpha)


popt, pcov = curve_fit(power_law, f, p)
print(popt)
print(np.sqrt(pcov[1, 1]))

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.set_xlim(np.min(f), np.max(f))
ax.plot(
    f, p, ".", markersize=0.8, color="tab:blue",
    label="LSP of Mrk501 lightcurve"
)
ax.plot(
    f, power_law(f, popt[0], popt[1]), color="tab:red",
    label="PSD slope fit: "+r"$-0.69\pm0.09$"
)
ax.set_xscale("log")
ax.set_yscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend()
fig.tight_layout()
fig.savefig("Mrk501SlopeFit.png", dpi=300)
fig.savefig("Mrk501SlopeFit.pdf")
