import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import seaborn as sns
import palettable as pt

from readYAML import allPlasmoidsFromYAML, loadConfiguration
from HistogramManager import HistogramManager
import referenceValues as refVals
from coloring import c_beta, c_size


# draw the evolution (x vs t) of plasmoids with extended lines
# color encodes beta
def plotEvolutionFilled(
    allPlasmoids, sorted=True, minLifeTime=0.35,
    cmap=None, Ncol=20, ax=None, labelpad=10
):
    print("Creating plot with ", max(allPlasmoids["index"]), " plasmoids")
    # sort plasmiods by index and time
    if not sorted:
        sort = allPlasmoids.sort_values(["index", "tau"])
        # use pandas groupby for efficiency
        sg = sort.groupby(["index"])
    else:
        sg = allPlasmoids.groupby(["index"])

    # use color schema for velocity
    if ax is None:
        # generate plot
        plt.close("all")
        fig, (ax, axcb) = plt.subplots(
            ncols=2, gridspec_kw={"width_ratios": [19, 1]},
            constrained_layout=True
        )
    else:
        ax, axcb = ax

    ax.set_xlim(-1, 1)
    ax.set_ylim(0, max(allPlasmoids["tau"]))
    ax.set_xlabel("x/L")
    ax.set_ylabel("ct/L")

    # set custom color cycle
    if cmap is not None:
        cmap = cmap
    else:
        cmap = c_beta(as_map=True)

    # loop over all elements of sg: group g with index i
    # for i, g in sg:
    #     gamma = 1. / np.sqrt(1. - g["beta"]**2)
    #     slab = 3./2 * g["s"] / gamma
    #     # ax.fill_between(g["tau"], g["r"] - slab/2,
    #     #                 g["r"] + slab/2)
    #     ax.fill_betweenx(g["tau"], g["r"] - slab/2,
    #                      g["r"] + slab/2)

    gamma = 1. / np.sqrt(1. - allPlasmoids["beta"]**2)
    allPlasmoids["slab"] = 3./2 * allPlasmoids["s"] / gamma
    allPlasmoids["color"] = pd.cut(
        abs(allPlasmoids["beta"]),
        np.linspace(0, 1, Ncol),
        labels=np.linspace(0, 1, Ncol-1)
    )
    # loop over all elements of sg: group g with index i
    for i, g in sg:
        if not i % 100:
            print(i)
        # cut on lifetime: tau-tau0 > minLifeTime
        if g["tau"].values[-1] - g["tau"].values[0] > minLifeTime:
            # determine color according to speed
            # print(g["color"])
            # loop over all elements of g: group gc with index j
            for j, gc in g.groupby("color"):
                if not gc.empty:
                    ax.fill_betweenx(
                        gc["tau"],
                        gc["r"] - gc["slab"]/2, gc["r"] + gc["slab"]/2,
                        color=cmap(j), linewidth=0
                    )
    # colorbar
    bounds = np.linspace(0, 1, Ncol-1)
    cb2 = cb.ColorbarBase(
        axcb, cmap=cmap,
        boundaries=bounds,
        ticks=np.linspace(0, 1, 5),
        orientation='vertical'
    )
    cb2.set_label(r"$\beta$", rotation=0, labelpad=labelpad)
    fig.tight_layout()
    return fig


def plotEvolutionSizeColor(
    allPlasmoids, sorted=True, minLifeTime=0.35,
    ax=None, cmap=None, Ncol=20, labelpad=10
):
    print("Creating plot with ", max(allPlasmoids["index"]), " plasmoids")
    # sort plasmiods by index and time
    if not sorted:
        sort = allPlasmoids.sort_values(["index", "tau"])
        # use pandas groupby for efficiency
        sg = sort.groupby(["index"])
    else:
        sg = allPlasmoids.groupby(["index"])

    if ax is None:
        # generate plot
        plt.close("all")
        fig, (ax, axcb) = plt.subplots(
            ncols=2, gridspec_kw={"width_ratios": [19, 1]}
        )
    else:
        ax, axcb = ax

    ax.set_xlim(-1, 1)
    ax.set_xlabel("x/L")
    ax.set_ylabel("ct/L")

    # set custom color cycle
    if cmap is not None:
        cmap = cmap
    else:
        cmap = c_size(as_map=True)

    gamma = 1. / np.sqrt(1. - allPlasmoids["beta"]**2)
    slab = 3./2 * allPlasmoids["s"] / gamma
    # determine color according to size
    allPlasmoids["color"] = pd.cut(
        np.log10(slab),
        np.linspace(-4, 0, Ncol),
        labels=np.linspace(0, 1, Ncol-1)
    )
    # loop over all elements of sg: group g with index i
    for i, g in sg:
        # cut on lifetime: tau-tau0 > minLifeTime
        if g["tau"].values[-1] - g["tau"].values[0] > minLifeTime:
            # loop over all elements of g: group gc with index j
            for j, gc in g.groupby("color"):
                if not gc.empty:
                    ax.plot(gc["r"], gc["tau"], color=cmap(j))

    # colorbar
    bounds = np.linspace(-4, 0, Ncol-1)
    cb2 = cb.ColorbarBase(
        axcb, cmap=cmap,
        boundaries=bounds,
        ticks=np.linspace(-4, 0, 5),
        orientation='vertical',
        spacing="proportional"
    )
    # cb2.ax.set_yticklabels = [
    #     r"$10^{%g}$" % (i) for i in np.linspace(-4, 0, 5)
    # ]
    cb2.set_label(r"$\log_{10}{\mathrm{s}}$", rotation=0, labelpad=labelpad)
    # ax.plot(np.linspace(0, 1, 3), np.linspace(0, 1, 3), c="r")
    fig.tight_layout()
    return fig


def plotSize(allPlasmoids, sorted=True, tauMin=0.05, tauMax=4, sigma=None,
             minLifeTime=0.4, cmap=None, Ncol=20, ax=None, ymax=0.3,
             labelpad=10):
    print("Creating Size-plot with ", max(allPlasmoids["index"]), " plasmoids")
    # sort plasmiods by index and time
    if not sorted:
        sort = allPlasmoids.sort_values(["index", "tau"])
        # use pandas groupby for efficiency
        sg = sort.groupby(["index"])
    else:
        sg = allPlasmoids.groupby(["index"])

    # use color schema for velocity
    if ax is None:
        # generate plot
        plt.close("all")
        fig, (ax, axcb) = plt.subplots(
            ncols=2, gridspec_kw={"width_ratios": [19, 1]},
            constrained_layout=True
        )
    else:
        ax, axcb = ax
    # set custom color cycle
    if cmap is not None:
        cmap = cmap
    else:
        cmap = c_beta(as_map=True)

    ax.set_ylabel(r"$\frac{w-w_{min}}{L}$", rotation=0)
    ax.set_xlabel(r"$\frac{c}{L}(t-t_0)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylim(0.002, ymax)
    ax.set_xlim(tauMin, tauMax)
    if sigma is not None:
        ax.set_title(r"$\sigma=%g$" % (sigma))

    # loop over all elements of sg: group g with index i
    for i, g in sg:
        if not i % 100:
            print(i)
        # cut on lifetime: tau-tau0 > 0.4
        if g["tau"].values[-1] - g["tau"].values[0] > minLifeTime:
            # initial parameters
            tau0 = g["tau"].values[0]
            s0 = g["s"].values[0]
            # plot only stuff visible
            g = g[g["tau"] - tau0 > tauMin]
            # determine color according to speed
            g["color"] = pd.cut(abs(g["beta"]),
                                np.linspace(0, 1, Ncol),
                                labels=np.linspace(0, 1, Ncol-1))
            # loop over all elements of g: group gc with index j
            for j, gc in g.groupby("color"):
                if not gc.empty:
                    ax.plot(
                        gc["tau"] - tau0, gc["s"] - s0,
                        color=cmap(j)
                    )

    # colorbar
    bounds = np.linspace(0, 1, Ncol-1)
    cb2 = cb.ColorbarBase(
        axcb, cmap=cmap,
        boundaries=bounds,
        ticks=np.linspace(0, 1, 5),
        orientation='vertical'
    )
    cb2.set_label(r"$\beta$", rotation=0, labelpad=labelpad)
    fig.tight_layout()
    return fig


def plotInitials(allPlasmoids, sigma=10):
    plt.close("all")
    fig, ax = plt.subplots()
    ax.set_xlabel(r"$x_0 / L$")
    ax.set_ylabel(r"$p_0 / mc$")
    # gaussian distribution
    r = np.linspace(-1, 1, 200)
    p0_mean = 0.66 * np.sqrt(sigma) * np.tanh(40 * r / sigma)
    p0_sigma = 0.3 * np.sqrt(sigma)
    ax.fill_between(r, p0_mean - p0_sigma, p0_mean + p0_sigma,
                    facecolor="tab:green", alpha=0.5)
    ax.plot(r, p0_mean, color="tab:green", linestyle="--", label="mean")
    # data
    r0 = allPlasmoids.groupby("index").head(1)["r"].values
    b0 = allPlasmoids.groupby("index").head(1)["beta"].values
    ax.scatter(r0, b0/np.sqrt(1. - b0**2), marker=".", zorder=5, label=None)
    ax.legend(loc="upper left")
    fig.tight_layout()
    return fig


def chainPlots(pathToConfigFile, id, minLifeTime=0.3, cmap=None, tauMax=4):
    # style
    # sns.set(style="ticks")
    # if cmap is not None:
    #     cmap = cmap
    # else:
    #     cmap = pt.scientific.sequential.Bamako_20.mpl_colormap

    # load from file
    allPlasmoids = allPlasmoidsFromYAML(pathToConfigFile, id=id)

    print("start plotting evolution..")
    fig_evo = plotEvolutionFilled(
        allPlasmoids, minLifeTime=minLifeTime,
        cmap=cmap, Ncol=50
    )

    print("start plotting sizes..")
    fig_size = plotSize(
        allPlasmoids, cmap=cmap, tauMax=tauMax, minLifeTime=minLifeTime
    )

    print("start plotting initials..")
    fig_ini = plotInitials(allPlasmoids)

    print("finished plotting!")
    return fig_evo, fig_size, fig_ini


def plotSizeHists(pathToConfigFile, tauMin=1, tauMax=100, reference=True):
    configs = loadConfiguration(pathToConfigFile)
    name = configs["name"]
    repetitions = int(configs["repetitions"])

    hm = HistogramManager()
    hm.loadHistogramDict(name + "_histograms.p")

    fig, ax = plt.subplots()
    ax.set_xlabel(r"$\log{w/L}$")
    ax.set_ylabel(r"$\frac{N[log(\frac{w}{L})] }{N_{Isl}} $", fontsize=18)
    ax.set_yscale("log")
    ax.set_title(
        r"$\sigma = %g, %g \geq \tau \geq %g$" % (
            configs["eom"]["sigma"], tauMin, tauMax
        ),
        loc="right"
    )

    for i in range(repetitions-1):
        sLabel = "log10s_" + name + "_" + str(i)
        sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
        ax = hm.get(sLabel).addToAx(
            ax, style="stairs", color="lightgrey"
        )

    # plot last histogram with entry in legend
    sLabel = "log10s_" + name + "_" + str(repetitions-1)
    sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(sLabel).addToAx(
        ax, style="stairs", color="lightgrey",
        label=str(repetitions) + " layers"
    )

    # plot mean
    sLabel = "log10s_" + name + "_mean"
    sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(sLabel).addToAx(
        ax, style="stairs", color="black", label="mean"
    )
    # plot reference
    if reference:
        if str(configs["eom"]["sigma"]) == "3":
            x, y = refVals.P18_s3_s
            ref_col = "tab:blue"
        elif str(configs["eom"]["sigma"]) == "10":
            x, y = refVals.P18_s10_s
            ref_col = "tab:orange"
        elif str(configs["eom"]["sigma"]) == "50":
            x, y = refVals.P18_s50_s
            ref_col = "tab:green"
        else:
            print("no reference for this sigma!")
        ax.plot(
            x, y, linestyle="", marker="v",
            color=ref_col, label="Petropoulou 2018",
        )
    ax.legend()
    fig.tight_layout()
    return fig


def plotMomentumHists(pathToConfigFile, tauMin=1, tauMax=100, reference=True):
    configs = loadConfiguration(pathToConfigFile)
    name = configs["name"]
    repetitions = int(configs["repetitions"])

    hm = HistogramManager()
    hm.loadHistogramDict(name + "_histograms.p")

    fig, ax = plt.subplots()
    ax.set_xlabel(r"$|p|/\sqrt{\sigma}$")
    ax.set_ylabel(r"$N(|p|/\sqrt{\sigma}) / N_{Isl}$", fontsize=18)
    ax.set_yscale("log")
    ax.set_title(
        r"$\sigma = %g, %g \geq \tau \geq %g$" % (
            configs["eom"]["sigma"], tauMin, tauMax
        ),
        loc="right"
    )
    for i in range(repetitions-1):
        pLabel = "p_normed_" + name + "_" + str(i)
        pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
        ax = hm.get(pLabel).addToAx(
            ax, style="stairs", color="lightgrey"
        )

    # plot last histogram with entry in legend
    pLabel = "p_normed_" + name + "_" + str(repetitions-1)
    pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(pLabel).addToAx(
        ax, style="stairs", color="lightgrey",
        label=str(repetitions) + " layers"
    )

    # plot mean
    pLabel = "p_normed_" + name + "_mean"
    pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(pLabel).addToAx(
        ax, style="stairs", color="black", label="mean"
    )
    # plot reference
    if reference:
        if str(configs["eom"]["sigma"]) == "3":
            x, y = refVals.P18_s3_p
            ref_col = "tab:blue"
        elif str(configs["eom"]["sigma"]) == "10":
            x, y = refVals.P18_s10_p
            ref_col = "tab:orange"
        elif str(configs["eom"]["sigma"]) == "50":
            x, y = refVals.P18_s50_p
            ref_col = "tab:green"
        else:
            print("no reference for this sigma!")
        ax.plot(
            x, y, linestyle="", marker="^",
            color=ref_col, label="Petropoulou 2018",
        )
    ax.legend()
    fig.tight_layout()
    return fig


def plotSizeMomentumHist(pathToConfigFile, tauMin=1, tauMax=100,
                         reference=True):
    configs = loadConfiguration(pathToConfigFile)
    name = configs["name"]
    repetitions = int(configs["repetitions"])
    hm = HistogramManager()
    hm.loadHistogramDict(name + "_histograms.p")

    fig = plt.figure(constrained_layout=True)
    gs = fig.add_gridspec(nrows=2, ncols=1)
    ax = fig.add_subplot(gs[0, 0])
    axp = fig.add_subplot(gs[1, 0])

    ax.set_xlabel(r"$\log_{10} (w/L)$")
    ax.set_ylabel(r"$\frac{N[\log_{10} (w/L)] }{N_{Isl}} $")
    ax.set_yscale("log")
    ax.set_title(r"$\sigma = %g$" % (configs["eom"]["sigma"]), loc="right")
    ax.set_title(r"mean of %g realizations" % (repetitions), loc="center")

    axp.set_xlabel(r"$|p|/\sqrt{\sigma}$")
    axp.set_ylabel(r"$N(|p|/\sqrt{\sigma}) / N_{Isl}$")
    axp.set_yscale("log")

    ax.grid()
    axp.grid()

    # size histograms
    for i in range(repetitions-1):
        sLabel = "log10s_" + name + "_" + str(i)
        sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
        ax = hm.get(sLabel).addToAx(
            ax, style="stairs", color="lightgrey"
        )

    # plot last histogram with entry in legend
    sLabel = "log10s_" + name + "_" + str(repetitions-1)
    sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(sLabel).addToAx(
        ax, style="stairs", color="lightgrey",
        label=str(repetitions) + " layers"
    )

    # plot mean
    sLabel = "log10s_" + name + "_mean"
    sLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    ax = hm.get(sLabel).addToAx(
        ax, style="stairs", color="black", label="mean"
    )

    # momentum plots
    for i in range(repetitions-1):
        pLabel = "p_normed_" + name + "_" + str(i)
        pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
        axp = hm.get(pLabel).addToAx(
            axp, style="stairs", color="lightgrey"
        )

    # plot last histogram with entry in legend
    pLabel = "p_normed_" + name + "_" + str(repetitions-1)
    pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    axp = hm.get(pLabel).addToAx(
        axp, style="stairs", color="lightgrey",
        label=str(repetitions) + " layers"
    )

    # plot mean
    pLabel = "p_normed_" + name + "_mean"
    pLabel += "_tau" + str(tauMin) + "-" + str(tauMax)
    axp = hm.get(pLabel).addToAx(
        axp, style="stairs", color="black", label="mean"
    )

    if reference:
        if str(configs["eom"]["sigma"]) == "3":
            x, y = refVals.P18_s3_s
            xp, yp = refVals.P18_s3_p
            ref_col = "tab:blue"
        elif str(configs["eom"]["sigma"]) == "10":
            x, y = refVals.P18_s10_s
            xp, yp = refVals.P18_s10_p
            ref_col = "tab:orange"
        elif str(configs["eom"]["sigma"]) == "50":
            x, y = refVals.P18_s50_s
            xp, yp = refVals.P18_s50_p
            ref_col = "tab:green"
        else:
            print("no reference for this sigma!")
        ax.plot(
            x, y, linestyle="", marker="v",
            color=ref_col, label="Petropoulou 2018",
        )
        axp.plot(
            xp, yp, linestyle="", marker="^",
            color=ref_col, label="Petropoulou 2018",
        )
    return fig
