import matplotlib.pyplot as plt


def configure(config):
    alpha = config["alpha"]
    plt.rc('font', size=alpha*config["small"])
    plt.rc('axes', titlesize=alpha*config["small"])
    plt.rc('axes', labelsize=alpha*config["medium"])
    plt.rc('xtick', labelsize=alpha*config["small"])
    plt.rc('ytick', labelsize=alpha*config["small"])
    plt.rc('legend',
           fontsize=alpha*config["small"],
           handlelength=config["legendHandleLength"]
           )
    plt.rc('figure',
           titlesize=alpha*config["big"],
           figsize=(config["width"], config["aspRatio"]*config["width"])
           )


def fullWidth(alpha=1, aspectRatio=4./6., legendHandleLength=2):
    config = {}
    config["alpha"] = alpha
    config["aspRatio"] = aspectRatio

    config["small"] = 12
    config["medium"] = 14
    config["big"] = 16

    config["width"] = 6.5

    config["legendHandleLength"] = legendHandleLength

    configure(config)


def halfWidth(alpha=1,  aspectRatio=4./6., legendHandleLength=1):
    config = {}
    config["alpha"] = alpha
    config["aspRatio"] = aspectRatio

    config["small"] = 7
    config["medium"] = 8
    config["big"] = 9

    config["width"] = 3.25

    config["legendHandleLength"] = legendHandleLength

    configure(config)
