#!rootifysql
WITH RunParams AS
(
    SELECT
        RunInfo.fNight,
        RunInfo.fRunID,
        
        RunInfo.fRunStart,
        RunInfo.fRunStop,
        TIME_TO_SEC(TIMEDIFF(fRunStop,fRunStart)) AS fRunDuration,

        RunInfo.fR750Cor,
        RunInfo.fR750Ref,
        RunInfo.fNumThreshold750,
       
        
        RunInfo.fEffectiveOn,
        RunInfo.fTNGDust,
        RunInfo.fThresholdMinSet,
        RunInfo.fThresholdMedian,
        RunInfo.fZenithDistanceMean

    FROM
        RunInfo
    WHERE
        fRunTypeKey=1
    AND
        -- Only Runs with consistent threshold setting
        fThresholdMedian = fThresholdMinSet
    AND
        -- Only runs with a non zero artificial trigger rate
        fNumThreshold750 > 0
    AND
        RunInfo.fNight BETWEEN 20160101 AND 20181110
)

SELECT
    * 
FROM
    RunParams
WHERE
    fRunDuration BETWEEN 298 AND 301