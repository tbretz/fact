import numpy as np
import pandas as pd

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
import setFigureConfig as sfc

nbins = 54

recalc = True
if recalc:
    OriginalMCFile = "../data/OriginalMC_Energy_Impact_Zd.root"
    dp = DataProcessor(data=dFrameFromRoot(OriginalMCFile, keys=["Result"]))

    # in model
    dp.data["I_m"] = dp.data["Impact"]/100.  # convert to meter
    # create histogram of impact called Impact_hist
    I_range = [0, 540]
    IHist = dp.getHistogram("Impact_hist", "I_m", histRange=I_range)

    # create Input Model with two linear distributions
    ImpactModelIn = Model("steplinear")
    # initial guess values: point of step, slope1, offset1, slope2, offset2
    ImpactModelIn.getParametersFromFit(IHist, [270, 170, 0, 100, 0])
    print("finished in Model")
    ImpactModelIn.plotModelWithData(data=IHist, yscale="linear").savefig(
        "Impact_reweighting_fit.pdf"
    )
    ImpactModelIn.save("ImpactModelIn_OriginalMC.p")
    print(ImpactModelIn.parameters)

    # calculate weights of complete distribution
    def reweight_steplinear(x, p):
        # new = p[1] * dpo.data["Impact"] + p[2]
        # old = p[3] * dpo.data["Impact"] + p[4]
        # fraction = new / old
        fraction = (p[1] * x + p[2]) / (p[3] * x + p[4])
        return (x >= p[0]) + (x < p[0]) * fraction

    factor = ImpactModelIn.parameters[1] / ImpactModelIn.parameters[3]
    dp.data["wI"] = pd.cut(
        dp.data["I_m"], bins=[0, 270, 540], labels=[1, factor]
    )

    # too slow
    # dp.data["wI"] = dp.data["I_m"].map(
    #     lambda I: reweight_steplinear(I, ImpactModelIn.parameters)
    # )

    # calc histograms for plot

    hm = HistogramManager()
    IHist = dp.getHistogram(
        "I_Hist", "I_m", useWeights=None, nBins=nbins, histRange=I_range
    )
    hm.addHistogram("produced", IHist)
    IHist_weighted = dp.getHistogram(
        "I_Hist_weighted", "I_m", useWeights=["wI"], nBins=nbins,
        histRange=I_range
    )
    hm.addHistogram("real", IHist_weighted)
    hm.saveHistogramDict("OriginalMC_reweightingImpact_%g.p" % (nbins))
else:
    hm = HistogramManager()
    hm.loadHistogramDict("OriginalMC_reweightingImpact_%g.p" % (nbins))

# create plot
sfc.halfWidth(aspectRatio=0.9)
fig = hm.plotHistograms(
    ["real", "produced"],
    ["full parameter space", "produced distribution"],
    yscale="linear", style="stairs", xlabel=r"Impact $I$ [m]",
    legloc=[None, "upper left"]
)
ax = fig.gca()
ax.set_ylabel(
    r"$\frac{\mathrm{d}N}{\mathrm{d} I}$",
    # rotation=90, labelpad=10
)
ax.set_xticks(np.linspace(0, 540, 5))
ax.set_yticklabels(["%g" % (yt*1e-6) for yt in ax.get_yticks()])
ax.set_ylabel(r"$10^{-6} \:\:  \frac{\mathrm{d} N }{ \mathrm{d} I}$")
ax.grid()
fig.tight_layout()
fig.savefig("OriginalMC_reweightingI_%g.pdf" % (nbins))
