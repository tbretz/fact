#!rootifysql

SELECT
    -- Identification
    fNight,
    fRunID,

    -- Analysis parameters (back to all BG)
    fNumBgEvts*5 AS fNumBgEvts, 
    fNumSigEvts

    FROM
        AnalysisResultsRunCutsLC
        LEFT JOIN RunInfo USING (fNight, fRunID)
    WHERE
        -- Sources:
        -- Mrk 421          = 1
        -- Mrk 501          = 2
        -- Crab             = 5
        RunInfo.fSourceKey = 2
    AND
        -- Only Data Runs
        RunInfo.fRunTypeKey = 1
