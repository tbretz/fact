import numpy as np
"""
    Still used in different scripts
"""


def deltaBeta(mu, sigma):
    beta = 0.5*np.tanh((280-mu)/sigma) + 0.5
    return np.abs(beta)


def thCorrection(th, gamma, mu, sigma, covM):
    delta = gamma - 1
    beta = 0.5*np.tanh((th-mu)/sigma) + 0.5
    c = 1./(1+beta*(mu**delta * th**(-delta) - 1))
    c0 = 1./(1+(0.5*np.tanh((280-mu)/sigma) + 0.5) *
             (mu**delta * 280**(-delta) - 1))
    c = c / c0
    CbyMu = c**2 * (2*beta/sigma * (beta+1) - beta *
                    delta * mu**(delta-1)*th**(-delta))
    CbySigma = c**2 * 2*beta/sigma * (beta+1) * (th-mu)/sigma
    sigmaCSq = 0

    CbyVar = np.array([CbyMu, CbySigma])
    for i in range(2):
        for j in range(2):
            sigmaCSq += CbyVar[i]*covM[i][j]*CbyVar[j]
    sigmaC = np.sqrt(sigmaCSq)
    sigmaC = sigmaC/c0
    # print(c0)
    return c, sigmaC


def zdCorrection(theta, gamma, chi, xi, kappa, covM,
                 alpha=0.065):
    z = np.cos(np.radians(np.array(theta)))
    delta = gamma-1
    chiTerm = z**(2*chi-delta*(5*chi+alpha-2))
    xiTerm = np.exp(1-1./z)**(-xi*delta)
    kappaTerm = (1-np.log(z))**(-2*kappa*delta)
    c = chiTerm*xiTerm*kappaTerm

    CbyChi = (2-5*delta)*np.log(z)*c
    CbyXi = (-delta)*(1-1./z)*c
    CbyKappa = (-2*delta)*np.log(1-np.log(z))*c
    CbyVar = np.array([CbyChi, CbyXi, CbyKappa])

    sigmaCSq = 0
    for i in range(3):
        for j in range(3):
            sigmaCSq += CbyVar[i]*covM[i][j]*CbyVar[j]
    sigmaC = np.sqrt(sigmaCSq)

    return c, sigmaC


def constThenLine(x, p0, p1, p2):
    x = np.array(x)
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p2:
            y[i] = p0*x[i]+p1
        else:
            y[i] = p0*p2+p1
    return y


def constThenPowerLaw(x, p0, p1, p2):
    x = np.array(x)
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p2:
            y[i] = x[i]**p0 * p1
        else:
            y[i] = p2**p0 * p1
    return y


def constThenLineFixedSlope(x, p0, p1):
    a = -1.49
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p1:
            y[i] = a*x[i]+p0
        else:
            y[i] = a*p1+p0
    return y


def constThenPowerLawFixedExp(x, p0, p1):
    a = -1.49
    x = np.array(x)
    y = np.zeros(len(x))
    for i in range(len(x)):
        if x[i] >= p1:
            y[i] = x[i]**a * p0
        else:
            y[i] = p1**a * p0
    return y
