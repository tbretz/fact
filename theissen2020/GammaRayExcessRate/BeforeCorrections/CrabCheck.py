import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import setFigureConfig as figConfig

runsCR = pd.read_hdf("../MergedExcessInfos/CrabExcess.h5", "CrabExcess")

runsCR = runsCR[runsCR["fNight"] > 20140723]
intColumnList = ["fNight", "fRunID", "fNumBgEvts", "fNumSigEvts"]
runsCR[intColumnList] = runsCR[intColumnList].astype(int)

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["fNumBgEvts"], bins=np.linspace(0, 84, num=85)+0.5,
    alpha=1, color="tab:blue",  width=0.85,
    label=r"$N_{B}$", zorder=1+1/100
)
ax.set_xlim(0, 86)
ax.set_ylim(0, 425)
ax.set_xlabel("number of background events "+r"$N_{B}$")
ax.set_ylabel("number of runs per bin")
ax.grid(axis="y", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./NumberOfBackground.png", dpi=300)
fig.savefig("./NumberOfBackground.pdf")


figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.hist(
    runsCR["fNumSigEvts"], bins=np.linspace(0, 28, num=29)+0.5,
    alpha=1, color="tab:blue", width=0.95,
    label=r"$N_{S}$", zorder=1+1/100
)
ax.set_xlim(0, 28)
ax.set_ylim(0, 1200)
ax.set_xlabel("number of signal events "+r"$N_{S}$")
ax.set_ylabel("number of runs per bin")
ax.grid(axis="y", alpha=0.5)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./NumberOfSignal.png", dpi=300)
fig.savefig("./NumberOfSignal.pdf")

"""
    data per month
"""


def monthFromNight(fNight):
    nightStr = np.str(fNight)
    monthStr = nightStr[4:-2]
    return np.int(monthStr)


monthFromNight = np.vectorize(monthFromNight)

runsCR["Month"] = monthFromNight(runsCR["fNight"])
grpRuns = runsCR.groupby("Month")
monthlyBinned = pd.DataFrame()
monthlyBinned["N"] = grpRuns.apply(lambda group: len(group))
monthlyBinned["t_eff"] = grpRuns["fEffectiveTime_h"].apply(
    lambda group: np.sum(group)
)
monthlyBinned["order"] = np.array([5, 6, 7, 0, 1, 2, 3, 4])
monthlyBinned = monthlyBinned.sort_values("N")
monthlyBinned = monthlyBinned.reset_index()


figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.bar(
    monthlyBinned["order"], monthlyBinned["t_eff"],
    alpha=1, color="tab:blue", width=0.95,
    label="total effective\nobservation time: " +
    str(int(round(np.sum(monthlyBinned["t_eff"]), 0)))
    + r"$\,$"+"h", zorder=1+1/100
)
ax.set_xticks([0, 1, 2, 3, 4, 5, 6, 7])
ax.set_xticklabels(
    [
        "Aug", "Sep", "Oct", "Nov", "Dez", "Jan", "Feb", "Mar"
    ]
)
ax.set_ylim(0, 200)
ax.set_ylabel("effective observation time in h")
ax.grid(axis="y", alpha=0.5)
ax.legend(handlelength=0, handletextpad=0)
ax.set_axisbelow(True)
fig.tight_layout()
fig.savefig("./DataPerMonth.png", dpi=300)
fig.savefig("./DataPerMonth.pdf")
print(monthlyBinned["t_eff"])
