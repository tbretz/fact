import matplotlib.pyplot as plt
import numpy as np

import setFigureConfig as figConfig

logSize = np.linspace(1.68, 3, 300)
logSizeSpec = np.linspace(1, 3, 300)
lcLim = np.log10((904.2*logSize - 1471)*0.000137342)
specLim = 0.267*logSize - 1.684
isdcLim = np.log10((912*logSize - 1512)*0.000137324)

figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.plot(logSizeSpec, specLim, linewidth=2, ls="-.", label="spectrum cut")
ax.plot(logSize, lcLim, linewidth=2, label="lightcurve cut")
ax.plot(logSize, isdcLim, linewidth=2, ls="--", label="ISDC analysis")
ax.legend(title="gamma-hadron-separations", handlelength=3)
ax.set_ylim(-2, -0.75)
ax.set_xlim(1.4, 2.9)
ax.set_xlabel(r"$\mathrm{log}_{10}(\mathrm{Size})$")
ax.set_ylabel(r"$\mathrm{log}_{10}(\mathrm{Area})$")
fig.tight_layout()
fig.savefig("allSizeAreaCuts.png", dpi=300)
fig.savefig("allSizeAreaCuts.pdf", dpi=300)


figConfig.halfWidth()
fig = plt.figure()
ax = fig.gca()
ax.plot(
    logSize, lcLim, lw=2, color="tab:green",
    label="gamma-hadron-separation"
)
ax.axvline(x=1.5, lw=2)
ax.axvline(x=1.67, lw=2, linestyle="--")
ax.arrow(
    1.505, -1.25, 0.15, 0, length_includes_head=True, zorder=20,
    edgecolor="tab:blue", facecolor="tab:blue", lw=2,
    head_width=0.025, head_length=0.015
)
ax.fill_between(
    logSize, lcLim, -2.1, alpha=0.4, color="tab:green",
    label=r"$\gamma$ parameter space"
)
ax.text(
    1.54, -1.35, r"$\Delta Th$", color="tab:blue",
    fontstyle="oblique",
)
ax.legend(loc=1)
ax.set_ylim(-2, -0.5)
ax.set_xlim(1.4, 2.7)
ax.set_xlabel(r"$\mathrm{log}_{10}(\mathrm{Size})$")
ax.set_ylabel(r"$\mathrm{log}_{10}(\mathrm{Area})$")
ax.grid(axis="both", alpha=0.5)
fig.tight_layout()
fig.savefig("LowTh.png", dpi=300)
fig.savefig("LowTh.pdf")


fig = plt.figure()
ax = fig.gca()
ax.plot(
    logSize, lcLim, lw=2, color="tab:green",
    label="gamma-hadron-separation"
)
ax.axvline(x=2.3, lw=2)
ax.axvline(x=2.47, lw=2, linestyle="--")
ax.arrow(
    2.305, -1.25, 0.15, 0, length_includes_head=True, zorder=20,
    edgecolor="tab:blue", facecolor="tab:blue", lw=2,
    head_width=0.025, head_length=0.015
)
ax.fill_between(
    logSize, lcLim, -2.1,
    where=logSize > 2.47,
    alpha=0.4, color="tab:green",
    label=r"$\gamma$ parameter space"
)
ax.fill_between(
    logSize, lcLim, -2.1,
    where=(logSize >= 2.3)*(logSize <= 2.47),
    alpha=0.4, color="tab:purple",
    label=r"lost $\gamma$ paramter space"
)
ax.text(
    2.34, -1.35, r"$\Delta Th$", color="tab:blue",
    fontstyle="oblique",
)
ax.legend(loc=2)
ax.set_ylim(-2, -0.5)
ax.set_xlim(1.4, 2.7)
ax.set_xlabel(r"$\mathrm{log}_{10}(\mathrm{Size})$")
ax.set_ylabel(r"$\mathrm{log}_{10}(\mathrm{Area})$")
ax.grid(axis="both", alpha=0.5)
fig.tight_layout()
fig.savefig("HighTh.png", dpi=300)
fig.savefig("HighTh.pdf")
