import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from peripylib import figures, query_fact, model
from scipy.stats import norm, gaussian_kde
from scipy.optimize import curve_fit
import seaborn as sns

def th1000_rate(row):
	# Returns TH1000 rate in Hz
	rate = row['num1000'] / (row['duration'] * row['effective_on'])
	return rate


def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

# Preparation
# ------------
# Get latest TH1000 data
data, _ = query_fact.query_fact(source='all', database='ISDC', apply_correction_to=[], verbose=True,
							 compute_nightly=False, cut_threshold=False, cut_zenith=False)

# -------------
# Only take sources with more than 700h of observation time
data = data[data['source_key'].isin([1, 2, 5, 3, 7, 12, 16, 17])]

# -----------
# Calculate run artificial trigger rates with fnum1000 / (duration * effective ontime)
data['th1000'] = data.apply(th1000_rate, axis=1)

# ----------
# Cut negative rates and rates over 5Hz, these
# are caused by false ontimes or LIDAR
data = data[data.th1000 < 5]
data = data[data.th1000 > 0]

# ----------------
#
# Calculate the dependency of the CR rate on the zenith angle
#
# ----------------

# ------------
# FIGURE Artificial Rates vs Zenith Angle
if True:
	# Make density plot. For this, combine all data in x and y arrays and stack them afterwards
	# Following https://stackoverflow.com/questions/20105364/how-can-i-make-a-scatter-plot-colored-by-density-in-matplotlib

	fig, ax = figures.newfig()
	# If we plot all at one, we just see three big blobs where the sources culminate. Instead, try plotting each degree
	# after the other, and normalized them to the same value. We should get a smoother image
	for deg in np.linspace(0, 92,  47):
		data_bin = data[(deg < (data['zdmin'] + data['zdmax']) / 2) & ((data['zdmin'] + data['zdmax']) / 2 < deg + 4)]
		if data_bin.empty:
			# No data taken at this zenith angle
			continue

		zenith = (data_bin['zdmin'].as_matrix() + data_bin['zdmax'].as_matrix()) / 2
		rate = data_bin['th1000'].as_matrix()

		# calculate the point density
		xy = np.vstack([zenith, rate])
		try:
			z = gaussian_kde(xy)(xy)
		except ValueError: # for zenith 82-83, x*y is NaN as there is only one point. continue
			print('NaN in gaussian_kde')
			continue

		# Sort the points by density, so that the densest points are plotted last
		idx = z.argsort()
		zenith, rate, z = zenith[idx], rate[idx], z[idx]

		sc = ax.scatter(zenith, rate, c=z, cmap='viridis', s=6, edgecolor='')

	# Now adding the colorbar
	cbar = fig.colorbar(sc, orientation='horizontal', fraction=0.046)
	cbar.ax.set_xticklabels(['Low run count','','','','','','', 'High run count'])  # horizontal colorbar

	ax.set(xlabel='Zenith Angle / deg', ylabel='Artificial Trigger Rate / Hz', ylim=(0., 4), xlim=(0, 90))
	figures.savefig('artificial_rate_vs_zenithangle', png=True, dpi=400)


# -------------
# Bin Runs by zenith angle

bin_width = 4  # in degree
bins = np.arange(4, 88., bin_width)
inds = np.digitize((data['zdmin']+data['zdmax'])/2, bins)

# For each bin plot distribution of log(N) vs Rate. Should be a Gaussian peak around 3Hz and a dropoff to lower rates due to bad weather
# Fit the Gaussian peak to determine best value for each bin, instead of caluclating mean.

# List to save the kde maxima, gauss means, which could be used as alternative best values. Maxima are taken from sns.distplot
kde_maxima, fwhm = [], []

for i in range(1, len(bins)):
	binned_data = data[inds == i]

	if binned_data.empty:
		kde_maxima.append(0)
		fwhm.append(0)
		continue

	# ------------
	# Make figure and plot

	# Need to do this to get KDE values. Saving plots is optional below
	fig, ax = figures.newfig()

	sns.distplot(binned_data['th1000'], bins=100, norm_hist=True,
		         label=r' {:.0f}$^\circ \leq \theta \leq$ {:.0f}$^\circ$, N={:.0f}'.format(bins[i-1], bins[i], len(binned_data['th1000'])))

	# Get maximum of kde. Using this as best value
	kde = ax.lines[0]
	kde_max = kde.get_xdata()[np.argmax(kde.get_ydata())]
	kde_maxima.append(kde_max)

	# Plot kde_max position
	ax.plot([kde_max, kde_max], [0, kde.get_ydata().max()], '-', lw=1, label='KDE Maximum', color=figures.colors(3))
	ax.annotate('{:.2f}'.format(kde_max), [kde_max, kde.get_ydata().max()*1.02], color=sns.color_palette()[1], bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))

	# Overlay lower FWHM on plot, this will be used as error in the fit
	kde_50 = kde.get_ydata().max()*0.5
	closest_to_limit = find_nearest(kde.get_ydata()[kde.get_xdata() > kde_max], kde_50)
	limit = kde.get_xdata()[np.where(kde.get_ydata() == closest_to_limit)][0]
	fwhm.append(kde_max - limit)

	ax.plot([kde_max, limit], [kde_50, kde_50], '--', lw=1, label='KDE FWHM', color=figures.colors(3))
	ax.annotate('{:.2f}'.format(limit-kde_max), [limit+0.05, kde_50*1.05], color=sns.color_palette()[1], bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))

	# Figure set-up and save
	ax.set(xlabel='Rate / Hz', ylabel='Density Function')#, xlim=(0, 1.5*limit))
	ax.legend()

	# ----------
	# FIGURE th1000_kde_bin
	if True:
		if i == 1:
			figures.savefig('th1000_kde_bin')
	plt.close()

# ------------
# Fit model function to KDE maxima of the zenith angle bins
fit_bins = np.linspace(0., 89., 90)
bins_centered = np.array([(bins[i] + bins[i-1])/2 for i in range(1,len(bins))])
bins = bins_centered

if True:
	popt, pcov = curve_fit(model.fit_model_proton, bins_centered, kde_maxima/max(kde_maxima), sigma=fwhm/max(kde_maxima), p0=[1.62084951, 0.11117925, 1.34746444], method='dogbox')
else:
	popt = [1.75267265, 0.33430478, 1.34746444]
print('Fit parameters', popt)

# ----------
# FIGURE Th1000 Rates vs Zenith angle with fit function on top
if True:
	fig, ax = figures.newfig()
	for deg in np.linspace(4, 92, 23):
		data_bin = data[(deg < (data['zdmin'].as_matrix() + data['zdmax']) / 2) & ((data['zdmin'].as_matrix() + data['zdmax']) / 2 < deg + 4)]
		if data_bin.empty:
			continue
		zenith = (data_bin['zdmin'].as_matrix() + data_bin['zdmax'].as_matrix()) / 2
		rate = data_bin['th1000'].as_matrix()

		# calculate the point density
		xy = np.vstack([zenith, rate])
		try:
			z = gaussian_kde(xy)(xy)
		except ValueError: # for zenith 82-83, x*y is NaN as there is only one point. continue
			print('NaN in gaussian_kde')
			continue

		# Sort the points by density, so that the densest points are plotted last
		idx = z.argsort()
		zenith, rate, z = zenith[idx], rate[idx], z[idx]
		sc = ax.scatter(zenith, rate, c=z, cmap='viridis', s=6, edgecolor='')


	# Add fit plot
	plot_bins = np.arange(0., 89., 0.1)
	ax.plot(plot_bins, model.fit_model_proton(plot_bins, *popt)*max(kde_maxima), label='Model Fit')
	ax.errorbar(bins_centered, kde_maxima, yerr=fwhm, ls='', marker='+', label='KDE Maxima')
	plt.legend(frameon=True)

	# Now adding the colorbar
	ax.set(xlabel='Zenith Angle / deg', ylabel='Artificial Trigger Rate / Hz', ylim=(0., 4), xlim=(0, 90))

	cbar = fig.colorbar(sc, orientation='horizontal', fraction=0.046)
	cbar.ax.set_xticklabels(['Low run count','','','','','','', 'High run count'])  # horizontal colorbar

	figures.savefig('artificial_rate_model_fit', png=True, dpi=400)

# ----------
# Calculate corrected and nightly rates

# Some rates at HZA explode as the fit function approaches zero
data = data[data['zdmean'] < 75]


def correct_dataframe(row, model_parameters, column='excevts'):
	'''
	Used to change the excess events in the dataframe
	'''
	norm = model.fit_model_proton(0, *model_parameters)
	return row[column] / model.fit_model_proton(row['zdmean'], *model_parameters) * norm

if False: # This is slow
	data['th1000_corrected'] = data['th1000']
	data['th1000_corrected'] = data.apply(correct_dataframe, args=(popt, 'th1000',), axis=1)
	data.to_csv('th1000_corrected_data.csv')
else:
	data = pd.read_csv('th1000_corrected_data.csv')

# Identify bad weather runs by fitting the peak of the corrected rate's KDE with a Gaussian
if True:
	# Plot distribution of normalized rates, take KDE lower limit like above as bad weather limit
	fig, ax = figures.newfig()
	y = data.th1000_corrected
	y = y[y < 5]
	y = y[y > 0]

	# Plot log
	sns.distplot(y, kde=True, bins=128, label='Density Function', kde_kws={'gridsize': 256})

	# Get maximum of kde. Could use this as best value
	kde = ax.lines[0]
	kde_max = kde.get_xdata()[np.argmax(kde.get_ydata())]

	# Plot kde_max position
	ax.annotate('{:.2f}'.format(kde_max), (kde_max, kde.get_ydata().max()*1.065), color=figures.colors(3), bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))

	# Plot 10% of KDE Max y-value. Lower x-value here is the bad weather limit
	kde_25 = kde.get_ydata().max()*0.20

	# Plot Gaussian, use value where Gaussian and KDE diverge as bad weather value
	# Fit Gaussian to (0.5 kde_max, 1.5 kde_max) data

	kde_fwhm = kde.get_xdata()[np.where(kde.get_ydata() == find_nearest(kde.get_ydata(), kde.get_ydata().max()*0.09 ))][0]

	gaussian_range = (kde_fwhm, kde_max + (kde_max - kde_fwhm))
	ranged_y = y[(y > gaussian_range[0]) & (y < gaussian_range[1])]
	(mu, sigma) = norm.fit(ranged_y)
	print(mu/np.sqrt(len(ranged_y)), sigma/np.sqrt(len(ranged_y)))
	# Plot Gaussian Fit
	plt.plot(kde.get_xdata(), norm.pdf(kde.get_xdata(), mu, sigma), '-', label='Gaussian Fit', color=figures.colors(3))
	gaussian = ax.lines[1]
	gaussian_max = gaussian.get_xdata()[np.argmax(gaussian.get_ydata())]

	# Get points where Gaussian fit and kde intersect. The first one is our bad weather limit
	idx = np.argwhere(np.diff(np.sign(gaussian.get_ydata() - kde.get_ydata())) != 0)[0]

	bad_weather_limit = kde.get_xdata()[idx][0]
	ax.annotate('{:.2f}'.format(bad_weather_limit), (kde.get_xdata()[idx]*0.88, kde.get_ydata()[idx]*1.01), color=figures.colors(3), bbox=dict(facecolor='white', alpha=0.5, edgecolor='white', pad=0))
	ax.plot([bad_weather_limit, bad_weather_limit], [0, kde.get_ydata()[idx]], label='Lower Rate Limit', color=figures.colors(3), ls='--')

	plt.legend(loc='upper left')
	ax.set(xlabel='Rate / Hz', xlim=(-0.5, 4.), ylim=(0, 1.5), ylabel='Density Function')
	figures.savefig('th1000_rates_kde_bad_weather_limit', png=True)

print('Bad Weather Run Limit:', bad_weather_limit)

# ----------
# FIGURE Th1000 Rates vs Zenith angle with fit function on top
if True:
	fig, ax = figures.newfig()
	for deg in np.linspace(4, 92, 23):
		data_bin = data[(deg < (data['zdmin'].as_matrix() + data['zdmax']) / 2) & ((data['zdmin'].as_matrix() + data['zdmax']) / 2 < deg + 4)]
		if data_bin.empty:
			continue
		zenith = (data_bin['zdmin'].as_matrix() + data_bin['zdmax'].as_matrix()) / 2
		rate = data_bin['th1000_corrected'].as_matrix()

		# calculate the point density
		xy = np.vstack([zenith, rate])
		try:
			z = gaussian_kde(xy)(xy)
		except ValueError: # for zenith 82-83, x*y is NaN as there is only one point. continue
			print('NaN in gaussian_kde')
			continue

		# Sort the points by density, so that the densest points are plotted last
		idx = z.argsort()
		zenith, rate, z = zenith[idx], rate[idx], z[idx]
		sc = ax.scatter(zenith, rate, c=z, cmap='viridis', s=6, edgecolor='')

	# Now adding the colorbar
	ax.set(xlabel='Zenith Angle / deg', ylabel='Artificial Trigger Rate / Hz', ylim=(0., 4), xlim=(0, 90))

	cbar = fig.colorbar(sc, orientation='horizontal', fraction=0.046)
	cbar.ax.set_xticklabels(['Low run count','','','','','','', 'High run count'])  # horizontal colorbar

	figures.savefig('artificial_corrected_rate', png=True, dpi=400)
