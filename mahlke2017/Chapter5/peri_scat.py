#!/usr/bin/python
# -*- coding: UTF-8 -*-
from astroML.time_series import lomb_scargle
import matplotlib.pyplot as plt
from peripylib import figures
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import chi2
import seaborn as sns
from stingray.simulator import simulator
import sys


# --------
# Simulate light curves with fixed spectral index.
# Show that the periodograms of a random light curve
# scatter following a Chi^2 distribution, while the averaged periodogram of 1000 light curves
# is close to the true underlying PSD

def power_law(x, A):
    return A * np.power(x, -2)

# --------
# Initiate simulator object using Stingray package
sim = simulator.Simulator(N=1000, mean=20, rms=0.5, dt=1, red_noise=1000, random_state=19)

ls_powers = [] # List to store the calculated Lomb-Scargle periodograms in, to
               # later compute statistics

num_lcs = 1000       # simulate 1000 light curves
spectral_index = 2   # spectral index of the underlying PSD

# Temporarily change some plot settings to remove grid lines and add ticks
with plt.rc_context({'xtick.major.size'     : 4,      # major tick size in points
                     'xtick.minor.size'     : 2,      # minor tick size in points
                     'ytick.major.size'     : 4,      # major tick size in points
                     'ytick.minor.size'     : 2,      # minor tick size in points
                     'axes.labelpad'        : 0.01,
                    }):
    # -----
    # Set up light curve plot

    fig, ax = figures.newfig(ratio=0.33)

    # -------
    # Simulation of lcs
    # The first light curve is plotted in the first plot,
    # and their periodograms are added to the second
    # The third plot shows the distribution of the periodogram powers
    # of one frequency bin with an overlaid Chi^2_2 distribution

    for i in range(num_lcs):

        # Short status update
        sys.stdout.write('\r')
        sys.stdout.write('{:d}/{:.0f}'.format(i+1, num_lcs))
        sys.stdout.flush()

        # Simulate the light-curve
        lc = sim.simulate(spectral_index)

        # Set up period grid the LS periodogram is evaluated on
        pmin = 5
        pmax = 1000
        N_periods = 5e2
        period = 10 ** np.linspace(np.log10(pmin), np.log10(pmax), N_periods)
        omega = 2 * np.pi / period

        # Compute Lomb-Scarlge periodogram, no error given
        power = lomb_scargle(lc.time, lc.counts, 1, omega, generalized=True)

        # Append to ls_powers list for statistics
        ls_powers.append(power)

        # Plot first LS and periodogram
        if i == 0:
            ax.plot(lc.time, lc.counts, c='gray')

    # Finish and save light curve plot
    ax.set(xlabel='Time', ylabel='Flux')
     # We don't need to know the order of magnitudes
    ax.get_xaxis().set_ticklabels(len(ax.get_xaxis().get_ticklabels())*'')
    ax.get_yaxis().set_ticklabels([])
    ax.grid(False)
    figures.savefig('simulated_lcs', pgf=True, dpi=400)

    # -----
    # Now calculate mean periodogram and
    # add plot. Also over plot the true underlying PSD
    fig, ax = figures.newfig(cols=2, ratio=0.33)

    mean_ls = np.mean(ls_powers, axis=0)
    popt, pcov = curve_fit(power_law, 1/period, mean_ls)

    for i, power in enumerate(ls_powers[:1]):
        ax[0].plot(1/period, power, ls='-', marker='', ms='2', c='gray')

    # Fit true PSD
    ax[0].plot(1/period, mean_ls, label=r'$\overline{P}(f_i)$', c=figures.colors(0), lw=1.25)
    ax[0].plot(1/period, power_law(1/period, *popt), label=r'$\mathcal{P}(f)\propto f^{-2}$', c=figures.colors(3), lw=1.25)

    # Add third plot. We use the middle frequency bin of the periodogram
    freq_bin = int(N_periods/2)
    dist = sns.distplot([power[freq_bin] for power in ls_powers], norm_hist=True, color=figures.colors(0), kde=False, ax=ax[1])

   # Add the chi2 distribution
    dof = 2
    x = np.arange(0., 0.04, 0.001)
    ax[1].plot(x, chi2.pdf(x, dof, scale=1/2/max([h.get_height() for h in dist.patches])), lw=0.75, alpha=0.6, label='chi2 pdf', ls='--', color=figures.colors(3))

    # ----
    # Finish figure

    ax[0].set(xlabel='Frequency', ylabel='Power', xscale='log', yscale='log')
    ax[1].set(xlabel='Power', ylabel='Density Function')
    ax[0].legend(frameon=False, loc='upper right')


    for axis in ax:
        axis.get_xaxis().set_ticklabels([])
        axis.get_yaxis().set_ticklabels([])
        axis.grid(False)

    figures.savefig('periodogram_stats', pgf=True, dpi=400)
