import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from peripylib import figures, query_fact
from scipy.stats import norm, gaussian_kde
from scipy.optimize import curve_fit
import seaborn as sns

# -------------------------------------
# Plot the threshold-dependence of the
# gamma-ray flux of the Crab Nebula


# -------------------------------------
# Start with plotting RunRates versus Threshold

# Prepatration
# ------------
# Get latest TH1000 data
source = 'Crab'
data = query_fact.query_fact(source=source, database='ISDC', cut_threshold=False)
print('\nRead in Crab data: ', data.shape)

# Make sure there is no zenith angle bias
data = data[(data['zdmean'] < 30.)]

# Short status update
print('Number of Runs in data: ', len(data['run_id']))

# Drop rows where evts is empty or infinite
data = data.replace([np.inf, -np.inf], np.nan)
data.dropna(subset=['excevts', 'duration', 'start', 'stop', 'zdmin', 'zdmax', 'thmean'], inplace=True)

# -----------
# Plot ExcEvts versus Threshold, binning by 50 DAC counts
bin_width = 50 # in DAC countes
bins = np.arange(200, 1300., bin_width)
inds = np.digitize(data['thmean'], bins)

# For each threshold group, caulcate rate of excevts
binned_rates, binned_errors = [], []

for i in range(1, len(bins)):

    binned_data = data[inds == i]
    # If no data was taken for these zenith distances, append 0 to arrays and continue
    if binned_data.empty:
        binned_rates.append(0)
        binned_errors.append(0)
        continue

    # Calculate binned event rate
    evts = binned_data['excevts_corr'].as_matrix()  # converts to numpy array
    durations = binned_data.duration * binned_data.effective_on

    rate = sum(evts)*3600. / sum(durations)
    correction = binned_data.excevts_corr/binned_data.excevts
    correction = correction.fillna(1.)
    error = np.sqrt(np.sqrt((sum(binned_data['bgevts']*correction))**2) + np.sqrt((sum(binned_data['sigevts']*correction))**2)  ) / sum(durations) * 3600

    # Drop rows where error is zero
    if error == 0.0:
        binned_rates.append(0)
        binned_errors.append(0)
        continue

    binned_rates.append(rate)
    binned_errors.append(error)


if True:
    # Make plot
    fig, ax = figures.newfig()

    bins_centered = np.array([(bins[i] + bins[i-1])/2 for i in range(1,len(bins))])
    bins = bins_centered

    ax.errorbar(bins, binned_rates, yerr=binned_errors, ls='', marker='x')
    ax.plot([650, 650], [0, 35], ls='--', color=figures.colors(3), alpha=0.5)
    ax.set(xlabel='Threshold / DAC Counts', ylabel=r'Excess Events Rate / h$^{-1}$')
    plt.legend(frameon=True)
    figures.savefig('th_excevts', pgf=True, dpi=300)

