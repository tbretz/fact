from stingray import Lightcurve
from stingray.simulator import simulator
import numpy as np
import pandas as pd
import sys
import matplotlib.pyplot as plt
from astropy.timeseries import LombScargle
from scipy.optimize import curve_fit
from consoleOutput import printProgressBar

# set random seed
np.random.seed(123)

reSample = True


def power_law(x, A, alpha):
    return A * x**(-alpha)


# Load Crab Lightcurve
lcMrk501 = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/Mrk501From2013.h5",
    "Mrk501From2013"
)
lcMrk501 = lcMrk501[lcMrk501["RExcErrCorr"] /
                    lcMrk501["RExcCorr"] < 2]

t = lcMrk501["mjd"]
N = int(np.floor(t.max()) - np.floor(t.min()))
mean = np.mean(lcMrk501["RExcCorr"])
rms = 0.7

red_noise = 100
num_lcs = 1000

sim = simulator.Simulator(
    N=N, mean=mean, rms=rms, dt=1, red_noise=red_noise
)

fitted_spec_indices = []
uneven_errs = []
simulated_range = np.linspace(0.5, 2.4, num=50)

if reSample:
    for i in range(len(simulated_range)):
        printProgressBar(i, len(simulated_range)-1)
        spectral_index = simulated_range[i]
        ls_powers = []
        for i in range(num_lcs):

            # Simulate the light-curve
            lc = sim.simulate(spectral_index)

            # Set lightcurve times to FACT timestamps
            # to get the same spectral window
            lc.time = lc.time - lc.time[0] + int(np.floor(t.min()))
            lc.counts = lc.counts[np.isin(lc.time, np.floor(t))]
            lc.time = lc.time[np.isin(lc.time, np.floor(t))]

            f = np.linspace(1e-3, 0.5, 10000)
            lsw = LombScargle(
                lc.time, lc.counts, center_data=False, fit_mean=False
            )
            ls_powers.append(lsw.power(f))

        meanLSP = np.mean(ls_powers, axis=0)
        varLSP = np.var(ls_powers, axis=0)

        popt, pcov = curve_fit(
            power_law, f, meanLSP, sigma=abs(meanLSP-varLSP)
        )

        fitted_spec_indices.append(popt[1])
        uneven_errs.append(np.sqrt(np.diag(pcov))[1])

    fitted_spec_indices = np.array(fitted_spec_indices)
    uneven_errs = np.array(uneven_errs)
    np.save("./fittedIndsMrk501_rms07.npy", fitted_spec_indices)
    np.save("./unevenErrsMrk501_rms07.npy", uneven_errs)

else:
    fitted_spec_indices = np.load("./fittedIndsMrk501_rms07.npy")
    uneven_errs = np.load("./unevenErrsMrk501_rms07.npy")

fig = plt.figure()
ax = fig.gca()
ax.errorbar(
    simulated_range, fitted_spec_indices, yerr=uneven_errs,
    label='FACT Uneven Sampling', fmt="o",
    color="tab:blue"
)
ax.set_xlabel("Index of Simulated LC")
ax.set_ylabel("Index of LS Periodogram")
ax.plot(
    simulated_range, simulated_range, ls="--", color="k"
)
ax.legend()
fig.savefig("./SimulationIndexMrk501_rms07.png", dpi=300)
fig.savefig("./SimulationIndexMrk501_rms07.pdf")
