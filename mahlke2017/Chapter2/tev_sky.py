import ephem
import matplotlib.pyplot as plt
from mypylib import figures
import numpy as np
import pandas as pd


# Define two helper functions
def ra_dec_to_mwd(RA, Dec):
    '''
    RA, Dec are the coordinates in degrees
    '''
    ra_origin = 180 # We want 180° RA in the middle of the plot
    ra = np.remainder(RA + 360 - ra_origin, 360) # shift RA values
    ra = np.array(ra)
    ra[ra > 180] -= 360    # scale conversion to [-180, 180]
    ra *= -1    # Using astronomical convention: East to the left
    return np.radians(ra), np.radians(Dec)

def plot_source(RA, dec, name, color='#f46d43', s=5, marker='o'):
    # Converts source coordinates to mollweide projection,
    # annotates the dot with the source's name
    convert_ra = lambda x, y, z: (float(x) + float(y)/60 + float(z)/3600) * 15
    convert_dec = lambda x, y, z: float(x) + float(y)/60 + float(z)/3600
    RA = convert_ra(*RA.split(' '))
    dec = convert_dec(*dec.split(' '))

    ra, dec = ra_dec_to_mwd(RA, dec)
    ax.scatter(ra, dec, color=color, s=s, marker=marker)

def plot_galactic_plane(): # From http://balbuceosastropy.blogspot.de/2013/09/the-mollweide-projection.html
    lon_array = np.arange(0,359)
    lat = 0.
    eq_array = np.zeros((359,2))
    for lon in lon_array:
        ga = ephem.Galactic(np.radians(lon), np.radians(lat))
        eq = ephem.Equatorial(ga)
        eq_array[lon] = np.degrees(eq.get())
    RA = eq_array[:,0]
    Dec = eq_array[:,1]
    plot_ra, plot_dec = ra_dec_to_mwd(RA, Dec)
    ax.plot(*zip(*[(ra, dec) for ra, dec in sorted(zip(plot_ra, plot_dec))]), ls='--', c='black', zorder=-100)

# Initiliaze the figure and axis instances
fig, ax = figures.newfig(projection='mollweide', ratio=0.5)

tev_cat = pd.read_csv('tev_cat.csv', sep='\t')

for ind, source in tev_cat.iterrows():
    if source.Class in ['HBL', 'IBL', 'FRI', 'Blazar', 'FSRQ', 'LBL', 'AGN']: # AGN
        plot_source(source.RA, source.DEC, source.Name, s=7, color=figures.colors(3), marker='x')
    else:
        plot_source(source.RA, source.DEC, source.Name, color=figures.colors(0))

plot_galactic_plane()

# ---------------
# Set-up the labels, colors, and sizes

# Tick labels
tick_labels = [r'330$^{\circ}$', '', r'270$^{\circ}$', '', r'210$^{\circ}$', '', r'150$^{\circ}$', '', r'90$^{\circ}$', '', r'30$^{\circ}$']
ax.set_xticklabels(tick_labels, fontsize=8)
for label in ax.get_yticklabels():
    label.set_fontsize(8)
ax.tick_params(axis='x', colors='gray')
ax.tick_params(axis='y', colors='gray')

# Axis labels
ax.set_xlabel('Right Ascension / deg', fontsize=10)
ax.set_ylabel('Declination / deg', fontsize=10)

plt.grid(True)

figures.savefig('tev_sky', dpi=600, pgf=True)