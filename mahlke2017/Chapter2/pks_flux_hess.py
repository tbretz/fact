import matplotlib.pyplot as plt
from peripylib import figures
import pandas as pd

# Plots the HESS Flux of PKS 2155-304
# Data from https://www.mpi-hd.mpg.de/hfm/HESS/pages/publications/auxiliary/ApJL664_L71.html

# Read in data
data = pd.read_csv('pks_flux_hess.csv', sep=' ', header=3)

# We want to show variability on short time scales,
# so include an 'Mintues from t0' column

data['Minutes'] = (data['MJD'] - 53944.0) * 24 * 60

fig, ax = figures.newfig(ratio=0.5)

ax.errorbar(data['Minutes'], data['Flux'] * 1e9, yerr=data['Error'] * 1e9, linestyle='', marker='.')

ax.set(xlabel='Epoch - MJD 53944.0 / min', ylabel=r'Integral Flux \textgreater\,200\,GeV / 10$^{-9}$cm$^{-2}$s$^{-1}$')

figures.savefig('pks2155_flux_hess', dpi=400, pgf=True)
