import numpy as np
import matplotlib.pyplot as plt

from Radiation_simps import Radiation
import setFigureConfig as sfc

r = Radiation()

n = 11
E = np.logspace(-10, 10, 201)

sfc.fullWidth(aspectRatio=0.6)
fig, ax = plt.subplots()
ax.set_xscale("log")
ax.set_yscale("log")

c_map = plt.get_cmap("viridis_r")
cy = c_map(np.linspace(0, 1, n))
ax.set_prop_cycle('color', cy)

for i in np.logspace(-5, 5, n):
    r.Theta_BB = i
    if np.log10(i) % 1 == 0:
        ax.plot(E, r.n_BB(E), label=r"$\Theta = 10^{%g} $" % (np.log10(i)))
    else:
        ax.plot(E, r.n_BB(E))

ax.legend(bbox_to_anchor=(1.02, 0.5), loc='center left')
ax.set_xlabel("x")
ax.set_ylabel(r"$n_\mathrm{BB}$")
ax.set_ylim(1e-27, 1e9)
ax.set_xlim(1e-11, 1e7)

plt.tight_layout()
fig.savefig("BB.pdf")
# plt.show(fig)
