import pandas as pd
import yaml
import sys
from LayerMC import Layer


def loadConfiguration(pathToConfigFile):
    """Load configuration from a yaml config file.

        Args:
            pathToConfigFile (str): Path to yaml config file.

        Returns:
            config (dict): Configuration dictionary.
    """

    if pathToConfigFile is not None:
        config = {}
        with open(pathToConfigFile, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    else:
        print(
            "Please insert a valid config file"
        )

    return config


def LayerFromYAML(pathToConfigFile):
    configs = loadConfiguration(pathToConfigFile)
    for i in range(configs["repetitions"]):
        sys.stdout.flush()
        print("\nPlasmoid Chain " + str(i+1))
        L = Layer(
            sigma=float(configs["eom"]["sigma"]),
            betaA=float(configs["eom"]["betaA"]),
            betaG=float(configs["eom"]["betaG"]),
            Asup=float(configs["eom"]["Asup"]),
            Bsup=float(configs["eom"]["Bsup"]),
            rMax=float(configs["grid"]["rMax"]),
            dtau=float(configs["grid"]["dtau"]),
            birth=bool(configs["birth"]),
            sMin=float(configs["sMin"]),
            initialDistanceFactor=float(configs["initialDistanceFactor"]),
            p0_variance=float(configs["p0_variance"]),
            merging=bool(configs["merging"]),
            boundaries=bool(configs["boundaries"])
        )
        L.initialize()
        L.calculateTime(
            tauMax=float(configs["grid"]["tauMax"]),
            dtau=float(configs["grid"]["dtau"]),
            saveBeforeBirth=bool(configs["saveBeforeBirth"]),
            printProgressEvery=int(configs["printProgressEvery"])
        )
        key = configs["save"]["key"] + "_" + str(i)
        L.saveToHDF(configs["save"]["path"], key=key)


def allPlasmoidsFromYAML(pathToConfigFile, id=0):
    configs = loadConfiguration(pathToConfigFile)
    return pd.read_hdf(
        configs["save"]["path"],
        key=configs["save"]["key"] + "_" + str(id)
    )
