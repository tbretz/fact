import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from Radiation_simps import Ne_fromFile, Ny_fromFile, fitPowerLaw,\
    plotEvolutionFromFileV
import setFigureConfig as sfc
import coloring as cl


sfc.fullWidth(aspectRatio=1)

filename = "RadData/F10_4_sigma10_105.h5"

ind = 700
eE, Ne = Ne_fromFile(filename)
yE, Ny = Ny_fromFile(filename)


fig, axe = plt.subplots()
divider = make_axes_locatable(axe)
axy = divider.append_axes("bottom", size="100%", pad=1)
axe.loglog(eE, eE**2 * Ne.iloc[ind], color="tab:blue")
axy.loglog(yE, yE**2 * Ny.iloc[ind], color="tab:green")

axe.set_ylabel(r"$\gamma^{2} n_\mathrm{e}$")
axe.set_xlabel(r"$\gamma$")
axy.set_ylabel(r"$x^{2} n_\gamma$")
axy.set_xlabel(r"$x$")
axe.grid()
axy.grid()
axe.set_title("FSRQ", loc="right")
# electron fit

# fit 1
fitRange1 = (eE > 1) & (eE < 100)
print("Fit e 1")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange1], Ne.iloc[ind].values[fitRange1],
    pivot=10, guess=[1e3, -2.17],
    ax=axe, slope=2, height=3e-4, textpos="logCenter",
    c="tab:orange"
)
 
# fit 2
fitRange2 = (eE > 115) & (eE < 370)
print("Fit e 2")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange2], Ne.iloc[ind].values[fitRange2],
    pivot=1e4, guess=[1e2, -3.17],
    ax=axe, slope=2, height=3e-4, textpos="logCenter",
    c="tab:purple"
)
# axe.axvspan(1, 500, color="tab:orange", alpha=0.3)
# fit 3
fitRange3 = (eE > 750) & (eE < 3900)
print("Fit e 3")
axe, popt, pcov = fitPowerLaw(
    eE[fitRange3], Ne.iloc[ind].values[fitRange3],
    pivot=1e4, guess=[1e2, -2.17],
    ax=axe, slope=2, height=3e-4, textpos="logCenter",
    c="tab:red"
)

axe.set_ylim(1e-6, 1e-3)
axe.set_xlim(1, 1e5)


# photon fit
axy.axvspan(1e-10, 4e-6, color="tab:blue", alpha=0.3)
axy.axvspan(6e-3, 40, color="tab:orange", alpha=0.3)
axy.axvspan(50, 250, color="tab:purple", alpha=0.3)
axy.axvspan(300, 4e3, color="tab:red", alpha=0.3)

'''
# fit 1
fitRange1 = (yE > 5e-12) & (yE < 6e-11)
print("\nFit y 1")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange1], Ny.iloc[ind].values[fitRange1],
    pivot=1e-8, guess=[1e1, 1.517],
    ax=axy, slope=2, height=2e-3, textpos="logCenter"
)

# fit 2
fitRange2 = (yE > 1e-10) & (yE < 8e-10)
print("\nFit y 2")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange2], Ny.iloc[ind].values[fitRange2],
    pivot=1e-7, guess=[1e3, -1.57],
    ax=axy, slope=2, height=1e-4, textpos="logCenter"
)
# fit 3
fitRange3 = (yE > 3e-9) & (yE < 2e-8)
print("\nFit y 3")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange3], Ny.iloc[ind].values[fitRange3],
    pivot=1e-7, guess=[1e3, -2.17],
    ax=axy, slope=2, height=2e-3, textpos="logCenter"
)
# fit 4
fitRange4 = (yE > 6e-8) & (yE < 2e-6)
print("\nFit y 4")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange4], Ny.iloc[ind].values[fitRange4],
    pivot=1e-7, guess=[1e3, -1.57],
    ax=axy, slope=2, height=5e-9, textpos="logCenter"
)
# fit 5
fitRange5 = (yE > 6e-6) & (yE < 8e-5)
print("\nFit y 5")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange5], Ny.iloc[ind].values[fitRange5],
    pivot=1e-7, guess=[1e3, -1.57],
    ax=axy, slope=2, height=2e-3, textpos="logCenter"
)
# fit 6
fitRange6 = (yE > 2e-4) & (yE < 2e-3)
print("\nFit y 6")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange6], Ny.iloc[ind].values[fitRange6],
    pivot=1e-7, guess=[1e3, -1.57],
    ax=axy, slope=2, height=5e-9, textpos="logCenter"
)
# fit 7
fitRange7 = (yE > 6e-3) & (yE < 23)
print("\nFit y 7")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange7], Ny.iloc[ind].values[fitRange7],
    pivot=1e-7, guess=[1e-3, -2.17],
    ax=axy, slope=2, height=5e-5, textpos="logCenter"
)
# fit 8
fitRange8 = (yE > 50) & (yE < 240)
print("\nFit y 8")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange8], Ny.iloc[ind].values[fitRange8],
    pivot=1e-7, guess=[1e3, -1.57],
    ax=axy, slope=2, height=1e-6, textpos="logCenter"
)
# fit 9
fitRange9 = (yE > 260) & (yE < 4000)
print("\nFit y 9")
axy, popt, pcov = fitPowerLaw(
    yE[fitRange9], Ny.iloc[ind].values[fitRange9],
    pivot=1e3, guess=[1e-4, -3.57],
    ax=axy, slope=2, height=5e-8, textpos="logCenter"
)
'''


# set limits
axy.set_xlim(1e-12, 1e4)
axy.set_ylim(1e-9, 1e-2)

fig.tight_layout()
fig.savefig("F10_ResultsPlot_max.pdf")
