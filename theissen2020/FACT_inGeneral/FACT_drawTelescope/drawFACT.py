import matplotlib.pyplot as plt
import matplotlib.patches as pats
import numpy as np

positions = np.loadtxt("./pixelMapping.txt", delimiter=",")
fig = plt.figure(figsize=(10, 10))
ax = fig.gca()

for i in range(len(positions)):
    hexPos = (positions[i][1], positions[i][2])
    #hexColor = (0, 0.2+np.random.random()/10, 0.9-+np.random.random()/10)
    random = np.random.random()/10
    hexColor = (0+random, 0+random, 0+random)
    hexagon = pats.RegularPolygon(
        hexPos,
        6, radius=0.06, color=hexColor)
    ax.add_patch(hexagon)

r = 0.6
theta = np.radians(np.array([0, 60, 120, 180, 240, 300]))
for i in range(len(theta)):
    wobblePos = (r*np.cos(theta[i])+0.12, r*np.sin(theta[i]))
    if i == 1:
        circleColor = "tab:red"
    else:
        circleColor = "tab:grey"
    circle = pats.Circle(
        wobblePos, radius=np.sqrt(0.028), color=circleColor, alpha=0.4
    )
    ax.add_patch(circle)
    ax.plot(
        wobblePos[0], wobblePos[1], color=circleColor, marker="x",
        markersize=7, mew=3
    )

ax.set_xlim(-2.4, 2.4)
ax.set_ylim(-2.4, 2.4)
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)
fig.tight_layout()
plt.savefig("./FACTWobble.png", dpi=300)
plt.savefig("./FACTWobble.pdf")
