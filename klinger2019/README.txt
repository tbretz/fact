Important:
1) Phython 3 is used
2) Lib folder is added to python directiory (e.g. in .bashrc)
    export PYTHONPATH="${PYTHONPATH}:/my/other/path/FACT/Lib"
    export PYTHONPATH="${PYTHONPATH}:/my/other/path/MagneticReconnectionModel/Lib"
    
    make sure you have an __init__.py in the Lib folder



FACT:
-----
The worklfow has been the following:
- download the MCs from database using SQL-files in MonteCarlos/data
- calculate and save models to reweight the MCs using the files in MonteCarlos/
    reweighting_energy, reweighting_impact and reweight_Zd
- calculate 2D trigger efficiency plots in TriggerEfficiency

- calculate 1D histograms using CutEfficiencies/
    EventsMChistograms.py and OriginalMChistograms.py
-> plot cut efficiencies with files in respective folder

- calculate quality cut histograms with CutEfficiencies/EventsMChistograms_QC.py
-> plot quality cut efficiencies with CutEfficiencies/QualityCuts/QC_plots.py

+ DrawFACT programs can be used to create example event plots
-> exampleEvent creates a conceptual image
-> exampleEvent creates a conceptual timing image
-> testEvent_Hillas creates a conceptual image useful to explain Hillas'
    parameters

MagneticReconnectionModel:
--------------------------
The worklfow has been in general:
- for readability a yaml file is created for each simulation containing the
    parameters
- additionally, a python script contains the details of
    - how it is actually executed
    - plotting the results
- results are saved to hdf5 file and plotted to pdf/png

1) the MC approach
- implemented in LayerMC, PlasmoidStatistics; plots from Plotting

2) the Radiation Modelling + Boosting to the observer
- Radiation_simps contains the radiation modelling (solving of dn/dt)
- RadiationForPlasmoid contins the additional boundary conditions for a plasmoid
- calcPlasmoids reads a yaml file and calls both of the above
- once the radiation modelling is saved to one file, it's not calculated again
  but loaded from the file
  -> this can be used to calculate the lightcurve of many plasmoids with 
     calcTotalLightcurve (saved to *.npy)

- B10_4_0.5 -> 4th realization with BLLac parmeters and additional time
  (t_add=0.5)