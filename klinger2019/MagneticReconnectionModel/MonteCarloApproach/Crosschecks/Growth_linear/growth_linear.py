import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from LayerMC import Layer
from readYAML import loadConfiguration

'''

check, whether a single particle's size growths linear in time

'''

calc = True
plot = True

configs = loadConfiguration("growth_linear.yaml")

if calc:

    L = Layer(
        sigma=float(configs["eom"]["sigma"]),
        betaA=float(configs["eom"]["betaA"]),
        betaG=float(configs["eom"]["betaG"]),
        Asup=float(configs["eom"]["Asup"]),
        Bsup=float(configs["eom"]["Bsup"]),
        rMax=float(configs["grid"]["rMax"]),
        dtau=float(configs["grid"]["dtau"]),
        birth=bool(configs["birth"]),
        sMin=float(configs["sMin"]),
        initialDistanceFactor=float(configs["initialDistanceFactor"]),
        p0_variance=float(configs["p0_variance"]),
        merging=bool(configs["merging"]),
        boundaries=bool(configs["boundaries"])
    )
    newPlasmoid = pd.DataFrame(
        {
            "r": [0.],
            "s": [L.sMin],
            "beta": [0.]
        },
        index=[1]
    )
    L.Plasmoids.append(newPlasmoid)
    L.initalValues.loc[1] = [0., 0.]
    L.times.append(0.)
    L.NTotal = 1
    L.calculateTime(
        tauMax=float(configs["grid"]["tauMax"]),
        dtau=float(configs["grid"]["dtau"]),
        saveBeforeBirth=bool(configs["saveBeforeBirth"]),
        printProgressEvery=int(configs["printProgressEvery"])
    )

    # for i in range(2000):
    #     if not i % 50:
    #         print(i)
    #     L.calcNextStep()

    L.saveToHDF(configs["save"]["path"], key=configs["save"]["key"])

if plot:
    # load plasmoids
    allPlasmoids = pd.read_hdf(
        configs["save"]["path"],
        key=configs["save"]["key"]
    )

    sns.set(style="ticks")
    figS, ax = plt.subplots()
    # plot MC curve
    s0 = allPlasmoids["s"].loc[0]
    ax.plot(
        allPlasmoids["tau"], allPlasmoids["s"] - s0,
        label="Monte Carlo"
    )
    # plot analytic solution
    betaG = float(configs["eom"]["betaG"])

    # analytic solution = linear growth
    def s_ana(tau):
        return betaG * tau

    ax.plot(
        [10**-3, 1.1], [s_ana(10**-3), s_ana(1.1)],
        linestyle=":", c="red", label="analytic"
    )
    ax.set_ylabel(r"$\frac{w-w_{min}}{L}$", rotation=0)
    ax.set_xlabel(r"$\frac{c}{L}(t-t_0)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylim(5e-5, 0.3)
    ax.set_xlim(5e-4, 2.5)
    ax.legend()
    figS.tight_layout()
    figS.savefig("growth_linear.pdf")
