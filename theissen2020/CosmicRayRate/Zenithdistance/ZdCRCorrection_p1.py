import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import gaussian_kde as gkde

import DataFrameBinning as dfb
import setFigureConfig as figConfig
from LibFunctions import weightedMean, findFWHM
from rootRoutines import dFrameFromRoot

# define output pdf for all plots produced by this script
outputPDF = PdfPages("./ZdCRRates.pdf")

# path to root file with artificial trigger rates information
runsCR = dFrameFromRoot(
    "../ArtificialTriggerRates.root",
    keys=["sql"], prints=False
)

# sources that are taken into account for this investigation are the five
# most observed ones
sources = {
    "Mrk421": 1,
    "Mrk501": 2,
    "1ES 2344+51.4": 3,
    "Crab": 5,
    "1ES 1959+650": 7
}
runsCR = runsCR[runsCR["fSourceKey"].isin(list(sources.values()))]

# minimum Quality Cuts
runsCR = runsCR[runsCR["fNight"] > 20140101]
runsCR = runsCR[runsCR["fEffectiveOn"] > 0.9]
runsCR = runsCR[runsCR["fRunDuration"] > 297]
runsCR = runsCR[runsCR["fTNGDust"] < 1]
runsCR = runsCR[runsCR["fR750Cor"] < 1000]
runsCR = runsCR[runsCR["fThresholdMinSet"] < 950]

# calculation of addional quantities
runsCR["RAWTime"] = (runsCR["fRunStart"] + runsCR["fRunStop"])/2
runsCR["Time"] = pd.to_datetime(runsCR["RAWTime"], unit="s")
runsCR.sort_values("Time")

runsCR["RCR"] = 60*runsCR["fNumThreshold750"] / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

runsCR["RCR_err"] = 60*np.sqrt(runsCR["fNumThreshold750"]) / \
    runsCR["fEffectiveOn"] / runsCR["fRunDuration"]

sourceGroups = runsCR.groupby("fSourceKey")
sourceCulminations = {}
for key, group in sourceGroups:
    for sourceName, sourceKey in sources.items():
        if sourceKey == key:
            sourceCulminations[sourceName] = np.min(group.fZenithDistanceMean)

# zenith distance binning in 20 bins
binEdges = dfb.addLinearBinning(
    runsCR, "fZenithDistanceMean", "ZdBin",
    20, maxVal=75
)[1]

# exclude all events in the -1 flagged bin as they exceed the given bin range
runsCR = runsCR[runsCR["ZdBin"] != -1]

# calculate bin related quantities
zdBinnedDf = pd.DataFrame()
zdBinnedGroup = runsCR.groupby("ZdBin")
zdBinnedDf["ZdMean"] = zdBinnedGroup["fZenithDistanceMean"].apply(
    lambda r: np.mean(r)
)
zdBinnedDf["ZdCount"] = zdBinnedGroup["fZenithDistanceMean"].apply(
    lambda r: len(r)
)

zdBinnedRates = zdBinnedGroup["RCR"]

zdBinnedDf["RCR_mean"] = zdBinnedRates.apply(lambda r: np.mean(r))
zdBinnedDf["RCR_std"] = zdBinnedRates.apply(lambda r: np.std(r, ddof=1))
zdBinnedDf["RCR_meanErr"] = zdBinnedRates.apply(
    lambda r: np.std(r, ddof=1)/np.sqrt(len(r))
)

zdBinnedDf["minZd"] = binEdges[:-1]
zdBinnedDf["maxZd"] = binEdges[1:]

KDEs = []
KDEs = zdBinnedRates.apply(lambda r: gkde(r))
KDEx = np.linspace(0, 400, num=1000)
KDEy = []
for i in range(len(KDEs)):
    KDEy.append(KDEs[i](KDEx))

zdBinnedDf["KDE_max"] = np.array(
    [KDEx[np.argmax(kde_y)] for kde_y in KDEy]
)
zdBinnedDf["KDE_fwhm"] = np.array(
    [findFWHM(KDEx, kde_y)[0] for kde_y in KDEy]
)
zdBinnedDf["KDE_std"] = zdBinnedDf["KDE_fwhm"]/(2*np.sqrt(2*np.log(2)))

calculated = False
if calculated:
    density_kde = np.load("./densityInfo.npy")
else:
    density = np.vstack([
        runsCR["fZenithDistanceMean"],
        runsCR["RCR"]
    ])
    density_kde = gkde(density)(density)
    np.save("./densityInfo.npy", density_kde)

figConfig.fullWidth()
fig = plt.figure()
ax = fig.gca()
ax.scatter(
    runsCR["fZenithDistanceMean"], runsCR["RCR"], s=2,
    c=density_kde, edgecolor='', zorder=-5, alpha=0.5,
)
ax.errorbar(
    zdBinnedDf["ZdMean"], zdBinnedDf["KDE_max"], yerr=zdBinnedDf["KDE_std"],
    fmt=".", markeredgecolor="k", markeredgewidth=1., marker=".",
    markersize=11, color="tab:orange", ecolor="k",
    zorder=5, label="KDE maxima in\n"+"corresponding\n"+r"$\theta$"+" bins"
)
ax.set_xlim(5, 80)
ax.set_ylim(0, 350)
ax2 = ax.twiny()
ax2.set_xlim(ax.get_xlim())
culminations = np.sort(np.array(list(sourceCulminations.values())))
ax2.set_xticks(culminations)
ax2.set_xticklabels(["", "", "Mrks", "2344", "1959"])
for value in culminations:
    ax2.axvline(
        x=value, alpha=0.8, color="grey",
        lw=0.5, linestyle="--", zorder=0
    )

ax.minorticks_off()

ax.set_xlabel("zenith distance in degrees")
ax.set_ylabel(r"$R_{CR}$ in events/min")
ax.grid(alpha=0.5, axis="both", zorder=10)
handels, labels = ax.get_legend_handles_labels()
ax.legend([handels[1]], [labels[1]], framealpha=1,
          loc="upper right")
fig.tight_layout()
fig.savefig("./SeperatePlots/ZdScatterKDE.png", dpi=300)
fig.savefig("./SeperatePlots/ZdScatterKDE.pdf")
outputPDF.savefig(fig)

outputPDF.close()
