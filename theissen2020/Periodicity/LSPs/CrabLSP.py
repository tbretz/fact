import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.timeseries import LombScargle
from consoleOutput import printDfKeys
import setFigureConfig as figConfig

# Load Crab Lightcurve
lcCrab = pd.read_hdf(
    "../../GammaRayExcessRate/Lightcurves/CrabFromSummer2014.h5",
    "CrabFromSummer2014"
)

t = lcCrab["mjd"]
exc = lcCrab["RExcCorr"]
excErr = lcCrab["RExcErrCorr"]

lsw = LombScargle(t, exc, dy=excErr, center_data=False, fit_mean=False)

f = np.linspace(1e-3, 0.5, 10000)
p = lsw.power(f)

config = {}
config["alpha"] = 0.8
config["aspRatio"] = 1./2.3
config["small"] = 12
config["medium"] = 14
config["big"] = 16
config["width"] = 6.5
config["legendHandleLength"] = 2

figConfig.configure(config)

fig = plt.figure()
ax = fig.gca()

# ls
ax.plot(f, p, lw=2, color="k", label="LSP", zorder=1.5)
ax.set_xlim(np.min(f), np.max(f))
# year
ax.axvline(x=1/365.25, lw=1, color="tab:blue",
           label="sidereal year", zorder=1.3)
ax.axvline(x=3/365.25, lw=1, color="tab:blue",
           ls="--", label="first alias", zorder=1.3)

# months
ax.axvline(x=1/29.5, lw=1, color="tab:orange",
           label="synodic month", zorder=1.3)
ax.axvline(
    x=2/29.5, lw=1, ls="--", color="tab:orange", label="first alias",
    zorder=1.3
)
ax.axvline(x=1/27.3, lw=1, color="tab:green",
           label="sidereal month", zorder=1.3)
ax.axvline(
    x=2/27.3, lw=1, ls="--", color="tab:green", label="first alias", zorder=1.3
)
ax.set_ylim(0, 1)
ax.set_xlabel("frequency in "+r"$\mathrm{days}^{-1}$")
ax.set_ylabel("Lomb-Scargle power")
ax.set_xscale("log")
ax.grid(axis="both", alpha=0.5)
ax.legend()
fig.tight_layout()
fig.savefig("./CrabLSP.png", dpi=300)
fig.savefig("./CrabLSP.pdf")
