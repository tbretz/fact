import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import simps
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import os.path as osp
import pandas as pd
import datetime
# import astropy.constants as const
import astropy.units as u

from Radiation_simps import Radiation, plotSpectralEvolution, Ne_fromFile,\
    Ny_fromFile, t_fromFile

default = {
    "Qe_injection": 1,  # electron injection
    "Qe_powerlaw": 0,  # pure power law injection
    "Qe_yy": 1,  # photon pair production
    "Le_sync": 1,  # synchr3otron
    "Le_icT": 1,  # inverse Compton - Thomson regime
    "Le_icKN": 1,  # inverse Compton - Klein-Nishina regime
    "Ly_esc": 1,  # photon escape
    "Ly_yy": 1,  # photon pair production
    "Ly_ssa": 1,  # synchrotron self-absorption
    "Qy_sync": 1,  # synchrotron
    "Qy_icT": 1,  # inverse Compton - Thomson regime
    "Qy_icKN": 1,  # inverse Compton - Klein-Nishina regime
    # "Qy_icFull": 0,  # inverse Compton - full (Gould-Blumenthal)
    "BB": 1,  # black body background field
    "SSC": 1  # self synchrotron compton
}


def RadiationForPlasmoid(
    Plasmoid,  # dataframe with plasmiod evolution
    B,  # magnetic field
    UinjUB,  # fraction of injected to magnetic energy
    T_BB,  # black body temperature normed to e-rest mass (kT/mc2)
    GammaJet,  # bulk Lorentz factor of jet
    theta1,  # angle between layer and z-axis in layer frame
    thetaObs,  # angle between observation direction and z-axis in obs. frame
    z_diss,  # z coordinate of layer
    R_BLR,  # distance between black hole and Broad Line Region
    L_BLR,  # luminosity of BLR
    L_Layer,  # size of layer
    inj_si, inj_min, inj_max,  # injection properties taken from PIC
    betaG, Asup, Bsup, sigma,  # growth parameter from PIC
    saveEvery,  # which time steps should be saved
    save=None,  # if not None, filename where to save h5 file of rad. modeling
    printEvery=1,  # how frequent progress is printed to command line
    dtmin=1e-4,  # minimal time step for radiation modelling
    # fraction of Tmax, when evolution is continued without inject. after Tmax
    addTimeFrac=0,
    addTime=0,  # additoinal time in plasmoid frame after injection stop
    stopAtMerger=True,  # stop calculation, if plasmoids life ends by merger
    eEgridSteps=500,  # size of electron energy grid
    yEgridSteps=500,  # size of photon energy grid
    nIntegration=500,  # number of grid points for evaluation of integrals
    processDict=default,
    plotInters0=None,
    plotInterbeta=None,
    plotDeltaP=None,
    plotDeltaBB=None,
    plotCourant=None,
    plotNegatives=None,
    plotRestFrame=None,
    plotSpectrum=None,
    plotLightCurve=None,
    lcBounds=[1e8, 3e11],
    pdfFile=None,
    showToo=False
):

    # Plasmoid contains
    # index: number of plasmoid
    # s    : size (w/L = w'/L)
    # beta : velocity in layer's frame (beta')
    # r    : position in layer's frame (r')
    # tau  : time in layer's frame (t')

    # naming: denote
    #     unprimed x (x  ) : x0  -> plasmoid's frame
    #       primed x (x' ) : x1  -> layer's frame
    # twice primed x (x'') : x2  -> observer's frame

    # check if plasmoid ends up in merger or at edges
    if stopAtMerger:
        epsilon = 0.01
        if (np.abs(Plasmoid["r"].values[-1]) < 1 - epsilon):
            # plasmoid has merged, stop calculating further
            addTimeFrac = 0
            addTime = 0

    ''' timing t' -> t '''
    t0, dt0, t0_t1, t1_ini, t0_add, t0_fin, t0_fin_add, t0_tot = timing(
        Plasmoid, dtmin, addTimeFrac, addTime
    )
    Nadd = len(t0_add)
    i_add = len(t0) - 1
    print("Finished calculation of timing: ", datetime.datetime.now())

    # interpolation of s, beta, r to t0-grid
    s0_inter = interp1d(t0_t1, Plasmoid["s"])(t0)
    beta1_inter = interp1d(t0_t1, Plasmoid["beta"])(t0)
    # r1_inter = interp1d(t0_t1, Plasmoid["r"])(t0)
    s0_inter_tot = np.append(s0_inter, s0_inter[-1]*np.ones(Nadd))
    beta1_inter_tot = np.append(beta1_inter, beta1_inter[-1]*np.ones(Nadd))
    print("Finished Interpolation: ", datetime.datetime.now())

    ''' control plots of interpolation '''
    if pdfFile is not None:
        if bool(plotInters0["do"]):
            pdfFile.savefig(
                s0InterpolationTestPlot(
                    t0_t1, Plasmoid["s"],
                    t0_tot, s0_inter_tot
                )
            )
        if bool(plotInterbeta["do"]):
            pdfFile.savefig(
                beta1InterpolationTestPlot(
                    t0_t1, Plasmoid["beta"],
                    t0_tot, beta1_inter_tot
                )
            )
    if showToo:
        if bool(plotInters0["do"]):
            plt.show(
                s0InterpolationTestPlot(
                    t0_t1, Plasmoid["s"],
                    t0_tot, s0_inter_tot
                )
            )
        if bool(plotInterbeta["do"]):
            plt.show(
                beta1InterpolationTestPlot(
                    t0_t1, Plasmoid["beta"],
                    t0_tot, beta1_inter_tot
                )
            )

    ''' relativistic boosting: doppler factors '''

    # black body Doppler factors at t0 grid
    betaJet = np.sqrt(1. - 1./GammaJet**2)
    deltaBB = doppler_BB(beta1_inter, theta1, betaJet,
                         orientation=-np.sign(z_diss-R_BLR))
    deltaBB_tot = np.append(deltaBB, deltaBB[-1]*np.ones(Nadd))

    # plasmoid's Doppler factors at t0 grid
    deltaP = doppler_p(beta1_inter, theta1, betaJet, thetaObs)
    deltaP_tot = np.append(deltaP, deltaP[-1]*np.ones(Nadd))
    print("Finished Doppler factors: ", datetime.datetime.now())

    ''' control plots of Doppler factors '''

    if pdfFile is not None:
        if bool(plotDeltaBB["do"]):
            pdfFile.savefig(deltaBBTestPlot(t0_tot, deltaBB_tot))
        if bool(plotDeltaP["do"]):
            pdfFile.savefig(deltaPTestPlot(t0_tot, deltaP_tot))
    if showToo:
        if bool(plotDeltaBB["do"]):
            plt.show(deltaBBTestPlot(t0_tot, deltaBB_tot))
        if bool(plotDeltaP["do"]):
            plt.show(deltaPTestPlot(t0_tot, deltaP_tot))

    ''' calculate radiation '''

    r = Radiation(
        L=L_Layer, B=B, UinjUB=UinjUB,
        inj_min=inj_min, inj_max=inj_max, inj_si=inj_si,
        Theta_BB=T_BB, L_BLR=L_BLR, R_BLR=R_BLR, z_diss=z_diss,
        betaG=betaG, Asup=Asup, Bsup=Bsup, sigma=sigma,
        nIntegration=nIntegration
    )

    # set grids
    r.setLinTime(dt0, saveEvery=saveEvery)
    r.setLogElectronEnergy(eexpmin=0, eexpmax=5, NeE=eEgridSteps)
    r.setLogPhotonEnergy(yexpmin=-15, yexpmax=7, NyE=yEgridSteps)

    # set initial distributions
    Ne_ini = r.n_co_Fnorm * (r.eE)**inj_si * (
        (r.eE > inj_min) & (r.eE < inj_max)
    )

    # initial electron term
    r.setInitalElectronDistribution(Ninitial=Ne_ini)
    # initial photon term
    r.setInitalPhotonDistribution(Ninitial=0*r.yE)  # no photons

    # everything on
    r.setProcesses(processDict)
    # check if radiation modelling already exists
    if save is not None:
        if osp.exists(save):
            print("Load radiation from file:", save)
            print("Start loading: ", datetime.datetime.now())
            r.t = t_fromFile(save)
            r.Ne_all = Ne_fromFile(save)[1].values
            r.Ny_all = Ny_fromFile(save)[1].values
            r.courant = pd.read_hdf(save, key="Courant").values
            print("Finished loading: ", datetime.datetime.now())
        else:
            # print("1", r.include)
            r.turnOn("Qe_injection")
            r.solveUntil(
                t0_fin,
                sArray=s0_inter,
                dopplerBBArray=deltaBB,
                betaArray=beta1_inter,
                printEvery=printEvery
            )
            r.turnOff("Qe_injection")
            r.solveUntil(
                t0_fin_add,
                sArray=s0_inter_tot[i_add:],
                dopplerBBArray=deltaBB_tot[i_add:],
                betaArray=beta1_inter_tot[i_add:],
                printEvery=printEvery
            )
            r.saveToh5(save)
    else:
        r.turnOn("Qe_injection")
        r.solveUntil(
            t0_fin,
            sArray=s0_inter,
            dopplerBBArray=deltaBB,
            betaArray=beta1_inter,
            printEvery=printEvery
        )
        r.turnOff("Qe_injection")
        r.solveUntil(
            t0_fin_add,
            sArray=s0_inter_tot[i_add:],
            dopplerBBArray=deltaBB_tot[i_add:],
            betaArray=beta1_inter_tot[i_add:],
            printEvery=printEvery
        )

    if np.any(np.array(r.Ne_all) < 0):
        print("N_e < 0 !")
    if np.any(np.array(r.Ny_all) < 0):
        print("N_y < 0 !")

    r.Ne_all = r.Ne_all[:-1]
    r.Ny_all = r.Ny_all[:-1]
    
    if pdfFile is not None:
        if bool(plotCourant["do"]):
            pdfFile.savefig(
                courantPlot(t0_tot, r.eE, np.array(r.courant), r.inj_max)
            )
        if bool(plotNegatives["do"]):
            pdfFile.savefig(
                negativeDensitiesTestPlot(t0_tot, r.Ne_all, r.Ny_all)
            )
        if bool(plotRestFrame["do"]):
            pdfFile.savefig(
                r.plotEvolution(
                    BB=bool(plotRestFrame["BB"]),
                    maxLines=int(plotRestFrame["maxLines"])
                )
            )
            pdfFile.savefig(
                r.plotEvolutionE(
                    maxLines=int(plotRestFrame["maxLines"])
                )
            )
            pdfFile.savefig(
                r.plotEvolutionY(
                    BB=bool(plotRestFrame["BB"]),
                    maxLines=int(plotRestFrame["maxLines"])
                )
            )
    if showToo:
        if bool(plotCourant["do"]):
            plt.show(courantPlot(t0_tot, r.eE, np.array(r.courant), r.inj_max))
        if bool(plotNegatives["do"]):
            plt.show(negativeDensitiesTestPlot(t0_tot, r.Ne_all, r.Ny_all))
        if bool(plotRestFrame["do"]):
            plt.show(
                r.plotEvolution(
                    BB=bool(plotRestFrame["BB"]),
                    maxLines=int(plotRestFrame["maxLines"])
                )
            )

    # Boosting (only if plots of spectrum/lightcurve needed)
    if bool(plotLightCurve["do"]) or bool(plotSpectrum["do"]):
        print("Start Boosting: ", datetime.datetime.now())

        # conversion from number density to escaped spectrum/LC
        me_c3 = 24544.325267191878  # erg cm / s
        sigma_T = 6.65245854533e-25  # cm**2
        conv = np.pi/2 * me_c3/sigma_T * L_Layer * s0_inter_tot**2

        # [time, energy]
        Ny0_esc = conv[:, None] * np.array(r.Ny_all)[:, :]  # 1/(erg s)

        # boost to observer's frame
        Ny2_esc = Ny0_esc[:, :] * deltaP_tot[:, None]**2
        yE2 = r.yE[None, :] * deltaP_tot[:, None]

        # time shifting of t0 grid to observer's frame
        # take only Doppler factor (t to t'')
        # chunk calculation to prevent memory error
        t2_t0 = chunkedIntegral(dt0 / deltaP_tot) + t1_ini / GammaJet
        # convert units to seconds
        c = 29979245800  # cm / s
        t2_t0 = L_Layer / c * t2_t0  # s
        print("Finished Boosting: ", datetime.datetime.now())

    # observed spectrum
    if bool(plotSpectrum["do"]):
        figObsframe = plotSpectralEvolution(
            yE2, Ny2_esc, t2_t0,
            maxLines=int(plotSpectrum["maxLines"]),
            unitx=str(plotSpectrum["unitx"]),
            unity=str(plotSpectrum["unity"]),
            unitE=str(plotSpectrum["unitE"]),
            unitt=str(plotSpectrum["unitt"])
        )
        if pdfFile is not None:
            pdfFile.savefig(figObsframe)
        if showToo:
            plt.show(figObsframe)

    # observed lightcurve
    if bool(plotLightCurve["do"]):
        lc = lightcurve(
            yE2, Ny2_esc, loweV=lcBounds[0], upeV=lcBounds[1]
        )
        if save is not None:
            np.save(save[:-3] + "_lc.npy", [t2_t0, lc])
        figLC = plotLC(
            t2_t0, lc, loweV=lcBounds[0], upeV=lcBounds[1],
            unitt=plotLightCurve["unitt"], unitE=plotLightCurve["unitE"]
        )
        if pdfFile is not None:
            pdfFile.savefig(figLC)
        if showToo:
            plt.show(figLC)

    print("Finished plotting: ", datetime.datetime.now())
    if pdfFile is not None:
        plt.close("all")


def timing(Plasmoid, dtmin, addTimeFrac=0, addTime=0):
    # Nt1 = len(Plasmoid)

    # assume linear spacing in tau
    dt1 = Plasmoid["tau"].values[1] - Plasmoid["tau"].values[0]
    # mininal timestep in layer time boosted to plasmoid
    dt0_min_gamma = dt1 / max(1./np.sqrt(1 - Plasmoid["beta"].values**2))
    # minimal timestep to keep quality of radiation simulation
    dt0_min_quality = dtmin
    # minimal timestep for radiation calculation
    dt0 = max(dt0_min_gamma, dt0_min_quality)
    print("dt0 =", dt0)
    # start time in layer frame
    t1_ini = min(Plasmoid["tau"])
    # end time in layer frame
    t1_fin = max(Plasmoid["tau"])

    # start time in plasmoid frame
    t0_ini = 0  # choice
    # grid of t1 in t0 (used to calculate interpolation function)
    t0_t1 = chunkedIntegral(np.sqrt(1 - Plasmoid["beta"].values**2) * dt1)

    # end time in plasmoid frame: Instantaneous Lorentz Trafo
    # integral dtau/gamma(tau) from t1_ini to t1_fin
    # t0_fin = np.sum(np.sqrt(1 - Plasmoid["beta"].values[:-1]**2) * dt1)
    t0_fin = t0_t1[-1]  # faster

    # grid for calculation of radiation with MC simulated s, beta
    t0 = np.arange(t0_ini, t0_fin, dt0)

    # additional time after layer simulation, s,beta=const
    gamma_add_inv = np.sqrt(1 - Plasmoid["beta"].values[-1]**2)
    t0_fin_add = t0_fin + addTimeFrac * t1_fin * gamma_add_inv

    # add time in plasmoids frame
    t0_fin_add += addTime

    # additional time
    t0_add = np.arange(t0[-1] + dt0, t0_fin_add, dt0)

    # total time
    t0_tot = np.arange(t0_ini, t0_fin_add, dt0)

    return t0, dt0, t0_t1, t1_ini, t0_add, t0_fin, t0_fin_add, t0_tot


def chunkedIntegral(f, k_chunk=5000):
    N = len(f)
    if N < k_chunk:
        integral = np.sum(
            np.tri(N, N, -1) * f,
            axis=1
        )
        return integral
    else:
        # number of chunks
        Nchunks = int(np.floor(float(N) / k_chunk))
        print(Nchunks)
        # first chunk
        integral = np.sum(
            np.tri(k_chunk, k_chunk, -1) * f[0:k_chunk],
            axis=1
        )
        # other cunks
        for i in np.arange(Nchunks-1)+1:
            print("iteration", i)
            integral = np.append(
                integral,
                np.sum(
                    np.tri(k_chunk, k_chunk, 0) * f[i*k_chunk:(i+1)*k_chunk],
                    axis=1
                ) + integral[-1]
            )
        # rest
        rest = N % k_chunk
        print(rest)
        integral = np.append(
            integral,
            np.sum(
                np.tri(rest, rest, 0) * f[-rest:],
                axis=1
            ) + integral[-1]
        )
        return integral


def s0InterpolationTestPlot(t0_t1, s0, t0, s0_inter, smin=1e-3):
    fig, ax = plt.subplots()
    ax.plot(t0_t1, s0-smin, "o")
    ax.loglog(t0, s0_inter-smin)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("time in plasmoid's frame [L/c]")
    ax.set_ylabel("$s - s_0$ [L]")
    fig.tight_layout()
    return fig


def beta1InterpolationTestPlot(t0_t1, beta1, t0, beta1_inter):
    fig, ax = plt.subplots()
    ax.plot(t0_t1, np.abs(beta1), "o")
    ax.loglog(t0, np.abs(beta1_inter))
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("time in plasmoid's frame [L/c]")
    ax.set_ylabel("plasmoid's velocity [c]")
    fig.tight_layout()
    return fig


def deltaBBTestPlot(t0, deltaBB):
    fig, ax = plt.subplots()
    ax.plot(t0, deltaBB, ".")
    # ax.set_yscale("log")
    ax.set_xlabel("time in plasmoid's frame [L/c]")
    ax.set_ylabel(r"black body Doppler factor $\delta_{BB}$")
    fig.tight_layout()
    return fig


def deltaPTestPlot(t0, deltaP):
    fig, ax = plt.subplots()
    ax.plot(t0, deltaP, ".")
    # ax.set_yscale("log")
    ax.set_xlabel("time in plasmoid's frame [L/c]")
    ax.set_ylabel(r"plasmoids Doppler factor $\delta_{P}$")
    fig.tight_layout()
    return fig


def negativeDensitiesTestPlot(t0, Ne_all, Ny_all):
    fig, ax = plt.subplots()
    ax.plot(t0, [-int(np.any(Ny < 0)) for Ny in Ny_all], label=r"$N_{\gamma}$")
    ax.plot(t0, [-int(np.any(Ne < 0)) for Ne in Ne_all], label=r"$N_{e}$")
    ax.set_yticks([-1, 0])
    ax.set_yticklabels(["N < 0", "N > 0"])
    ax.set_xlabel("time in plasmoid's frame [L/c]")
    ax.set_ylim(-2, 1)
    ax.grid()
    ax.legend()
    fig.tight_layout()
    return fig


def courantPlot(t, E, cs, injmax, maxlines=1000):
    Nt = len(cs)
    if Nt > maxlines:
        dL = int(np.ceil(Nt / float(maxlines)))
        cs = cs[::dL]
        Nt = len(cs)
    fig, (ax, axcb) = plt.subplots(
        ncols=2, gridspec_kw={"width_ratios": [20, 1]}
    )
    # colors
    c_map = plt.get_cmap("viridis_r")
    cy = c_map(np.linspace(0, 1, Nt))
    ax.set_prop_cycle('color', cy)
    # plotting
    for c in cs:
        ax.plot(E, c)
    # line at 1
    ax.axhline(1, c="black", linestyle=":")
    ax.axvline(injmax, c="tab:red", label="max. injected energy")
    # title, scale and labels
    ax.set_title("Courant criterion")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\gamma$")
    ax.set_ylabel(r"$v\:dt / d\gamma $")
    ax.grid()
    ax.legend()
    # colorbar
    cb2 = cb.ColorbarBase(axcb, cmap=c_map,
                          boundaries=t,
                          ticks=np.linspace(0, max(t), 5),
                          orientation='vertical',
                          spacing="proportional")
    cb2.set_label(r"$ \frac{c}{L} t$", rotation=0, labelpad=20)
    fig.tight_layout()
    return fig


# general considerations

# doppler factor
# for completeness
def doppler(beta, theta):
    return np.sqrt(1 - beta**2) / (1 - beta * np.cos(theta))


# relativistic abberation
# angle of velcity vector in frame 1 (characterized by theta1, beta1)
# as seen in frame 2 moving with velocity beta_diff
def theta2(theta1, beta1, beta_diff):
    return np.arctan2(
        beta1 * np.sin(theta1) * np.sqrt(1 - beta_diff**2),
        beta_diff + beta1 * np.cos(theta1)
    )


# relativistic velocity transformation (addition)
# velocity in frame 1 (characterized by theta1, beta1)
# as seen in frame 2 moving with velocity beta_diff
def beta2(theta1, beta1, beta_diff):
    return (
        beta_diff + beta1 * np.cos(theta1)
    ) / (
        1. + beta_diff * beta1 * np.cos(theta1)
    )


# Lorentz factor from beta
def Gamma(beta):
    return 1./np.sqrt(1. - beta**2)


# Doppler factor of plasmoid seen in observer's frame
def doppler_p(beta1, theta1, betaJet, thetaObs):
    return 1./(
        # Gamma_p as measured in observer's frame, analytically simplified
        Gamma(betaJet) * Gamma(beta1) * (
            1. + betaJet * beta1 * np.cos(theta1)
        ) *
        # rest of Doppler factor
        (
            1. - beta2(theta1, beta1, betaJet) * np.cos(
                theta2(theta1, beta1, betaJet) - thetaObs
            )
        )
    )


# Doppler factor of black body as seen by plasmoid
# orientation = +1 for BL Lac (BLR inside), -1 for FSRQ (BLR outside)
# alpha: angle between direction of motion of plasmoid and direct
#        connection to BLR, if BLR is not assumed to be "all around"
#        measured in plasmoids frame
def doppler_BB(beta1, theta1, betaJet, orientation, alpha=0):
    return 1./(
        # Gamma_p as measured in observer's frame, analytically simplified
        Gamma(betaJet) * Gamma(beta1) * (
            1 + betaJet * beta1 * np.cos(theta1)
        ) *
        # rest of Doppler factor
        (
            1 - orientation * beta2(theta1, beta1, betaJet) * np.cos(
                theta2(theta1, beta1, betaJet) - alpha
            )
        )
    )


def lightcurve(x_all, Ny_all, loweV=1e8, upeV=3e11):
    me_c2 = 8.18710565e-07  # erg
    lower = loweV / me_c2 * (u.eV).to("erg")
    upper = upeV / me_c2 * (u.eV).to("erg")

    dx = x_all[:, 1:] - x_all[:, :-1]
    x = (x_all[:, 1:] + x_all[:, :-1]) / 2
    Ny = (Ny_all[:, 1:] + Ny_all[:, :-1]) / 2

    # integrate energy
    # lc = np.sum(
    #     np.greater_equal(x, lower) * np.less_equal(x, upper) *
    #     Ny * dx,
    #     axis=1
    # )
    Ns = []
    n = 500
    xint = np.linspace(lower, upper, n)
    for i in range(len(Ny)):
        Ny_i = np.zeros(n)
        Ny_inter = interp1d(x[i], Ny[i], axis=0)
        Ny_i[(xint > np.min(x[i])) & (xint < np.max(x[i]))] = Ny_inter(
            xint[(xint > np.min(x[i])) & (xint < np.max(x[i]))]
        )
        Ns.append(Ny_i)

    # x1 = np.linspace(np.minimum(x, axis=1), np.maximum(x, axis=1), n).T
    lc = simps(
        np.array(Ns),
        xint
    )
    return lc


def plotLC(t, lc, loweV=1e8, upeV=3e11, unitt="s", unitE="eV"):
    # convert time to sec
    t_unit = t * (u.s).to(unitt)
    lo = loweV * (u.eV).to(unitE)
    up = upeV * (u.eV).to(unitE)

    # plot
    fig, ax = plt.subplots()
    ax.set_title(r"$%g - %g$" % (lo, up) + unitE)
    ax.plot(t_unit - t_unit[0], lc, color="tab:blue")
    ax.set_xlabel("t [" + unitt + "]")
    ax.set_ylabel("L [erg/s]")
    fig.tight_layout()
    return fig
