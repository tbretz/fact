from astroML.time_series import lomb_scargle
from astropy.time import Time
import numpy as np
import pandas as pd
from peripylib import figures
from scipy.optimize import curve_fit

def model_function(t, omega, tau, a, b, c):
    return a*np.cos(omega*(t-tau)) + b*np.sin(omega*(t-tau)) + c

# -----------
# Lomb-Scargle analysis of Sunspot Data
# The data is from http://www.sidc.be/silso/datafiles
# 90% of the data are randomly removed to simulate uneven sampling

# Read Sunspot data
data = pd.read_csv('sunspots_mjd.csv', delimiter=',')

# Remove numbers without error
data = data[data.Std != 0.]

# Randomly remove 90% of the data
np.random.seed(17)
drop_indeces = np.random.choice(data.index, int(data.shape[0]*9/10), replace=False)
data = data.drop(drop_indeces)

# -------
# Plot Sunspot data + Periodogram

fig, ax = figures.newfig(rows=2)

# Upper plot: Sunspots over time
ax[0].errorbar(data.mjd, data.Sunspots, yerr=data.Std, ls='', alpha=0.5)

ax[0].set(ylabel=r'Number of Sunspots', xlabel=r'Year')
ax[0].set_xticks([-12371, -3240, 5891, 15022, 24151, 33282, 42413, 51544, 60676])
tick_labels = ['', '1850', '', '1900', '', '1950', '', '2000', '']
ax[0].set_xticklabels(tick_labels)

# Fit the data with sinusoid model function
popt, _ = curve_fit(model_function, data.mjd, data.Sunspots, p0=[2*np.pi/(11*365.), 1, 100, 100, 100])
best_fit_period = 2*np.pi/popt[0] / 365
print('Best fit period: {:.2f} years'.format(best_fit_period))

time_grid = np.arange(data.mjd.min(), data.mjd.max(), 100)
ax[0].plot(time_grid, model_function(time_grid, *popt), color=figures.colors(3), ls='--')

# -------
# Compute LS periodogram

pmin = 30  # in days
pmax = 20 * 365  # 20 years
N = len(data.mjd)
period = 10 ** np.linspace(np.log10(pmin), np.log10(pmax), N)
omega = 2 * np.pi / period
power = lomb_scargle(data.mjd, data.Sunspots, data.Std, omega, generalized=True)

ax[1].plot(period / 365, power)

# Add position of best fit period
ax[1].plot([best_fit_period, best_fit_period], [0, power[peak[0]]], ls='--', color=figures.colors(3))



ax[1].set(xlabel=r'Period / years', ylabel=r'LS Power', ylim=(0, 0.3), xlim=(0, 20))

# Save plot
figures.savefig('sunspots')