import argparse
import random

import numpy as np
import pandas as pd
import yaml
from scipy.stats import norm, poisson, uniform
from time import sleep


def standard_excess(s, b, alpha=0.2):
    return s-alpha*b, np.sqrt(s + alpha**2*b)


def better_excess(s, b, alpha=0.2):
    return s+1-alpha*(b+1), np.sqrt(s+1 + alpha**2*(b+1))


def weighted_mean(x, sigma):
    x, sigma = np.array(x), np.array(sigma)
    mean = sum(x/sigma**2)/sum(sigma**-2)
    var = 1/sum(sigma**-2)
    return mean, np.sqrt(var)


def weighted_mean_Time(x, sigma, effOn):
    x, sigma, effOn = np.array(x), np.array(sigma), np.array(effOn)
    mean = sum(x*effOn)/sum(effOn)
    std = sum((sigma*effOn)**2)**(1/2)/sum(effOn)
    return mean, std


def unweighted_mean(x, sigma):
    x, sigma = np.array(x), np.array(sigma)
    mean = sum(x)/len(x)
    std = sum(sigma**2)**(1/2)/len(sigma)
    return mean, std


def printProgressBar(
        iteration, total, prefix='', suffix='',
        decimals=1, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in
                                  percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(
        100 * (iteration / float(total))
    )
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def full_run(
    n_nights, n_runs, lambda_e_tuple, lambda_b_tuple,
    excCorrMode=2, excCorrPars=[3, 8],
    effOnMode=0, effOnPars=[0.5, 0.5],
    alpha=0.2
):

    lambda_b_range = np.linspace(
        lambda_b_tuple[0], lambda_b_tuple[1], num=lambda_b_tuple[2]
    )
    lambda_e_range = np.linspace(
        lambda_e_tuple[0], lambda_e_tuple[1], num=lambda_e_tuple[2])

    lambda_b_vals, lambda_e_vals = np.meshgrid(
        lambda_b_range, lambda_e_range)
    lambda_b_vals = lambda_b_vals.flatten()
    lambda_e_vals = lambda_e_vals.flatten()
    n_params = len(lambda_e_vals)

    df = []
    corrsExcAll = []
    effOnAll = []
    printProgressBar(
        0, int(1.2*n_params),
        prefix='Progress:', suffix='Complete', length=50
    )
    for fParamId in range(n_params):

        lambda_b_true = lambda_b_vals[fParamId]
        lambda_e_true = lambda_e_vals[fParamId]
        lambda_s_true = lambda_b_true+lambda_e_true

        for fNight in range(n_nights):

            k_s = []
            k_b = []
            corrsExc = []

            if effOnMode == 0:
                effOn = np.ones(n_runs)
            elif effOnMode == 1:
                effOn = np.random.uniform(
                    low=effOnPars[0], high=effOnPars[0]+effOnPars[1],
                    size=n_runs
                )
            else:
                print(
                    "On time handling Mode " + str(effOnMode) +
                    " is not valid. Valid options are:\n" +
                    "\t 0: EffOn=1 for all runs \n" +
                    "\t 1: Uniform effOn"
                )

            for run in range(n_runs):

                # True correction factor
                # (0) All correction factors 1
                if excCorrMode == 0:
                    corrsExcTrue = 1
                # (1) Min + absolute(gaussian)
                elif excCorrMode == 1:
                    corrExcTrue = excCorrPars[0] + \
                        np.abs(norm.rvs(loc=0, scale=excCorrPars[1]))
                # (2) Uniform distribution
                elif excCorrMode == 2:
                    corrExcTrue = uniform.rvs(
                        loc=excCorrPars[0],
                        scale=excCorrPars[1]
                    )
                else:
                    print(
                        "Excess Correction Mode " + str(excCorrMode) +
                        " is not valid. Valid options are:\n" +
                        "\t 0: No correction factors included.\n" +
                        "\t 1: Minimum value + absolute value of gaussian\n" +
                        "\t 2: Uniform Distribution"
                    )
                lambda_s = lambda_e_true/corrExcTrue + lambda_b_true
                k_s_run = poisson.rvs(lambda_s*effOn[run])
                k_b_run = poisson.rvs(lambda_b_true/alpha*effOn[run])

                k_s.append(k_s_run/effOn[run])
                k_b.append(k_b_run/effOn[run])
                corrsExc.append(corrExcTrue)

            k_s = np.array(k_s)
            k_b = np.array(k_b)
            corrsExc = np.array(corrsExc)

            # method 0
            excess0 = (sum(k_s) - 0.2*sum(k_b))/n_runs
            sigma0 = np.sqrt(sum(k_s) + alpha**2*sum(k_b))/n_runs

            # method 1
            excess1, sigma1 = standard_excess(k_s, k_b, alpha)
            excess1 = excess1*corrsExc
            sigma1 = sigma1*corrsExc
            excess1, sigma1 = weighted_mean(
                excess1[sigma1 > 0], sigma1[sigma1 > 0])

            # method 2
            excess2, sigma2 = better_excess(k_s, k_b, alpha)
            excess2 = excess2*corrsExc
            sigma2 = sigma2*corrsExc
            excess2, sigma2 = weighted_mean(excess2, sigma2)

            # method 3
            excess3, sigma3 = better_excess(k_s, k_b, alpha)
            excess3 = excess3*corrsExc
            sigma3 = sigma3*corrsExc
            excess3, sigma3 = unweighted_mean(excess3, sigma3)

            # method 4
            excess4, sigma4 = standard_excess(k_s, k_b, alpha)
            excess4 = excess4*corrsExc
            sigma4 = sigma4*corrsExc
            excess4, sigma4 = unweighted_mean(excess4, sigma4)

            # method 5
            excess5, sigma5 = standard_excess(k_s, k_b, alpha)
            excess5 = excess5*corrsExc
            sigma5 = sigma5*corrsExc
            excess5, sigma5 = weighted_mean_Time(excess5, sigma5, effOn)

            corrsExcAll.extend(corrsExc)
            effOnAll.extend(effOn)
            df.append(
                [fParamId, fNight, n_runs,
                 lambda_e_true, lambda_s_true, lambda_b_true,
                 excess0, sigma0, excess1, sigma1,
                 excess2, sigma2, excess3, sigma3,
                 excess4, sigma4, excess5, sigma5])

        printProgressBar(
            fParamId, int(1.2*n_params),
            prefix='Progress:', suffix='Complete', length=50
        )

    corrsExcAll = np.array(corrsExcAll)
    df = pd.DataFrame(
        df,
        columns=[
            'fParamId', 'fNight', 'fNumRuns', 'LambdaExcess',
            'LambdaSignal', 'LambdaBackground',
            'Excess0', 'Sigma0', 'Excess1', 'Sigma1',
            'Excess2', 'Sigma2', 'Excess3', 'Sigma3',
            'Excess4', 'Sigma4', 'Excess5', 'Sigma5'
        ]
    )
    return df, corrsExcAll, effOnAll


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--configFile", help="Path to input config file", type=str
    )
    args = parser.parse_args()

    configFile = args.configFile

    if configFile is not None:
        config = {}
        with open(configFile, 'r') as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
    else:
        print(
            "Please call the script with a valid config file (-i config.yaml)."
        )
        return 1

    random.seed(config["randomSeed"])
    saveLocation = config["savingLocation"]["simulationFolder"]
    pSpace = config["parameterSpace"]
    excSupp = config["excessSuppression"]
    timeHandl = config["onTimeHandling"]

    results_frame, corrsExcAll, effOnAll = full_run(
        config["numberOfNightsPerSet"],
        config["numberOfRunsPerNight"],
        (pSpace["excess"]["lowerLimit"],
         pSpace["excess"]["upperLimit"],
         pSpace["excess"]["num"]),
        (pSpace["background"]["lowerLimit"],
         pSpace["background"]["upperLimit"],
         pSpace["background"]["num"]),
        excCorrMode=excSupp["mode"],
        excCorrPars=[excSupp["minimumCf"], excSupp["cfRange"]],
        effOnMode=timeHandl["mode"],
        effOnPars=[timeHandl["minimumEffOn"], timeHandl["effOnRange"]]
    )

    printProgressBar(
        11, 12,
        prefix='Progress:', suffix='Complete', length=50
    )

    np.save(
        saveLocation +
        config["savingLocation"]["cfNpyFileName"], corrsExcAll
    )
    np.save(
        saveLocation +
        config["savingLocation"]["effOnNpyFileName"],  effOnAll
    )
    results_frame.to_hdf(
        saveLocation+config["savingLocation"]["rawHDF5FileName"],
        key=config["savingLocation"]["rawHDF5ResultsKey"], mode='a'
    )

    printProgressBar(
        1, 1,
        prefix='Progress:', suffix='Complete', length=50
    )

    sleep(0.2)

    return 0


if __name__ == "__main__":
    main()
