import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import os
import argparse
import sys

from RadiationForPlasmoid import RadiationForPlasmoid
from readYAML import loadConfiguration

parser = argparse.ArgumentParser(
    prog="calcPlasmoids",
    description='calculate radiation and create plots'
)

parser.add_argument(
    "-c", "--configFile",
    help="Path to input config file",
    type=str
)
args = parser.parse_args()
configs = loadConfiguration(args.configFile)


# parse configs
path = os.path.join(configs["MCPath"], configs["MCFile"])
allPlasmoids = pd.read_hdf(path, key=str(configs["MCKey"]))

# convert list of plasmoid ids
if isinstance(configs["PlasmoidID"], (int, float)):
    numbers = np.array([configs["PlasmoidID"]], dtype=int)
elif isinstance(configs["PlasmoidID"], list):
    numbers = np.array(configs["PlasmoidID"], dtype=int)
if isinstance(configs["PlasmoidID"], str):
    if configs["PlasmoidID"] == "all":
        numbers = np.arange(np.max(allPlasmoids['index']))+1

# convert list of energy grid specifications for electrons
if isinstance(configs["grid"]["eEgridSteps"], (int, float)):
    eEgrid = int(configs["grid"]["eEgridSteps"]) * np.ones(
        len(numbers), dtype=int)
elif isinstance(configs["grid"]["eEgridSteps"], list):
    eEgrid = np.array(configs["grid"]["eEgridSteps"], dtype=int)
# convert list of energy grid specifications for photons
if isinstance(configs["grid"]["yEgridSteps"], (int, float)):
    yEgrid = int(configs["grid"]["yEgridSteps"]) * np.ones(
        len(numbers), dtype=int)
elif isinstance(configs["grid"]["yEgridSteps"], list):
    yEgrid = np.array(configs["grid"]["yEgridSteps"], dtype=int)


# convert values to bool
processDict = configs["processes"]
for i in processDict:
    processDict[i] = bool(processDict[i])

# for shorter notation
plots = configs["plots"]

# calculate radiation etc. for every plasmoid
print("start calculating " + configs["name"])
for i in range(len(numbers)):
    sys.stdout.flush()
    PlasmoidID = numbers[i]
    if len(allPlasmoids[allPlasmoids["index"] == PlasmoidID]) > 1:
        print(
            "\nlifetime of plasmiod ", PlasmoidID, ":",
            allPlasmoids.groupby(
                "index"
            ).get_group(PlasmoidID).tail(1)["tau"].values -
            allPlasmoids.groupby(
                "index"
            ).get_group(PlasmoidID).head(1)["tau"].values,
            "L/c" +
            "\n"
        )

        # new pdf file
        if bool(configs["pdf"]["use"]):
            pdf = PdfPages(
                os.path.join(
                    configs["pdf"]["path"],
                    (
                        configs["name"] +
                        "Plasmoid_%g_%gx%g.pdf" %
                        (PlasmoidID, eEgrid[i], yEgrid[i])
                    )
                )
            )
        else:
            pdf = None

        # calculate actual radiation
        RadiationForPlasmoid(
            allPlasmoids[allPlasmoids["index"] == PlasmoidID],
            B=float(configs["Layer"]["B"]),
            z_diss=float(configs["Layer"]["z_diss"]),
            L_Layer=float(configs["Layer"]["L_Layer"]),
            GammaJet=float(configs["AGN"]["GammaJet"]),
            theta1=float(configs["AGN"]["theta1"]),
            thetaObs=float(configs["AGN"]["thetaObs"]),
            R_BLR=float(configs["BLR"]["R_BLR"]),
            T_BB=float(configs["BLR"]["T_BB"]),
            L_BLR=float(configs["BLR"]["L_BLR"]),
            inj_si=float(configs["injection"]["si"]),
            inj_min=float(configs["injection"]["min"]),
            inj_max=float(configs["injection"]["max"]),
            UinjUB=float(configs["injection"]["UinjUB"]),
            sigma=float(configs["MCParameter"]["sigma"]),
            betaG=float(configs["MCParameter"]["betaG"]),
            Asup=float(configs["MCParameter"]["Asup"]),
            Bsup=float(configs["MCParameter"]["Bsup"]),
            saveEvery=int(configs["save"]["saveEvery"]),
            save=os.path.join(
                configs["save"]["path"],
                (
                    configs["name"] + "_" +
                    configs["MCFile"].split(".")[0] +
                    "_" + str(PlasmoidID) + ".h5"
                )
            ),
            processDict=processDict,
            dtmin=float(configs["grid"]["dtmin"]),
            addTimeFrac=float(configs["grid"]["addTimeFrac"]),
            addTime=float(configs["grid"]["addTime"]),
            stopAtMerger=float(configs["grid"]["stopAtMerger"]),
            eEgridSteps=int(configs["grid"]["eEgridSteps"]),
            yEgridSteps=int(configs["grid"]["yEgridSteps"]),
            nIntegration=int(configs["grid"]["nIntegration"]),
            plotInters0=plots["Inters0"],
            plotInterbeta=plots["Interbeta"],
            plotDeltaP=plots["DeltaP"],
            plotDeltaBB=plots["DeltaBB"],
            plotRestFrame=plots["RestFrame"],
            plotSpectrum=plots["Spectrum"],
            plotCourant=plots["Courant"],
            plotNegatives=plots["Negatives"],
            plotLightCurve=plots["LightCurve"],
            lcBounds=[
                float(configs["lightcurve"]["loweV"]),
                float(configs["lightcurve"]["upeV"])
            ],
            pdfFile=pdf,
            showToo=bool(configs["showToo"]),
            printEvery=50
        )

        if bool(configs["pdf"]["use"]):
            pdf.close()
