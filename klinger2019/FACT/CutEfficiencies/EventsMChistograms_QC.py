import numpy as np

from rootRoutines import dFrameFromRoot
from Model import Model
from DataProcessor import DataProcessor
from HistogramManager import HistogramManager
'''
calculates additional histograms of triggered and processed data
- after processing
- for each quality cut separatly

saves them to data/QC_histograms.p (make sure the folder exists)
takes root file from MonteCarlos/data/
takes the Models from MonteCarlos/reweighting_energy, ..._impact and ..._Zd
'''

EBins = 64
IBins = 54
ZdBins = 60
tsqBins = 20  # bins for theta squared histograms
si = -2.5

lgERange = [2.3, 4.7]
ZdRange = [0, 60]
ImpactRange = [0, 54000]

EventsMCFile = "../MonteCarlos/data/MC_Gamma.root"
dp = DataProcessor(data=dFrameFromRoot(EventsMCFile, keys=["Result"]))

# Energy reweighting
dp.calcLogs(["Energy"])
energyPath = "../MonteCarlos/reweighting_energy/"
MCG_E_in = Model.fromFile(energyPath + "/EnergyModelIn_MCGamma.p")
MCG_E_out = Model.fromFile(
    energyPath + "/EnergyModelOut_MCGamma_%g.p" % (si)
)
dp.updateModels(MCG_E_in, MCG_E_out)
dp.addWeights("logEnergy", name="wE")
print("finished energy reweighting")


# create and save histograms

hm = HistogramManager()

# triggered
hm.addHistogram(
    "lgE_triggered",
    dp.getHistogram(
        "lgE_triggered", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_triggered",
    dp.getHistogram(
        "Zd_triggered", "Zd",
        histRange=ZdRange, nBins=ZdBins
    )
)
hm.addHistogram(
    "Impact_triggered",
    dp.getHistogram(
        "Impact_triggered", "Impact",
        histRange=ImpactRange, nBins=ZdBins
    )
)

# distributions of each variable
hm.addHistogram(
    "NumIslands",
    dp.getHistogram("NumIslands", "NumIslands", nBins=10, histRange=[0, 10])
)
dp.calcLogs(["NumUsedPixels"])
hm.addHistogram(
    "NumUsedPixels",
    dp.getHistogram(
        "NumUsedPixels", "NumUsedPixels",
        nBins=20  #, histRange=[0, 5]
    )
)
hm.addHistogram(
    "lgNumUsedPixels",
    dp.getHistogram(
        "lgNumUsedPixels", "logNumUsedPixels",
        nBins=20  #, histRange=[0, 5]
    )
)
hm.addHistogram(
    "Leakage1",
    dp.getHistogram("Leakage1", "Leakage1", nBins=20, histRange=[0, 1])
)

dpI = dp.copy()
dpP = dp.copy()
dpL = dp.copy()

# Num Islands
dpI.doCut(0, 3.5, "NumIslands")
hm.addHistogram(
    "lgE_NumIslands",
    dpI.getHistogram(
        "lgE_NumIslands", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_NumIslands",
    dpI.getHistogram("Zd_NumIslands", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_NumIslands",
    dpI.getHistogram("Impact_NumIslands", "Impact", histRange=ImpactRange,
                     nBins=IBins)
)

# Num Used Pixels
dpP.doCut(5.5, 1e10, "NumUsedPixels")
hm.addHistogram(
    "lgE_NumUsedPixels",
    dpP.getHistogram(
        "lgE_NumUsedPixels", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_NumUsedPixels",
    dpP.getHistogram("Zd_NumUsedPixels", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_NumUsedPixels",
    dpP.getHistogram("Impact_NumUsedPixels", "Impact", histRange=ImpactRange,
                     nBins=IBins)
)

# Leakage
dpL.doCut(0, 0.1, "Leakage1")
hm.addHistogram(
    "lgE_Leakage1",
    dpL.getHistogram(
        "lgE_Leakage1", "logEnergy",
        useWeights=["wE"], histRange=lgERange, nBins=EBins
    )
)
hm.addHistogram(
    "Zd_Leakage1",
    dpL.getHistogram("Zd_Leakage1", "Zd", histRange=ZdRange, nBins=ZdBins)
)
hm.addHistogram(
    "Impact_Leakage1",
    dpL.getHistogram("Impact_Leakage1", "Impact", histRange=ImpactRange,
                     nBins=IBins)
)

hm.saveHistogramDict("data/QC_histograms_si%g_nBins[%g,%g,%g].p"%
    (si, EBins, ZdBins, IBins)
)
