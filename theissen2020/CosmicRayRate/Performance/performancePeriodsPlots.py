import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde as gkde

import ModifiedJulianDay as mjd
import setFigureConfig as figConfig
from secondAxis import createYearAxisToMjdAxis


def plotNightsCR(nightsCR, color="tab:blue"):

    figConfig.fullWidth(aspectRatio=3./6)
    fig = plt.figure()
    ax = fig.gca()
    # ax.set_title("Artifical Software Trigger (CR) Rate")
    ax.set_xlabel("modified julian day")
    ax.set_ylabel(r"$R_{\mathrm{CR}}$"+" in events/min")
    ax.set_xlim(min(nightsCR["mjd"]), max(nightsCR["mjd"]))
    ax2 = createYearAxisToMjdAxis(ax, yearlabel=False)

    ax.errorbar(
        nightsCR["mjd"], nightsCR["wMean"],
        yerr=nightsCR["std"], fmt=".", color=color, markersize=4, lw=0.8
    )

    fig.tight_layout()

    return fig, ax


def addNightsCR(ax, nightsCR, color="tab:red"):
    ax.errorbar(
        nightsCR["mjd"], nightsCR["wMean"],
        yerr=nightsCR["wMeanErr"], fmt=".", color=color
    )
    return ax


def addRefValuesToPlot(
    ax, bBlockEdges, refs, color="tab:orange", markjumps=True,
    colorJumps="tab:blue", markrefs=True,
):
    for i in range(len(bBlockEdges)-1):
        if not i == 0:
            if markjumps:
                ax.axvline(x=bBlockEdges[i], zorder=0,
                           color=colorJumps, linewidth=0.8,
                           linestyle="-", alpha=1)
            if markrefs:
                ax.plot(
                    (bBlockEdges[i], bBlockEdges[i]),
                    (refs[i-1], refs[i]),
                    color=color, linewidth=2, zorder=20)
        if markrefs:
            ax.plot(
                (bBlockEdges[i], bBlockEdges[i+1]), (refs[i], refs[i]),
                color=color, linewidth=2, zorder=20
            )

    return ax


def addCRToSecondYAxis(
    ax, nightsCR, color="tab:blue"
):
    oldRange = ax.get_ylim()
    ax.set_ylim(oldRange[0]-(oldRange[1]-oldRange[0]), oldRange[1])
    ax2 = ax.twinx()
    ax2.errorbar(
        nightsCR["mjd"], nightsCR["wMean"],
        yerr=nightsCR["wMeanErr"], fmt=".", color=color
    )
    ax2.set_ylim(oldRange[0], oldRange[1]+(oldRange[1]-oldRange[0]))
    ax2.set_ylabel("CR Rate in Events/min", rotation=270, labelpad=18)

    return ax2


def calcNewRefsfromBB(bayesian, goodRuns, allRuns, withPlots=True):

    refCalcFigs = []
    newRefFromMeans = []
    newRefFromKdeG = []
    newRefFromKdeA = []

    for i in range(len(bayesian)-1):

        gPeriodRuns = goodRuns[goodRuns["mjd"] >= bayesian[i]]
        gPeriodRuns = gPeriodRuns[gPeriodRuns["mjd"] < bayesian[i+1]]

        aPeriodRuns = allRuns[allRuns["mjd"] >= bayesian[i]]
        aPeriodRuns = aPeriodRuns[aPeriodRuns["mjd"] < bayesian[i+1]]

        if len(gPeriodRuns) > 0:
            mean = np.mean(gPeriodRuns["RCR_corr"])
            std = np.std(gPeriodRuns["RCR_corr"], ddof=1)
            newRefFromMeans.append(mean)

            kernelG = gkde(gPeriodRuns["RCR_corr"])
            xKernelG = np.linspace(mean-4*std, mean+4*std, num=500)
            yKernelG = kernelG(xKernelG)
            kdeMaxG = xKernelG[np.argmax(yKernelG)]
            newRefFromKdeG.append(kdeMaxG)

        kernelA = gkde(aPeriodRuns["RCR_corr"])
        meanAll = np.mean(aPeriodRuns["RCR_corr"])
        stdAll = np.std(aPeriodRuns["RCR_corr"], ddof=1)
        xKernelA = np.linspace(meanAll-4*stdAll, meanAll+4*stdAll, num=500)
        yKernelA = kernelA(xKernelA)
        kdeMaxA = xKernelA[np.argmax(yKernelA)]
        newRefFromKdeA.append(kdeMaxA)

        firstPNight = str(int(min(aPeriodRuns["fNight"])))
        firstPNight = firstPNight[0:4]+"/"+firstPNight[4:6] + \
            "/"+firstPNight[6:8]
        lastPNight = str(int(max(aPeriodRuns["fNight"])))
        lastPNight = lastPNight[0:4]+"/"+lastPNight[4:6]+"/"+lastPNight[6:8]

        if withPlots:

            figConfig.fullWidth()
            figure = plt.figure()
            plot = figure.gca()
            plot.set_title(
                "performance period " +
                str(i+1) + " ("+firstPNight+" - "+lastPNight+")"
            )
            if len(gPeriodRuns) > 0:
                gPeriodHist = plot.hist(
                    gPeriodRuns["RCR_corr"],
                    bins=40, range=(mean-5*std, mean+5*std),
                    color="tab:blue", zorder=1.2,
                    alpha=0.4,
                    label="N="+str(len(gPeriodRuns.index))+" runs\n" +
                    "in stable nights"
                )

            aPeriodHist = plot.hist(
                aPeriodRuns["RCR_corr"],
                bins=40, range=(meanAll-5*stdAll, meanAll+5*stdAll),
                color="tab:purple", zorder=1.1,
                alpha=0.4,
                label="N="+str(len(aPeriodRuns.index))+" runs\nin total"
            )

            plot2 = plot.twinx()

            if len(gPeriodRuns) > 0:
                yKernelA = yKernelA/max(yKernelA)*1.1*max(aPeriodHist[0])
                yKernelG = yKernelG/max(yKernelG)*1.1*max(gPeriodHist[0])
                plot2.plot(
                    xKernelG, yKernelG, color="tab:blue", zorder=1.3, lw=2
                )

            plot2.set_ylim(0, 1.1*max(yKernelA))
            plot2.plot(
                xKernelA, yKernelA, color="tab:purple", zorder=1.3, lw=2
            )
            plot2.set_yticks([])
            plot2.set_yticklabels([])

            plot.axvline(
                x=kdeMaxG, color="k", linewidth=1.5,
                zorder=1.5, linestyle="--",
                label="KDE max\n(stable nights): \n" +
                str(np.round(kdeMaxG, 2)) + " events/min")
            plot.axvline(
                x=kdeMaxA, color="tab:red", linewidth=1.5, zorder=1.5,
                label="KDE max\n(all nights): \n" +
                str(np.round(kdeMaxA, 2)) + " events/min")

            plot.set_xlim(meanAll-5.2*stdAll, meanAll+4*stdAll)
            plot.set_xlabel("corrected CR-rate " +
                            r"$R_{\mathrm{CR}}$"+" in events/min")
            plot.set_ylabel("number of runs per bin")
            plot.legend(loc="upper left").set_zorder(50)
            plot.grid(axis="y", alpha=0.5)
            plot.set_axisbelow(True)
            figure.tight_layout()
            refCalcFigs.append(figure)

    return newRefFromMeans, newRefFromKdeG, newRefFromKdeA, refCalcFigs


def reduceBlocksByAllowedMinDuration(blocks, minDuration):
    goodblocks = []
    kickedInd = []
    goodblocks.append(blocks[0])
    for i in range(len(blocks)-1):
        i = i+1
        if blocks[i]-goodblocks[-1] >= minDuration:
            goodblocks.append(blocks[i])
        else:
            kickedInd.append(i)
    return goodblocks, kickedInd


def generateTableFromPeriods(borders, refs):

    rows = []

    for i in range(len(refs)):
        start = mjd.mjd_to_fNight(borders[i])
        stop = mjd.mjd_to_fNight(borders[i+1])
        length = int(borders[i+1]-borders[i])
        ref = round(refs[i], 3)

        row = (start, stop, length, ref)
        rows.append(row)

    tableDf = pd.DataFrame(rows, columns=('Start', 'Stop', 'Length', 'Ref'))

    return tableDf
