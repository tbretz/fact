import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
import matplotlib.colorbar as cb

from PlasmoidStatistics import create_2D_hist
import setFigureConfig as sfc

configPath = "sigma10.yaml"

norm = LogNorm(1e-4, 1)
cmap = plt.get_cmap("viridis")
cmap.set_bad("grey")
cmap.set_under("k")

prange = [0, 2.6]
srange = [-3, 0.5]
nBins = [26, 25]








# initial plot
mean_counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
    configPath, 0, pRange=prange, sRange=srange, tauMin=0, tauMax=0,
    nBins=nBins
)
for id in np.arange(9)+1:
    counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
        configPath, id, pRange=prange, sRange=srange, tauMin=0, tauMax=0,
        nBins=nBins
    )
    mean_counts += counts

sfc.halfWidth(aspectRatio=0.9)
fig, ax = plt.subplots()
ax.set_xlabel(r"$\log_{10}(s)$")
ax.set_ylabel(r"$|p| / \sigma^{1/2}$")
ax.set_title(r"$\tau=0$", loc="right")
ax.set_title(r"10 realisations", loc="left")

X, Y = np.meshgrid(xEdges, yEdges)
pcm = ax.pcolormesh(X, Y, mean_counts.T, norm=norm, cmap=cmap)
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
ax.set_xlim(-3, -0.5)

# ax.axhspan(ymin=0.66-0.3, ymax=0.66+0.3, alpha=0.2, color="tab:red")
ax.axhline(y=0.66, c="tab:red", linestyle="--")
ax.text(-2, 0.8, r"mean $p_0$", color="tab:red")
# cb2.set_label(r"$\mathrm{d}N \sigma^{1/2}/\mathrm{d}\log_{10}(s) \mathrm{d}|p|$")
fig.tight_layout()
fig.savefig("2DHists_thesis/initial.png", dpi=500)









# medium
mean_counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
    configPath, 0, pRange=prange, sRange=srange, tauMin=0.1, tauMax=0.2,
    nBins=nBins
)
for id in np.arange(9)+1:
    counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
        configPath, id, pRange=prange, sRange=srange, tauMin=0.1, tauMax=0.2,
        nBins=nBins
    )
    mean_counts += counts

sfc.halfWidth(aspectRatio=0.9)
fig, ax = plt.subplots()
ax.set_xlabel(r"$\log_{10}(s)$")
ax.set_ylabel(r"$|p| / \sigma^{1/2}$")
ax.set_title(r"$0.1 \leq \tau \leq 0.2$", loc="right")
ax.set_title(r"10 realisations", loc="left")

X, Y = np.meshgrid(xEdges, yEdges)
pcm = ax.pcolormesh(X, Y, mean_counts.T, norm=norm, cmap=cmap)

ax.axhline(y=1.66, c="tab:red", linestyle="--")
ax.text(-2, 1.8, r"mean $p_\mathrm{max}$", color="tab:red")

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
ax.set_xlim(-3, -0.5)

fig.tight_layout()
fig.savefig("2DHists_thesis/0.1-0.2.png", dpi=500)






# latest
mean_counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
    configPath, 0, pRange=prange, sRange=srange, tauMin=1.9, tauMax=2,
    nBins=nBins
)
for id in np.arange(9)+1:
    counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
        configPath, id, pRange=prange, sRange=srange, tauMin=1.9, tauMax=2,
        nBins=nBins
    )
    mean_counts += counts

sfc.halfWidth(aspectRatio=0.9)
fig, ax = plt.subplots()
ax.set_xlabel(r"$\log_{10}(s)$")
ax.set_ylabel(r"$|p| / \sigma^{1/2}$")
ax.set_title(r"$1.9 \leq \tau \leq 2.0$", loc="right")
ax.set_title(r"10 realisations", loc="left")


X, Y = np.meshgrid(xEdges, yEdges)
pcm = ax.pcolormesh(X, Y, mean_counts.T, norm=norm, cmap=cmap)

ax.axhline(y=1.66, c="tab:red", linestyle="--")
ax.text(-2, 1.8, r"mean $p_\mathrm{max}$", color="tab:red")

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cb2 = cb.ColorbarBase(
    cax, cmap=cmap, norm=norm,
    orientation='vertical',
    spacing="proportional"
)
ax.set_xlim(-3, -0.5)
fig.tight_layout()
fig.savefig("2DHists_thesis/1.9-2.0.png", dpi=500)

'''
for i in range(1):
    tauMin = i*0.01
    tauMax = (i+1)*0.0
    mean_counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
        configPath, 0, pRange=[0, 3], tauMin=tauMin, tauMax=tauMax, nBins=30
    )
    for id in np.arange(9)+1:
        counts, xCenters, yCenters, xEdges, yEdges = create_2D_hist(
            configPath, id, pRange=[0, 3], tauMin=tauMin, tauMax=tauMax,
            nBins=30
        )
        mean_counts += counts


    fig, ax = plt.subplots()

    X, Y = np.meshgrid(xCenters, yCenters)
    pcm = ax.pcolormesh(X, Y, mean_counts.T)
    # fig.colorbar(pcm)
    ax.set_xlabel(r"$\log_{10}(s)$")
    ax.set_ylabel(r"$|p| / \sigma^{1/2}$")
    
    fig.savefig("2DHists_thesis/%.2f-%.2f.png" % (tauMin, tauMax))
'''