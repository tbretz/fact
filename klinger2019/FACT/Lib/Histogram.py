import numpy as np
from scipy.stats import poisson
import matplotlib.pyplot as plt
import _pickle as pckl

'''
class histogram

wrapper for np.histogram with poisson error and save/load function
contains basically:
-counts     : number of counts in histogram
-sigCounts  : errors on counts
-bins       : bin edges as from np.histogram
-centers    : mean values as centers of bins

input parameters as from np.histogram:
nBins, range, weights

save/load using pickle


'''


class Histogram:

    def __init__(self, label, data=None, nBins=10, histRange=None,
                 weights=None, counts=None, sigCounts=None, bins=None):
        self.label = label
        if counts is not None:
            self.counts = counts
        else:
            self.counts = None
        if sigCounts is not None:
            self.sigCounts = sigCounts
        else:
            self.sigCounts = None
        if bins is not None:
            self.bins = bins
            self.centers = (self.bins[:-1] + self.bins[1:]) / 2
        else:
            self.bins = None
            self.centers = None
        if data is not None:
            self.updateData(data, nBins, histRange, weights)

    def __add__(self, other):
        if self.counts is None and other.counts is None:
            return Histogram("")

        elif self.counts is None and other.counts is not None:
            if "chunk" in other.getLabel():
                newLabel = self.label
            else:
                newLabel = other.getLabel()
            newCounts = other.counts
            newSigCounts = other.sigCounts
            newBins = other.bins
            return Histogram(newLabel, counts=newCounts,
                             sigCounts=newSigCounts, bins=newBins)

        elif self.counts is not None and other.counts is None:
            if "chunk" in self.getLabel():
                newLabel = other.getLabel()
            else:
                newLabel = self.label
            newCounts = self.counts
            newSigCounts = self.sigCounts
            newBins = self.bins
            return Histogram(newLabel, counts=newCounts,
                             sigCounts=newSigCounts, bins=newBins)

        elif np.all(self.getBins() == other.getBins()):
            if ("chunk" in self.getLabel() and
                    "chunk" not in other.getLabel()):
                newLabel = other.label
            elif ("chunk" not in self.getLabel() and
                  "chunk" in other.getLabel()):
                newLabel = self.label
            else:
                newLabel = self.label + " + " + other.getLabel()
            newCounts = self.counts + other.counts
            newSigCounts = np.sqrt(self.sigCounts**2 + other.sigCounts**2)
            newBins = self.bins
            return Histogram(newLabel, counts=newCounts,
                             sigCounts=newSigCounts, bins=newBins)

    def __mul__(self, other):
        self.counts *= other
        self.sigCounts *= other
        return self

    def __truediv__(self, other):
        self.counts = self.counts / other
        self.sigCounts = self.sigCounts / other
        return self

    def updateData(self, data, nBins=10, histRange=None, weights=None):
        # calculate histogram
        self.counts, self.bins = np.histogram(
            data,
            bins=nBins, range=histRange, weights=weights
        )
        # calculate centers of bins
        self.centers = (self.bins[:-1] + self.bins[1:]) / 2
        # calculate uncertainties from poisson distribution
        # unweighted histogram for weights
        counts0, bins0 = np.histogram(data, bins=nBins, range=histRange)
        self.sigCounts = self.counts / np.sqrt(counts0)

    def getCounts(self):
        return self.counts

    def getSigCounts(self):
        return self.sigCounts

    def getBins(self):
        return self.bins

    def getCenters(self):
        return self.centers

    def getNEvents(self):
        return np.sum(self.counts)

    def getLabel(self):
        return self.label

    def setLabel(self, label):
        self.label = label

    def drawStairs(self, ax, label, zorder, color, ecolor):
        x = np.reshape(self.bins[:, None] * np.ones(2), (2*len(self.bins)))
        y = np.concatenate(
            (
                [0],
                np.reshape(
                    self.counts[:, None] * np.ones(2), (2*len(self.counts))
                ),
                [0]
            )
        )
        ax.plot(x, y, label=label, zorder=zorder, color=color)
        ax.errorbar(
            self.centers, self.counts, yerr=self.sigCounts,
            linestyle="", label=None, ecolor=ecolor,
            zorder=zorder+0.5
        )
        return ax

    def drawBars(self, ax, label, zorder, color, ecolor):
        width = self.bins[1] - self.bins[0]
        ax.bar(
            self.centers, self.counts, width=width,
            fc=color, zorder=zorder, label=label, edgecolor="white", lw=0.1
        )
        ax.errorbar(
            self.centers, self.counts, yerr=self.sigCounts,
            linestyle="", label=None, ecolor=ecolor,
            zorder=zorder+0.5
        )
        return ax

    # adds a figure with the histogram to ax with label
    # style can be bar for barplot, otherwise it is the format (fmt)
    def addToAx(self, ax, style="bar", label=None, zorder=1, color=None,
                ecolor="black"):
        if color is None:
            color = next(ax._get_lines.prop_cycler)["color"]

        if label == "label":
            label = self.label

        if style == "bar":
            ax = self.drawBars(
                ax, label=label, zorder=zorder, color=color, ecolor=ecolor
            )
        elif style == "stairs":
            ax = self.drawStairs(
                ax, label=label, zorder=zorder, color=color, ecolor=ecolor
            )
        else:
            ax.errorbar(
                self.centers, self.counts, yerr=self.sigCounts,
                fmt=style, label=label
            )
        return ax

    # creates a figure with the histogram
    # style can be bar for barplot, otherwise it is the format (fmt)
    def draw(self, xlabel="", title=None, xscale="linear", yscale="linear",
             xlim=None, ylim=None, style="bar", figsize=None, legloc=None,
             color=None, ecolor="black"):
        fig, ax = plt.subplots(figsize=figsize)
        if title is None:
            ax.set_title(self.label)
        else:
            ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel("#")
        ax = self.addToAx(ax, style=style, color=color, ecolor=ecolor)
        if xscale is not None:
            ax.set_xscale(xscale)
        if yscale is not None:
            ax.set_yscale(yscale)
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.setylim(ylim)
        if legloc is not None:
            bbox_to_anchor, loc = legloc
            if bbox_to_anchor is None:
                if loc is not None:
                    ax.legend(loc=loc)
            else:
                ax.legend(loc=loc, bbox_to_anchor=bbox_to_anchor)
        # else:
            # ax.legend(loc="center left", bbox_to_anchor=[1, 0.5])
        return fig

    def save(self, filename):
        with open(filename, 'wb') as f:
            save = [self.counts, self.sigCounts, self.bins, self.centers,
                    self.label]
            pckl.dump(save, f)

    @classmethod
    def fromFile(cls, filename, label):
        new = cls(label)
        with open(filename, 'rb') as f:
            (new.counts, new.sigCounts,
             new.bins, new.centers, new.label) = pckl.load(f)
        return new
