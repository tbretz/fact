from rootRoutines import dFrameFromRoot
import argparse
import pandas as pd
import numpy as np
import ExcessCalculation as ec

parser = argparse.ArgumentParser()
parser.add_argument(
    "-e", "--excess",
    help="Path to the excess h5 file", type=str, default=""
)
parser.add_argument(
    "-c", "--cosmicray",
    help="Path to the cosmic ray h5 file", type=str,
    default="../../CosmicRayRate/CRSelectedRuns.h5"
)
parser.add_argument(
    "-o", "--out",
    help="Path to the combined h5 file", type=str, default=""
)
args = parser.parse_args()

excFile = args.excess
crFile = args.cosmicray
newFile = args.out

excessDf = pd.read_hdf(excFile, key=(excFile.split("/")[-1])[:-3])
crDf = pd.read_hdf(crFile, key=(crFile.split("/")[-1])[:-3])

df = crDf.merge(excessDf, how="inner", on=["fNight", "fRunID"])

# define effective time
df["fEffectiveTime_s"] = df["fRunDuration"]*df["fEffectiveOn"]
df["fEffectiveTime_h"] = df["fRunDuration"]*df["fEffectiveOn"]/3600
df["N_E"] = ec.calcExcessEvents(df["fNumSigEvts"], df["fNumBgEvts"])
df["sigma_E"] = ec.calcExcessError(df["fNumSigEvts"], df["fNumBgEvts"])

# excess per run
df["rExc"] = df["N_E"]/df["fEffectiveTime_h"]
df["rExcErr"] = df["sigma_E"]/df["fEffectiveTime_h"]

df.to_hdf(newFile, (newFile.split("/")[-1])[:-3])
