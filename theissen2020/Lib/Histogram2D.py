import numpy as np
from scipy.stats import poisson
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.cm as cm
import _pickle as pckl

'''
class histogram

wrapper for np.histogram with poisson error and save/load function
contains basically:
-counts     : number of counts in histogram
-sigCounts  : errors on counts
-bins       : bin edges as from np.histogram
-centers    : mean values as centers of bins

input parameters as from np.histogram:
nBins, range, weights

save/load use pickle


'''


class Histogram2D:

    def __init__(self, label, xData=None, yData=None, nBins=10, histRange=None,
                 weights=None):
        if xData is not None and yData is not None:
            self.updateData(xData, yData, nBins, histRange, weights)
        self.label = label

    def updateData(self, xData, yData, nBins=10, histRange=None, weights=None):
        # calculate histogram
        self.counts, self.xEdges, self.yEdges = np.histogram2d(
            xData, yData,
            bins=nBins, range=histRange, weights=weights
        )
        # calculate centers of bins
        self.xCenters = (self.xEdges[:-1] + self.xEdges[1:]) / 2
        self.yCenters = (self.yEdges[:-1] + self.yEdges[1:]) / 2
        # calculate errors from poisson distribution
        self.sigCounts = np.sqrt(poisson.stats(self.counts, moments='v'))

    def getCounts(self):
        return self.counts

    def getSigCounts(self):
        return self.sigCounts

    def getXEdges(self):
        return self.xEdges

    def getYEdges(self):
        return self.yEdges

    def getXCenters(self):
        return self.xCenters

    def getYCenters(self):
        return self.yCenters

    def getNEvents(self):
        return np.sum(self.counts)

    def getLabel(self):
        return self.label

    def setLabel(self, label):
        self.label = label

    def draw(self, label="", xlabel="", ylabel="", title="", scale="linear",
             xscale="linear", yscale="linear", xlim=None, ylim=None,
             cmap=None, bad_color='darkgrey'):
        fig, ax = plt.subplots()
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        if cmap is None:
            cmap = cm.get_cmap('Blues_r')
        cmap.set_bad(color=bad_color)
        X, Y = np.meshgrid(self.xEdges, self.yEdges)

        if scale == "linear":
            pcm = ax.pcolormesh(
                X, Y,
                np.ma.masked_where(self.counts.T == 0, self.counts.T),
                cmap=cmap
            )
            # pcm = ax.pcolormesh(X, Y, self.counts.T, cmap="Blues")
        elif scale == "log":
            pcm = ax.pcolormesh(
                X, Y,
                np.ma.masked_where(self.counts.T == 0, self.counts.T),
                cmap=cmap,
                norm=LogNorm(vmin=np.maximum(np.min(self.counts), 1),
                             vmax=np.max(self.counts))
            )

        cb = fig.colorbar(pcm, ax=ax)
        #cb = plt.colorbar(pcm, ax=ax)
        cb.set_label(label)
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.setylim(ylim)
        return fig, ax

    def save(self, filename):
        with open(filename, 'wb') as f:
            save = [self.counts, self.sigCounts, self.bins, self.centers,
                    self.label]
            pckl.dump(save, f)

    @classmethod
    def fromFile(cls, filename):
        new = cls()
        with open(filename, 'rb') as f:
            new.counts, new.sigCounts, new.bins, new.centers, new.label = pckl.load(f)
        return new
