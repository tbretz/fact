import numpy as np
from matplotlib.colors import LinearSegmentedColormap


def c_beta(
    n=50, min_light=0, max_light=0.75, gamma=0.3, reverse=False, as_map=True
):
    # lambd is effectively the color map grid
    lambd = np.linspace(min_light, max_light, n)

    # apply the gamma correction
    grad = np.linspace(0, 1, n)

    amp = lambd ** gamma
    r = amp * np.ones(n)
    g = 0.75 * amp * grad + 0.1
    b = 0.75 * amp * grad/2 + 0.1
    rgb = np.array([r, g, b]).T
    colors = rgb.astype(float).tolist()
    if reverse:
        colors = list(reversed(colors))
    if as_map:
        return LinearSegmentedColormap.from_list("c_beta", colors)
    else:
        return colors


def c_size(
    n=50, min_light=0, max_light=0.75, gamma=0.2, reverse=False, as_map=True
):
    # lambd is effectively the color map grid
    lambd = np.linspace(min_light, max_light, n)

    # apply the gamma correction
    grad = np.linspace(0, 1, n)

    amp = lambd ** gamma
    r = amp * grad * 0.95
    g = amp * (0.5 * grad**0.8 + 0.4)
    b = amp * (0.45)
    rgb = np.array([r, g, b]).T
    colors = rgb.astype(float).tolist()
    if reverse:
        colors = list(reversed(colors))
    if as_map:
        return LinearSegmentedColormap.from_list("c_size", colors)
    else:
        return colors

def c_time(
    n=50, min_light=0, max_light=0.55, gamma=0.15, reverse=False, as_map=True
):
    # lambd is effectively the color map grid
    lambd = np.linspace(min_light, max_light, n)

    # apply the gamma correction
    grad = np.linspace(0, 1, n)

    amp = lambd ** gamma
    r = amp * grad * 0.75
    g = amp * (0.3 * grad**0.8 + 0.5)
    b = amp * (0.2 + 0.8 * grad)
    rgb = np.array([r, g, b]).T
    colors = rgb.astype(float).tolist()
    if reverse:
        colors = list(reversed(colors))
    if as_map:
        return LinearSegmentedColormap.from_list("c_size", colors)
    else:
        return colors
