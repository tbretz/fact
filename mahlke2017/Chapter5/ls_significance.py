from astroML.time_series import lomb_scargle
from DELCgen import PL, Lightcurve, Simulate_DE_Lightcurve, Mixture_Dist, PSD_Prob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from peripylib import figures, query_fact
import os
import random
import scipy.stats.distributions as dist
import sys

# --------
# Simulate light curves using the PSD and PDF of
# the FACT light curves


def compute_LSP(data_lc):
    # -----
    # Calculate the LSP

    pmin = np.min(np.diff(sorted(data_lc.time)))*2  # in days
    pmax = int(data_lc.time.max() - data_lc.time.min())
    N = len(data_lc.flux)
    period = 10 ** np.linspace(np.log10(pmin), np.log10(pmax), N)
    omega = 2 * np.pi / period

    power = lomb_scargle(data_lc.time, data_lc.flux, data_lc.errors, omega, generalized=True)

    return [1/period, power]

def plot_PSD_fit(data_lc, source, mode):
    '''
    Plots the LSP of the data and the PSD fit
    '''

    with plt.rc_context({
                     'xtick.major.size'     : 4,      # major tick size in points
                     'xtick.minor.size'     : 2,      # minor tick size in points
                     'ytick.major.size'     : 4,      # major tick size in points
                     'ytick.minor.size'     : 2,      # minor tick size in points
                     'xtick.major.pad'      : 4,      # distance to major tick label in points
                     'axes.labelpad'        : 0.04,
                    }):

        fig, ax = figures.newfig(ratio=0.5)

        freq = data_lc.periodogram[0]
        power = data_lc.periodogram[1]

        ax.plot(freq, power, color='black', label=r'LSP %s' % (source))

        # Add PL fit
        ax.plot(freq, PL(freq, *data_lc.psdFit['x']), color=figures.colors(3), lw=1.5, ls='--', label=r'Power Law Fit, $\alpha$=%.2f' % data_lc.psdFit['x'][1])
        ax.legend(frameon=False)
        ax.set(xlabel=r'Frequency / days$^{-1}$', ylabel='LS Power', xscale='log', yscale='log')

        ax.tick_params(axis='x', which='minor', top='off')
        # Add period axis
        ax2 = ax.twiny()
        # Copy the limits of the first x-axis
        limit = lambda x, y: (1/y, 1/x)
        ax2.set_xlim(limit(*ax.get_xlim()[::-1]))
        # Set ticks and ticks locations
        ax2.set_xscale('log')
        ax2.set_xlabel(r"Period / day", labelpad=3)
        ax2.tick_params(axis='x', which='minor', top='on')

        figures.savefig('lsp_fit_%s_%s' % (source, mode), pgf=True, png=True)


def plot_PDF_fit(data_lc, source, mode):

    fig, ax = figures.newfig()

    # Plot histogram of rates
    ax.hist(data_lc.flux, 100, normed=True, alpha=0.7, label=r'%s, %s Binning' % (source, mode), facecolor=figures.colors(0))

    # Plot PDF Fit
    x = np.arange(0, np.max(data_lc.flux) * 1.2, 0.01)
    ax.plot(x, data_lc.pdfModel.Value(x, data_lc.pdfFit['x']), color=figures.colors(3), linewidth=1, ls='--', label='PDF Fit')

    ax.legend(frameon=False)
    ax.set(xlabel=r'Rate / h$^{-1}$', ylabel='Density Function')
    figures.savefig('pdf_fit_%s_%s' % (source, mode), pgf=True)


def get_simulated_lcs(data_lc, save_path, plot_random=0):
    '''
    Returns a dictionary with lists of all simulated fluxes, evts, and LSPs
    of all light curves in save_path
    '''

    light_curves = [f for f in os.listdir(os.getcwd() + '/' + save_path) if f.endswith('.dat')]

    lc_epochs = []
    lc_evts = []
    lc_lsps = []

    freq = data_lc.periodogram[0]

    for i, light_curve in enumerate(light_curves):
        with open(os.getcwd() + '/' + save_path + light_curve, 'r') as file:

            random.seed(i)  # for reproducibility

            # Lists to store file
            time = []
            evts = []

            # Skip header
            file.readline()

            # Read light curve. 2000 evenly-spaced days were simulated. We choose the days
            # closest to our FACT data points and discard the others to get unevenly sampled data.
            for line in file:
                t, e = line.strip('\n').split(' ')
                time.append(float(t))
                evts.append(float(e))

            time, evts = np.asarray(time), np.asarray(evts)

            # Apply FACT window function
            # Timestamps start at zero, but light curve is random so we can move that up
            time, evts = np.asarray(time), np.asarray(evts)
            time += int(nightly.mjd.min())

            # Now keep only those MJDs which can also be found in the FACT data
            fact_mjds = [round(mjd) for mjd in nightly.mjd]

            mask = [t in fact_mjds for t in time]
            evts = evts[mask]
            time = time[mask]

            # At this point, we have deleted the days when FACT did not observe from the simulated light curve
            #  We now match the time stamps to make sure that the periodograms reflect the FACT spectral window
            time = nightly.mjd

            # Normalise evts
            evts /= (evts.max()/data_lc.flux.max())

            # Calculate LSP. Each light curve point gets a random FACT error
            # Frequency grid is the same as for the sample light curve LSP
            omega = 2 * np.pi * freq

            # Compute Lomb-Scarlge periodogram, no error given
            # we pass a randomized list of the evts_err to the periodogram to simulate our errors
            power = lomb_scargle(time, evts, np.random.choice(data_lc.errors, len(time), replace=True), omega, generalized=True)
            # Normalize periodogram
            power /= (power.max()/data_lc.periodogram[1].max())

            # Plot random light curves
            if i in range(plot_random):
                fig, ax = figures.newfig()

                ax.errorbar(time, evts,  ls='', marker='.', yerr=np.random.choice(data_lc.errors, len(time), replace=True))

                ax.set(xlabel='Epoch / MJD', ylabel=r'Rate / h$^{-1}$')
                figures.savefig(os.getcwd() + '/' + save_path + str(i), pgf=True)

                # Periodogram checkplots
                with plt.rc_context({
                                 'xtick.major.size'     : 4,      # major tick size in points
                                 'xtick.minor.size'     : 2,      # minor tick size in points
                                 'ytick.major.size'     : 4,      # major tick size in points
                                 'ytick.minor.size'     : 2,      # minor tick size in points
                                 'axes.labelpad'        : 0.01,
                                }):
                    fig, ax = figures.newfig()
                    ax.plot(freq, power, color=figures.colors(0), ls='-')

                    ax.set(xlabel=r'Frequency / days$^{-1}$', ylabel='LS Power', xscale='log', yscale='log')
                    figures.savefig(os.getcwd() + '/' + save_path + str(i) + '_lsp', pgf=True)

        lc_epochs.append(time)
        lc_evts.append(evts)
        lc_lsps.append(power)

    return {'epochs': lc_epochs, 'evts': lc_evts, 'lsp': lc_lsps}


def plot_quality_plots(data_lc, sim_lcs, source, mode):
    '''
    Compare mean periodogram and fitted power law
    and fitted PDF versus PDF of all light curves
    '''

    # Periodogram comparison
    with plt.rc_context({
                     'xtick.major.size'     : 4,      # major tick size in points
                     'xtick.minor.size'     : 2,      # minor tick size in points
                     'ytick.major.size'     : 4,      # major tick size in points
                     'ytick.minor.size'     : 2,      # minor tick size in points
                     'xtick.major.pad'      : 4,      # distance to major tick label in points
                     'axes.labelpad'        : 0.04,
                    }):
        fig, ax = figures.newfig()

        freq = data_lc.periodogram[0]
        power = data_lc.periodogram[1]

        # Mean periodogram of simulated lcs
        mean_sim_power = np.mean(sim_lcs['lsp'], axis=0)

        ax.plot(freq, power, color='gray', ls='-', label=r'%s, %s LSP' % (source, mode), alpha=0.6)
        ax.plot(freq, PL(freq, *data_lc.psdFit['x']), color=figures.colors(0), ls='--', label=r'Power Law Fit')
        ax.plot(freq, mean_sim_power, color=figures.colors(3), ls='-', label=r'Mean $P_{\mathrm{sim}}(\nu)$')
        ax.legend(frameon=False)
        ax.set(xlabel=r'Frequency / days$^{-1}$', ylabel='LS Power', xscale='log', yscale='log')

        ax.tick_params(axis='x', which='minor', top='off')
        # Add period axis
        ax2 = ax.twiny()
        # Copy the limits of the first x-axis
        limit = lambda x, y: (1/y, 1/x)
        ax2.set_xlim(limit(*ax.get_xlim()[::-1]))
        # Set ticks and ticks locations
        ax2.set_xscale('log')
        ax2.set_xlabel(r"Period / days", labelpad=3)
        ax2.tick_params(axis='x', which='minor', top='on')


        figures.savefig('psd_fit_sim_comparison_%s_%s' % (source, mode), pgf=True)


    # PDF comparison
    fig, ax = figures.newfig()

    # Plot histogram of rates
    ax.hist([flux for sublist in sim_lcs['evts'] for flux in sublist], 100, normed=True, alpha=0.7, label=r'Sim. Rates', facecolor=figures.colors(0))

    # Plot PDF Fit
    x = np.arange(0, np.max(data_lc.flux) * 1.2, 0.01)
    ax.plot(x, data_lc.pdfModel.Value(x, data_lc.pdfFit['x']), color=figures.colors(3), linewidth=1, ls='--', label=r'%s, %s PDF Fit' % (source, mode))

    ax.legend(frameon=False)
    ax.set(xlabel=r'Rate / h$^{-1}$', ylabel='Density Function')
    figures.savefig('pdf_fit_sim_comparison_%s_%s' % (source, mode), pgf=True)


def plot_significance(data_lc, sim_lcs, source, mode):
    '''
    Compare mean periodogram and fitted power law
    and fitted PDF versus PDF of all light curves
    '''

    # Periodogram comparison
    with plt.rc_context({
                     'xtick.major.size'     : 4,      # major tick size in points
                     'xtick.minor.size'     : 2,      # minor tick size in points
                     'ytick.major.size'     : 4,      # major tick size in points
                     'ytick.minor.size'     : 2,      # minor tick size in points
                     'xtick.major.pad'      : 4,      # distance to major tick label in points
                     'axes.labelpad'        : 0.04,
                    }):
        fig, ax = figures.newfig(ratio=0.5)

        freq = data_lc.periodogram[0]
        power = data_lc.periodogram[1]

        # Mean periodogram of simulated lcs
        mean_sim_power = np.mean(sim_lcs['lsp'], axis=0)

        ax.plot(freq, power, color='black', ls='-', label=r'%s, %s LSP' % (source, mode))
        ax.plot(freq, mean_sim_power, color=figures.colors(3), ls='-', label=r'Mean $P_{\mathrm{sim}}(\nu)$')

        # -------
        # Add significances
        # Add 50%, 95%, 99% lines using the X% quantile of the distribution of powers in the simulated LSPs

        for quantile, linewidth in [(99, 0.5), (99.9, 0.5)]:
            # For every frequency, calculate the quantiles of the simulated LSPs
            quantiles = np.percentile(sim_lcs['lsp'], quantile, axis=0)
            print(quantiles)
            ax.plot(freq, quantiles, label=r'%.1f Sign.' % quantile, ls='--', linewidth=linewidth , color='#f03b20')

        ax.legend()
        ax.set(xlabel=r'Frequency / days$^{-1}$', ylabel='LS Power', yscale='log', xscale='log')

        ax.tick_params(axis='x', which='minor', top='off')
        # Add period axis
        ax2 = ax.twiny()
        # Copy the limits of the first x-axis
        limit = lambda x, y: (1/y, 1/x)
        ax2.set_xlim(limit(*ax.get_xlim()[::-1]))
        # Set ticks and ticks locations
        ax2.set_xscale('log')
        ax2.set_xlabel(r"Period / days", labelpad=3)
        ax2.tick_params(axis='x', which='minor', top='on')


        figures.savefig('psd_fit_significance_%s_%s' % (source, mode), pgf=True)



if __name__=='__main__':

    # Number of light curves to simulate
    num_lcs = 10000

    for mode in ['Nightly']:

        for source in ['Mrk421', 'Mrk501']:

            # Simulate light curves or read from file?
            simulate = False

            # Get latest FACT data, apply cuts and corrections
            rates, nightly = query_fact.query_fact(source)

            # Create sample light curve instance
            if mode == 'Nightly':
                data_lc = Lightcurve(nightly.mjd, nightly.evts, tbin=np.mean(np.diff(nightly.mjd)), errors=nightly.evts_err)
            elif mode == 'Run':
                rates = rates[rates.rate_err_corr != 0]
                data_lc = Lightcurve(rates.mjd, rates.rate_corr, tbin=np.mean(np.diff(rates.mjd)), errors=rates.rate_err_corr)
            # --------
            # Compute LSP
            data_lc.periodogram = compute_LSP(data_lc)

            # Fit PSD using Bending Power Law model
            # The result is stored in the lightcurve object as 'psdFit'
            data_lc.Fit_PSD(initial_params=[1, 1, 1e-6], model=PL, verbose=True)

            # Plot the PSD Fit and the LS periodogram
            plot_PSD_fit(data_lc, source, mode)


            # Fit PDF using gamma + lognormal distribution and plot
            #data_lc.Fit_PDF(model=dist.lognorm, initial_params=[0.1 , 1, 5])
            data_lc.Fit_PDF(initial_params=[15, 9, 0.5, 10, 1, 0.5])
            plot_PDF_fit(data_lc, source, mode)

            # --------
            # Simulate light curves

            save_path = './%s_%s/' % (source, mode)

            if simulate:

                # Make sure folder structure exists
                os.makedirs(save_path, exist_ok=True)

                for i in range(num_lcs):
                    # Short status update
                    sys.stdout.write('\r')
                    sys.stdout.write('{:d}/{:.0f}'.format(i+1, num_lcs))
                    sys.stdout.flush()

                    # Simulate
                    DE_lc = Simulate_DE_Lightcurve(PSDmodel=data_lc.psdModel, PSDparams=data_lc.psdFit['x'],
                                                   PDFmodel=data_lc.pdfModel, PDFparams=data_lc.pdfFit['x'],
                                                   tbin=1, LClength=2000, randomSeed=10*i, lightcurve=data_lc)

                    # Save the simulated light curve to file
                    DE_lc.Save_Lightcurve(save_path + 'de_lc_%i.dat' % i)

            # Get properties of simulates light curves: Epochs, Fluxes, LSP
            # Plot X random light curves
            sim_lcs = get_simulated_lcs(data_lc, save_path, plot_random=5)

            # --------
            # Simulation Qualit Plots

            # Compare mean periodogram and fitted power law
            # and fitted PDF versus PDF of all light curves
            plot_quality_plots(data_lc, sim_lcs, source, mode)

            # Plot LSP, mean simulated LSP and significance lines
            plot_significance(data_lc, sim_lcs, source, mode)